import { Component, OnInit, ViewChild, ElementRef, NgZone  } from '@angular/core';
import { UserService } from '../http.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
//import { StripeScriptTag } from "stripe-angular";
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { Http} from '@angular/http';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
import * as $AB from 'jquery';
import * as bootstrap from 'bootstrap';
import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
import {DomSanitizer} from "@angular/platform-browser";
import { environment } from '../../environments/environment';
interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})

export class ModelsComponent implements OnInit {
  //@ViewChild('formDirective' {static: false}) private formDirective: NgForm;
  postsCol: AngularFirestoreCollection<Post>;
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  @ViewChild('editForm', {static: false})
  private publishableKey:string = "pk_test_XsCV2lAW6EG6jRrulmXgma2M00cMQ8KnqI"
  modelData: any =[];
  editmode =true;
  modelPrice:any =[];
  modelID:any =[];
  private formData = new FormData();
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  nameMain:string;
  display='none';
  elementsOptions: ElementsOptions = {
    locale: 'es'
  };
  showModal=true;
  paymentConfirmation:any =[];
  loading: boolean;
  form: FormGroup;
  editForm: NgForm;
  searchFilter:string;
  public params;
  search:any;
  searchedOn:boolean;
  isLoading:boolean=true;
  featured:boolean=false;
  featuredName:string;
  ifLogin:boolean;
  addClass:string;
  liveData:any=[];
  modelMainId:Number;
  modePosition:string;
  page: number = 1;  
  hideShowMore:boolean = false
  isLoadingLoader:boolean = false;
  formUrl:any =  this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton.php')
  liveModel:any = [];
  liveModelData : any = [];
  AddActiveClass:string = 'finpay';
  expYear:string
  expMonth:string
  nameCard:string
  opacity:any = 0;
  classACTIVE:any = "notactive"
  constructor(private userService:UserService,private router: Router,private stripeService: StripeService,private http : Http,
    private fb: FormBuilder,private _zone: NgZone, private route:ActivatedRoute,private afs: AngularFirestore,private domSanitizer : DomSanitizer) { 
      // this.route.queryParams.subscribe((params: Params) => this.params = params);
      // //console.log.log(this.params);
      this.searchedOn=true;

  }

  ngOnInit() {
    //alert(this.expiryMonth);
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1'){
     
      this.ifLogin =true;
      this.addClass = 'right-main'
      this.getAllModels(1,'ListModel');
    }
    else{
      this.addClass = 'right-mainNologin'
      this.ifLogin =false;
      this.getAllModels(1,'listModelHome');
    }
    //this.getAllModels();
    this.searchedOn=true;
   
  }
  getInfo(event){
    //console.log(event)
    var elem = document.getElementsByClassName('active12')
    if(elem.length>0){
      if(event.target.id == "changeSlider"){
        this.opacity = 1;
      }
      else if(event.target.id == 'dropdownMenuButton' ){
        this.classACTIVE = "notactive"
        this.opacity =0;
      }
      else{
        this.classACTIVE = "notactive"
        this.opacity = 0;
      }
    }
    
    if(event.target.id == 'dropdownMenuButton' ){
      this.classACTIVE = "active12"
      this.opacity =1;
    }
    // else{
    //   console.log("!@#42342222")
    //   this.opacity =0;
    // }
  }
  getInfo2(event){
    var elem = document.getElementsByClassName('active12')
    if(elem.length>0){
     if(event.target.id == 'dropdownMenuButton' ){
        this.classACTIVE = "notactive"
        this.opacity =0;
      }
      else{
        this.classACTIVE = "notactive"
        this.opacity = 0;
      }
    }
    
    if(event.target.id == 'dropdownMenuButton' ){
      this.classACTIVE = "active12"
      this.opacity =1;
    }
  }

  getLiveModel(data){
  //console.log.log(data);
  var that = this;
  var ref = that.afs.collection('notiLive');
  var listener = ref.valueChanges();
  listener.subscribe(res => {
  const liveDat = [];
  //console.log.log("res",res);
  for( var i = 0;i<data.length;i++){
    for(var j = 0;j<res.length;j++){
      if(data[i]['model_id'] == res[j]['model_id']){
        liveDat.push(that.modelData[i])
      }
      // else{
      //   liveDat.splice(liveDat.findIndex(item => item[j].model_id === data[i]['model_id']), 1)
      // }
    }
   
    that.liveData = that.getUnique(liveDat);
    //     ////console.log.log("that.liveData.length",that.liveData.length)
    //   }
    // }
  }

   })
}



  include(arr, obj) {
      for(var i=0; i<arr.length; i++) {
          if (arr[i] == obj) return true;
      }
  }



  getAllModels(page,listmodel){
    
    const api = {
      api_token:localStorage.getItem("loggedInUser"),
      page:page
     }
     this.isLoadingLoader=true;
     this.userService.getAllModels(api,listmodel).subscribe(
       response => {
       // console.log("response",response);
        if(response['success']==1){
          this.liveData=[];
          //this.modelData = []
          //this.modelData = response['data'];
          this.isLoading=false;
          this.isLoadingLoader = false;
          ////console.log.log(response['data'])
          // for(var i=0;i<response['data']['length'];i++){
          //   if(response['data'][i]['live_model'] == '1'){
          //     this.liveData.push(response['data'][i])
          //   }
          // }
          for (let i = 0; i < response['data'].length ; i++) {
            this.modelData.push(response['data'][i]);
            if(response['data'][i]['next_page'] == null){
              this.hideShowMore = true;
            }
          }
          this.getLiveModel(this.modelData);
          //console.log("this.modelData",this.modelData);
        }
        else{
          this.hideShowMore = true;
          //alert("No models Found");
          this.isLoading=false;
          this.isLoadingLoader = false;
        }
         
       },
       err => {
         this.router.navigate(['/login']);
         //console.log.log(err);
       }
     );
  }

  liveModelClick(live){
    //alert("qq")
    ////console.log.log(this.liveModelData);
    this.isLoading=true;
    this.searchedOn = false;
    const model_id = {
      model_ids : this.liveModelData
    }
    this.userService.modelFilterLive(model_id).subscribe(
      response => {

        ////console.log.log(response);
        if(response['success']==1){
          this.modelData = response['data'];
          for(var i =0 ;i<this.modelData.length ; i++){
            this.modelData[i]['live_model'] = 1;
          }
          ////console.log.log(response['data']);
          this.isLoading=false;
        }
        else{
          if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1'){
     
            this.ifLogin =true;
            this.addClass = 'right-main'
            this.getAllModels(this.page,'ListModel');
          }
          else{
            this.addClass = 'right-mainNologin'
            this.ifLogin =false;
            this.getAllModels(this.page,'listModelHome');
          }  
          this.isLoading=false;
        }  
      },
      err => {
       //console.log.log(err);
      }
    )
  }

  loadMore(){
    this.page = this.page + 1;  
    this.getAllModels(this.page,'ListModel');
  }

  FollowModel(id,model_id,i){
    this.editmode = false;
    
    // //console.log.log(id);
    // //console.log.log(model_id);
    const followData = {
      id: id,
      model_id: model_id,
      api_token:localStorage.getItem("loggedInUser")
    }
    var userId= localStorage.getItem("id");
    this.userService.FollowUnfollow(followData).subscribe(
      response => {
       ////console.log.log(response);
       if(response['success']==1){
        // alert(id)
        if(id==1){
          this.modelData[i]['status'] =2;
          //alert(this.modelData[i]['status'])
          var mod =0;
          const afs2 =  this.afs;
          this.afs.collection("chatCounter", ref => ref.where('userId', '==', Number(userId)).where('modelId', '==', Number(model_id)))
          .get().toPromise().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
              ////console.log.log(doc.id, " => ", doc.data());
                //this.chatId = doc.id;
                //console.log.log(doc.id);
                doc.ref.delete();
                afs2.collection("chatStatus").doc(doc.id).delete().then(function() {
                  //console.log.log("Document successfully deleted!");
                }).catch(function(error) {
                    //console.log.error("Error removing document: ", error);
                });
            });
          })
        }
        else if(0==id){
          var mod =1;
          this.afs.collection('chatCounter').add({'userId': Number(userId), 'sender': 'user','modelId': Number(model_id), 'MaxChat': Number(10),'time':firebase.firestore.FieldValue.serverTimestamp(),'totalChat':Number(0)});
        }
        this.modelData[i]['following_status'] =mod;
        this.modelData[i]['status'] =response['data'];
       
        this.editmode = true;
       } 
      },
      err => {
        //this.router.navigate(['/login']);
        //console.log.log(err);
        alert("something went wrong")
      }
    );
  }

  getModelPrice(id,routeId){
    this.modelMainId = id;
    this.modePosition = routeId;
   // this.model
    if(localStorage.getItem('loggedInUser') == null  && localStorage.getItem('role') != '1'){
      this.router.navigate(['/login']);  
    }
   
   // //console.log.log(id);
    const followData = {
      model_id: id,
    }
    this.userService.ModelPrice(followData).subscribe(
      response => {
        document.getElementById('openpaymentSelector').click()
        //console.log.log(response);
        this.modelPrice['price']=response['data']['price'];
        this.modelID['id']=id;
        this.modelPrice['routeId']=routeId;
        this.display='block';
        jQuery.noConflict();

      },
      err => {
       
      }
    )
  }




 
  submitSearch(){
    //alert("rl")
    ////console.log.log(localStorage.getItem('loggedInUser'));
    if(localStorage.getItem('loggedInUser') != null  ){
      this.isLoading=true;
      this.searchedOn = false;
      this.search={
          order:this.searchFilter,
      }
      this.searchFilter ="";
      this.userService.modelFilter(this.search).subscribe(
        response => {
  
          //console.log.log(response);
          if(response['success']==1){
            this.modelData = response['data'];
            //console.log.log(response['data']);
            this.isLoading=false;
          }
          else{
            if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1'){
       
              this.ifLogin =true;
              this.addClass = 'right-main'
              this.page = 1;
              this.modelData = []
              this.getAllModels(this.page,'ListModel');
              //this.isLoading=false;
            }
            else{
              this.addClass = 'right-mainNologin'
              this.ifLogin =false;
              this.page = 1;
              this.modelData = []
              this.getAllModels(this.page,'listModelHome');
              //this.isLoading=false;
            }  
           
          }  
        },
        err => {
         //console.log.log(err);
        }
      )
    }
    else{
      this.isLoading=true;
      //this.searchedOn = false;
      this.search={
          order:this.searchFilter,
      }
      this.searchFilter ="";
      this.userService.modelFilterWithoutLogin(this.search).subscribe(
        response => {
  
          //console.log.log(response);
          if(response['success']==1){
            this.modelData = response['data'];
           // //console.log.log(response['data']);
            this.isLoading=false;
            // this.addClass = 'right-mainNologin';
            // this.ifLogin =false;
          }
          else{
            this.page =1
            this.modelData = []
            this.getAllModels(this.page,'listModelHome');
          }
        },
        err => {
         //console.log.log(err);
        }
      )
    }

  }

  filterModel(type){
 
    this.isLoading=true;
    this.searchedOn = false;
    if(type == "ASC"){
      this.search={
        sort:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "DESC"){
      this.search={
        sort:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "featured"){
      this.search={
        featured:type,
      }
      this.featured=true;
      this.featuredName='featured';
    }
    if(type == "trending"){
      const search={
        trending:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "popular"){
      const search={
        popular:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "nameSortASC"){
      this.search={
        nameSort:'ASC',
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "nameSortDESC"){
     // alert(type)
      this.search={
        nameSort:'DESC',
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "mostUploads"){
      this.search={
        mostUploads:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type == "topSelling"){
      this.search={
        topSelling:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type== "recentlyActive"){
      this.search={
        recentlyActive:type,
      }
      this.featured=false;
      this.featuredName='';
    }
    if(type== "live"){
      this.search={
        live:type,
      }
      this.featured=false;
      this.featuredName='live';
    }


    this.userService.modelFilter(this.search).subscribe(
      response => {
        //console.log.log(response);
        if(response['success']==1){
          this.modelData = response['data'];
          //console.log.log(response['data']);
          this.isLoading=false;
        }
        else{
          if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1'){
     
            this.ifLogin =true;
            this.addClass = 'right-main'
            this.getAllModels(this.page + 1,'ListModel');
          }
          else{
            this.addClass = 'right-mainNologin'
            this.ifLogin =false;
            this.getAllModels(this.page + 1,'listModelHome');
          }
          this.isLoading=false;
        }
       
         
       
       
      },
      err => {
       //console.log.log(err);
      }
    )

  }


  // close function to get data after following model
  modalClose(){
    // alert(this.modelMainId)    
    // alert(this.modePosition)
    
    const getFollowstatus ={
      user_id:localStorage.getItem('id'),
      model_id:this.modelMainId
    }
    this.userService.getFollowstatus(getFollowstatus).subscribe(
      response => {
        //console.log.log(response);
        if(response['data'] == 1){
          this.afs.collection('chatCounter').add({'userId': Number(localStorage.getItem('id')), 'sender': 'user','modelId': Number(this.modelMainId), 'MaxChat': Number(10),'time':firebase.firestore.FieldValue.serverTimestamp(),'totalChat':Number(0)});
          document.getElementById('modalCloseClick').click();
          this.modelData[this.modePosition]['following_status'] =1;
          this.modelData[this.modePosition]['status'] =1;
        }
        else{
          document.getElementById('modalCloseClick').click();
        }
      },
      err => {
       //console.log.log(err);
      }
    )

    // this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
    // //this.router.navigate(["model-profile",{uid: idUser } ])); 
    // this.router.navigate(['/models']));  
  }


  getUnique(array){
    var uniqueArray = [];
    for(var i=0; i < array.length; i++){
        if(uniqueArray.indexOf(array[i]) === -1) {
            uniqueArray.push(array[i]);
        }
    }
    return uniqueArray;
}

convertDateUtc(date,type){
  var dates = new Date(date * 1000) ;
  if(type == "weekday"){
    var dates = new Date(date * 1000) ;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

    return dates.getDate() +' '+ monthNames[dates.getMonth()];
  }
  else if(type == "day"){
    return dates.getDate()
  }
  else if(type == "start_time"){
    return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  }
  else if(type == "end_time"){
    return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  }
}


changePaymentMethod(type){
  if(type == "ccbill"){
    this.AddActiveClass = "ccbill"
  }
  else{
    this.AddActiveClass = "finpay"
  }
}

continuePayment(type){
  //this.modalClose()
  document.getElementById('closeCCfinpay').click();
  if(type == 'ccbill'){
    document.getElementById('paymentPopup').click();
    this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton.php?model_id='+this.modelMainId+'&user_id='+localStorage.getItem('id')+'&price='+ this.modelPrice['price']);

  }
  else{
    //document.getElementById('finPayPopup').click();
    const data={
      model_id :this.modelMainId,

    }
    this.userService.PaymentStatus(data).subscribe(
      response => {
        //console.log(response);
        if(response['success']== 0){
          document.getElementById('finPayPopup').click();
        }
        else{
          console.log(response)
        }
       
      },
      err => {
       //console.log.log(err);
      }
    )
  }
}


finpayPayment(){
  const data={
    model_id :this.modelMainId,
    firstName:this.nameCard,
    lastname:"123",
    exp_year:this.expYear,
    exp_month:this.expMonth,
    number:this.cardNumber,
    cvc:this.cvc,
  }
  this.userService.CreatePayment(data).subscribe(

    response => {
      console.log(response);
      if(response['success']== 0){
        //document.getElementById('finPayPopup').click();
      }
      else{
        console.log(response)
      }
     
    },
    err => {
     //console.log.log(err);
    }
  )
}

checkDigit() {
  var allowedChars = "0123456789";
  //alert( this.cardNumber)
  var entryVal = this.cardNumber;
  var flag;

  for(var i=0; i<entryVal.length; i++){       
      flag = false;

      for(var j=0; j<allowedChars.length; j++){
          if(entryVal.charAt(i) == allowedChars.charAt(j)) {
              flag = true; 
          }
      }

      if(flag == false) { 
          entryVal = entryVal.replace(entryVal.charAt(i),""); i--; 
      }
  }

  return true;
}



}
