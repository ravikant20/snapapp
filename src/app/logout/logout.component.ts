import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
previousId :string;
role :string;

  constructor( private router: Router) { 
    
  }

  ngOnInit() {
    this.previousId = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.onlineOffline();
    localStorage.removeItem('loggedInUser');
    localStorage.removeItem('role');
    localStorage.removeItem('status');
    localStorage.removeItem('id');
    localStorage.removeItem("profileImage");

    //localStorage.clear();
    this.router.navigate(['/login']);
  }

  onlineOffline(){
    if(localStorage.getItem('role')=='2'){
        // Fetch the current user's ID from Firebase Authentication.
        var uid = this.previousId;

        var userStatusDatabaseRef = firebase.database().ref('/status/' + uid);

        // var isOfflineForDatabase = {
        //   state: 'offline',
        //   last_changed: firebase.database.ServerValue.TIMESTAMP,
        //   id: uid,
        // };

        var isOnlineForDatabase = {
          state: 'offline',
          last_changed: firebase.database.ServerValue.TIMESTAMP,
          id: uid,
          type:'model'
        };
        userStatusDatabaseRef.set(isOnlineForDatabase);
        // firebase.database().ref('.info/connected').on('value', function(snapshot) {
        // // If we're not currently connected, don't do anything.
        //   if (snapshot.val() == false) {
        //     return;
        //   };
        //   userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase).then(function() {
            
        //   });
        // });
    }
    else{
      // Fetch the current user's ID from Firebase Authentication.
      var uid = this.previousId;

      var userStatusDatabaseRef = firebase.database().ref('/status/' + uid);

      // var isOfflineForDatabase = {
      //   state: 'offline',
      //   last_changed: firebase.database.ServerValue.TIMESTAMP,
      //   id: uid,
        
      // };

      var isOnlineForDatabase = {
        state: 'offline',
        last_changed: firebase.database.ServerValue.TIMESTAMP,
        id: uid,
        type:'subscriber'
      };
      userStatusDatabaseRef.set(isOnlineForDatabase);
      // firebase.database().ref('.info/connected').on('value', function(snapshot) {
      // // If we're not currently connected, don't do anything.
      //   if (snapshot.val() == false) {
      //     return;
      //   };
      //   userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase).then(function() {
          
      //   });
      // });
    }
    


  
  }

}
