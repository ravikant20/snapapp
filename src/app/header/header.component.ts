import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from "../data.service";
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpEvent } from '@angular/common/http';
import { UserService } from '../http.service';
import {DomSanitizer} from "@angular/platform-browser";
import { WindowState } from '@progress/kendo-angular-dialog';

import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import { MessageService } from 'primeng/api';
//import { AngularFireDatabase } from 'angularfire2/database';


import { environment } from '../../environments/environment';
interface Post {
  title: string;
  content: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})
export class HeaderComponent implements OnInit {
  myHeaderShow: any =[];

  postsCol: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;

  postsColChat: AngularFirestoreCollection<Post>;
  postsChat: Observable<Post[]>;

  display: boolean = false;
  getProfileData:any =[];
  message:string;
  imageChange:string;
  UId:string;
  public opened = false;
  IdUrl;
  public windowState: WindowState = 'default';
  notiCount:any;
  //model_id:string = localStorage.getItem('id');
  liveDataNoti :any=[];
  formUrl2= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton2.php');

  modelMainId:string;  
  mainURl:string; 
  mainPrice:string;
  uniqueToken:string;
  FinalArray:any =[];
  unreadMessage:any;
  unreadChat:any;
  liveModel:any = [];
  preViousId :string;


  passwordValidation2: any =[];
  newPassword :string;
  oldPassword :string;
  confirmCheck:string;
  private formDataReset = new FormData();
  loading2:boolean;
  selectedCountry:any=[];
  country: any=[];
  activateDeactivate:string;

  startedLiveChat:boolean=false;
  LiveBroadCastingChatId :string;
  LiveBroadCastingChatData :any = [];
  inputChatBroadcast:any;
  currentEmail:any
  pending:boolean=false;

  mid = localStorage.getItem('profileUrl');

  constructor(private messageService: MessageService,private data: DataService, private router: Router,private http : HttpClient,private userService:UserService,private domSanitizer : DomSanitizer,private afs: AngularFirestore) { 
    this.UId = localStorage.getItem('id');
    this.IdUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('');
    if(localStorage.getItem('role')=='1'){
      // this.postsCol = this.afs.collection('chatCounter', ref => ref.where('userId', '==', Number(localStorage.getItem('id'))).orderBy('time'));
      // this.posts = this.postsCol.valueChanges();
      this.getNoti();
     /// this.getNotiOneToOne();
    }
    this.getLiveChatStatus();
    this.onlineOffline();
   // this.getLiveModel();
    
  }


  ngOnInit() {
    if(localStorage.getItem('status') =="0" && localStorage.getItem('role') =="2"){
      this.router.navigate(['/frontapplication']);
      this.pending = true;
    }
    //alert(this.pending)
    //alert(localStorage.getItem('status'))
    this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message)
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    ////console.log.log(localStorage.getItem('profileImage'))
    if(localStorage.getItem('loggedInUser') && localStorage.getItem('role') == '1'){
      this.myHeaderShow['token']="loggedIn";
      if(localStorage.getItem('profileImage') != '0' ){
        this.data.changeImage(localStorage.getItem('profileImage'))
       // this.getProfileData['profileImage']=localStorage.getItem('profileImage');
      }
      else{
        this.data.changeImage('assets/images/dummy-profile.png')
        //this.getProfileData['profileImage']='assets/images/dummy-profile.png';
      }
      this.data.changeMessage(localStorage.getItem('name'))
      //this.data.currentMessage.subscribe(message => this.message = message)
      //this.getProfileData['name']=localStorage.getItem('name');
    }
    else if(localStorage.getItem('loggedInUser') && localStorage.getItem('role') == '2'){
      this.myHeaderShow['token']="loggedInModel";
      if(localStorage.getItem('profileImage') != '0' ){
        this.data.changeImage(localStorage.getItem('profileImage'))
       // this.getProfileData['profileImage']=localStorage.getItem('profileImage');
      }
      else{
        this.data.changeImage('assets/images/dummy-profile.png')
        //this.getProfileData['profileImage']='assets/images/dummy-profile.png';
      }
      this.data.changeMessage(localStorage.getItem('name'))
      //this.data.currentMessage.subscribe(message => this.message = message)
      //this.getProfileData['name']=localStorage.getItem('name');
    }
    else{
      this.myHeaderShow['token']="";
     // this.router.navigate(['/login']);
    }





  // setInterval(()=>{    //<<<---    using ()=> syntax
	// 		this.changeSlider();
	// 		// console.log('a')
  //      }, 19000);
  }


  copyText(val: string,val2:string){
    
    var selBox = document.createElement('textarea');
    selBox.setAttribute('readonly', 'true');
    selBox.setAttribute('contenteditable', 'true');
    selBox.style.position = 'fixed'; // prevent scroll from jumping to the bottom when focus is set.
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.setSelectionRange(0, 99999); /*For mobile devices*/
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }
  changeSlider(){
    var elem = document.getElementById('changeSlider');
    if(elem!=null){
      document.getElementById('changeSlider').click();
    }
    else{
      //clearTimeout(this.timer1)
    }
	}
  showDialog() {
   // alert("k")
    this.display = true;
  }   
changeStatus(){
  this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
  //this.router.navigate(["model-profile",{uid: idUser } ])); 
  this.router.navigate(['/signup'], { queryParams: {join: 'model' } }));
}

checkStatusAccount(){
  this.userService.checkStatusAccount().subscribe(
    response => {
      console.log(response)
    if(response['success']==1){
      localStorage.setItem('status','1')
      this.router.navigate(['/model-content']);
    }
    else{
      alert("your account is not verified yet");
    }
      
    },
    err => {

    }
  );
}
  goLive(){
    //this.IdUrl=""
    this.opened = true;
    var id=localStorage.getItem('id');
    ////console.log.log(this.model_id)
    document.cookie = "id="+id; 
    document.cookie = "role=model"; 
    document.cookie = "name="+localStorage.getItem('name'); 
    document.getElementById('OpenGoLivePopup').click();
    this.LiveBroadCastingChatId = (Math.random() * 100).toString().replace('.', '')
    this.IdUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/live/#'+this.LiveBroadCastingChatId);
    this.getChatForLiveBroadCast();
    this.LiveBroadCastingChatData = [];
  }
  onConfirm() {
    this.messageService.clear('c');
  }
  
  onReject() {
    this.messageService.clear('c');
  }
  getChatForLiveBroadCast(){

    var ChatGroupRefrence = this.afs.collection('livebroadcastingChat');
    var that =this;
    this.postsColChat =ChatGroupRefrence.doc(this.LiveBroadCastingChatId).collection('chats', ref => ref.orderBy("time","asc"));
        that.postsChat = that.postsColChat.valueChanges();
        that.postsChat.subscribe(res => {
          this.LiveBroadCastingChatData = res;
        })

    this.postsColChat =ChatGroupRefrence.doc(this.LiveBroadCastingChatId).collection('chats');
        that.postsChat = that.postsColChat.valueChanges();
        that.postsChat.subscribe(res => {
          for(var i =0;i<res.length;i++){
            if(res[i]['user_id'] == localStorage.getItem('id'))
            {

            }
          }
    })    

  }

  submitChatBroadcast(){
    var chatReference =  this.afs.collection('livebroadcastingChat').doc(this.LiveBroadCastingChatId);
    var messageReference = chatReference.collection('chats');
    if(this.inputChatBroadcast.trim() == ""){
      return false;
    }
    if(localStorage.getItem('profileImage') != '0' ){
      var image = localStorage.getItem('profileImage');
    }
    else{
      var image = 'assets/images/dummy-profile.png'
    }
    var timestamp = firebase.firestore.Timestamp.now();
    messageReference.add({'message': this.inputChatBroadcast, 'sender': localStorage.getItem('id'),'image': image,'time':timestamp,'name':localStorage.getItem('name')});
    this.inputChatBroadcast = ""
    //console.log(this.LiveBroadCastingChatData)
  }


  public openClose(isOpened: boolean) {
    //alert("r")
    if(localStorage.getItem('role') == '2')
    {
       this.IdUrl= this.domSanitizer.bypassSecurityTrustResourceUrl('');
      //alert(localStorage.getItem('id'))
        var userId=[];
        this.userService.getAllFollowingUsers(localStorage.getItem('id')).subscribe(
          response => {
          if(response['success']==1){
            ////console.log.log(response);
            for(var i =0 ; i<response['data'].length;i++){
              ////console.log.log("response['data'][i]['user_id']",response['data'][i]['user_id'])
              this.stopChat(response['data'][i]['user_id'],response['data'].length - 1)
              //userId.push(response['data'][i]['user_id'])
            }
            //userId.push()
          }
          else{

          }
            
          },
          err => {

          }
        );
     }
     else{
      this.IdUrl= this.domSanitizer.bypassSecurityTrustResourceUrl('');
     }
    this.opened = isOpened;
    //alert("rk")
  }
 stopChat(id:string,length){
  // alert(localStorage.getItem('id'));
   var uid =id.toString();
      var notRefrence = this.afs.collection('notifications');
      notRefrence.doc(uid).collection("message", ref => ref.where('model_id', '==',localStorage.getItem('id') ))
         .get().toPromise().then(function(querySnapshot) {
         querySnapshot.forEach(function(doc) {
           /////console.log.log("doc.id",doc.id)
                    notRefrence.doc(uid).collection("message").doc(doc.id).update({
                    liveUrl : "",
                    stopTime : firebase.firestore.FieldValue.serverTimestamp(),
                    read:false,
                    message:localStorage.getItem('name')+ ' Was Live' 
            }).then(function() {

               })
               .catch(function(error) {

               });
           });
         })

         //var notiLive = this.afs.collection('notiLive');
         var that =this;
         this.afs.collection("notiLive", ref => ref.where('model_id', '==',localStorage.getItem('id') ))
            .get().toPromise().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
              /////console.log.log("doc.id",doc.id)
              that.afs.collection("notiLive").doc(doc.id).delete().then(function() {
                //console.log.log("Document successfully deleted!");
              }).catch(function(error) {
                  //console.log.error("Error removing document: ", error);
              });
              });
          })     
}

  getLiveChatStatus(){
    const that = this;
    if(localStorage.getItem('role') == '1'){
      var loggedUser = 'userId';
    }
    else{
      var loggedUser = 'modelId';
    }
		this.postsCol = this.afs.collection('posts', ref => ref.where(loggedUser, '==', Number(localStorage.getItem('id')) ) );
		this.posts = this.postsCol.valueChanges();
		this.posts.subscribe(res => {
      that.unreadMessage= 0;
      that.unreadChat = 0;
			res.forEach(element1 => {
				var exists = false;
        that.unreadMessage =  that.unreadMessage+element1['unreadCount'][Number(localStorage.getItem('id'))];
        if(element1['unreadCount'][localStorage.getItem('id')] != 0){
          that.unreadChat++;
        }
      });
  
		})
	}


  getNoti(){
       var that = this;
       var notRefrence = that.afs.collection('notifications');

        that.postsCol =notRefrence.doc(localStorage.getItem('id')).collection('message', ref => ref.orderBy("liveTime","desc").limit(10));
        that.posts = that.postsCol.valueChanges();
        that.posts.subscribe(res => {
          // //console.log.log(that.getchatData)
          ////console.log.log("res","=>",res);
          that.notiCount=0;
          for(var i=0;i<res.length;i++){
            if(res[i]['read']==false){
              that.notiCount++;
            }
          }
          that.liveDataNoti =[]
          for(var i =0;i< res.length;i++){
           // if(!res[i]['read']){
              that.liveDataNoti.push(res[i]);
            //}
          }
        })
  }

  getNotiOneToOne(){
    //alert(localStorage.getItem('id'))
    var that = this;
    this.afs.collection("oneToOne", ref => ref.where('userId', '==',Number(localStorage.getItem('id'))).where('readOneToOne', '==', false).where('stopTime','==',''))
    .get().toPromise().then(function(querySnapshot) {
      const abc =''; 
      querySnapshot.forEach(function(doc) {
       // //console.log.log("dfs",doc.data());
        
      });
     
    }).then(() => {
      that.postsCol = that.afs.collection('oneToOne', ref => ref.where('userId', '==', Number(localStorage.getItem('id'))).where('readOneToOne', '==', false).where('stopTime','==','').orderBy("liveTimeOneToOne","desc").limit(10));
    
    ////console.log.log(firebase.firestore.FieldValue.serverTimestamp().)
      that.posts = that.postsCol.valueChanges();
      that.posts.subscribe(res => {
        // //console.log.log(that.getchatData)
        ////console.log.log(res);
         that.notiCount=0;
         for(var i=0;i<res.length;i++){
           if(res[i]['readOneToOne']==false){
            that.notiCount++;
           }
          
         }
         for(var i =0;i< res.length;i++){
          if(!res[i]['readOneToOne']){
           that.liveDataNoti.push(res[i]);
          }
        }
        ////console.log.log(that.liveDataNoti)
      })

    })
  }

  liveCam(URL,model_id,price,uniqueToken,chatId,type){
    //alert("rk")
    this.formUrl2= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton2.php?model_id='+model_id+'&user_id='+localStorage.getItem('id')+'&price='+price+'.00&token='+uniqueToken);
    document.getElementById('paymentPopup2').click();
    //this.showDialog();
    this.modelMainId=  model_id
    this.mainURl=URL
    this.mainPrice = price
    this.uniqueToken = uniqueToken
    this.IdUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('');
    //console.log(chatId)
    this.LiveBroadCastingChatId = chatId;
  }

  modalClose2(){
    // alert(this.modelMainId)    
    // alert(this.modePosition)
    
    const getStatus ={
      user_id:localStorage.getItem('id'),
      model_id:this.modelMainId,
      token: this.uniqueToken
     // model_id:this.modelMainId , mainURl ,mainPrice,uniqueToken
    }
    this.userService.getVideostatus(getStatus).subscribe(
      response => {
        ////console.log.log(response);
        if(response['data'] >0){
          //this.afs.collection('chatCounter').add({'userId': Number(localStorage.getItem('id')), 'sender': 'user','modelId': Number(this.modelMainId), 'MaxChat': Number(10),'time':firebase.firestore.FieldValue.serverTimestamp(),'totalChat':Number(0)});
          document.getElementById('modalCloseClick2').click();
          this.liveCamFinal(this.mainURl,this.modelMainId,this.mainPrice,this.uniqueToken,'null','allStream')
          this.getChatForLiveBroadCast();
        }
        else{
          alert('payment not confirmed')
          document.getElementById('modalCloseClick2').click();
        }
      },
      err => {
       //console.log.log(err);
      }
    )

    // this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
    // //this.router.navigate(["model-profile",{uid: idUser } ])); 
    // this.router.navigate(['/models']));  
  }

  liveCamFinal(URL,model_id,price,uniqueToken,chatId,type){
      if(type =="oneStream"){
        //alert(chatId)
        this.LiveBroadCastingChatId = chatId;
        this.getChatForLiveBroadCast();
      }
      this.updateStatus();
      this.IdUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('');
      document.getElementById('OpenGoLivePopup').click();
      var id=localStorage.getItem('id');
      ////console.log.log(this.model_id)
      document.cookie = "id="+id; 
      document.cookie = "role=subscriber"; 
      document.cookie = "model_id="+model_id; 
      document.cookie = "minute="+price;  /// this was because we can't pass extra param
      document.cookie = "name="+localStorage.getItem('name'); 
      this.IdUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(URL);
      
  }

  UpdateReadStatus(){
    //alert("sdfsd")
    this.updateStatus();
  }
  
  updateStatus(){
    const afs2 =  this.afs;

    
      this.afs.collection("notifications").doc(localStorage.getItem('id')).collection('message', ref => ref)
      .get().toPromise().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
            afs2.collection("notifications").doc(localStorage.getItem('id')).collection('message').doc(doc.id).update({'read': true}).then(function() {
            })
            .catch(function(error) {
            });
        });
      })
  }

  // copyText(val: string){
  //   let selBox = document.createElement('textarea');
  //   selBox.style.position = 'fixed';
  //   selBox.style.left = '0';
  //   selBox.style.top = '0';
  //   selBox.style.opacity = '0';
  //   selBox.value = val;
  //   document.body.appendChild(selBox);
  //   selBox.focus();
  //   selBox.select();
  //   document.execCommand('copy');
  //   document.body.removeChild(selBox);
  // }

  
  onlineOffline(){
    if(localStorage.getItem('role')=='2'){
      var type = "model";
    }
    else{
      var type = "subscriber";
    }
        // Fetch the current user's ID from Firebase Authentication.
        var uid = localStorage.getItem('id');

        var userStatusDatabaseRef = firebase.database().ref('/status/' + uid);

        var isOfflineForDatabase = {
          state: 'offline',
          last_changed: firebase.database.ServerValue.TIMESTAMP,
          id: uid,
          type:type
        };

        var isOnlineForDatabase = {
          state: 'online',
          last_changed: firebase.database.ServerValue.TIMESTAMP,
          id: uid,
          type:type
        };
        firebase.database().ref('.info/connected').on('value', function(snapshot) {
        // If we're not currently connected, don't do anything.
          if (snapshot.val() == false) {
            return;
          };
          userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase).then(function() {
            userStatusDatabaseRef.set(isOnlineForDatabase);
          });
        });
    //}
    


  
  }

  hideErrors(){
    //alert("rk")
    this.passwordValidation2['result'] = "";
    this.passwordValidation2['oldpassword'] = "";
    this.passwordValidation2['newpassword'] = "";
    this.passwordValidation2['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation2['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation2['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation2['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation2['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation2['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation2['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation2['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation2['result'] = "Old Password does not matched";
        }
        if(response['success']==2){
          this.passwordValidation2['result'] = "Some error occured please try after sometime";
        }
        
      },
      err => {
       
          this.passwordValidation2['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }
  
  checkDeactivate2(){
    this.userService.getModelDeactivateStatus().subscribe(
      response => {
        //console.log.log(response);
        this.activateDeactivate = response['data']['accountStatus'];
        document.getElementById('DeactivateDeleteButton2').click();
      },
      err => {
    
      }
    );
  }
  
  setDeactivate(type){
    const types={
      type:type
    }
    if(type == 2){
      document.getElementById('closeDeactivatePopup').click();
      document.getElementById('model-close2').click();
     
      this.router.navigate(['/logout']);
    }
    this.userService.setDeactivate(types).subscribe(
      response => {
        //console.log.log(response);
        if(type == 1){
          this.activateDeactivate = type;
        }
        else{
          this.activateDeactivate = type;
        }
        if(type == 2){
          document.getElementById('closeDeactivatePopup').click();
          document.getElementById('model-close2').click();
         
          this.router.navigate(['/logout']);
        }
       
        document.getElementById('closeDeactivatePopup').click();
      },
      err => {
    
      }
    );
  }
  
  delete(){
    this.activateDeactivate = '2';
  }
  



}


