import { Component, OnInit } from '@angular/core';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
//const nisPackage = require('../../package.json');
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
showData:any=[];
selectedValues: any = [];
isLoading:boolean=true;
loading3:boolean = false;
page: number = 1;  
isLoading2:boolean=false;
hideShowMore :boolean =false
  constructor(private userService:UserService,private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
      this.router.navigate(['/model-account']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.getyourNoti(1);
  }

  getyourNoti(page){
    this.isLoading2  =true
     this.userService.getNotifications(page).subscribe(
       response => {
        this.isLoading2  =false
          //console.log.log(response);
          for (let i = 0; i < response['data'].length ; i++) {
					

					
            this.showData.push(response['data'][i]);
            if(response['data'][i]['nextPage'] == null){
              this.hideShowMore = true;
            }
          }
          //this.showData.push(response['data']);
          //console.log(this.showData[0]);
          this.isLoading = false;
       },
       err => {
        this.isLoading2  =false
          //console.log.log(err);
       }
     );
  }
  
  deleteNotification(){
    this.loading3 = true;
    this.userService.deleteNotifications(this.selectedValues).subscribe(
      response => {
         //console.log.log(response);
         this.getyourNoti(this.page);
         this.selectedValues = [];
         this.loading3 = false;
      },
      err => {
        this.loading3 = false;
         //console.log.log(err);
      }
    );
  }

  loadMore(){
		this.page = this.page + 1;  
		this.getyourNoti(this.page);
	}

}
