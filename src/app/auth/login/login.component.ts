import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from 'src/app/http.model';
import { UserService } from 'src/app/http.service';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {
  //user:User;
  public email: String ;
  public password: String;
  myEmailResponse: any = [];
  myPasswordResponse: any = [];
  myIncorrectPassword: any =[];
  loading: boolean;
  isLoading:boolean=true;
  public params;
  typePassword ='password';
  typePasswordConfirm ='password';
  ConfirmCheck:boolean =true
  check:boolean =true
  remember:boolean =false;
  emailReset:string;
  loading2:boolean =false;

  constructor(private userService:UserService, private router: Router,private messageService: MessageService, private route:ActivatedRoute) { 
    //alert(localStorage.getItem('loggedInUser'));

    if(localStorage.getItem('loggedInUser') != null && localStorage.getItem('role') == '1'){
      this.router.navigate(['/your-feed']);
    }
    if(localStorage.getItem('loggedInUser') != null && localStorage.getItem('role') == '2'){
      this.router.navigate(['/model-account']);
    }
    this.isLoading=false;
      
    
  }

  onConfirm() {
    this.messageService.clear('c');
  }

onReject() {
    this.messageService.clear('c');
  }
  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => this.params = params); 
    if(this.params.id != undefined ){
      this.route.queryParams.subscribe((params: Params) => this.params = params);
      this.verifyEmail();
    }
    let x = document.getElementsByClassName("modal-backdrop");
    console.log(x)
     if(x.length > 0) {
       for(var i=0;i<x.length;i++){
        x[i].classList.remove("modal-backdrop"); 
       }
        
        //x[1].classList.remove("modal-backdrop"); 
      }

  }

  verifyEmail(){
   const uid={
      uid:this.params.id
    }
    this.userService.verifyEmail(uid).subscribe(
      response => {
        ////console.log.log(response)
        if(response['success'] == 1){
          this.messageService.add({severity:'success', summary: 'Success ', detail:'Congratulations! You can now log in'});
        }
        else if(response['success'] == 0){
          this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        }
        else if(response['success'] == 2){
          this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        }
      },
      err => {
   
      }
  );
  }
  public loginUser(){
    // //console.log.log(this.remember);
    //return 
        
  const loginData = {
    email: this.email,
    password: this.password,
    remember:this.remember
  }
  ////console.log.log(this.email)
  if(undefined == this.email || ''==this.email ){
    this.myEmailResponse["response"] = "Email Cannot Be Empty!";
    return false;
  }
   else{
    this.myEmailResponse["response"] = "";
    //   //return false;
   }
  if(this.password == undefined || ''==this.password){
    this.myPasswordResponse["response"] = "Password Cannot Be Empty!";
    return false;
  }
   else {
    this.myPasswordResponse["response"] = "";
  //   //return false;
  }
  this.loading=true;
  // alert(this.remember);
  this.userService.loginUser(loginData).subscribe(
    response => {
      //console.log.log(response)
      this.loading=false;
      if(response['success']['token']!="" && response['success']['role'] == 1 && response['success']['emailVerify'] !=0){
        


        localStorage.setItem("loggedInUser", response['success']['token']);
        localStorage.setItem("role", '1');
        localStorage.setItem("status", response['success']['status']);
        localStorage.setItem("id", response['success']['id']);
        ////console.log.log(response['success']['profileImage']);
        //console.log.log(response['success']['token']);
        if(response['success']['profileImage']){
          localStorage.setItem("profileImage", response['success']['profileImage']);
        }
        else{
          localStorage.setItem("profileImage", '0');
        }
       
        localStorage.setItem("name", response['success']['name']);
        if(this.remember){
          localStorage.setItem("keepMeLoggedIn",'yes');
        }
        else{
          localStorage.setItem("keepMeLoggedIn",'no');
        }
        //localStorage.setItem("keepMeLoggedIn", this.remember);
        this.router.navigate(['/your-feed']);
      }
      else if(response['success']['token']!="" && response['success']['role'] == 2 && response['success']['emailVerify'] !=0){
        if(response['success']['accountStatus'] == '2'){
          this.messageService.add({severity:'error', summary: 'Error', detail:"Your Account has been deleted."});
          return;
        }
        // if(response['success']['status'] == "0"){
        //   this.messageService.add({severity:'error', summary: 'Error', detail:"pending review"});
        //   return;
        // }
        if(response['success']['status'] == "2"){
          this.messageService.add({severity:'error', summary: 'Error', detail:"account deactivated"});
          return;
        }
        if(response['success']['status'] == "3"){
          this.messageService.add({severity:'error', summary: 'Error', detail:"account deleted"});
          return;
        }
        
        if(response['success']['status'] == "4"){
          this.messageService.add({severity:'error', summary: 'Error', detail:"account disabled by admin"});
          return;
        }
        localStorage.setItem("role", '2');
        localStorage.setItem("status", response['success']['status']);
        localStorage.setItem("loggedInUser", response['success']['token']);
        localStorage.setItem("id", response['success']['id']);
        localStorage.setItem("profileUrl", response['success']['profileUrl']);
        //console.log.log(response['success']['token']);
        if(response['success']['profileImage']){
          localStorage.setItem("profileImage", response['success']['profileImage']);
        }
        else{
          localStorage.setItem("profileImage", '0');
        }
        
        localStorage.setItem("name", response['success']['name']);
        if(response['success']['status'] == "0"){
          //alert("rk")
          this.router.navigate(['/frontapplication']);
          return;
        }
        //alert("model")
        this.router.navigate(['/model-content']);
        
      }
      else if(response['success']['emailVerify'] ==0){
        //alert("account not verified")
        document.getElementById('activateResend').click()
        // this.messageService.add({severity:'error', summary: 'Error', detail:"Account not Verified Please Check Your Mail"});
      }

      else{
        this.router.navigate(['/login']);
        if(response['success']['profileImage']){
          localStorage.setItem("profileImage", response['success']['profileImage']);
        }
        else{
          localStorage.setItem("profileImage", '0');
        }
       
        localStorage.setItem("name", response['success']['name']);
      }
    },
    err => {
      this.loading=false;
      if(err.error.error == "Unauthorised"){
        this.myIncorrectPassword["response"] = "Incorrect Email Or Password";
        
      }
      else{
        this.myIncorrectPassword["response"] = "Something went wrong please try after sometime";
      }
    }
);
  }


  PasswordHide(){
    this.typePassword ='password';
    this.check = true;
  }
  PasswordCheck(){
    this.typePassword ='input';
    this.check = false;
    //typePasswordConfirm ='password';
  }

  resetLink(){
    this.loading2 = true;
    this.userService.ResetPasswordLink(this.emailReset).subscribe(
      response => {
        this.loading2 = false;
        document.getElementById('model-closeEmailReset').click()
        ////console.log.log(response)
        if(response['success'] == 0){
          this.messageService.add({severity:'error', summary: 'error ', detail:"This Email Doesn't exists"});
        }
        else{
          this.messageService.add({severity:'success', summary: 'success ', detail:"Reset link sent successfully"});
        }
        // else if(response['success'] == 0){
        //   this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        // }
        // else if(response['success'] == 2){
        //   this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        // }
      },
      err => {
        this.loading2 = false;
      }
    );
  }


  resendLink(){
    this.loading2 = true;
    this.userService.resendLink(this.email).subscribe(
      response => {
        //this.loading2 = false;
        document.getElementById('model-closeEmailReset').click()
        ////console.log.log(response)
        if(response['success'] == 0){
          this.loading2 = false;
          this.messageService.add({severity:'error', summary: 'error ', detail:"This Email Doesn't exists"});
        }
        else{
          this.loading2 = false;
          document.getElementById('model-closeEmailReset2').click();
          this.messageService.add({severity:'success', summary: 'success ', detail:"Confirmation link send successfully"});
        }
        // else if(response['success'] == 0){
        //   this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        // }
        // else if(response['success'] == 2){
        //   this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        // }
      },
      err => {
        this.loading2 = false;
      }
    );
  }


}
