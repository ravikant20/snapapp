import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from 'src/app/http.model';
import { UserService } from 'src/app/http.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
import {RadioButtonModule} from 'primeng/primeng';
import {MessageService} from 'primeng/api';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataService } from "../../data.service";
@Component({
  selector: 'app-root',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class SignupComponent implements OnInit {

  public email: string ='' ;
  public password: String='';
  public name: String='';
  PasswordConfirm:string='';
  joinASa: string='1';
  validation: any = [];
  validator: any;
  isLoading: boolean =true;
  validation_top:any=[];
  label:any=[];
  loading: boolean;
  show = true;   
  user = true;
  model = false;
  public params;
  status =1;
  typePassword ='password';
  typePasswordConfirm ='password';
  ConfirmCheck:boolean =true
  check:boolean =true
  whatTobecome:any = 1;
  //isLoading:boolean=false
  //val2: string = 'Option 2';
  constructor(private userService:UserService, private router: Router,private route:ActivatedRoute,private messageService: MessageService,private data: DataService) { 
    //alert(localStorage.getItem('loggedInUser'));
    if(localStorage.getItem('loggedInUser') != null && localStorage.getItem('role') == '1'){
      this.router.navigate(['/your-feed']);
    }
    if(localStorage.getItem('loggedInUser') != null && localStorage.getItem('role') == '2'){
      this.router.navigate(['/model-account']);
    }

    this.data.currentjoinAsa.subscribe(joinASa => this.joinASa = joinASa)
  }

  ngOnInit() {
    //document.getElementById('signupSubscriberModel').click();
    this.isLoading=false;
    this.label['name'] = 'name';
    this.label['email'] = 'email';
    this.label['password'] = 'password';
    this.route.queryParams.subscribe((params: Params) => this.params = params);
    //console.log.log(this.params.join)
    if(this.params.join == 'model'){
      this.joinASa='2';
      document.getElementById('closeChooseModel').click();
      this.netChange(2)
    }
    else{
      this.joinASa='1';
      this.netChange(1)
      document.getElementById('signupSubscriberModel').click();
      document.getElementById('closeChooseModel').click();
    }
    // (<HTMLInputElement>document.getElementById('cb2')).checked = true;
    // document.getElementById('cb2').click();
  }
  chekWhattobecome(){
    // alert(this.joinASa)
  }
  FInalChoose(){
   document.getElementById('closeChooseModel').click();
   this.netChange(parseInt(this.joinASa))
    // alert(this.joinASa)
  }
  public RegisterUser(){
    
  
    if(this.joinASa == '2'){
      this.status = 1;
    }    
    const RegisterData = {
      email: this.email,
      password: this.password,
      name:this.name,
      role:this.joinASa,
    }
    ////console.log.log(RegisterData);
    if(this.name == undefined || ''==this.name){
      this.validation["name"] = "Name Cannot Be Empty!";
      return false;
    }
     else {
      this.validation["name"] = "";
    //   //return false;
    }
    if(undefined == this.email || ''==this.email ){
      this.validation["email"] = "Email Cannot Be Empty!";
      return false;
    }
     else{
      this.validation["email"] = "";
      //   //return false;
     }
     var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

    if (!pattern.test(this.email) ) {
      //alert("ds")
      this.validation["email"] = "Email Not Valid!";
      return false;
    }
    else{
      this.validation["email"] = "";
    }
    if(this.password == undefined || ''==this.password){
      this.validation["password"] = "Password Cannot Be Empty!";
      return false;
    }
     else {
      this.validation["password"] = "";
    //   //return false;
    }
    if(this.PasswordConfirm != this.password){
      //console.log.log("this.PasswordConfirm ",this.PasswordConfirm )
      //console.log.log("this.PasswordConfirm ",this.password )
      this.validation["passwordConfirm"] = "Password does not matched";
      return false;
    }
     else {
      this.validation["passwordConfirm"] = "";
    //   //return false;
    }
    
    if(this.joinASa == undefined || ''==this.joinASa){
      this.validation["name"] = "Join As Cannot Be Empty!";
      return false;
    }
     else {
      this.validation["type"] = "";
    //   //return false;
    }
    this.loading=true;
    this.userService.RegisterUser(RegisterData).subscribe(
      
      response => {
        this.loading = false;
        //console.log.log(response);
        if(response['success']['token'] !=""){
          if(response['success']['role']== 1){
            this.messageService.add({severity:'success', summary: 'Success ', detail:'Check Your Mail To Verify Your Account'});
          }
          else{
            this.messageService.add({severity:'success', summary: 'Success ', detail:'Check Your Mail To Verify Your Account'});
          }
          this.email="";
          this.password="";
          this.name="";
          this.joinASa="1";
         this.netChange(1)
          //this.messageService.add({severity:'error', summary: 'Congrats ', detail:'You signed up as'+d});
          //this.router.navigate(['/']);
        }
        else{
          this.validation['emailtaken']="Some Thing Went Wrong";
          this.router.navigate(['/login']);
        }
        
      },
      err => {
        this.loading = false;
        //console.log.log(err.error);
       
        this.messageService.add({severity:'error', summary: 'Error', detail:err.error.data.message});
        //this.validation_top['message']=err.error.data.message;
        
        
        
        // alert("something went wrong!");
        // //console.log.log(err);
      }
  );
  }

  public netChange(val){
    //alert(val)
    if(val == 2){
      this.show = false;
      this.user = false;
      this.model = true;
      this.label['name'] = 'Your Model Name';
      this.label['email'] = 'Your Email';
      this.label['password'] = 'Create Password';
      this.label['passwordConfirm'] = 'Confirm Password';
    }
    else{
      this.show = true;
      this.user = true;
      this.model = false;
      this.label['name'] = 'name';
      this.label['email'] = 'email';
      this.label['password'] = 'password';
      this.label['passwordConfirm'] = 'Confirm Password';
    }
  }


	CheckName(){
		//console.log.log(this.name)
		//alert("rk")
		if(this.name != "" ){
			this.validation["name"] = "";
		}
		else{
			this.validation["name"] = "Name Cannot Be Empty!";
		}
	}

	PasswordConfirmCheck(){
		//typePassword ='password';
		this.typePasswordConfirm ='input';
		this.ConfirmCheck = false;
	}



	ConfirmPasswordHide(){
		this.typePasswordConfirm ='password';
		this.ConfirmCheck = true;
	}

	PasswordHide(){
		this.typePassword ='password';
		this.check = true;
	}
	PasswordCheck(){
		this.typePassword ='input';
		this.check = false;
		//typePasswordConfirm ='password';
	}

	CheckEmail(){
		if(this.email != "" ){
			this.validation["email"] = "";
		}
		else{
			this.validation["email"] = "Email Cannot Be Empty!";
		}

		var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

		if (!pattern.test(this.email) ) {
			//alert("ds")
			this.validation["email"] = "Email Not Valid!";
			return false;
		}
	}

	CheckPassword(){
	// alert(this.password)
		if(this.password == ""){
			//    //console.log.log("here")
			this.validation["password"] = "Password Cannot Be Empty!";  
		}
		else{
			//  //console.log.log("there")
			this.validation["password"] = "";
		}
	}

	CheckPasswordConfirm(){
		// alert(this.password)
		if(this.password != this.PasswordConfirm){
	//    //console.log.log("here")
		this.validation["passwordConfirm"] = "Your Password Not Matched";  
		}
		else{
		//  //console.log.log("there")
		this.validation["passwordConfirm"] = "";
		}
	}

	onConfirm() {
		this.messageService.clear('c');
	}

	onReject() {
		this.messageService.clear('c');
	}
}
