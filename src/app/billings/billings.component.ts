import { Component, OnInit } from '@angular/core';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-billings',
  templateUrl: './billings.component.html',
  styleUrls: ['./billings.component.css']
})

export class BillingsComponent implements OnInit {
  billingStatus:any=[];
  postsCol: AngularFirestoreCollection<Post>;
  isLoading:boolean = true;
  loadingCancel:boolean = false;
  modeCancel :string;
  constructor(private userService:UserService,private router: Router,private afs: AngularFirestore) { }

  ngOnInit() {
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
      this.router.navigate(['/model-account']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.getBillingstatus();
  }


  getBillingstatus(){
    this.userService.Billingstatus().subscribe(
      response => {
        this.billingStatus = response['data']
        //console.log.log(response);
        this.isLoading = false
      },
      err => {
        this.router.navigate(['/logout']);
        //console.log.log(err);
      }
    );
  }

  // cancelSubscription(model_id,user_id){

  // }


  cancelSubscription(model_id){
    this.modeCancel = model_id
    this.loadingCancel = true;
    var userId= localStorage.getItem("id");
    const followData = {
      id: 1,
      model_id: model_id,
      api_token:localStorage.getItem("loggedInUser")
    }
    this.userService.FollowUnfollow(followData).subscribe(
      response => {
       this.loadingCancel = false;
       ////console.log.log(response);
       if(response['success']==1){
        this.loadingCancel = false;

          //alert("rk")
          var mod =0;
          const afs2 =  this.afs;
          this.afs.collection("chatCounter", ref => ref.where('userId', '==', Number(userId)).where('modelId', '==', Number(model_id)))
          .get().toPromise().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
              ////console.log.log(doc.id, " => ", doc.data());
                //this.chatId = doc.id;
                //console.log.log(doc.id);
                doc.ref.delete();
                afs2.collection("chatStatus").doc(doc.id).delete().then(function() {
                  //console.log.log("Document successfully deleted!");
                }).catch(function(error) {
                    //console.log.error("Error removing document: ", error);
                });
            });
          })
          this.getBillingstatus();

  
        ////console.log.log(mod)

        // //console.log.log('following_status', this.modelData.following_status);
        //this.getAllModels();
       } 
      },
      err => {
        //this.router.navigate(['/login']);
        //console.log.log(err);
        alert("something went wrong")
      }
    );
  }


}
