import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ModelProfileComponent } from './model-profile/model-profile.component';
import { YourfeedComponent } from './yourfeed/yourfeed.component';
import { SettingsComponent } from './settings/settings.component';
import { LogoutComponent } from './logout/logout.component';
import { ModelsComponent } from './models/models.component';
import { FollowsComponent } from './follows/follows.component';
import { ExploreComponent } from './explore/explore.component';
import { MessagesComponent } from './messages/messages.component';
import { ApplicationComponent } from './model-page/application/application.component';
import { MessageModelComponent } from './model-page/message-model/message-model.component';
import { ContentComponent } from './model-page/content/content.component';
import { ModelPageComponent } from './model-page/model-page.component';
import { ViewProfileComponent } from './model-page/view-profile/view-profile.component';
import { AvailabilityComponent } from './model-page/availability/availability.component';
import { SubscriberComponent } from './model-page/subscriber/subscriber.component';
import { ShareComponent } from './share/share.component';
import { NotificationComponent } from './notification/notification.component';
import { AppointmentComponent } from './model-page/appointment/appointment.component';
import { BillingsComponent } from './billings/billings.component';
import { BookingsComponent } from './bookings/bookings.component';
import { ContactusComponent } from './contactus/contactus.component';
import { FaqComponent } from './faq/faq.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { NotificationsComponent } from './model-page/notifications/notifications.component';
import { ForgotComponent } from './forgot/forgot.component';
import { PriceComponent } from './model-page/price/price.component';
import { HowitworksComponent } from './howitworks/howitworks.component';
import { ProfileComponent } from './model-page/profile/profile.component';
import { FrontapplicationComponent } from './model-page/frontapplication/frontapplication.component';


//import { ApplicationComponent } from './messages/messages.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'model-profile', component: ModelProfileComponent },
  
  { path: 'your-feed', component: YourfeedComponent },
  { path: 'settings', component: SettingsComponent },
  {path :'logout',component:LogoutComponent},
  {path:'models',component:ModelsComponent},
  {path:'follows',component:FollowsComponent},
  {path:'explore',component:ExploreComponent},
  {path:'messages',component:MessagesComponent},
  {path:'model-account',component:ApplicationComponent},
  {path:'model-message',component:MessageModelComponent},
  {path:'model-content',component:ContentComponent},
  {path:'RefrshComponent',component:ModelPageComponent},
  {path:'ViewAS',component:ViewProfileComponent},
  {path:'availability',component:AvailabilityComponent},
  {path:'subscriber',component:SubscriberComponent},
  {path:'share',component:ShareComponent},
  {path:'notification',component:NotificationComponent},
  {path:'Appointment',component:AppointmentComponent},
  {path:'billings',component:BillingsComponent},
  {path:'booking',component:BookingsComponent},
  {path:'contact',component:ContactusComponent},
  {path:'faq',component:FaqComponent},
  {path:'terms',component:TermsComponent},
  {path:'privacy',component:PrivacyComponent},
  {path:'notification-model',component:NotificationsComponent},
  {path:'forgot-password',component:ForgotComponent},  
  {path:'price',component:PriceComponent},  
  {path:'howitworks',component:HowitworksComponent},  
  {path:'profile',component:ProfileComponent},  
  {path:'frontapplication',component:FrontapplicationComponent},  
  
  


  { path: ':modelName', component: ModelProfileComponent },
  
  
  // ContactusComponent
  ///{ path: 'auth', component: AuthComponent },
  // { path: 'home', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash : false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
