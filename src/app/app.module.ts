import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './auth/signup/signup.component';
import { ModelProfileComponent } from './model-profile/model-profile.component';
import { HeadermodelprofileComponent } from './model-profile/headermodelprofile/headermodelprofile.component';
import { FootermodelprofileComponent } from './model-profile/footermodelprofile/footermodelprofile.component';
import { SettingsComponent } from './settings/settings.component';
import { YourfeedComponent } from './yourfeed/yourfeed.component';
import { LogoutComponent } from './logout/logout.component';
import { ModelsComponent } from './models/models.component';
import { FollowsComponent } from './follows/follows.component';
import { DataService } from './data.service';
import { Module as StripeModule } from "stripe-angular"
import {RadioButtonModule, MenuItem} from 'primeng/primeng';
import { EditorModule } from 'primeng/primeng';
import { NgxStripeModule } from 'ngx-stripe';
import { HttpModule } from '@angular/http';
import {ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ExploreComponent } from './explore/explore.component';
import { environment } from '../environments/environment';
import { MessagesComponent } from './messages/messages.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestore } from 'angularfire2/firestore';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModelPageComponent } from './model-page/model-page.component';
import { ApplicationComponent } from './model-page/application/application.component';
import { MessageModelComponent } from './model-page/message-model/message-model.component';
import { ContentComponent } from './model-page/content/content.component';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ViewProfileComponent } from './model-page/view-profile/view-profile.component';
import {CalendarModule} from 'primeng/calendar';
// import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
 import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import {AccordionModule} from 'primeng/accordion';
import { AvailabilityComponent } from './model-page/availability/availability.component';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SubscriberComponent } from './model-page/subscriber/subscriber.component';
import {RatingModule} from 'primeng/rating';
import {ToastModule} from 'primeng/toast';
import { ShareComponent } from './share/share.component';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { NotificationComponent } from './notification/notification.component';
import { AppointmentComponent } from './model-page/appointment/appointment.component';
import {GalleriaModule} from 'primeng/galleria';
import {LightboxModule} from 'primeng/lightbox';
import { BillingsComponent } from './billings/billings.component';
import { BookingsComponent } from './bookings/bookings.component';
import { ContactusComponent } from './contactus/contactus.component';
import { FaqComponent } from './faq/faq.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import {DialogModule} from 'primeng/dialog';
import {CheckboxModule} from 'primeng/checkbox';
import { NotificationsComponent } from './model-page/notifications/notifications.component';
import {DropdownModule} from 'primeng/dropdown';
import { ForgotComponent } from './forgot/forgot.component';
import { PriceComponent } from './model-page/price/price.component';
import {TooltipModule} from 'primeng/tooltip';
import { HowitworksComponent } from './howitworks/howitworks.component';
import { ProfileComponent } from './model-page/profile/profile.component';
import { FrontapplicationComponent } from './model-page/frontapplication/frontapplication.component';

import {ProgressBarModule} from 'primeng/progressbar';


//accordion and accordion tab

// import { AngularFireDatabase, AngularFireObject, AngularFireDatabaseModule } from 'angularfire2/database';

export const firebaseConfig = {

    apiKey: "AIzaSyBINVnyMMm96TMtJ8mzzP7XRtWOMR_Ou2Y",
    authDomain: "snap-management.firebaseapp.com",
    databaseURL: "https://snap-management.firebaseio.com",
    projectId: "snap-management",
    storageBucket: "",
    messagingSenderId: "157167417093",
    appId: "1:157167417093:web:4ded7fd32ab5930c"
  
};
@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SignupComponent,
    ModelProfileComponent,
    HeadermodelprofileComponent,
    FootermodelprofileComponent,
    SettingsComponent,
    YourfeedComponent,
    LogoutComponent,
    ModelsComponent,
    FollowsComponent,
    ExploreComponent,
    MessagesComponent,
    ModelPageComponent,
    ApplicationComponent,
    MessageModelComponent,
    ContentComponent,
    ViewProfileComponent,
    AvailabilityComponent,
    SubscriberComponent,
    ShareComponent,
    NotificationComponent,
    AppointmentComponent,
    BillingsComponent,
    BookingsComponent,
    ContactusComponent,
    FaqComponent,
    TermsComponent,
    PrivacyComponent,
    NotificationsComponent,
    ForgotComponent,
    PriceComponent,
    HowitworksComponent,
    ProfileComponent,
    FrontapplicationComponent,
    
    
     //HttpClientModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    //StripeModule.forRoot(),
    RadioButtonModule,
    EditorModule,
    NgxStripeModule.forRoot('pk_test_XsCV2lAW6EG6jRrulmXgma2M00cMQ8KnqI'),
    ReactiveFormsModule,
    HttpModule,
    InfiniteScrollModule,
    AngularFireModule.initializeApp(firebaseConfig),
    NgSelectModule,
    SlimLoadingBarModule.forRoot(),
    CalendarModule,
    BrowserAnimationsModule,
    AccordionModule,
    DateInputsModule,
    NgbModule,
    RatingModule,
    ToastModule,
    DialogsModule,
    GalleriaModule,
    LightboxModule,
    DialogModule,
    CheckboxModule,
    DropdownModule,
    TooltipModule,
    ProgressBarModule
    // OwlDateTimeModule,
    // OwlNativeDateTimeModule,
    // BrowserAnimationsModule
	  // AngularFireDatabaseModule
    // //BrowserModule,
  
  ],
  providers: [DataService,AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
