
import { Component, OnInit } from '@angular/core';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

import {DomSanitizer} from "@angular/platform-browser";
import { WindowState } from '@progress/kendo-angular-dialog';
import { environment } from '../../environments/environment';
interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  bookingStatus:any=[];

  bookingStatusActive:any = [];
  bookingStatusPast:any = [];
  public openedss = false;
  IdUrl3;
  isLoading:boolean=true;
  public windowState: WindowState = 'default';

  postsColChat: AngularFirestoreCollection<Post>;
  postsChat: Observable<Post[]>;


  startedLiveChat:boolean=false;
  LiveBroadCastingChatId :string;
  LiveBroadCastingChatData :any = [];
  inputChatBroadcast:any;
  imageDisplay:string;


  constructor(private userService:UserService,private router: Router,private afs: AngularFirestore,private domSanitizer : DomSanitizer) { }
  ngOnInit() {
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
      this.router.navigate(['/model-account']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.getBookingstatus();
    //this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl('https://enacteservices.net/snapapp/livebroadcasting/oneToOne.php#'+(Math.random() * 100).toString().replace('.', ''));
    this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl('');

    if(localStorage.getItem('profileImage') != '0' ){
      this.imageDisplay = localStorage.getItem('profileImage');
    }
    else{
      this.imageDisplay = 'assets/images/dummy-profile.png'
    }

  }

  getBookingstatus(){
    this.userService.bookingstatus().subscribe(
      response => {
        this.bookingStatus = response['data']
        //console.log.log(response['data']);
       for(var i=0;i<response['data']['length'];i++){
        if(response['data'][i]['completed'] == 'no'){
          this.bookingStatusActive.push(response['data'][i])
        }
        else{
          this.bookingStatusPast.push(response['data'][i])
        }

       }
       this.isLoading = false;
        // if(response['data'][])
        ////console.log.log(response);
      },
      err => {
        this.router.navigate(['/logout']);
        ////console.log.log(err);
      }
    );
  }

  joinLiveChat(id,liveId,minute,day){
    //alert("rk")
    var seconds = new Date().getTime() / 1000;
    if(Number(day)>seconds){
      alert("You are Too Early for this broadcast wait till time comes. ");
      return false;
    }
    if(Number(day) + 300 < seconds){
      alert("Unfortunately, you are late. We have cancelled the session and you may contact admin for the refund");
      return false;
    }

    // this.openedss = true;
    document.getElementById('OpenGoLivePopup2').click();
    var ids=localStorage.getItem('id');
    document.cookie = "id="+ids; 
    document.cookie = "name="+localStorage.getItem('name'); 
    document.cookie = "user_id="+id; 
    document.cookie = "role=subscriber"; 
    document.cookie = "type=oneWay"; 
    document.cookie = "minute="+minute; 
    ////console.log.log(document.cookie)
    this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/live/oneToOne.php#'+liveId);
    this.LiveBroadCastingChatId = liveId
    this.getChatForLiveBroadCast();
  }

  getChatForLiveBroadCast(){

    var ChatGroupRefrence = this.afs.collection('livebroadcastingChat');
    var that =this;
    this.postsColChat =ChatGroupRefrence.doc(this.LiveBroadCastingChatId).collection('chats', ref => ref.orderBy("time","asc"));
        that.postsChat = that.postsColChat.valueChanges();
        that.postsChat.subscribe(res => {
          this.LiveBroadCastingChatData = res;
        })

  }

  submitChatBroadcast(){
    // this.LiveBroadCastingChatData = "";
    var chatReference =  this.afs.collection('livebroadcastingChat').doc(this.LiveBroadCastingChatId);
    var messageReference = chatReference.collection('chats');
    if(this.inputChatBroadcast.trim() == ""){
      return false;
    }
    if(localStorage.getItem('profileImage') != '0' ){
      var image = localStorage.getItem('profileImage');
    }
    else{
      var image = 'assets/images/dummy-profile.png'
    }
    var timestamp = firebase.firestore.Timestamp.now();
    messageReference.add({'message': this.inputChatBroadcast, 'sender': localStorage.getItem('id'),'image': image,'time':timestamp,'name':localStorage.getItem('name')});
    // this.LiveBroadCastingChatData = "";
    this.inputChatBroadcast = ""
    //console.log(this.LiveBroadCastingChatData)
  }

  


  public openCloseq(isOpened: boolean) {
    this.openedss = isOpened;
    this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl('');

  }

  convertDateUtc(date,type){
		var dates = new Date(date * 1000) ;
		if(type == "weekday"){
			var dates = new Date(date * 1000) ;
			var weekday=new Array(7);
			weekday[0]="Sun";
			weekday[1]="Mon";
			weekday[2]="Tue";
			weekday[3]="Wed";
			weekday[4]="Thu";
			weekday[5]="Fri";
			weekday[6]="Sat";
		  //console.log.log(date)
		 return weekday[dates.getDay()];
		  
		}
		else if(type == "day"){
		  return dates.getDate()
		}
		else if(type == "start_time"){
		  return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "end_time"){
		  return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "year"){
			return dates.getFullYear()
		}
		else if(type == "month"){
			return dates.getMonth()+1
    }
    else if(type == "monthShort"){
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        return monthNames[dates.getMonth()]
		}
  }

}
