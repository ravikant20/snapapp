import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../http.service';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { ElementsOptions, StripeService } from 'ngx-stripe';
import { Http } from '@angular/http';
import * as $AB from 'jquery';
import * as bootstrap from 'bootstrap';
import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-follows',
  templateUrl: './follows.component.html',
  styleUrls: ['./follows.component.css']
})
export class FollowsComponent implements OnInit {
  postsCol: AngularFirestoreCollection<Post>;
  //posts: Observable<Post[]>;


  modelDataFollowing: any =[];
  editmode =true;
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  @ViewChild('editForm', {static: false})
  private publishableKey:string = "pk_test_XsCV2lAW6EG6jRrulmXgma2M00cMQ8KnqI"
  modelData: any =[];

  modelPrice:any =[];
  modelID:any =[];
  private formData = new FormData();
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  display='none';
  elementsOptions: ElementsOptions = {
    locale: 'es'
  };
  showModal=true;
  paymentConfirmation:any =[];
  loading: boolean;
  form: FormGroup;
  editForm: NgForm;
  isLoading:boolean=true;
  searchFilter:string;
  nameMain:string;
  constructor(private userService:UserService,private router: Router,private stripeService: StripeService,private http : Http,
    private fb: FormBuilder,private _zone: NgZone,private afs: AngularFirestore,) { 
    // this.StripeScriptTag.setPublishableKey( this.publishableKey )
  }

  //constructor(private userService:UserService,private router: Router) { }

  ngOnInit() {
    this.getFollowedModel();
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
      this.router.navigate(['/model-account']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
  }

  getFollowedModel(){
    const api = {
      api_token:localStorage.getItem("loggedInUser")
     }
     this.userService.getAllFollowingModels(api).subscribe(
       response => {
        //console.log.log(response);
        if(response['success']==1){
          this.modelDataFollowing = response['data'];
          //console.log.log(response['data']);
          this.isLoading=false;
        }
        else{
          alert("No models Found");
          this.isLoading=false;
        }
         
       },
       err => {
         this.router.navigate(['/login']);
         //console.log.log(err);
       }
     );
  }

  FollowModel(id,model_id,i){
    this.editmode = false;
    //console.log.log(id);
    //console.log.log(model_id);
    var userId= localStorage.getItem("id");
    const followData = {
      id: id,
      model_id: model_id,
      api_token:localStorage.getItem("loggedInUser")
    }
    this.userService.FollowUnfollow(followData).subscribe(
      response => {
       ////console.log.log(response);
       if(response['success']==1){
        if(id==1){
          //alert("rk")
          var mod =0;
          const afs2 =  this.afs;
          this.afs.collection("chatCounter", ref => ref.where('userId', '==', Number(userId)).where('modelId', '==', Number(model_id)))
          .get().toPromise().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
              ////console.log.log(doc.id, " => ", doc.data());
                //this.chatId = doc.id;
                //console.log.log(doc.id);
                doc.ref.delete();
                afs2.collection("chatStatus").doc(doc.id).delete().then(function() {
                  //console.log.log("Document successfully deleted!");
                }).catch(function(error) {
                    //console.log.error("Error removing document: ", error);
                });
                //alert("!2")
                // Build doc ref from doc.id
                // afs2.collection("chatStatus").doc(doc.id).update({'content': content,'time':firebase.firestore.FieldValue.serverTimestamp(),'type':type,'userProfile':userProfile,'modelProfile': modelProfile,'chatNotificationUser':true,'chatNotificationModel':false}).then(function() {
                //   //console.log.log("Document successfully written!");
                // })
                // .catch(function(error) {
                //   //console.log.error("Error writing document: ", error);
                // });
            });
          })
        }
        else if(0==id){
          var mod =1;
          this.afs.collection('chatCounter').add({'userId': Number(userId), 'sender': 'user','modelId': Number(model_id), 'MaxChat': Number(10),'time':firebase.firestore.FieldValue.serverTimestamp(),'totalChat':Number(0)});

        }
        ////console.log.log(mod)
        this.modelDataFollowing[i]['following_status'] =mod;
        this.modelDataFollowing[i]['status'] =response['data'];
       
        this.editmode = true;
        // //console.log.log('following_status', this.modelData.following_status);
        //this.getAllModels();
       } 
      },
      err => {
        //this.router.navigate(['/login']);
        //console.log.log(err);
        alert("something went wrong")
      }
    );
  }
  getModelPrice(id,routeId){

    // //console.log.log(id);
     const followData = {
       model_id: id,
     }
     this.userService.ModelPrice(followData).subscribe(
       response => {
         //console.log.log(response);
         this.modelPrice['price']=response['data']['price'];
         this.modelID['id']=id;
         this.modelPrice['routeId']=routeId;
         this.display='block';
         jQuery.noConflict();
         //this.editForm.resetForm();
         this.mytemplateForm.reset();
         document.getElementById('paymentPopup').click();
         
         //this.display='none';
         //$("#payment").modal('show');
       },
       err => {
        
       }
     )
   }

   getToken(price,routeId,modelId) {
    this.loading=true;
    // //console.log.log(routeId);
    // //console.log.log(modelId);
   // this.message = 'Loading...';

    (<any>window).Stripe.card.createToken({
      number: this.cardNumber,
      exp_month: this.expiryMonth,
      exp_year: this.expiryYear,
      cvc: this.cvc,
      //amount: 2000
      //amount:25
    }, (status: number, response: any) => {
          // //console.log.log(response);
          // //console.log.log(status);
      // Wrapping inside the Angular zone
      this._zone.run(() => {
        if (status === 200) {
          const paymentToken = {
            stripeToken: response.id,
            model_id: modelId,
          }
          this.userService.Payment(paymentToken).subscribe(
            response => {
              //console.log.log(response);
              if(response['success'] == '1'){
                alert("payment successfull");
                this.FollowModel(0,modelId,routeId);
                jQuery.noConflict();
                document.getElementById('closeModel').click();
                this.loading=false;

               // this.myForm.reset();
               //this.form.reset()
                
              }
              else{
                this.loading=false;
                this.message='payment failed please try again'
                alert("payment failed please try after sometime");
              }
             
            },
            err => {
             
            }
          )
         
        } else {
          alert(response['error']['code']);
          this.loading=false;
          this.message =response['error']['code']
        }
      });
    }
    );
  }


  submitSearch(){
    this.isLoading=true;
    const search={
      order:this.searchFilter,
    }
    this.userService.modelFilterFollowing(search).subscribe(
      response => {
        //console.log.log(response);
        if(response['success']==1){
          this.modelDataFollowing = response['data'];
          //console.log.log(response['data']);
          this.isLoading=false;
        }
        else{
          alert("No models Found");
          this.isLoading=false;
        }
      },
      err => {
       //console.log.log(err);
      }
    )
  }


  checkDigit() {
    // alert("rk")
     var allowedChars = "0123456789";
     var entryVal = (<HTMLInputElement>document.getElementById('txt_cardNumber')).value;
     //alert(entryVal.length);
     var flag;
     if(entryVal.length == 4 || entryVal.length == 9|| entryVal.length == 14){
       this.cardNumber = this.cardNumber+" ";
     }
     // for(var i=0; i<entryVal.length; i++){       
     //     flag = false;
 
     //     for(var j=0; j<allowedChars.length; j++){
     //         if(entryVal.charAt(i) == allowedChars.charAt(j)) {
     //             flag = true; 
     //         }
     //     }
 
     //     if(flag == false) { 
     //         entryVal = entryVal.replace(entryVal.charAt(i),""); i--; 
     //     }
     // }
 
     return true;
 }

 convertDateUtc(date,type){
  var dates = new Date(date * 1000) ;
  if(type == "weekday"){
    var dates = new Date(date * 1000) ;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

    return dates.getDate() +' '+ monthNames[dates.getMonth()];
  }
  else if(type == "day"){
    return dates.getDate()
  }
  else if(type == "start_time"){
    return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  }
  else if(type == "end_time"){
    return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  }
}

}
