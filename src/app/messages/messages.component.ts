import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, ViewChildren, QueryList, NgZone, ViewEncapsulation } from '@angular/core';
import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { interval, Subscription } from 'rxjs';
import { by } from 'protractor';
import { keyframes } from '@angular/animations';
import { isUndefined } from 'util';
import { timer } from 'rxjs';
import {DomSanitizer} from "@angular/platform-browser";
import { environment } from '../../environments/environment';
import {MessageService} from 'primeng/api';
// import { NavController,Content } from 'ionic-angular';
interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  providers: [MessageService],
  encapsulation: ViewEncapsulation.None,
})


export class MessagesComponent implements OnInit {

	//@ViewChild('scrollMe',{static: false}) private myScrollContainer: ElementRef;
	@ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
	@ViewChildren("scrollMe") messageContainers: QueryList<ElementRef>;
	@ViewChild('window',{static: false}) window;
	//@ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
	postsCol: AngularFirestoreCollection<Post>;
	posts: Observable<Post[]>;

	tsCol: AngularFirestoreCollection<Post>;
	sts: Observable<Post[]>;

	getChatount: AngularFirestoreCollection<Post>;
	countChatUser: Observable<Post[]>;

	getNotoficationCount: AngularFirestoreCollection<Post>;
	NotificationcountChat: Observable<Post[]>;
	//  readNot: AngularFirestoreCollection<Post>;
	//  notification: Observable<Post[]>;
	title:string;
	content:string ='';
	getProfileData:any =[];
	loading:boolean;
	isLoading:boolean = true;
	userID:any;
	modelId:string;
	modelImage:string;
	userImage:string;
	isModelChat:boolean = false;
	imageSrc: string | ArrayBuffer;
	files: any[];
	isUpload:boolean=false;
	type:string;
	messages:string;
	editmode:boolean=true;
	private formData = new FormData();
	chatCount:number;
	chatId:string;
	maxChat:number;
	totalChat:number;
	msgOver:boolean=true;
	nameMain:string;
	imageMain:string;
	selectedIndex: number;
	status: boolean = false;
	getCounter:Number;
	hightlightStatus: Array<boolean> = [];
	getchatData=[];
	nowTime:any;
	subscription: Subscription;
	modelNameToshow:string;
	modelImageToshow:string;
	imageFile:string;
	typing:boolean =false;
	myVar;

	chatIdString:string;
	chatReference:any;
	messageReference:any;
	arrayModelId:any=[];
	ChatToShow:any=[];
	postRefrenceArray:any=[];


	loggedInuser:any = localStorage.getItem('id');
	currentActiveModel:any ='undefined';
	timer1  = setTimeout(()=>{
		// this.stopTyping();
	}, 5000);
	typingStatus:boolean =false;
	liveModel:any=[];
	liveModelData:any=[];
	minChat:Number;
	uniqueId:any;
	messagePrice:any;
	formUrl:any =  this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton.php')
	device:any;
	AddActiveClass:any;
	constructor(private afs: AngularFirestore,private userService:UserService,private router: Router,private domSanitizer : DomSanitizer,private messageService: MessageService) {

	}

	ngOnInit() {
		this.nameMain = localStorage.getItem('name');
		console.log( "localStorage.getItem('name')", localStorage.getItem('profileImage'))
		if(localStorage.getItem('profileImage') != '0'){
			this.imageMain = localStorage.getItem('profileImage');
		}
		else{
			this.imageMain = 'assets/images/dummy-profile.png'
		}
		//this.imageMain = localStorage.getItem('profileImage');
		this.userID = localStorage.getItem('id');
	
		if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
		this.router.navigate(['/model-account']);
		}
		else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
		this.router.navigate(['/model-account']);
	
		}
		else if(localStorage.getItem('loggedInUser') == null ){
		this.router.navigate(['/login']);
		}

		this.getallChat();
	}

	getLiveModel(){
		firebase.database().ref('/status/');
		const that = this;
		firebase.database().ref('/status/').on('value', function(snapshot) {
		  snapshot.forEach(function(childSnapshot) {
			var childData = childSnapshot.val();
			if(childData['state'] == 'online' ){
			  that.liveModel.push(childData['id']);
			}
			else{
				var index = that.liveModel.indexOf(childData['id']);
				if (index > -1) {
					that.liveModel.splice(index, 1);
				 }
			}
		  });
		  that.liveModelData = that.getUnique(that.liveModel);
		  for(var i =0 ; i<that.ChatToShow.length ; i++){
			if(that.include(that.getUnique(that.liveModelData),that.ChatToShow[i]['id'])){
			  that.ChatToShow[i]['live'] = 'yes';
			}
			else{
				that.ChatToShow[i]['live'] = 'no';
			}
		  }
	  });
	  
	  }

	  include(arr, obj) {
		for(var i=0; i<arr.length; i++) {
			if (arr[i] == obj) return true;
		}
	}  

	getUnique(array){
		var uniqueArray = [];
		for(var i=0; i < array.length; i++){
			if(uniqueArray.indexOf(array[i]) === -1) {
				uniqueArray.push(array[i]);
			}
		}
		return uniqueArray;
	}

	getLiveChatStatus(){
		const that = this;
		this.postsCol = this.afs.collection('posts', ref => ref.where('userId', '==', Number(this.userID)));
		//that.ChatToShow = []; 
		this.posts = this.postsCol.valueChanges();
		this.posts.subscribe(res => {
			res.forEach(element1 => {
				var exists = false;
				for (var i=0; i<this.ChatToShow.length; i++) {
				var element2 = this.ChatToShow[i];
				if (element1['chatId'] == element2['chatId']) {
					this.ChatToShow[i]['lastMessage'] = element1['lastMessage'];
					this.ChatToShow[i]['time'] = element1['time'];
					this.ChatToShow[i]['unreadCount'] = element1['unreadCount'];
					this.ChatToShow[i]['unreadCount'] = element1['unreadCount'];
					this.ChatToShow[i]['typingStatus'] = element1['typingStatus'];
					//this.typingStatus = element1['typingStatus'];
					exists = true;
					//////////console.log.log.log.log.log("this.currentActiveModel",this.currentActiveModel)
					//if(this.currentActiveModel)
					if(this.currentActiveModel != 'undefined'){
						////////console.log.log.log.log.log("type",element1['typingStatus']);
						////////console.log.log.log.log.log(this.currentActiveModel)
						//this.typingStatus = element1['typingStatus'][this.currentActiveModel]
						if(element1['modelId'] == this.currentActiveModel){
							this.typingStatus = element1['typingStatus'][this.currentActiveModel];
						}
						////////console.log.log.log.log.log('his.typingStatus',this.typingStatus)
					}
					// else{
					// 	////////console.log.log.log.log.log("this.currentActiveModel else",this.currentActiveModel)
					// }
					
					break;
				}
				}
				if (exists == false) {
					this.getModelData([element1['modelId']]);	
					this.ChatToShow.push(element1)
				}
				else{
					this.isLoading = false
				}
			});
			this.ChatToShow.sort(function (a, b) {
				return b['time'].seconds - a['time'].seconds;
			});
		})
	}

	sendUnreadCount(modelId,UserID,docId){
		var pinDocRef = this.chatReference.ref;
		return firebase.firestore().runTransaction(function(transaction) {
			return transaction.get(pinDocRef).then(function(pinDoc) {
				if(!pinDoc.exists){
					return false;
				}
				var unreadCount = pinDoc.data().unreadCount;
				for (var key in unreadCount) {
					if (key == localStorage.getItem('id')) {
						unreadCount[key] = 0;
					}
					else {
						unreadCount[key] = Number(pinDoc.data().unreadCount[key]) + 1;
					}
				}

				transaction.update(pinDocRef, { 'unreadCount': unreadCount});
			});
		  }).then(function() {
			  // ////////console.log.log.log.log.log("Transaction successfully committed!");
		  }).catch(function(err) {
			  // ////////console.log.log.log.log.log("Transaction failed: ", err);
		  });
	}


	setUnreadCountZeroForReceiver(){
		var pinDocRef = this.chatReference.ref;
		return firebase.firestore().runTransaction(function(transaction) {
			return transaction.get(pinDocRef).then(function(pinDoc) {
				if(!pinDoc.exists){
					return false;
				}
				var unreadCount = pinDoc.data().unreadCount;
				unreadCount[localStorage.getItem('id')] = 0;
				transaction.update(pinDocRef, { 'unreadCount': unreadCount});
			});
		  }).then(function() {
			  // ////////console.log.log.log.log.log("Transaction successfully committed!");
		  }).catch(function(err) {
			  // ////////console.log.log.log.log.log("Transaction failed: ", err);
		  });
	}



	
  getallChat(){
		this.arrayModelId= [];
		var afs2 = this.afs;
		var that = this;
		this.afs.collection("posts", ref => ref.where('userId', '==', Number(this.userID)).orderBy('time','desc'))
		.get().toPromise().then(function(querySnapshot) {
			const sizeOfarray = querySnapshot.size;
			querySnapshot.forEach(function(doc) {
				that.ChatToShow.push(doc.data());
				that.arrayModelId.push(doc.data().modelId);
				if(sizeOfarray == that.arrayModelId.length){
					that.getModelData(that.arrayModelId);
				}
			});
			that.arrayModelId.forEach(element => {
				var modelId = element;
				var userId = localStorage.getItem('id');
				var chatId = userId + '_' + modelId;
				if (modelId < userId) {
					chatId = modelId + '_' + userId;
				}

				//////////console.log.log.log.log.log("chatId", chatId);

				var ref = that.afs.collection('posts').doc(chatId).collection("messsages");
				var listener = ref.valueChanges();
				listener.subscribe(res => {
					//////////console.log.log.log.log.log("newdfsdf",res);
					if (res.length > 0) {
					var aMsg = res[0];
					var mId = aMsg['modelId'];
					var uId = aMsg['userID'];
					var cId = uId + '_' + mId;
					if (modelId < uId) {
						cId = mId + '_' + uId;
					}
					//////////console.log.log.log.log.log('chatId', cId);

					res.sort(function (a, b) {
						return a['time'].seconds - b['time'].seconds;
					});
					that.getchatData = res;

					}

				})
				that.postRefrenceArray.push(listener);
			});
		})
		this.isLoading = false
  }

  getModelData(data){
    this.userService.getModelForChat(data).subscribe(
      response => {
      //  ////////console.log.log.log.log.log(response);
      //  ////////console.log.log.log.log.log(this.ChatToShow)
        for(var i =0;i<response['data'].length;i++){
			for(var j =0;j<this.ChatToShow.length;j++){
				if(this.ChatToShow[j]['modelId'] == response['data'][i]['id']){
					this.ChatToShow[j]['name'] =  response['data'][i]['name']
					this.ChatToShow[j]['profilePic'] =  response['data'][i]['profileImage']
					this.ChatToShow[j]['id'] =  response['data'][i]['id']
					this.ChatToShow[j]['userId'] =  localStorage.getItem('id');
					break;
				}
			}
        }  
        this.ChatToShow.sort(function (a, b) {
          return b['time'].seconds - a['time'].seconds;
		});
		this.getLiveModel();
		////////console.log.log.log.log.log(this.ChatToShow)
        // ////////console.log.log.log.log.log("sdfsd",this.ChatToShow);
		this.getLiveChatStatus();
		this.isLoading =false;
      },
      err => {
		this.isLoading =false;
        //////////console.log.log.log.log.log(err);
      }
    );
  }


  addPost() {
   this.editmode=false;  
   //////////console.log.log.log.log.log(this.content);return;
    if(this.isUpload){
      this.type="photo";
      const api = {
        api_token:localStorage.getItem("loggedInUser")
       }
       this.userService.uploadChatPic(this.formData).subscribe(
         response => {
         // ////////console.log.log.log.log.log(response);
         
          if(response['success']==1){
           // ////////console.log.log.log.log.log(response);
             this.content =response['data'];
           
             this.getDataChat();
          }
          else{
			
          }
           
         },
         err => {
			this.messageService.add({severity:'error', summary: 'error', detail:'Image not valid'});
			this.content="";
			this.editmode=true;
			this.loading=false;
			//this.messageService.add({severity:'error', summary: 'error', detail:'Image not valid'});
           //this.router.navigate(['/login']);
          // ////////console.log.log.log.log.log(err);
         }
       );
     }
     else{
      
      if(this.content.trim() == "" || this.content == null  || this.content == undefined || this.content == " " ){
        this.editmode=true;  
        return false;   
     }
      this.type="text";
      this.getDataChat();
     }
   
  }
  onConfirm() {
	this.messageService.clear('c');
  }
  
  onReject() {
	this.messageService.clear('c');
  }
  getDataChat(){

	
    this.imageFile="";
    //  ÷(this.maxChat);
		// alert(this.totalChat);
		
	if(Number(this.maxChat) <= Number(this.minChat)){
		alert("sorry this message can't be sent as your transaction is over recharge now to get or post message");
		this.msgOver = false;
		this.editmode=true;
		return false;
		
	}
	else{
		this.msgOver = true;
		this.editmode=false;
	}
	this.minChat =  Number(this.minChat) + 1
	this.afs.collection('message_counter').doc(this.chatIdString).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":this.maxChat,"minChat":Number(this.minChat)}).then(function(){
		console.log("Added")
	});
    
    this.isUpload=false;
	this.loading=true;
	
	var timestamp = firebase.firestore.Timestamp.now();

	//var milliseconds = (new Date).getTime();
	
    this.messageReference.add({'message': this.content, 'sender': this.userID,'userID': this.userID, 'modelId': Number(this.modelId),'time':timestamp,'type':this.type,'read':false});
    var chatId = this.userID +'_'+ this.modelId;
    if(this.userID > this.modelId){
       var chatId = this.modelId +'_'+ this.userID;
	}

    // this.afs.collection('posts').add({'chatId': chatId,'lastMessage':this.content, 'lastMessageTime':firebase.firestore.FieldValue.serverTimestamp(),'modelId':Number(this.modelId),'userId':this.userID});
	const that =this;
	if(this.type=="photo"){
		this.content = "@@@@@@@";
	}
	this.chatReference.update({'lastMessage':this.content,'time':timestamp}).then(function() {
		that.sendUnreadCount(Number(that.modelId),Number(that.userID),chatId);
	}).catch(function(error) {
		if (error.code == 'not-found') {
			// Set because update is not available
			var unreadCount = [];
			if(localStorage.getItem('id') == that.userID) {
				unreadCount[String(that.userID)] = 0;
				unreadCount[String(that.modelId)] = 1;
			}
			else{
				unreadCount[String(that.userID)] = 1;
				unreadCount[String(that.modelId)] = 0;
			}

			var typingStatus = [];
			typingStatus[String(that.userID)] = false;
			typingStatus[String(that.modelId)] = false;

			that.chatReference.set({'chatId': chatId,'lastMessage':that.content,'time':timestamp,'modelId':Number(that.modelId),'userId':Number(that.userID), 'unreadCount':unreadCount, 'typingStatus':typingStatus}).then(function(){
				that.sendUnreadCount(Number(that.modelId),Number(that.userID),chatId);
			});
		}
		else {
			// Unknown error
			
		}
	});

	//this.afs.collection('posts').doc(chatId).set({'chatId': chatId,'lastMessage':this.content,'time':timestamp,'modelId':Number(this.modelId),'userId':Number(this.userID)});
  

	



    ///to post chat count of unread message
    const afs3 =  this.afs;
    const USERID =this.userID
    const MODELID =this.modelId;

	
  this.getchatData.push({'message': this.content,'sender': this.userID,'userID': this.userID,'modelId': Number(this.modelId),'time':timestamp,'type':this.type,'read':true})
    
  	
    if(this.type =="photo"){
      this.content="Image";
    }
    else{
      this.content=this.content;
    }
      this.content="";

    this.editmode=true;
    this.loading=false;
  }


  postNotiStatus(USERRID,MODELID){
    const afs2 =  this.afs;
     this.afs.collection("chatStatus", ref => ref.where('userID', '==', Number(USERRID)).where('modelId', '==', Number(MODELID)))
        .get().toPromise().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
              afs2.collection("chatStatus").doc(doc.id).update({'unreadUser':0}).then(function() {
              })
              .catch(function(error) {
///////////console.log.log.log.log.error("Error writing document: ", error);
              });
          });
        })

        this.afs.collection("posts", ref => ref.where('userID', '==', Number(USERRID)).where('modelId', '==', Number(MODELID)).where('sender', '==', 'model'))
        .get().toPromise().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
              afs2.collection("posts").doc(doc.id).update({'read':true}).then(function() {
              })
              .catch(function(error) {
                //////////console.log.log.log.log.error("Error writing document: ", error);
              });
          });
        })  
  }



  getChat(){
   
    const api = {
      api_token:localStorage.getItem("loggedInUser")
     }
     this.userService.getMsgChat(api).subscribe(
       response => {
        if(response['success']==1){
          this.getProfileData =response['data'];
          this.isLoading=false;
        }
        else{
          this.isLoading=false;
        }
         
       },
       err => {
         this.router.navigate(['/login']);
       }
     );
  }


  modelChat(model_id:string,user_id,model_image,i,modelNAME,device){
	this.AddActiveClass = i
	if(device == 'phone'){
		document.getElementById('phoneViewList').style.display ='none';
		//document.getElementById('phoneViewData').style.display = 'block';
		
	}
	this.currentActiveModel = model_id;


    if ((typeof this.chatIdString != 'undefined') ) {
		//////////console.log.log.log.log.log("123")
	  var countId = 'unreadCount.' + user_id;
	  this.afs.collection('posts').doc(this.chatIdString).update({[countId]: 0});
		if(device == 'phone'){
			document.getElementById('phoneViewData').style.display = 'block';
		}
	}
	else{
		//////////console.log.log.log.log.log("321")
	}
	

	this.device = device
    this.modelNameToshow= modelNAME
    this.modelImageToshow= model_image;
    this.getchatData = [];

    if(Number(user_id)>Number(model_id)){
      this.chatIdString = model_id+'_'+user_id;
    }
    else{
      this.chatIdString = user_id+'_'+model_id;
	}
	


    this.chatReference =  this.afs.collection('posts').doc(this.chatIdString);
    this.messageReference = this.chatReference.collection('messsages');


    this.modelId = model_id
    this.postNotiStatus(user_id,model_id)
    this.status = !this.status;    
    var userId= localStorage.getItem("id");
    const afs2 =  this.afs;
    var maxCha;
    
    const that =this

  
    this.userID = user_id;
    this.modelId = model_id;
    this.modelImage = model_image;
    this.userImage =  localStorage.getItem("profileImage");
                                               

    const thats  = this;  
    this.messageReference.get().toPromise().then(function(querySnapshot) {
    //const sizeOfarray = querySnapshot.size;
    //////////console.log.log.log.log.log("sdsdfsdff",querySnapshot.size)
     
    querySnapshot.forEach(function(doc) {
      //////////console.log.log.log.log.log(doc.data());
      thats.getchatData.push(doc.data());

      });
      thats.getchatData.sort(function (a, b) {
        return a['time'].seconds - b['time'].seconds;
      });
    })
    this.isModelChat=true;
   
	this.messageCounter(this.chatIdString)
	this.setUnreadCountZeroForReceiver();
  }
  
  
  
  backMobile(){
	  document.getElementById('phoneViewList').style.display ='block';
	  document.getElementById('phoneViewData').style.display = 'none';
  }
  
  
  
  
  readURL(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        this.files = event.target.files;
        this.formData.append("file", event.target.files[0]);
      //  ////////console.log.log.log.log.log(event.target.files)
        reader.readAsDataURL(file);
        this.isUpload=true; 
      } 
  }

  
  
  
  
  
  
  
  
  
  
  convertTime(time){

    var a = new Date(parseInt(time) * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes().toString();
    if(min.toString().length ==1){
      var min = '0'+min
    }
    var sec = a.getSeconds();
    var times = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min ;
    return times;
  }


  
  
  
  
  
  
//   getMessagePrice(){
//     document.getElementById('showModel').click();
//   }

  
  
  
  
  
  
  
	
	rechargeMessage(price){
		this.messagePrice =price;
		this.uniqueId = (new Date()).getTime();
		this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton4.php?model_id='+this.currentActiveModel+'&user_id='+localStorage.getItem('id')+'&price='+price+'.00&uniqueId='+this.uniqueId);
		//document.getElementById('modalClose').click();
		document.getElementById('paymentPopup').click();

		
		}

	modalClose(){
		document.getElementById('modalCloseClick').click();
		this.modelChat(this.currentActiveModel,localStorage.getItem('id'),this.modelImageToshow,1,this.modelNameToshow,this.device)
	}

	valuechange(): void { 
		clearTimeout(this.timer1)
		if(this.typing == false) {
			this.typing = true;
			var typingStatus = 'typingStatus.' + this.userID;
			this.chatReference.update({[typingStatus]: true});
		}
		this.timer1  = setTimeout(()=>{
			this.stopTyping();
		}, 3000); 
	}
  
	stopTyping() {
		this.typing = false; 
		var typingStatus = 'typingStatus.' + this.userID;
		this.chatReference.update({[typingStatus]: false});
	}

	messageCounter(chat_id){
		var docRef = this.afs.collection("message_counter").doc(chat_id);
		const  that =this;
		docRef.get().toPromise().then(function(doc) {
			if (doc.exists) {
				console.log("Document data:", doc.data());
				that.userService.GetMsgCount({'model_id':that.currentActiveModel}).subscribe(
					response => {
						// console.log(response)
						if(response['success'] == 1){
							that.msgOver = true
							if(Number(response['data'][0]['max_chat']) >  Number(doc.data().maxChat)){
								that.afs.collection("message_counter").doc(chat_id).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":Number(response['data'][0]['max_chat']),"minChat":doc.data().minChat});
								that.maxChat = Number(response['data'][0]['max_chat']) 
								that.minChat = doc.data().minChat
								that.editmode=true;
								that.msgOver = true
								//console.log("daaa");
								return false;
								//alert(that.maxChat)
							}
							
							else{
								const data ={
									model_id:that.currentActiveModel,
									max_chat:doc.data().maxChat
								}
								console.log(data)
								that.userService.updateMsgCount(data).subscribe(
									response => {
										console.log(response)
									},
									err => {
				
									}
								);
								that.maxChat = doc.data().maxChat;
								that.minChat = doc.data().minChat;
							}

							if(Number(doc.data().minChat) ==  Number(doc.data().maxChat)){
								that.msgOver = false;
								//that.editmode=true;
								return false;
							}
							else{
								that.msgOver = true;
								//that.editmode=false;
							}
						}
						else{
							const data ={
								model_id:that.currentActiveModel,
								max_chat:doc.data().maxChat
							}
							console.log(data)
							that.userService.updateMsgCount(data).subscribe(
								response => {
									console.log(response)
								},
								err => {
			
								}
							);
						}
						
					},
					err => {

					}
				);
			} else {
				//alert("123")
				that.editmode=true;
				// doc.data() will be undefined in this case
				const model_id = {
					model_id:that.currentActiveModel
				}			
				that.userService.GetMsgCount(model_id).subscribe(
					response => {
						console.log(response)
						that.maxChat =0;
						that.minChat = 0;
						if(response['success'] == 0){
							that.afs.collection("message_counter").doc(chat_id).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":0,"minChat":0});
						}
						else{
							that.afs.collection("message_counter").doc(chat_id).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":0,"minChat":0});
						}
					},
					err => {

					}
				);
			}
		}).catch(function(error) {
			console.log("Error getting document:", error);
		});
	  }

	  messageLeft(max,min){
		return parseInt(max) - parseInt(min)
	  }




}
