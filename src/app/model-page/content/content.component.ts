import { Component, OnInit,ViewEncapsulation, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/http.service';
//import { DataService } from 'src/app/data.service';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { HttpClient, HttpEvent, HttpEventType, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Http} from '@angular/http';
import * as $AB from 'jquery';
import * as bootstrap from 'bootstrap';
import { DataService } from "../../data.service";
import {CalendarModule} from 'primeng/calendar';
import {MessageService} from 'primeng/api';
//declare var $ : any;
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css',],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
  // // "../../../../../node_modules/primeicons/primeicons.css",
  // "../../../../node_modules/primeng/resources/themes/nova-light/theme.css",
  // "../../../../node_modules/primeng/resources/primeng.min.css",]
})
export class ContentComponent implements OnInit ,AfterViewInit{
@ViewChild('myCanvas',{static:false}) canvas:ElementRef;
@ViewChild('fileUploader', {static: false}) fileUploader:ElementRef;
myModelContent:any =[];
title:string;
uploadAs:string;
type:string;
selectedFile: any = [];
uploadedPercentage = 0;
showMessage = false;
message: string = '';
showTo:string ='1' ;
noOfUpload:string = '0';
uploadFile:string;
rootUrl: string;
editmode:boolean=true;
loading:boolean;
myError:any =[];
messages:string;
date7:string;
date8=new Date();
date9:string;
showToMain:string ='1';
dateTime = new Date();
video:SafeResourceUrl;
modelStats:any=[]
profileUrlData:any;
imgsrc:any;

passwordValidation: any =[];
newPassword :string;
oldPassword :string;
confirmCheck:string;
private formDataReset = new FormData();
loading2:boolean;
activateDeactivate:string;


payOutValidation:any=[];
NamePayout:string;
routingNo:string;
accountNumber:string;
accountType:string;
swiftCode:string;
IBAN:string;
selectedCountry:any=[];
country: any=[];
isLoading:boolean =true;
model_ID :any
selectedValues:any =['1'];
index2:any;
currentEmail:string;
PostNow:any =['1'];
timeSelected:any;
counter:any;
value: number = 0;
totalSize:any = 0;
uploadedValue:any = 0;
loaded:any = [];
hideprogress:boolean=false;
uploadCounter:any = 0;
mid = localStorage.getItem('profileUrl');
  constructor(public sanitizer:DomSanitizer,private data: DataService, private router: Router,
    private userService:UserService,private slimLoadingBarService: SlimLoadingBarService, 
    private http: HttpClient,private https : Http,private messageService: MessageService) { }



  ngAfterViewInit() {
    //console.log('after')
   
  }
  ngOnInit() {
    //this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
   //console.log('this.selectedValues',this.selectedValues)
    this.data.currentMessage.subscribe(message => this.messages = message);
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }

    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
    ]; 
    
    this.getModelStats();
    this.getModelMedia();
    this.uploadAs='1';
    
    this.type="image/png, image/jpeg";
    //alert(this.noOfUpload)
    this.dateTime.setDate(this.dateTime.getDate() );    
    this.date8.setDate(this.date8.getDate())

    this.profileUrlData = localStorage.getItem('profileUrl')
    
    this.model_ID = localStorage.getItem('id')
  }

  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }
  


  // onFileSelected(event) {
  //   const number = event.target.files.length;
  //   this.noOfUpload = event.target.files.length;
  //   for(var i=0;i<number;i++){
  //     this.selectedFile[i] = <File>event.target.files[i];
  //     var file = event.target.files[i];
  //     //console.log(file);
  //     this.message = "";
  //     var fileReader = new FileReader();
  //   }
  //   //console.log(this.selectedFile);  
  // }

  getModelMedia(){
    this.userService.getModelData().subscribe(
      response => {
       if(response['success']==1){
         console.log(response['data']);

        this.myModelContent = response['data'];
        // console.log(this.myModelContent)
        if(this.myModelContent){
          setInterval(()=>{    //<<<---    using ()=> syntax
            this.getImage();
       }, 4000);
          // setTimeout(,100000);
        }
        this.isLoading =false;
       }
       else{
        this.isLoading =false;
       // console.log(response);
       }
        
      },
      err => {
        this.isLoading =false;
        //this.router.navigate(['/logout']);
       // console.log(err);
      }
      
    );
   
  }

  getModelStats(){
    this.userService.getModelStats().subscribe(
      response => {
       if(response['success']==1){
        this.modelStats=response['data'][0]; 
        // console.log( this.modelStats)
       }
       else{
       // console.log(response);
       }
        
      },
      err => {
        //this.router.navigate(['/logout']);
        console.log(err);
      }
      
    );
  }

getImage(): any{
  //console.log("ra")
  for(var i=0; i<this.myModelContent.length;i++){
    //console.log(this.myModelContent)
    if(this.myModelContent[i]['mediaType'] == 2 || this.myModelContent[i]['mediaType'] == 5){
      //console.log(i)
        var video = document.getElementById('videoId'+i) as HTMLCanvasElement;var a  = 400;var b = 400;
        var canvas = document.getElementById('canvas'+i) as HTMLCanvasElement;
        canvas.getContext('2d').drawImage(video,2,3);
   
    }
  }
}
  changeUploadType(value){
    //alert(value);
    if(value==5){
      this.type="video/*";
    }
    else if(value==2){
      this.type="video/*";
    }
    else if(value==1){
      this.type="image/png, image/jpeg";
    }
  }
  // submitMediaUpload(time){
  //   //alert(this.date7);
  //   // if(this.title =='' || this.title == undefined || this.title == 'undefined'){
  //   //     this.myError['title']="Title Cannot Be Blank";
  //   //     return false;
  //   // }
  //   // else{
  //   //   this.myError['title']="";
  //   // }
  //   // console.log(time);
  //   //  return;
  //   if(time == 'time'){
  //     if(this.date7 =='' || this.date7 == undefined || this.date7 == 'undefined'){
  //       this.myError['date']="Date Cannot Be Blank";
  //       return false;
  //     }
  //     else{
  //       this.myError['date']="";
  //     }
  //   }
    
    

  // // console.log(this.selectedFile);
  // // console.log(this.selectedFile.length)
  // if(this.selectedFile.length==0){
  //  // console.log('here');
  //   this.showMessage = true;
  //   this.message = "Please Select Media to Upload";
  //   return false;
  // }
  // else{
  //   this.showMessage = false;
  //   //console.log('there');
  //   this.message = "";
  // }
  //   //return false;
  //   this.editmode=false;
  //   this.loading=true;
  //   const fd = new FormData();
  //   this.showMessage = false;
  //   //alert(this.showTo)
  //   //console.log(this.selectedFile.length);
  //   var j=0;
  //   for(var i=0; i<this.selectedFile.length; i++){
  //     console.log(this.selectedFile[i].size);
  //     // break;
  //     // return false;
  //     if(this.selectedFile[i].size > 3230000000){
  //       console.log("this.selectedFile[i].size",this.selectedFile[i].size);
  //       j=1;
  //       break;
  //     }
  //     // console.log(this.selectedFile[i])
  //     // console.log(this.selectedFile[i].name)
  //     fd.append('file[]', this.selectedFile[i], this.selectedFile[i].name);
  //   }
  //   //return false;
  //   if(j == 1){
  //     this.message = "file size cannot be larger than 3 gb";
  //     this.loading = false;
  //     this.editmode=true;
  //     this.showMessage = true;
  //     this.selectedFile = []
  //     return false;
  //   }
  //   fd.append('title', this.title);
  //   fd.append('type', this.uploadAs);
  //   fd.append('showTo', this.showTo);
  //   if(time == 'time'){
  //     fd.append('showOn', new Date(this.formatDate(this.date7)).toUTCString());
  //   }
  //   else{
  //     fd.append('showOn', new Date(this.formatDate(new Date())).toUTCString());
  //   }
    

  //   // this.date7 = new Date();
  //   //console.log(new Date(this.formatDate(this.date7)).toUTCString());return;
  //   //alert(this.showTo);
  //   //fd.append('file', this.selectedFile, this.selectedFile.name);
  //   const api_token = localStorage.getItem('loggedInUser')
  //   let headers = new HttpHeaders({
  //     "Accept": "multipart/form-data" ,
  //     'Authorization': 'Bearer '+api_token 
    
  //   });
  //     let options = { headers: headers};
  //     this.rootUrl = "https://enacteservices.net/uploader/fileuploader.php"
  //   this.http.post(this.rootUrl, fd,{reportProgress: true, observe: "events",headers: new HttpHeaders(
  //     { "Accept": "multipart/form-data"  },
  //   )}).subscribe( (event: HttpEvent<any>) => {
  //     switch (event.type) {
  //       case HttpEventType.Sent:
  //         this.slimLoadingBarService.start();
  //         break;
  //       case HttpEventType.Response:
  //         this.slimLoadingBarService.complete();
  //         this.message = "Uploaded Successfully";
  //         this.showMessage = true;

  //         console.log("event",event.body.data);
  //         for(var i = 0 ;i<event.body.data.length;i++){
  //           fd.append('file[]', event.body.data[i]);
  //         }
  //         fd.append('uploader', "1");
  //         console.log(fd)
  //         this.userService.UploadContent(fd).subscribe(
  //           response => {
  //            console.log(response);
  //            if(response['success']==1){
  //                 this.myModelContent = [];
  //                 this.getModelMedia();
  //                 if(this.myModelContent){
  //                   setTimeout(()=>{    //<<<---    using ()=> syntax
  //                     this.getImage();
  //               }, 3000);
  //                   // setTimeout(,100000);
  //                 }
  //                 this.title='';
  //                 this.uploadAs='1';
  //                 this.selectedFile=[];
  //                 this.showTo='1';
  //                 this.noOfUpload='0';
  //                 this.uploadFile=undefined;
  //                 this.message="";
  //                 this.date7 ="";
  //                 this.type="image/png, image/jpeg";
  //                 document.getElementById("model-closeContent").click();
  //                 this.editmode=true;
  //                 this.loading=false;
  //            }
  //            else{
  //             this.title='';
  //             this.uploadAs='1';
  //             this.selectedFile=[];
  //             this.showTo='1';
  //             this.noOfUpload='0';
  //             this.uploadFile=undefined;
  //             this.message="";
  //             this.date7 ="";
  //             this.type="image/png, image/jpeg";
  //             document.getElementById("model-closeContent").click();
  //             this.editmode=true;
  //             this.loading=false;
  //             alert("Something Went Wrong Please Contact Admin");
  //            }
              
  //           },
  //           err => {
  //             this.title='';
  //             this.uploadAs='1';
  //             this.selectedFile=[];
  //             this.showTo='1';
  //             this.noOfUpload='0';
  //             this.uploadFile=undefined;
  //             this.message="";
  //             this.date7 ="";
  //             this.type="image/png, image/jpeg";
  //             document.getElementById("model-closeContent").click();
  //             this.editmode=true;
  //             this.loading=false;
  //             //this.router.navigate(['/logout']);
  //             console.log(err);
  //           }
  //         );
  //         // event.body.type = 
  //         //console.log(event.body.data[0]);
  //        //this.myModelContent = event.body.data[0];
  //       //  this.myModelContent = [];
  //       //   this.getModelMedia();
  //       //   if(this.myModelContent){
  //       //     setTimeout(()=>{    //<<<---    using ()=> syntax
  //       //       this.getImage();
  //       //  }, 3000);
  //       //     // setTimeout(,100000);
  //       //   
  //         break;
  //       case 1: {
  //         if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)){
  //           this.uploadedPercentage = event['loaded'] / event['total'] * 100;
  //           this.slimLoadingBarService.progress = Math.round(this.uploadedPercentage);
  //         }
  //         break;
  //       }
  //     }
  //   },
    
  //   error => {
  //     console.log(error);
  //     alert(error.message)
  //     if(typeof error.error != "undefined"){
  //       alert(error.error.error)
  //       this.message = "Error: "+error.error.error;
  //     }
  //     else{
  //       this.message = "Something went wrong";
  //     }
  //     this.showMessage = true;
  //     this.slimLoadingBarService.reset();
  //     this.loading = false;
  //     this.editmode=true;
  //     this.showMessage = true;
  //     this.selectedFile = []
  //     return false;
  //   }
  //   );
  // }
  
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/')+' ' +
                [d.getHours(),
                d.getMinutes(),
                d.getSeconds()].join(':');
  }

  
  ChackValue(){
    if(this.title =='' || this.title == undefined || this.title == 'undefined'){
      //this.myError['title']="Title Cannot Be Blank";
      //return false;
    }
    else{
      //this.myError['title']="";
    }
  }

  
  // $('#add-content').on('hidden.bs.modal', function (e) {
  //   // do something...
  // })
  showModal(){
    this.title='';
    this.uploadAs='1';
    this.selectedFile=[];
    this.showTo='1';
    this.noOfUpload='0';
    this.uploadFile=undefined;
    this.message="";
    this.date7 ="";
    this.type="image/png, image/jpeg";
    this.myError['date']="";
    this.loading = false;
    this.value = 0;
    this.hideprogress = false;
    this.uploadCounter = 0
    // this.selectedFile = [];
    //$('#add-content').modal('show');
  }


  selectedData(event: any,id) {
   // alert(id);
    const ids ={
      id:id,
      showTo:event.target.value
    }
    this.userService.ChangePostStatus(ids).subscribe(
      response => {
       console.log(response);
       if(response['success']==1){
        this.messageService.add({severity:'success', detail:'Done'});

         //alert("done")
       }
       else{
        alert("Something Went Wrong Please Contact Admin");
       }
        
      },
      err => {
        //this.router.navigate(['/logout']);
        console.log(err);
      }
    );
   }

   deletePost(id,i){
   // alert(i)
    console.log(this.myModelContent);
    

    const ids ={
      id:id,
    }
    this.userService.DeletePost(ids).subscribe(
      response => {
       console.log(response);
       if(response['success']==1){
        
        this.myModelContent.splice(i,1);
        this.messageService.add({severity:'success', summary: 'success', detail:'post deleted successfully'});

       }
       else{
        alert("Something Went Wrong Please Contact Admin");
       }
        
      },
      err => {
        //this.router.navigate(['/logout']);
        console.log(err);
      }
    );
   }

 
   
   mediaShow(urls){
    //  this.video=urls;
    //  console.log(this.video)
    this.video = this.sanitizer.bypassSecurityTrustResourceUrl(urls);  
    document.getElementById('showModalVideo').click();
   }

 

   open(index: number,image,title): void {
    //alert("2")
    this.index2 = index
    this.imgsrc = image;
    console.log(this.imgsrc)
    var modal = document.getElementById("myModalImage");
    modal.style.display = "block";
    var captionText = document.getElementById("caption");
    captionText.innerHTML=title;
  }
  changeSlideShow(type){
		var length = this.myModelContent.length;
		if(type == 'right'){
			//alert(this.index2)
        if(length - 1   == this.index2){

        }
        else{
          this.index2  = parseInt(this.index2) + 1;
        }
        this.imgsrc =this.myModelContent[this.index2]['media'];
        var captionText = document.getElementById("caption");
        captionText.innerHTML =this.myModelContent[this.index2]['title'];
    }
    else{
        //alert(this.index2)
        if(this.index2 == 0){
          this.index2 = length - 1
        }
        else{
          this.index2  = parseInt(this.index2) - 1;
        }
        this.imgsrc =this.myModelContent[this.index2]['media'];
        var captionText = document.getElementById("caption");
        captionText.innerHTML =this.myModelContent[this.index2]['title'];
		}
  }
  closeModel(){
    var modal = document.getElementById("myModalImage");
    modal.style.display = "none";
  }


  hideErrors(){
   // alert("r")
    document.getElementById('myModalPasswordResetButton').click();
    //document.getElementById('oldPasswordField').innerHtml = "";
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      //console.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      //console.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      //console.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation['result'] = "Old Password does not matched";
          this.loading2=false;
        }
        if(response['success']==2){
          this.passwordValidation['result'] = "Some error occured please try after sometime";
          this.loading2=false;
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          console.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      console.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopupContent').click();
    document.getElementById('model-close2Content').click();
   
    //this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      console.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        // document.getElementById('closeDeactivatePopup').click();
        // document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

  delete(){
    this.activateDeactivate = '2';
  }

  onSelect(){
    ///console.log(value)
    //console.log(this.date8)
  }

  onSelect2(){
      this.selectedValues =[''];
      var fromDate = 	 new Date(this.formatDate(this.date8)).toUTCString();
      var toDate = 	 new Date(this.formatDate(this.date9)).toUTCString();
      const date ={
        fromDate:fromDate,
        toDate:toDate
      }

      this.userService.getModelStatsFilter(date).subscribe(
        response => {
          if(response['data'][0]){
            this.modelStats=response['data'][0]; 
            if(this.modelStats['price'] == null ){
              this.modelStats['price'] = 0
            }
          }
          else{
            this.modelStats = []
            if(this.modelStats['price'] == null ){
              this.modelStats['price'] = 0
            }
          }
          
        },
        err => {
      
        }
      );
    
  }

  getAll(){
    if( this.selectedValues.length > 0){
      this.getModelStats();
      this.date8=new Date();
      this.date9=""

    }
  }

  convertDateUtc(date,type){
		var dates = new Date(date * 1000) ;
		if(type == "weekday"){
			var dates = new Date(date * 1000) ;
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
								"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
							];

			return dates.getDate() +' '+ monthNames[dates.getMonth()];
		}
		else if(type == "day"){
			return dates.getDate()
		}
		else if(type == "start_time"){
			return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "end_time"){
			return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
	}

  


// uploadFile:string;
// 	selectedFile: any = [];
// 	noOfUpload:string = '0';
	
	onFileSelected(event) {
		var fileTypes = ["image/jpeg", "image/jpg", "image/png", "video/x-flv", "video/mp4", "video/mov", "video/avi", "video/mkv", "application/x-mpegURL", "video/MP2T", "video/3gpp", "video/quicktime", "video/x-msvideo", "video/x-ms-wmv"];
		const number = event.target.files.length;
		this.noOfUpload = event.target.files.length;
		for(var i=0;i<number;i++){
			this.selectedFile[i] = <File>event.target.files[i];

			var file = event.target.files[i];
			var fileReader = new FileReader();

			var mimeType = this.selectedFile[i].type;
			console.log(mimeType);
			var isSuccess = fileTypes.indexOf(mimeType) > -1;  
			if(!isSuccess){
				this.fileUploader.nativeElement.value = null;
				alert('Please selecte valid File');
				break;
			}
			
		}
	}

  submitMediaUpload(time){
    this.editmode=false;
    this.loaded =[];
    var total = this.selectedFile.length;
    console.log(this.selectedFile);
    // return;
		for(var i=0;i<this.selectedFile.length;i++){
      // console.log("this.selectedFile[i]",this.selectedFile[i])
      this.totalSize = this.totalSize +this.selectedFile[i].size;
      this.loaded[this.selectedFile[i].name] = 0;
			this.uploadFiles(this.selectedFile[i],i,total);
    }
    console.log("this.totalSize",this.totalSize)
    this.timeSelected = time
    
	}
	

	uploadFiles(file,index,total) {
    this.hideprogress = true
    this.counter = 1;
    const contentType = file.type;
    this.loading= true;
		const bucket = new S3(
			{
				accessKeyId: 'AKIA4JXVAIZQ2G5B3T5Z',
				secretAccessKey: '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',
				region: 'ap-southeast-2'
			}
		);
		const params = {
			Bucket: 'yespipe',
			Key: 'video/' + file.name,
			Body: file,
			ACL: 'public-read',
			ContentType: contentType
		};
  
    var that = this;
    var i = index + 1;
    // var loadedTotal = 0;
		bucket.upload(params).on('httpUploadProgress', function (evt) {
      
      that.showMessage = true;
      var loadedTotal = 0;
      
      that.loaded[this.body.name] = evt.loaded;
      console.log('loaded Arr', that.loaded)
			for (var j in that.loaded) {
        console.log(j)
				loadedTotal += that.loaded[j];
      }
      console.log(loadedTotal)
      var progress = Math.round(loadedTotal / that.totalSize * 100);
      // var progress = (loadedTotal / that.totalSize) * 100;
      
      // console.log('progresss', progress,that.value)
      if(Number(progress) > Number(that.value)){
        that.value = parseInt(progress.toFixed(2));
      }
      if (loadedTotal === that.totalSize) {
        //successCallback(form, {files: resp.files, form: form.serialize()});
        that.value = 100;
      }
      if (loadedTotal >= that.totalSize) {
        //successCallback(form, {files: resp.files, form: form.serialize()});
        that.value = 100;
      }
      // if(progress > that.value){
      //   that.value = parseInt(progress.toFixed(2));
      // }
		}).send(function (err, data) {
			if (err) {
        // that.slimLoadingBarService.complete();
      }
        
     
      // var that = this;
      const fdata = {
        title:that.title,
        type:that.uploadAs,
        showTo:that.showTo,
      }
      var files=[];
      console.log(data)
      files.push(data.Location)
        
      fdata['file']=files;
      that.uploadCounter = that.uploadCounter + 1;
      fdata['uploader']="1"
      
      // console.log(fdata)
      if(that.timeSelected == 'time'){
        fdata['showOn']= new Date(that.formatDate(that.date7));
        // fd.append('showOn', new Date(this.formatDate(this.date7)).toUTCString());
      }
      else{
        fdata['showOn']= new Date(new Date(that.formatDate(new Date() )));
      }
      that.counter++;
      //that.value = 0
      that.userService.UploadContent(fdata).subscribe(
        response => {
        //  console.log(response);
         if(response['success']==1){
          this.loading= false;
              that.myModelContent = [];
              that.getModelMedia();
              if(that.myModelContent){
                setTimeout(()=>{    //<<<---    using ()=> syntax
                  that.getImage();
            }, 3000);
                // setTimeout(,100000);
              }
              
              
              
              console.log(total,that.uploadCounter);
              if(that.value  == 100 || total == that.uploadCounter){
                document.getElementById("model-closeContent").click();
                that.editmode=true;
                that.loading=false;
                that.value = 0;
                // that.uploadAs='1';
                that.selectedFile=[];
                that.showTo='1';
                that.noOfUpload='0';
                that.uploadFile=undefined;
                that.message="";
                that.date7 ="";
                that.type="image/png, image/jpeg";
                that.title='';
                that.hideprogress = false;
                that.selectedFile = [];
                that.loaded = [];
                that.totalSize=0;
                that.uploadCounter = 0;
              }
              // 
              
         }
         else{
          that.title='';
          that.uploadAs='1';
          that.selectedFile=[];
          that.showTo='1';
          that.noOfUpload='0';
          that.uploadFile=undefined;
          that.message="";
          that.date7 ="";
          that.type="image/png, image/jpeg";
          document.getElementById("model-closeContent").click();
          that.editmode=true;
          that.loading=false;
          alert("Something Went Wrong Please Contact Admin");
         }
          
        },
        err => {
          that.title='';
          that.uploadAs='1';
          that.selectedFile=[];
          that.showTo='1';
          that.noOfUpload='0';
          that.uploadFile=undefined;
          that.message="";
          that.date7 ="";
          that.type="image/png, image/jpeg";
          document.getElementById("model-closeContent").click();
          that.editmode=true;
          that.loading=false;
          //this.router.navigate(['/logout']);
          console.log(err);
        }
      );
      //return true;
      that.slimLoadingBarService.complete();
		});
  }
}
