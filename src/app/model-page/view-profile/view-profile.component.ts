import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from 'src/app/http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from "../../data.service";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import {MessageService} from 'primeng/api';
interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class ViewProfileComponent implements OnInit {
  myDataProfile: any =[];
  checkFollowing: any;
  isLoading: boolean =true;
  imageSrc: string | ArrayBuffer;
  imageSrc2:string | ArrayBuffer;
  files: any[];
  private formData = new FormData();
  postsCol: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;
  type :string;
  emptyGetdata:boolean;
  showAverage:string;
	totalComment:string;
  alreadyCommented:boolean;
  getData:any=[];
  imgsrc:any;
  
  constructor(private userService:UserService,private router: Router, private route:ActivatedRoute,private data: DataService,private afs: AngularFirestore,private messageService: MessageService) { }

  ngOnInit() {
    this.getModelProfile(localStorage.getItem('id'));
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
       this.router.navigate(['/your-feed']);
    }
     else if(localStorage.getItem('loggedInUser') == null ){
       this.router.navigate(['/login']);
     }
 
  }


  public getModelProfile(id: String){
		//console.log.log(id);
		const Data = {
      id: id,
      callback:"id"
		}
		//this.isLoading=true;
		this.userService.getProfile(Data).subscribe(
			response => {
				//console.log.log(response);
				if(response['success'] == 1){
					this.isLoading=false;
					//console.log.log(response);
					this.myDataProfile["media"] = response['data']['media'];
					if(response['data']['cover_Image'] ==undefined || response['data']['cover_Image'] == null || response['data']['cover_Image'] == ""){
						this.myDataProfile["cover"] = "assets/images/placeholder.png";
					}
					else{
						this.myDataProfile["cover"] = response['data']['cover_Image'];
					}
					if(response['data']['profile_image']==undefined || response['data']['profile_image'] == null || response['data']['profile_image'] == ""){
						this.myDataProfile["profile"] = "assets/images/dummy-profile.png";
					}
					else{
						this.myDataProfile["profile"] = response['data']['profile_image'];
					}
					
					this.myDataProfile["name"] = response['data']['name'];
					this.myDataProfile["website"] = response['data']['website'];
					this.myDataProfile["countVideo"] = response['data']['countVideo'];
					this.myDataProfile["countImage"] = response['data']['countImage'];
					this.myDataProfile["price"] = response['data']['price'];
					this.myDataProfile["model_id"] = response['data']['model_id'];
          this.myDataProfile["user_id"] = response['data']['user_id'];
          this.myDataProfile["model_comment"] = response['data']['model_comment'];
					this.checkFollowing = response['data']['following'];
					//console.log.log(this.myDataProfile);
					this.isLoading=false;
				}
				else{
					alert("something went wrong!");
				}
					
			},
			err => {
				alert("something went wrong!");
				//console.log.log(err);
			}
		);
  }
  
  readURL(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        this.formData.append("MediaType", '1');
        reader.readAsDataURL(file);
        this.changePic();
        this.type ='photo'
      } 
  }
  readURL2(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc2 = reader.result; 
        this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        this.formData.append("MediaType", '3');
        reader.readAsDataURL(file);
        this.changePic();
        this.type ='cover' 
      } 
  }

  changePic(){
    this.userService.ChangeImage(this.formData).subscribe(
      response => {
        //console.log.log(response);
        if(this.type == "photo"){
          this.messageService.add({severity:'success', summary: 'Success', detail:'Profile Pic Updated'});
          this.data.changeImage(response['data']['mainImage']);
          localStorage.setItem("profileImage", response['data']['mainImage']);
          const afs2 =  this.afs;
          this.afs.collection("chatStatus", ref => ref.where('modelId', '==', Number(localStorage.getItem('id'))))
            .get().toPromise().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                //console.log.log(doc.id, " => ", doc.data());
                  //this.chatId = doc.id;
                  //console.log.log(doc.id);
                  // Build doc ref from doc.id
                  afs2.collection("chatStatus").doc(doc.id).update({'modelProfile': response['data']['mainImage']}).then(function() {
                    //console.log.log("Document successfully written!");
                  })
                  .catch(function(error) {
                    //console.log.error("Error writing document: ", error);
                  });
              });
            })
        }
        else{
          this.messageService.add({severity:'success', summary: 'Success', detail:'Cover Pic Updated'});
        }      
      },
      err => {
        this.messageService.add({severity:'error', summary: 'error', detail:'Image not valid'});
        
      }
    );
  }
  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}



getModelComment(close){
  const id ={
    id:localStorage.getItem('id')
  }
  this.userService.getModelComment(id).subscribe(
    response => {
      if(response['success'] == 1){
        ////console.log.log("here")
        this.emptyGetdata = false;
        this.showAverage = response['data'][0]['totalaverage']
        this.totalComment = response['data'][0]['totalCommentCount']
        if(response['data'][0]['alreadyCommented'] > 0){
          this.alreadyCommented = true;
        }
        else{
          this.alreadyCommented = false;
        }
      }
      else{
        this.emptyGetdata = true;
        this.alreadyCommented = false;
      }
      ////console.log.log(response['data'][0]['posted_on'])
      
      this.getData=response['data'];
      // //console.log.log(this.getData);
      
      //this.emptyGetdata = false;
      if(close =='close'){
        document.getElementById('commentPopup').click();
      }
      
    },
    err => {
      //console.log.log(err);
      
    }
  );
    
  }
  convertDate(date){
		// var dat = new Date(date).toString();
		// date  = new Date(dat);
		var a = new Date(date);
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var dates = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		var times = dates + ' ' + month + ' ' + year + ' ' + hour + ':' + min ;
		return times;
	   }


     open(index: number,image): void {
      //alert("2")
      this.imgsrc = image;
      //console.log.log(this.imgsrc)
      var modal = document.getElementById("myModalImage");
      modal.style.display = "block";
      var captionText = document.getElementById("caption");
    }
    closeModel(){
      var modal = document.getElementById("myModalImage");
      modal.style.display = "none";
    }  
}
