import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { UserService } from 'src/app/http.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {MessageService} from 'primeng/api';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
interface Post {
	title: string;
	content: string;
  }
@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.css'],  
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})


export class SubscriberComponent implements OnInit {
  imageChange: string;
  message: string;
  currentEmail: string;
  modelDataFollowing: any =[];
  isLoading:boolean=true;
  profileUrlData:any;
  passwordValidation: any =[];
  newPassword :string;
  oldPassword :string;
  confirmCheck:string;
  private formDataReset = new FormData();
  loading2:boolean;
  blockedUser:any=[];
  activateDeactivate:string;

  payOutValidation:any=[];
  NamePayout:string;
  routingNo:string;
  accountNumber:string;
  accountType:string;
  swiftCode:string;
  IBAN:string;
  selectedCountry:any=[];
  country: any=[];

  postsCol: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;
  typingStatus:boolean =false;
  user_id:any;
  chatIdString:string;
  getchatData=[];
  chatReference:any;
  messageReference:any;
  loggedInuser:any = localStorage.getItem('id');
  userProfile:any = localStorage.getItem('profileImage');
  userImage:any
  editmode:boolean=true;
  isUpload:boolean=false;
  imageSrc: string | ArrayBuffer;
  files: any[];
  private formData = new FormData();
  type:string;
  content:string ='';
  imageFile:string;
  loading:boolean;

  mid = localStorage.getItem('profileUrl');
  //copied:string ="Click to copy";
  constructor(private data: DataService, private router: Router,private userService:UserService,private messageService: MessageService,private afs: AngularFirestore) { }

  ngOnInit() {
    this.getData();
    this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message)
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.profileUrlData = localStorage.getItem('profileUrl') 
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
    ]; 
    
   
  }

  copyText(val: string,val2: string){
    //this.copied = "copied"
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }
  getAllchat(id){
    this.user_id = Number(id)
    this.getLiveChatStatus();
   // this.livechatStatus();
  }
	getLiveChatStatus(){
		const that = this;
		this.postsCol = this.afs.collection('posts', ref => ref.where('userId', '==', Number(this.user_id)).where('modelId', '==', Number(localStorage.getItem('id'))));
		//that.ChatToShow = []; 
		this.posts = this.postsCol.valueChanges();
		this.posts.subscribe(res => {
			res.forEach(element1 => {
				////console.log.log("wawaw",element1['typingStatus'][this.params.uid]);
				this.typingStatus = element1['typingStatus'][ this.user_id]
				
			});
			
		})
  }
  modelChat(user_id,image){
    this.userImage = image
    document.getElementById('openChatPopup').click()
    this.user_id = Number(user_id)
    const model_id = Number(localStorage.getItem('id'))
 
  
  
      if ((typeof this.chatIdString != 'undefined') ) {
      //////////console.log.log.log.log.log("123")
      var countId = 'unreadCount.' + user_id;
      this.afs.collection('posts').doc(this.chatIdString).update({[countId]: 0});

      this.getchatData = [];
  
      if(Number(user_id)>model_id){
        this.chatIdString = model_id+'_'+user_id;
      }
      else{
        this.chatIdString = user_id+'_'+model_id;
      }
    }
    
      if(Number(user_id)>model_id){
        this.chatIdString = model_id+'_'+user_id;
      }
      else{
        this.chatIdString = user_id+'_'+model_id;
      }
  
      this.chatReference =  this.afs.collection('posts').doc(this.chatIdString);
      this.messageReference = this.chatReference.collection('messsages');
  
  
      this.postNotiStatus(user_id,model_id)
      // this.status = !this.status;    
      var userId= localStorage.getItem("id");
      const afs2 =  this.afs;
      var maxCha;
      
      const that =this
 
      const thats  = this;  
      // this.messageReference.get().toPromise().then(function(querySnapshot) {
      // //const sizeOfarray = querySnapshot.size;
      // //////////console.log.log.log.log.log("sdsdfsdff",querySnapshot.size)
     
      // // querySnapshot.forEach(function(doc) {
      // //   //////////console.log.log.log.log.log(doc.data());
      // //   thats.getchatData.push(doc.data());
  
      // //   });
      // //   thats.getchatData.sort(function (a, b) {
      // //     return a['time'].seconds - b['time'].seconds;
      // //   });
      // })
      this.getallChat(this.chatIdString) 
      // console.log(this.getchatData)
      this.setUnreadCountZeroForReceiver();
    }

    getallChat(chatId){
      var afs2 = this.afs;
      var that = this;
          var ref = that.afs.collection('posts').doc(chatId).collection("messsages");
          var listener = ref.valueChanges();
          listener.subscribe(res => {
            //////////console.log.log.log.log.log("newdfsdf",res);
            if (res.length > 0) {
            var aMsg = res[0];
            var mId = aMsg['modelId'];
            var uId = aMsg['userID'];
            var cId = uId + '_' + mId;
            if (Number(localStorage.getItem('id')) < uId) {
              cId = mId + '_' + uId;
            }
            //////////console.log.log.log.log.log('chatId', cId);
  
            res.sort(function (a, b) {
              return a['time'].seconds - b['time'].seconds;
            });
            that.getchatData = res;
  
            }
  
          })
    }
    
	setUnreadCountZeroForReceiver(){
		var pinDocRef = this.chatReference.ref;
		return firebase.firestore().runTransaction(function(transaction) {
			return transaction.get(pinDocRef).then(function(pinDoc) {
				if(!pinDoc.exists){
					return false;
				}
				var unreadCount = pinDoc.data().unreadCount;
				unreadCount[localStorage.getItem('id')] = 0;
				transaction.update(pinDocRef, { 'unreadCount': unreadCount});
			});
		  }).then(function() {
			  // ////////console.log.log.log.log.log("Transaction successfully committed!");
		  }).catch(function(err) {
			  // ////////console.log.log.log.log.log("Transaction failed: ", err);
		  });
	}  
    postNotiStatus(USERRID,MODELID){
      const afs2 =  this.afs;
       this.afs.collection("chatStatus", ref => ref.where('userID', '==', Number(USERRID)).where('modelId', '==', Number(MODELID)))
          .get().toPromise().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
                afs2.collection("chatStatus").doc(doc.id).update({'unreadUser':0}).then(function() {
                })
                .catch(function(error) {
  ///////////console.log.log.log.log.error("Error writing document: ", error);
                });
            });
          })
  
          this.afs.collection("posts", ref => ref.where('userID', '==', Number(USERRID)).where('modelId', '==', Number(MODELID)).where('sender', '==', 'model'))
          .get().toPromise().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
                afs2.collection("posts").doc(doc.id).update({'read':true}).then(function() {
                })
                .catch(function(error) {
                  //////////console.log.log.log.log.error("Error writing document: ", error);
                });
            });
          })  
    }
    readURL(event) 
    { 
      if (event.target.files && event.target.files[0]) 
        { const file = event.target.files[0]; 
          const reader = new FileReader(); 
          reader.onload = e => this.imageSrc = reader.result; 
          this.files = event.target.files;
          this.formData.append("file", event.target.files[0]);
        //  ////////console.log.log.log.log.log(event.target.files)
          reader.readAsDataURL(file);
          this.isUpload=true; 
        } 
    }
    addPost() {
      this.editmode=false;  
      //////////console.log.log.log.log.log(this.content);return;
       if(this.isUpload){
         this.type="photo";
         const api = {
           api_token:localStorage.getItem("loggedInUser")
          }
          this.userService.uploadChatPic(this.formData).subscribe(
            response => {
            // ////////console.log.log.log.log.log(response);
            
             if(response['success']==1){
              // ////////console.log.log.log.log.log(response);
                this.content =response['data'];
              
                this.getDataChat();
             }
             else{
             //  ////////console.log.log.log.log.log(response);
                
             }
              
            },
            err => {
              this.router.navigate(['/login']);
             // ////////console.log.log.log.log.log(err);
            }
          );
        }
        else{
         
         if(this.content.trim() == "" || this.content == null  || this.content == undefined || this.content == " " ){
           this.editmode=true;  
           return false;   
        }
         this.type="text";
         this.getDataChat();
        }
      
     }

  getDataChat(){

	
    this.imageFile="";

  
      
    this.isUpload=false;
    this.loading=true;
    
    var timestamp = firebase.firestore.Timestamp.now();
  
    //var milliseconds = (new Date).getTime();
    
      this.messageReference.add({'message': this.content, 'sender':localStorage.getItem('id'),'userID': this.user_id, 'modelId': Number(localStorage.getItem('id')),'time':timestamp,'type':this.type,'read':false});
      var chatId = this.user_id +'_'+ Number(localStorage.getItem('id'));
      if(this.user_id > Number(localStorage.getItem('id'))){
         var chatId =  Number(localStorage.getItem('id')) +'_'+ this.user_id;
    }
  
      // this.afs.collection('posts').add({'chatId': chatId,'lastMessage':this.content, 'lastMessageTime':firebase.firestore.FieldValue.serverTimestamp(),'modelId':Number(this.modelId),'userId':this.userID});
    const that =this;
    if(this.type=="photo"){
      this.content = "@@@@@@@";
    }
    const contents = this.content
    this.chatReference.update({'lastMessage':this.content,'time':timestamp}).then(function() {
      that.sendUnreadCount(Number(Number(localStorage.getItem('id'))),Number(that.user_id),chatId);
    }).catch(function(error) {
      if (error.code == 'not-found') {
       
        var typingStatus = {[that.user_id]:false,[localStorage.getItem('id')]:false,};
				var unreadCount = {[that.user_id]:1,[localStorage.getItem('id')]:0};
				// var typingStatus2 = 'typingStatus.' + that.params.uid;
				// var unreadCount1 = 'unreadCount.' + localStorage.getItem('id');
				// var unreadCount2 = 'unreadCount.' + that.params.uid;
			
				//console.log.log("striong",{'chatId': that.chatIdString,'lastMessage':cont,'time':timestamp,'modelId':Number(that.params.uid),'userId':Number(localStorage.getItem('id')), typingStatus,unreadCount})
				that.chatReference.set({'chatId': that.chatIdString,'lastMessage':contents,'time':timestamp,'modelId':Number(localStorage.getItem('id')),'userId':Number(that.user_id),typingStatus,unreadCount}).then(function(){
          that.sendUnreadCount(Number(localStorage.getItem('id')),Number(that.user_id),chatId);
				});
        
      }
      else {
        console.log('321')
        // Unknown error
        
      }
    });
      const afs3 =  this.afs;

  
    
    this.getchatData.push({'message': this.content,'sender': Number(localStorage.getItem('id')),'userID': this.user_id,'modelId': Number(localStorage.getItem('id')),'time':timestamp,'type':this.type,'read':true})
      
      
      if(this.type =="photo"){
        this.content="Image";
      }
      else{
        this.content=this.content;
      }
        this.content="";
  
      this.editmode=true;
      this.loading=false;
    }   
  	sendUnreadCount(modelId,UserID,docId){
      var pinDocRef = this.chatReference.ref;
      return firebase.firestore().runTransaction(function(transaction) {
        return transaction.get(pinDocRef).then(function(pinDoc) {
          if(!pinDoc.exists){
            return false;
          }
          var unreadCount = pinDoc.data().unreadCount;
          for (var key in unreadCount) {
            if (key == localStorage.getItem('id')) {
              unreadCount[key] = 0;
            }
            else {
              unreadCount[key] = Number(pinDoc.data().unreadCount[key]) + 1;
            }
          }
  
          transaction.update(pinDocRef, { 'unreadCount': unreadCount});
        });
        }).then(function() {
          // ////////console.log.log.log.log.log("Transaction successfully committed!");
        }).catch(function(err) {
          // ////////console.log.log.log.log.log("Transaction failed: ", err);
        });
    }   

  getData(){
    const api = {
      api_token:localStorage.getItem("loggedInUser")
     }
     this.userService.getAllFollowingUsers(api).subscribe(
       response => {
        //console.log.log(response);
        if(response['success']==1){
          this.modelDataFollowing = response['data'];
          ////console.log.log(response['data']);
          this.isLoading=false;
          for(var i =0; i<response['data'].length;i++){
           //this.blockedUser=
            if(response['data'][i]['blocked'] == 1){
              this.blockedUser.push(response['data'][i]['user_id'].toString())
            }
          }
          //console.log.log(this.blockedUser)
        }
        else{
          // alert("No Subscriber Found");
          this.isLoading=false;
        }
         
       },
       err => {
         this.router.navigate(['/login']);
         //console.log.log(err);
       }
     );
  }


  hideErrors(){
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation['result'] = "Old Password does not matched";
          this.loading2=false;
        }
        if(response['success']==2){
          this.passwordValidation['result'] = "Some error occured please try after sometime";
          this.loading2=false;
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

  checkStatus(checked,userid){
    //alert(checked);
    ////console.log.log(this.blockedUser)
    const user_id = {
      user_id:userid,
      type:checked
    }
   //this.blockedUser = [];
    this.userService.blockUser(user_id).subscribe(
      response => {
       //console.log.log(response);
       if(response['success']==1){
         //alert("success");
         //this.blockedUser = [];
        //  this.modelDataFollowing = response['data'];
        //  ////console.log.log(response['data']);
        //  this.isLoading=false;
       }
       else{
        //  alert("No Subscriber Found");
        //  this.isLoading=false;
       }
        
      },
      err => {
        // this.router.navigate(['/login']);
        //console.log.log(err);
      }
    );
  }

onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      //console.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      //console.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}



}
