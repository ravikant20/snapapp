import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from "../../data.service";
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { UserService } from 'src/app/http.service';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class ApplicationComponent implements OnInit {
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  message:string;
  imageChange:string; 
  currentEmail:string;
  nameMain:string ;
  bornMonth:string;
  bornYear:any ; 
  bornDate:string; 
  phone:string;
  price:string="10"; 
  email:string;
  street_number:string;
  postal_code:string;
  street_address:string;
  city:string;
  county:string;
  suburb:string;
  residence:string="US";
  userName:string;
  private formData = new FormData();
  form: FormGroup;
  profileWebsite1:string='';
  profileWebsite2:string='';
  legalName:string;
  DateToshow:any ;
  yearToshow:any ;
  monthToshow:any ;
  loading:boolean = false;
  id_type:string ;
  //userService: any;
  idNumber:string;
  issuedBy:string;
  Aliases1:string;
  Aliases2:string;
  Aliases3:string;
  Incomplete:boolean=true;
  uploadFile:string;
  selectedFile:any =[];
  iAgreeCheckBox:boolean=true;
  CheckBox2:boolean=false;
  Videoprice:string;
  LiveCamprice:string;
  uploadFileSelfie:string;
  selectedFileSelfie:any=[];
  IncompleteSelfie:boolean=true;
  fd = new FormData();
  PhotoId:string;
  PhotoSelfie:string;
  imageSrc: string | ArrayBuffer;
  imageSrc2:string | ArrayBuffer;
  idPhoto:boolean=false;
  idSelfie:boolean=false;
  agreementModel:any;
  profileUrl:any;
  profileUrlData:any;
  date1: any =  new Date();
  passwordValidation3: any =[];
  newPassword2 :string;
  oldPassword2 :string;
  confirmCheck2:string;
  private formDataReset = new FormData();
  loading2:boolean;
  selectedCountry:any=[];
  country: any=[];
  activateDeactivate:string;

  myFormPay: NgForm;
  payOutValidation:any=[];
  NamePayout:string;
  routingNo:string;
  accountNumber:string;
  accountType:string;
  swiftCode:string;
  IBAN:string;
  isLoading:boolean =true;
  bsb:string;
  minDate :Date;

  mid = localStorage.getItem('profileUrl');

  constructor(private data: DataService, private router: Router,private userService:UserService,private messageService: MessageService) {
    //alert(this.mid)
   }

  ngOnInit() {
    this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message)
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.getModelData();
    this.getYear();

    this.profileUrlData = localStorage.getItem('profileUrl');
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
  ]; 
    this.minDate = new Date();
  }

  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }

  getModelData(){
    this.userService.getAccoutDetail().subscribe(
      response => {
       ////////console.log.log.log(response);
       if(response['success']==1){
         if(response['data'][0]['terms'] == 1){
          this.iAgreeCheckBox = true;
          this.CheckBox2 = true;
         }
         ////////console.log.log.log(response);
        // street_number
        // postal_code
        // street_address
        // city
        // county
        // suburb
         this.nameMain = this.nullRemover(response['data'][0]['model_name']);
         this.street_number = this.nullRemover(response['data'][0]['street_number']);
         this.postal_code = this.nullRemover(response['data'][0]['postal_code']);
         this.street_address = this.nullRemover(response['data'][0]['street_address']);
         this.city = this.nullRemover(response['data'][0]['city']);
         this.county = this.nullRemover(response['data'][0]['county']);
         this.suburb = this.nullRemover(response['data'][0]['suburb']);
         this.email = this.nullRemover(response['data'][0]['model_email']);
         this.phone = this.nullRemover(response['data'][0]['modelPhone']);
         this.legalName = this.nullRemover(response['data'][0]['legal_name']);
         this.idNumber = this.nullRemover(response['data'][0]['id_number']);
         this.idNumber = this.nullRemover(response['data'][0]['id_number']);
         this.issuedBy = this.nullRemover(response['data'][0]['issuedBy']);
         this.profileWebsite1 = this.nullRemover(response['data'][0]['profile_url']);
         console.log(response['data'][0]['id_expiry'] )
         if(response['data'][0]['id_expiry'] == null){
          this.minDate = new Date();
         }
         else{
          this.date1 = new Date(response['data'][0]['id_expiry'])
         }
         
         this.Videoprice = response['data'][0]['video_price']
         this.LiveCamprice= response['data'][0]['liveCamPrice']

         if(response['data'][0]['photo_id'] == null){
          this.Incomplete =true;

         }
         else{
          this.Incomplete =false;
          this.PhotoId=response['data'][0]['photo_id']
          
         } 
         if(response['data'][0]['photo_selfie'] == null){
          this.IncompleteSelfie =true;
         }
         else{
          this.IncompleteSelfie =false;
          this.PhotoSelfie=response['data'][0]['photo_selfie'];
         }  
        // alert(this.id_type)
         if( response['data'][0]['id_type'] == null){
          this.id_type =undefined;
         }
         else{
          this.id_type =response['data'][0]['id_type'];
         // alert(this.id_type)
         }
         
         //alert(this.id_type)
         if(response['data'][0]['Dob']!=null){
          let x = response['data'][0]['Dob'].split("/");
          ////////console.log.log.log(x);
          this.bornMonth = x[1];
          //alert(x[1])
          this.bornYear = x[2];

          this.bornDate = x[0];
          this.changedMonth();
          
          //////////console.log.log.log(this.changedMonth());
         }
         
         if(response['data'][0]['price']!=null){
          this.price = response['data'][0]['price']
         }
         else{
          this.price = '10';
         }
         if(response['data'][0]['residence']!=null){
          this.residence = response['data'][0]['residence']
         }
         else{
          this.residence = 'US';
         }
         this.userName = this.nullRemover(response['data'][0]['social_id']);
       }
       else{
         ////////console.log.log.log(response);
       }
        this.isLoading = false;
      },
      err => {
       // this.router.navigate(['/login']);
        ////////console.log.log.log(err);
        this.isLoading = false;
      }
    );
  }






  nullRemover(dat){
    if(dat == null || dat == 'null'){
      return ''
    }
    else{
      return dat;
    }
  }


  getYear(){
    this.yearToshow= [];
    var year = (new Date()).getFullYear() - Number(18);
    ////////console.log.log.log(year);
    for(var i=year;i>=Number(year) - Number(30); i--){
      let newName = {
        id:i.toString(),
     };
     this.yearToshow.push(newName)
    }

  }
  changedMonth(){
    //alert(this.bornMonth);
    this.DateToshow=[];
    if(this.bornMonth=="1" || this.bornMonth=="3" || this.bornMonth=="5" ||  this.bornMonth=="7" || this.bornMonth=="8" ||  this.bornMonth=="10"||  this.bornMonth=="12"){
      for(var i=1;i<=31; i++){
        let newName = {
          id:i.toString(),
       };
      // ////////console.log.log.log(newName);
       this.DateToshow.push(newName)
      }
    }
    else if(this.bornMonth=="4" || this.bornMonth=="6" || this.bornMonth=="9" ||  this.bornMonth=="1" ){
      for(var i=1;i<=30; i++){
        let newName = {
          id:i.toString(),
       };
       this.DateToshow.push(newName)
      }
    }
    else if(this.bornMonth=="2" && Number(this.bornYear) % Number(4) == 0){
      for(var i=1;i<=29; i++){
        let newName = {
          id:i.toString(),
       };
       this.DateToshow.push(newName)
      }
    }
    else{
      for(var i=1;i<=28; i++){
        let newName = {
          id:i.toString(),
       };
       this.DateToshow.push(newName)
      }
    }
    
    //////////console.log.log.log(this.DateToshow)
  }



  submitData(){
    //alert("form")
    //alert(this.nameMain)
     
    this.fd.append('name', this.nameMain);
    this.fd.append('phone', this.phone);
    this.fd.append('dateOfBirth',this.bornDate +'/'+this.bornMonth+'/'+this.bornYear);
    this.fd.append('email', this.email);
    this.fd.append('residence', this.residence);
    // this.fd.append('price', this.price);
    this.fd.append('social_id', this.userName);
    this.fd.append('profile_url', this.profileWebsite1);
    this.fd.append('legal_name', this.legalName);
    this.fd.append('id_type', this.id_type);
    this.fd.append('idNumber', this.idNumber);
    this.fd.append('issuedBy', this.issuedBy);
    this.fd.append('Aliases1', this.Aliases1);
    this.fd.append('Aliases2', this.Aliases2);
    this.fd.append('Aliases3', this.Aliases3);
    this.fd.append('Aliases1', this.Aliases1);

    
// street_number
// postal_code
// street_address
// city
// county
// suburb

    this.fd.append('street_number', this.street_number);
    this.fd.append('postal_code', this.postal_code);
    this.fd.append('street_address', this.street_address);
    this.fd.append('city', this.city);
    this.fd.append('county', this.county);
    this.fd.append('suburb', this.suburb);


    // this.fd.append('video_price',this.Videoprice);
    // this.fd.append('liveCamPrice',this.LiveCamprice);
    this.fd.append('expirydate',this.date1);

    //////////console.log.log.log(this.selectedFile)
   
   
    
    this.formsubmit(this.fd)
    //////////console.log.log.log(formValue);
  }

  formsubmit(formValue){
   var check = this.fullValidation();
   //alert(check);
   if(check == false){
     return false;
   }
    if(this.CheckBox2==false){
      alert("please accept our terms")
      return false;
    }
    //alert(this.iAgreeCheckBox); alert(this.CheckBox2)
    this.loading=true;
    this.userService.updateModelData(formValue).subscribe(
      response => {
       ////////console.log.log.log(response);
       //this.isLoading=true;
       if(response['success']==1){
         ////////console.log.log.log(response);
         this.loading=false;
       //  alert(this.nameMain);
         this.data.changeMessage(this.nameMain);
         localStorage.setItem("name",this.nameMain);
         this.messageService.add({severity:'success', summary: 'success', detail:'We have received your profile. We will review and post you updates once your profile is approved.'});
         // this.isLoading=false;
       }
       else{
         ////////console.log.log.log(response);
         this.loading=false;
         this.messageService.add({severity:'error', summary: 'error', detail:'Something went Wrong Please Try After SomeTime'});
         //this.isLoading=false;
         //alert("No models Found");
       }
        
      },
      err => {
        //this.router.navigate(['/login']);
        this.messageService.add({severity:'error', summary: 'error', detail:'The Email Is already has been taken'});
        ////////console.log.log.log(err);
        this.loading=false;
      }
    );
    
  }
  customTrackBy(index: number, obj: any): any {
    return index;
  }


  scrollToTop() {
    (function smoothscroll() {
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
            window.requestAnimationFrame(smoothscroll);
            window.scrollTo(0, currentScroll - (currentScroll / 8));
            // element.classList.add("newClass");
          
        }
    })();
}

changeTab(){
  if(this.nameMain == "" || this.nameMain == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Name Field Cannot be Empty'});
    return false;
  }
  if(this.phone == "" || this.phone == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Phone Field Cannot be Empty'});
    return false;
  }
  if(this.bornYear == "" || this.bornYear == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Year SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornMonth == "" || this.bornMonth == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Month SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornDate == "" || this.bornDate == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Date SelectBox Cannot be Empty'});
    return false;
  }
  if(this.email == "" || this.email == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Field Cannot be Empty'});
    return false;
  }
  var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
  if(!pattern.test(this.email)){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Not Valid'});
    return false;
  }

  if(this.userName == "" || this.userName == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Add Your Social Account'});
    return false;
  }
  if(this.profileWebsite1 == "" || this.profileWebsite1 == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Profile URL Cannot be blank'});
    return false;
  }
  if(this.street_number == "" || this.street_number == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Street Number Cannot be blank'});
    return false;
  }
  if(this.postal_code == "" || this.postal_code == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Postal Code Cannot be blank'});
    return false;
  }
  if(this.street_address == "" || this.street_address == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Street address Cannot be blank'});
    return false;
  }
  if(this.city == "" || this.city == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'City name Cannot be blank'});
    return false;
  }
  if(this.county == "" || this.county == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Country name Cannot be blank'});
    return false;
  }
  if(this.suburb == "" || this.suburb == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Suburb name Cannot be blank'});
    return false;
  }


  // street_number
  // postal_code
  // street_address
  // city
  // county
  // suburb

  if(this.iAgreeCheckBox != true){

    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Accept Our terms'});
    return false;
  }
  
  document.getElementById('documentTab').click();
  this.scrollToTop();
}

changeTab2(){
  document.getElementById('documentTab2').click();
  this.scrollToTop();
}


onFileSelected(event) {
 // alert("rk");
  const number = event.target.files.length;
  this.selectedFile = <File>event.target.files[0]; 
  this.Incomplete =false;
  ////////console.log.log.log(this.selectedFile)
  this.fd.append('file[photo_id]', this.selectedFile, this.selectedFile.name);
  this.PhotoId="";
  
  // const reader = new FileReader(); 
  // reader.onload = e => this.imageSrc = reader.result;
    const file = event.target.files[0]; 
    const reader = new FileReader(); 
    reader.onload = e => this.imageSrc = reader.result; 
    reader.readAsDataURL(file); 
      
}


onFileSelectedSelfie(event){
 // alert("ka");
  const number = event.target.files.length;
  this.selectedFileSelfie = <File>event.target.files[0]; 
  this.IncompleteSelfie =false;
  ////////console.log.log.log(this.selectedFileSelfie)
  this.fd.append('file[photo_Selfie]', this.selectedFileSelfie, this.selectedFileSelfie.name);
  this.PhotoSelfie="";
  
    const file = event.target.files[0]; 
    const reader = new FileReader(); 
    reader.onload = e => this.imageSrc2 = reader.result; 
    reader.readAsDataURL(file);
  
}

agreeCheckBox(){
  // alert("E")
  this.iAgreeCheckBox=true;
 // alert(this.iAgreeCheckBox)
}
checkBoxCheck(){
  this.CheckBox2=true;
}



fullValidation(){
  if(this.nameMain == "" || this.nameMain == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Name Field Cannot be Empty'});
    return false;
  }
  if(this.phone == "" || this.phone == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Phone Field Cannot be Empty'});
    return false;
  }
  if(this.bornYear == "" || this.bornYear == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Year SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornMonth == "" || this.bornMonth == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Month SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornDate == "" || this.bornDate == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Date SelectBox Cannot be Empty'});
    return false;
  }
  if(this.email == "" || this.email == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Field Cannot be Empty'});
    return false;
  }
  var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
  if(!pattern.test(this.email)){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Not Valid'});
    return false;
  }

  if(this.userName == "" || this.userName == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Add Your Social Account'});
    return false;
  }
  if(this.profileWebsite1 == "" || this.profileWebsite1 == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Profile Slug Cannot be blank'});
    return false;
  }
  // if(this.profileWebsite2 == "" || this.profileWebsite2 == null){
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Profile Slug Cannot be blank'});
  //   return false;
  // }
  // if(this.iAgreeCheckBox != true){
  //  // alert(this.iAgreeCheckBox)
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Please Accept Our terms'});
  //   return false;
  // }



  if(this.legalName == "" || this.legalName == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Fill Your Legal Name'});
    return false;
  }
  if(this.id_type == "" || this.id_type == null){
    //alert(this.iAgreeCheckBox)
    this.messageService.add({severity:'error', summary: 'Error', detail:'Id type Cannot Be Blank'});
    return false;
  }
  if(this.idNumber == "" || this.idNumber == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Id Number Cannot be blank'});
    return false;
  }
  if(this.issuedBy == "" || this.issuedBy == null){
   // alert(this.iAgreeCheckBox)
    this.messageService.add({severity:'error', summary: 'Error', detail:'Issued By cannot Be Blank'});
    return false;
  }
  ////////console.log.log.log(this.date1)
  if(this.date1 == "" || this.date1 == null){
    // alert(this.iAgreeCheckBox)
     this.messageService.add({severity:'error', summary: 'Error', detail:'Expiry date cannot be blank'});
     return false;
   }
  // if(this.Aliases1 == "" || this.Aliases1 == null){
  //   //alert(this.iAgreeCheckBox)
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Aliases cannot Be Blank'});
  //   return false;
  // }
  // if(this.Aliases2 == "" || this.Aliases2 == null){
  //  // alert(this.iAgreeCheckBox)
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Aliases cannot Be Blank'});
  //   return false;
  // }

  if(this.CheckBox2 != true){
    // alert(this.iAgreeCheckBox)
     this.messageService.add({severity:'error', summary: 'Error', detail:'Please Accept Our terms'});
     return false;
   }


}
onConfirm() {
  this.messageService.clear('c');
}

onReject() {
  this.messageService.clear('c');
}


showId(id){
  this.idPhoto =true;
  this.idSelfie =false;
  this.agreementModel ="type";
  //alert(id);
  if(id != ""){

    this.imageSrc = id;
  }
  document.getElementById('openModal').click();
}

showSelfie(selfie){
  this.idSelfie =true;
  this.idPhoto =false;
  this.agreementModel ="type";
  if(selfie != ""){
    this.imageSrc2 = selfie;
  } 
  document.getElementById('openModal').click();
}



agreement(type){
  //alert(type);
  this.agreementModel =type;
  this.idSelfie =false;
  this.idPhoto =false;
  document.getElementById('openModal').click();
  
}


hideErrors(){
  //alert("rk")
  this.passwordValidation3 = [];
  ////////console.log.log.log(this.passwordValidation3)
  this.newPassword2 = "";
  this.oldPassword2 ="";
  this.confirmCheck2 =""
  this.passwordValidation3['result'] = "";
  this.passwordValidation3['oldPassword'] = "Old password field cannot be blank wefrwefwafsdfws";
  this.passwordValidation3['newpassword'] = "";
  this.passwordValidation3['confirmPassword'] = "";
  document.getElementById('openPasswordrest').click();
}

onSubmitPasswordReset(){
  if(this.oldPassword2 =="undefined" || this.oldPassword2 ==undefined || this.oldPassword2 == ""){
    //////////console.log.log.log("here");
    this.passwordValidation3['oldpassword'] = "Old password field cannot be blank";
    return false;
  }
  else{
    this.passwordValidation3['oldpassword'] = "";
  }
  if(this.newPassword2 =="undefined" || this.newPassword2 ==undefined || this.newPassword2 == "") {
    //////////console.log.log.log("here");
    this.passwordValidation3['newpassword'] = "New password field cannot be blank";
    return false;
  }
  else{
    this.passwordValidation3['newpassword'] = "";
  }
  if(this.confirmCheck2 !="undefined" && this.confirmCheck2 != this.newPassword2) {
    //////////console.log.log.log("here");
    this.passwordValidation3['confirmPassword'] = "Confirm Pasword and New Password must be same";
    return false;
  }
  else{
    this.passwordValidation3['confirmPassword'] = "";
  }
  this.formDataReset.append('newPassword',this.newPassword2);
  this.formDataReset.append('oldPassword',this.oldPassword2);
  this.loading2=true;
  this.userService.ResetPassword(this.formDataReset).subscribe(
    response => {
      if(response['success']==1){
        this.passwordValidation3['result'] = "Password changed successfully";
        //$('#myModal').modal('hide')
        document.getElementById("model-close").click();
        this.loading2=false;
        this.newPassword2= "" ;
        this.oldPassword2= "";
        this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
        //this.passwordValidation['result']=""
      }
      if(response['success']==0){
        this.passwordValidation3['result'] = "Old Password does not matched";
      }
      if(response['success']==2){
        this.passwordValidation3['result'] = "Some error occured please try after sometime";
      }
      if(response['success']==3){
        this.passwordValidation3['result'] = "Some error occured please try after sometime";
      }
      
    },
    err => {
     
        this.passwordValidation3['result'] = "password must be of 6 chracter";
     
        //this.loading=false;
        //////////console.log.log.log(err);
        //alert("Something Went Wrong")
    }
  );
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      ////////console.log.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      //alert(this.activateDeactivate)
      if(this.activateDeactivate == "0"){
        //alert("y")
      }
      else if(this.activateDeactivate == "1"){
        //alert("y")
      }
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup2').click();
    document.getElementById('model-close2').click();
    var that = this
    setTimeout(function(){ that.router.navigate(['/logout']); }, 300);
    ;
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      ////////console.log.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup2').click();
        document.getElementById('model-close2').click();
       
        setTimeout(function(){ that.router.navigate(['/logout']); }, 300);

      }
     
      document.getElementById('closeDeactivatePopup2').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}

checkpayout(){
  //alert("r")
  //getPayoutDetails(data){
    //
  this.userService.getPayoutDetails().subscribe(
      response => {
          ////////console.log.log.log(response);
          if(response['success'] == 0){
            // ////////console.log.log.log('')
              //document.getElementById('PayoutPopup').click();
              this.payOutValidation['NamePayout'] = '';
              this.payOutValidation['bankName'] = '';
              this.payOutValidation['accountType'] = '';
              this.payOutValidation['routingNo'] = '';
              this.payOutValidation['swiftCode'] = '';
              this.payOutValidation['IBAN'] = '';
          }
          else{
            this.selectedCountry = response['data']['location'];
            this.selectedCountry = this.country.find(country => country.name === this.selectedCountry);
            if(response['data']['location']== 'United States' ){
              this.routingNo = response['data']['routing_no']
              this.accountNumber = response['data']['account_no']
              this.accountType = response['data']['account_type']
            }
            else if(response['data']['location']!= 'United States' && response['data']['location'] != null && response['data']['location'] != 'Australia' && response['data']['location'] != 'New Zealand'){
              this.swiftCode = response['data']['swift_code']
              this.IBAN = response['data']['iban']
            }
            else if(response['data']['location']== 'Australia' && response['data']['location'] != null){
              this.bsb = response['data']['BSB']
              this.accountNumber = response['data']['account_no']
            }
            else{
              this.accountNumber = response['data']['account_no']
            }
            
            //this.selectedCountry = response['data']['location'];
          }
          //////////console.log.log.log("response['data']['location']",response['data']['location'])
      },
      err => {
       
       
          //this.loading=false;
          ////////console.log.log.log(err);
          //alert("Something Went Wrong")
      }
  );
  
  
}

openPayoutPopup(){
  document.getElementById('PayoutPopup').click();
}

payoutSubmit(){
  //alert("rk");
////////console.log.log.log(this.selectedCountry);
//return false
  if(this.selectedCountry['name'] == 'United States' && this.selectedCountry['name'] != null){
// routingNo:string;
// accountNumber:string;
// accountType:string;
// swiftCode:string;
// IBAN:string;
//////////console.log.log.log("this.routingNo",this.routingNo)
    if(this.routingNo == "" || this.accountType == undefined){
      this.payOutValidation['routingNo'] = 'Routing No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['routingNo'] = '';
    }
    if(this.accountNumber == "" || this.accountNumber == undefined){
      this.payOutValidation['accountNumber'] = 'Account No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountNumber'] = '';
    }
    if(this.accountType == "" || this.accountType == undefined){
      this.payOutValidation['accountType'] = 'Account Type Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountType'] = '';
    }

   

        const data = {
          routingNo : this.routingNo,
          accountNumber: this.accountNumber,
          accountType: this.accountType,
          location: this.selectedCountry
        }
        ////////console.log.log.log(data)
      //alert("success")
      this.submitPayoutDetails(data)
  }
  else if(this.selectedCountry['name']!= 'United States' && this.selectedCountry['name'] != null && this.selectedCountry['name'] != 'Australia' && this.selectedCountry['name'] !='New Zealand'){
    //alert(this.selectedCountry)
    if(this.swiftCode == "" || this.swiftCode == undefined){
      this.payOutValidation['swiftCode'] = 'Swift Code Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['swiftCode'] = '';
    }
    if(this.IBAN == "" || this.IBAN == undefined){
      this.payOutValidation['IBAN'] = 'IBAN Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['IBAN'] = '';
    }
   
      const data = {
        swiftCode : this.swiftCode,
        IBAN: this.IBAN,
        location: this.selectedCountry
      }
      ////////console.log.log.log(data)
      this.submitPayoutDetails(data)
  }

  else if(this.selectedCountry['name'] == 'New Zealand'){
    //alert("here");
    if(this.accountNumber == "" || this.accountNumber == undefined){
      this.payOutValidation['accountNumber'] = 'Account No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountNumber'] = '';
    }
    const data = {
      accountNumber: this.accountNumber,
      location: this.selectedCountry
    }
    console.log(data);
    this.submitPayoutDetails(data)
  }
  else{
    //alert("there");
    if(this.accountNumber == "" || this.accountNumber == undefined){
      this.payOutValidation['accountNumber'] = 'Account No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountNumber'] = '';
    }
    if(this.bsb == "" || this.bsb == undefined){
      this.payOutValidation['bsb'] = 'BSB numberCannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['bsb'] = '';
    }

    const data = {
      accountNumber: this.accountNumber,
      bsb: this.bsb,
      location: this.selectedCountry
    }
    console.log(data);
    this.submitPayoutDetails(data)
  }
 
}

  submitPayoutDetails(data){
    this.userService.submitPayoutDetails(data).subscribe(
      response => {
          ////////console.log.log.log(response);
          document.getElementById('model-closepayout').click();
      },
      err => {
      
      
          //this.loading=false;
          ////////console.log.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

  ageCalculator(){
      var year = new Date().getFullYear();
      return year - this.bornYear
  }


}
