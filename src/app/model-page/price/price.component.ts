import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {MessageService} from 'primeng/api';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/http.service';
@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css'],
  providers: [MessageService],
  encapsulation: ViewEncapsulation.None,
})
export class PriceComponent implements OnInit {
  price:string;
  LiveCamprice:string;
  Videoprice:string;
  loading:boolean;
  isLoading:boolean = true;
  message:string;

  passwordValidation: any =[];
  newPassword :string;
  oldPassword :string;
  confirmCheck:string;
  private formDataReset = new FormData();
  loading2:boolean;
  activateDeactivate:string;
  currentEmail :string;

  mid = localStorage.getItem('profileUrl');

  constructor(private data: DataService, private router: Router,private userService:UserService,private messageService: MessageService) { }

  ngOnInit() {
    this.getModelPrice();
    // this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message);
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    // this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
  }


  priceSetSubscription(){
    if(Number(this.price) < 5){
      this.price = '5';
    }
    if(Number(this.price) > 180){
      this.price = '180';
    }
  }

  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }


  priceSetvideo(){
    //alert("rl")
    if(Number(this.LiveCamprice) < 5){
      this.LiveCamprice = '5';
    }
    if(Number(this.LiveCamprice) > 180){
      this.LiveCamprice = '180';
    }
  }

  priceSetlivevideo(){
    //alert("rl")
    if(Number(this.Videoprice) < 1){
      this.Videoprice = '1';
    }
    if(Number(this.Videoprice) > 180){
      this.Videoprice = '180';
    }
  }

  getModelPrice(){
    this.userService.getAccoutPrice().subscribe(
      response => {
          //console.log(response);
          this.price = response['data'][0]['price']
          this.LiveCamprice = response['data'][0]['liveCamPrice']
          this.Videoprice = response['data'][0]['video_price'];
          this.isLoading =false
      },
      err => {

      }
    );
  }

  submitData(){
    this.loading = true;
    const data ={
      price:this.price,
      video_price:this.Videoprice,
      liveCamPrice:this.LiveCamprice,
    }
    this.userService.updateModelPrices(data).subscribe(
      response => {
        this.loading = false;
        this.messageService.add({severity:'success', summary: 'success', detail:'Price Updated'});

      },
      err => {

      }
    );
  }


  hideErrors(){
    // alert("r")
     document.getElementById('openPasswordrest').click();
     //document.getElementById('oldPasswordField').innerHtml = "";
     this.passwordValidation['result'] = "";
     this.passwordValidation['oldpassword'] = "";
     this.passwordValidation['newpassword'] = "";
     this.passwordValidation['confirmPassword'] = "";
     this.newPassword = "";
     this.oldPassword ="";
     this.confirmCheck =""
   }
   
   onSubmitPasswordReset(){
     if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
       //console.log("here");
       this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
       return false;
     }
     else{
       this.passwordValidation['oldpassword'] = "";
     }
     if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
       //console.log("here");
       this.passwordValidation['newpassword'] = "New password field cannot be blank";
       return false;
     }
     else{
       this.passwordValidation['newpassword'] = "";
     }
     if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
       //console.log("here");
       this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
       return false;
     }
     else{
       this.passwordValidation['confirmPassword'] = "";
     }
     this.formDataReset.append('newPassword',this.newPassword);
     this.formDataReset.append('oldPassword',this.oldPassword);
     this.loading2=true;
     this.userService.ResetPassword(this.formDataReset).subscribe(
       response => {
         if(response['success']==1){
           this.passwordValidation['result'] = "";
           //$('#myModal').modal('hide')
           document.getElementById("model-close").click();
           this.loading2=false;
           this.newPassword= "" ;
           this.oldPassword= "";
           this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
           //this.passwordValidation['result']=""
         }
         if(response['success']==0){
           this.passwordValidation['result'] = "Old Password does not matched";
           this.loading2=false;
         }
         if(response['success']==2){
           this.passwordValidation['result'] = "Some error occured please try after sometime";
           this.loading2=false;
         }
         
       },
       err => {
        
           this.passwordValidation['result'] = "password must be of 6 chracter";
        
           //this.loading=false;
           console.log(err);
           //alert("Something Went Wrong")
       }
     );
   }
 
 onConfirm() {
     this.messageService.clear('c');
 }
 
 onReject() {
     this.messageService.clear('c');
 }
 
 checkDeactivate(){
   this.userService.getModelDeactivateStatus().subscribe(
     response => {
       console.log(response);
       this.activateDeactivate = response['data']['accountStatus'];
       document.getElementById('DeactivateDeleteButton').click();
     },
     err => {
   
     }
   );
 }
 
 setDeactivate(type){
   const types={
     type:type
   }
   if(type == 2){
     document.getElementById('closeDeactivatePopup').click();
     document.getElementById('model-close2').click();
    
     this.router.navigate(['/logout']);
   }
   this.userService.setDeactivate(types).subscribe(
     response => {
       console.log(response);
       if(type == 1){
         this.activateDeactivate = type;
       }
       else{
         this.activateDeactivate = type;
       }
       if(type == 2){
         document.getElementById('closeDeactivatePopup').click();
         document.getElementById('model-close2').click();
        
         this.router.navigate(['/logout']);
       }
      
       document.getElementById('closeDeactivatePopup').click();
     },
     err => {
   
     }
   );
 }
 
   delete(){
     this.activateDeactivate = '2';
   }

}
