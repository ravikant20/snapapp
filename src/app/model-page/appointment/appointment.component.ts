import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/http.service';
import {DomSanitizer} from "@angular/platform-browser";
import { WindowState } from '@progress/kendo-angular-dialog';
import { AngularFireModule, } from 'angularfire2';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import {MessageService} from 'primeng/api';
import { DataService } from 'src/app/data.service';
import { environment } from '../../../environments/environment';

interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css'],
  providers: [MessageService],
  encapsulation: ViewEncapsulation.None,
})
export class AppointmentComponent implements OnInit {

  dataToshow:any=[];
  public windowState: WindowState = 'default';
  formUrl23= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton2.php');
  public openeds = false;
  IdUrl3;
  idTochat;
  message:string;
  imageChange:string; 
  currentEmail:string;
  profileUrlData:any;
  passwordValidation: any =[];
  newPassword :string;
  oldPassword :string;
  confirmCheck:string;
  private formDataReset = new FormData();
  loading2:boolean;
  activateDeactivate:string;


  payOutValidation:any=[];
  NamePayout:string;
  routingNo:string;
  accountNumber:string;
  accountType:string;
  swiftCode:string;
  IBAN:string;
  selectedCountry:any=[];
  country: any=[];
  isLoading:boolean = true;
  IdUrl;


  postsColChat: AngularFirestoreCollection<Post>;
  postsChat: Observable<Post[]>;


  startedLiveChat:boolean=false;
  LiveBroadCastingChatId :string;
  LiveBroadCastingChatData :any = [];
  inputChatBroadcast:any;
  
  activeBooking:any = [];
  pastBooking:any = [];

  imageDisplay:string;

  mid = localStorage.getItem('profileUrl');
  constructor(private router: Router,private userService:UserService,private domSanitizer : DomSanitizer,private afs: AngularFirestore,private messageService: MessageService,private data: DataService) {
    // this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl('https://enacteservices.net/snapapp/livebroadcasting/oneToOne.php#'+(Math.random() * 100).toString().replace('.', ''));
    this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message)
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl('');
    this.profileUrlData = localStorage.getItem('profileUrl');
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
    ];  
   }


  ngOnInit() {
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.notification();

    if(localStorage.getItem('profileImage') != '0' ){
      this.imageDisplay = localStorage.getItem('profileImage');
    }
    else{
      this.imageDisplay = 'assets/images/dummy-profile.png'
    }


  }

  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});

  }


  notification(){
    this.userService.AppointmentNotification().subscribe(
      response => {
        this.isLoading = false;
       ////console.log.log(response);
       this.dataToshow=response['data'][0];
        for(var i = 0;i<this.dataToshow.length;i++){
          if(this.dataToshow[i]['passed'] == 'no'){
            this.activeBooking.push(this.dataToshow[i])
          }
          else{
            this.pastBooking.push(this.dataToshow[i])
          }

        }
        
       ////console.log.log("sadf",this.dataToshow);
       this.isLoading = false;
      },
      err => {
        this.isLoading = false;
        //console.log.log(err);
      }
    );
  }

  changeStatusAppointment(NotId,status,i){
    // alert("v")
    // alert(NotId);
     //alert(status);
    if(status == 1){
      var stat = '1';
      //alert("qw")
    }
    else{
      var stat = '2';
      //alert("qwert")
    }
    const data = {
      not_id : NotId,
      status : stat
    }
    this.userService.ChangeNotificationStatus(data).subscribe(
      response => {
       //console.log.log(response);
       if(response['success'] == 1){
        if(status ==1){
          var sta = 1;
        }
        else if(status == 0){
          var sta = 2;
        }
        this.dataToshow[i]['confirmation'] = sta;
       }
       //this.dataToshow=response['data'];
       ////console.log.log("sadf",this.dataToshow);
      },
      err => {
        //console.log.log(err);
      }
    );
  }

  goLiveOneToOne(user_id,id,liveUrlId,minute,day){
    var seconds = new Date().getTime() / 1000;
    // //console.log.log(seconds)
    // //console.log.log(day)
    if(Number(day)>seconds){
      alert("You are Too Early for this broadcast wait till time comes. ");
      return false;
    }
    if(Number(day) + 300 < seconds){
      alert("Unfortunately, you are late. We have cancelled the session and initiated the refund to the customer");
      return false;
    }
    
    //if((seconds + 300) < )
    //this.openeds = true;
    document.getElementById('OpenGoLivePopup2').click();
    var ids=localStorage.getItem('id');
    document.cookie = "id="+ids; 
    document.cookie = "name="+localStorage.getItem('name'); 
    document.cookie = "user_id="+user_id; 
    document.cookie = "role=model"; 
    document.cookie = "type=oneWay"; 
    document.cookie = "minute="+minute; 
    this.idTochat = user_id;
    this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/live/oneToOne.php#'+liveUrlId);
    this.LiveBroadCastingChatId = liveUrlId
    this.getChatForLiveBroadCast();

  }
  getChatForLiveBroadCast(){

    var ChatGroupRefrence = this.afs.collection('livebroadcastingChat');
    var that =this;
    this.postsColChat =ChatGroupRefrence.doc(this.LiveBroadCastingChatId).collection('chats', ref => ref.orderBy("time","asc"));
        that.postsChat = that.postsColChat.valueChanges();
        that.postsChat.subscribe(res => {
          this.LiveBroadCastingChatData = res;
        })

  }

  submitChatBroadcast(){
    // this.LiveBroadCastingChatData = "";
    var chatReference =  this.afs.collection('livebroadcastingChat').doc(this.LiveBroadCastingChatId);
    var messageReference = chatReference.collection('chats');
    if(this.inputChatBroadcast.trim() == ""){
      return false;
    }
    if(localStorage.getItem('profileImage') != '0' ){
      var image = localStorage.getItem('profileImage');
    }
    else{
      var image = 'assets/images/dummy-profile.png'
    }
    var timestamp = firebase.firestore.Timestamp.now();
    messageReference.add({'message': this.inputChatBroadcast, 'sender': localStorage.getItem('id'),'image': image,'time':timestamp,'name':localStorage.getItem('name')});
    // this.LiveBroadCastingChatData = "";
    this.inputChatBroadcast = ""
    //console.log(this.LiveBroadCastingChatData)
  }

  public openCloseq(isOpened: boolean) {
    if(localStorage.getItem('role') == '2')
    {
      // alert(this.idTochat)
         this.stopChat(this.idTochat)
    }
    this.IdUrl3 = this.domSanitizer.bypassSecurityTrustResourceUrl('');

  }
 stopChat(id:string){
   this.savechatLogs();
  // alert(localStorage.getItem('id'));
   var uid =id.toString();
      var notRefrence = this.afs.collection('notifications');
      notRefrence.doc(uid).collection("message", ref => ref.where('model_id', '==',localStorage.getItem('id') ))
         .get().toPromise().then(function(querySnapshot) {
         querySnapshot.forEach(function(doc) {
           /////console.log.log("doc.id",doc.id)
                    notRefrence.doc(uid).collection("message").doc(doc.id).update({
                    liveUrl : "",
                    liveUrlOneToOne:"",
                    stopTime : firebase.firestore.FieldValue.serverTimestamp(),
                    read:false,
                    message:localStorage.getItem('name')+" Was Live"
            }).then(function() {
                      // alert("SDF")
               })
               .catch(function(error) {

               });
           });
         })
}

savechatLogs(){
  var model_id = localStorage.getItem('OneToOnemodel_id');
  var user_id = localStorage.getItem('OneToOneuser_id');
  var start_time = localStorage.getItem('OneToOnestart_time');
  var type = localStorage.getItem('OneToOnetype');
  var minute = localStorage.getItem('OneToOneminute');
  var status = localStorage.getItem('OneToOnestatus');

  if(status == '0'){
    var data = {
      model_id:model_id,
      user_id:user_id,
      start_time:start_time,
      type:type,
      minute:minute,
      end_time:Math.floor((new Date()).getTime() / 1000)
    }
    this.apiLiveChatLogs(data);
    localStorage.setItem('OneToOnestatus','1');
  }
  else{
    localStorage.removeItem('OneToOnemodel_id');
    localStorage.removeItem('OneToOneuser_id');
    localStorage.removeItem('OneToOnestart_time');
    localStorage.removeItem('OneToOnetype');
    localStorage.removeItem('OneToOneminute');
    localStorage.removeItem('OneToOneminute');
    localStorage.removeItem('OneToOnestatus');
  }
}

apiLiveChatLogs(data){
  this.userService.LiveChatLogs(data).subscribe(
    response => {
     //console.log.log(response);
    },
    err => {
      //console.log.log(err);
    }
  );
}

hideErrors(){
  this.passwordValidation['result'] = "";
  this.passwordValidation['oldpassword'] = "";
  this.passwordValidation['newpassword'] = "";
  this.passwordValidation['confirmPassword'] = "";
  this.newPassword = "";
  this.oldPassword ="";
  this.confirmCheck =""
}

onSubmitPasswordReset(){
  if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
    ////console.log.log("here");
    this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
    return false;
  }
  else{
    this.passwordValidation['oldpassword'] = "";
  }
  if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
    ////console.log.log("here");
    this.passwordValidation['newpassword'] = "New password field cannot be blank";
    return false;
  }
  else{
    this.passwordValidation['newpassword'] = "";
  }
  if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
    ////console.log.log("here");
    this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
    return false;
  }
  else{
    this.passwordValidation['confirmPassword'] = "";
  }
  this.formDataReset.append('newPassword',this.newPassword);
  this.formDataReset.append('oldPassword',this.oldPassword);
  this.loading2=true;
  this.userService.ResetPassword(this.formDataReset).subscribe(
    response => {
      if(response['success']==1){
        this.passwordValidation['result'] = "Password changed successfully";
        //$('#myModal').modal('hide')
        document.getElementById("model-close").click();
        this.loading2=false;
        this.newPassword= "" ;
        this.oldPassword= "";
        this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
        //this.passwordValidation['result']=""
      }
      if(response['success']==0){
        this.passwordValidation['result'] = "Old Password does not matched";
        this.loading2=false;
      }
      if(response['success']==2){
        this.passwordValidation['result'] = "Some error occured please try after sometime";
        this.loading2=false;
      }
      
    },
    err => {
     
        this.passwordValidation['result'] = "password must be of 6 chracter";
     
        //this.loading=false;
        //console.log.log(err);
        //alert("Something Went Wrong")
    }
  );
}

onConfirm() {
  this.messageService.clear('c');
}

onReject() {
  this.messageService.clear('c');
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      //console.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      //console.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}

convertDateUtc(date,type){
  var dates = new Date(date * 1000) ;
  if(type == "weekday"){
    var dates = new Date(date * 1000) ;
    var weekday=new Array(7);
    weekday[0]="Sun";
    weekday[1]="Mon";
    weekday[2]="Tue";
    weekday[3]="Wed";
    weekday[4]="Thu";
    weekday[5]="Fri";
    weekday[6]="Sat";
    //console.log.log(date)
   return weekday[dates.getDay()];
    
  }
  else if(type == "day"){
    return dates.getDate()
  }
  else if(type == "start_time"){
    return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  }
  else if(type == "end_time"){
    return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  }
  else if(type == "year"){
    return dates.getFullYear()
  }
  else if(type == "month"){
    return dates.getMonth()+1
  }
  else if(type == "monthShort"){
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
      ];
      return monthNames[dates.getMonth()]
  }
}

}
