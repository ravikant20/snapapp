import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontapplicationComponent } from './frontapplication.component';

describe('FrontapplicationComponent', () => {
  let component: FrontapplicationComponent;
  let fixture: ComponentFixture<FrontapplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontapplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontapplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
