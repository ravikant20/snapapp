import { Component, ViewChild, OnInit,ElementRef, ViewEncapsulation } from '@angular/core';
import { DataService } from "../../data.service";
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { UserService } from 'src/app/http.service';
import {MessageService} from 'primeng/api';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import * as S3 from 'aws-sdk/clients/s3';
@Component({
  selector: 'app-frontapplication',
  templateUrl: './frontapplication.component.html',
  styleUrls: ['./frontapplication.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class FrontapplicationComponent implements OnInit {
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  @ViewChild('fileUploader', {static: false}) fileUploader:ElementRef;
  message:string;
  imageChange:string; 
  currentEmail:string;
  nameMain:string ;
  bornMonth:string;
  bornYear:any ; 
  bornDate:string; 
  phone:string;
  price:string="10"; 
  email:string;
  residence:string="US";
  userName:string;
  private formData = new FormData();
  form: FormGroup;
  profileWebsite1:string='';
  profileWebsite2:string='';
  legalName:string;
  DateToshow:any ;
  yearToshow:any ;
  monthToshow:any ;
  loading:boolean = false;
  id_type:string ;
  //userService: any;
  idNumber:string;
  issuedBy:string;
  Aliases1:string;
  Aliases2:string;
  Aliases3:string;
  Incomplete:boolean=true;
  uploadFile:string;
  selectedFile:any =[];
  iAgreeCheckBox:boolean=true;
  CheckBox2:boolean=false;
  Videoprice:string;
  LiveCamprice:string;
  uploadFileSelfie:string;
  selectedFileSelfie:any=[];
  IncompleteSelfie:boolean=true;
  fd = new FormData();
  PhotoId:string;
  PhotoSelfie:string;
  imageSrc: string | ArrayBuffer;
  imageSrc2:string | ArrayBuffer;
  imageSrc3:string | ArrayBuffer;
  idPhoto:boolean=false;
  idSelfie:boolean=false;
  agreementModel:any;
  profileUrl:any;
  profileUrlData:any;
  date1: any =  new Date();
  passwordValidation3: any =[];
  newPassword2 :string;
  oldPassword2 :string;
  confirmCheck2:string;
  private formDataReset = new FormData();
  loading2:boolean;
  selectedCountry:any=[];
  country: any=[];
  activateDeactivate:string;

  myFormPay: NgForm;
  payOutValidation:any=[];
  NamePayout:string;
  routingNo:string;
  accountNumber:string;
  accountType:string;
  swiftCode:string;
  IBAN:string;
  isLoading:boolean =true;
  bsb:string;
  minDate :Date;
  
  mid = localStorage.getItem('profileUrl');
  loading3:boolean=false;
  loading4:boolean=false;
  files: any;
  imagecCheck:Number = 0 ;
  PhotoIDCheck:Number = 0 ;
  SelfieCheck:Number = 0 ;


  myModelContent:any =[];
title:string ="";
uploadAs:string;
type:string;
selectedFile2: any = [];
uploadedPercentage = 0;
showMessage = false;
message2: string = '';
showTo:string ='2' ;
noOfUpload:string = '0';
uploadFile2:string;
rootUrl: string;
editmode:boolean=true;
loading33:boolean;
myError:any =[];
messages:string;
date7:string;
date8=new Date();
date9:string;
showToMain:string ='1';
dateTime = new Date();
video:SafeResourceUrl;

imgsrc:any;
contentLength : Number;

timeSelected:any;
counter:any;
value: number = 0;
totalSize:any = 0;
uploadedValue:any = 0;
loaded:any = [];
hideprogress:boolean=false;
uploadCounter:any = 0;
street_number:string;
postal_code:string;
street_address:string;
city:string;
county:string;
suburb:string;
  constructor(private data: DataService, private router: Router,private userService:UserService,private messageService: MessageService) {
    //alert(this.mid)
   }

  ngOnInit() {
    if(localStorage.getItem('status') !="0"){
      this.router.navigate(['/login']);
    }
    this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message)
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.getModelData();
    this.getYear();
    this.getModelMedia();

    this.profileUrlData = localStorage.getItem('profileUrl');
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
  ]; 
    this.minDate = new Date();
    this.checkpayout();
  }
  editRemove(){
    this.editmode = true;
  }
  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }

  getImage(): any{
    //console.log("ra")
    // for(var i=0; i<this.myModelContent.length;i++){
    //   //console.log(this.myModelContent)
    //   if(this.myModelContent[i]['mediaType'] == 2 || this.myModelContent[i]['mediaType'] == 5){
    //     //console.log(i)
    //       var video = document.getElementById('videoId'+i) as HTMLCanvasElement;var a  = 400;var b = 400;
    //       var canvas = document.getElementById('canvas'+i) as HTMLCanvasElement;
    //       canvas.getContext('2d').drawImage(video,2,3);
     
    //   }
    // }
  }
  getModelMedia(){
    this.userService.getModelData().subscribe(
      response => {
       if(response['success']==1){
         //console.log(response['data']);

        this.myModelContent = response['data'];
        this.contentLength = response['data'].length
        // alert(response['data'].length)
        // console.log(this.myModelContent)
        if(this.myModelContent){
          setInterval(()=>{    //<<<---    using ()=> syntax
            this.getImage();
       }, 4000);
          // setTimeout(,100000);
        }
        this.isLoading =false;
       }
       else{
        this.isLoading =false;
       // console.log(response);
       }
        
      },
      err => {
        this.isLoading =false;
        //this.router.navigate(['/logout']);
       // console.log(err);
      }
      
    );
   
  }
  changeUploadType(value){
    //alert(value);
    if(value==5){
      this.type="video/*";
    }
    else if(value==2){
      this.type="video/*";
    }
    else if(value==1){
      this.type="image/png, image/jpeg";
    }
  }
  getModelData(){
    this.userService.getAccoutDetail().subscribe(
      response => {
       ////////console.log.log.log(response);
       if(response['success']==1){
         if(response['data'][0]['terms'] == 1){
          this.iAgreeCheckBox = true;
          this.CheckBox2 = true;
         }
         ////////console.log.log.log(response);
         this.nameMain = this.nullRemover(response['data'][0]['model_name']);
         this.email = this.nullRemover(response['data'][0]['model_email']);
         this.phone = this.nullRemover(response['data'][0]['modelPhone']);
         this.legalName = this.nullRemover(response['data'][0]['legal_name']);
         this.idNumber = this.nullRemover(response['data'][0]['id_number']);
         this.idNumber = this.nullRemover(response['data'][0]['id_number']);
         this.issuedBy = this.nullRemover(response['data'][0]['issuedBy']);
         this.imageSrc3 = this.nullRemover(response['data'][0]['profileImage']);
         if(this.imageSrc3 == ""){
          this.imageSrc3 =" http://placehold.it/180";
         }
         else{
          this.imageSrc3 = this.nullRemover(response['data'][0]['profileImage']);
         }
         this.profileWebsite1 = this.nullRemover(response['data'][0]['profile_url']);
         this.street_number =this.nullRemover(response['data'][0]['street_number']);
         this.postal_code = this.nullRemover(response['data'][0]['postal_code']);
         this.street_address =this.nullRemover(response['data'][0]['street_address']);
         this.city =this.nullRemover(response['data'][0]['city']);
         this.county=this.nullRemover(response['data'][0]['county']);
         this.suburb =this.nullRemover(response['data'][0]['suburb']);
         if(response['data'][0]['profileImage'] != ""){
           this.imagecCheck = 1
         }
         //console.log(response['data'][0]['id_expiry'] )
         if(response['data'][0]['id_expiry'] == null){
          this.minDate = new Date();
         }
         else{
          this.date1 = new Date(response['data'][0]['id_expiry'])
         }
         
         this.Videoprice = response['data'][0]['video_price'] 
         this.LiveCamprice= response['data'][0]['liveCamPrice']

         if(response['data'][0]['photo_id'] == null){
          this.Incomplete =true;

         }
         else{
          this.Incomplete =false;
          this.PhotoIDCheck = 1 ;
          this.PhotoId=response['data'][0]['photo_id']
          
         } 
         if(response['data'][0]['photo_selfie'] == null){
          this.IncompleteSelfie =true;
         }
         else{
          this.IncompleteSelfie =false;

          this.SelfieCheck = 1 ;
          this.PhotoSelfie=response['data'][0]['photo_selfie'];
         }  
        // alert(this.id_type)
         if( response['data'][0]['id_type'] == null){
          this.id_type =undefined;
         }
         else{
          this.id_type =response['data'][0]['id_type'];
         // alert(this.id_type)
         }
         
         //alert(this.id_type)
         if(response['data'][0]['Dob']!=null){
          let x = response['data'][0]['Dob'].split("/");
          ////////console.log.log.log(x);
          this.bornMonth = x[1];
          //alert(x[1])
          this.bornYear = x[2];

          this.bornDate = x[0];
          this.changedMonth();
          
          //////////console.log.log.log(this.changedMonth());
         }
         
         if(response['data'][0]['price']!=null){
          this.price = response['data'][0]['price']
         }
         else{
          this.price = '10';
         }
         if(response['data'][0]['residence']!=null){
          this.residence = response['data'][0]['residence']
         }
         else{
          this.residence = 'US';
         }
         this.userName = this.nullRemover(response['data'][0]['social_id']);
       }
       else{
         ////////console.log.log.log(response);
       }
        this.isLoading = false;
      },
      err => {
       // this.router.navigate(['/login']);
        ////////console.log.log.log(err);
        this.isLoading = false;
      }
    );
  }



  accountrtab(){
    document.getElementById("documentTab0").click()
    //alert("rk")
  }


  nullRemover(dat){
    if(dat == null || dat == 'null'){
      return ''
    }
    else{
      return dat;
    }
  }


  getYear(){
    this.yearToshow= [];
    var year = (new Date()).getFullYear() - Number(18);
    ////////console.log.log.log(year);
    for(var i=year;i>=Number(year) - Number(30); i--){
      let newName = {
        id:i.toString(),
     };
     this.yearToshow.push(newName)
    }

  }
  changedMonth(){
    //alert(this.bornMonth);
    this.DateToshow=[];
    if(this.bornMonth=="1" || this.bornMonth=="3" || this.bornMonth=="5" ||  this.bornMonth=="7" || this.bornMonth=="8" ||  this.bornMonth=="10"||  this.bornMonth=="12"){
      for(var i=1;i<=31; i++){
        let newName = {
          id:i.toString(),
       };
      // ////////console.log.log.log(newName);
       this.DateToshow.push(newName)
      }
    }
    else if(this.bornMonth=="4" || this.bornMonth=="6" || this.bornMonth=="9" ||  this.bornMonth=="1" ){
      for(var i=1;i<=30; i++){
        let newName = {
          id:i.toString(),
       };
       this.DateToshow.push(newName)
      }
    }
    else if(this.bornMonth=="2" && Number(this.bornYear) % Number(4) == 0){
      for(var i=1;i<=29; i++){
        let newName = {
          id:i.toString(),
       };
       this.DateToshow.push(newName)
      }
    }
    else{
      for(var i=1;i<=28; i++){
        let newName = {
          id:i.toString(),
       };
       this.DateToshow.push(newName)
      }
    }
    
    //////////console.log.log.log(this.DateToshow)
  }



  submitData(){
    //alert("form")
    //alert(this.nameMain)
     
    this.fd.append('name', this.nameMain);
    this.fd.append('phone', this.phone);
    this.fd.append('dateOfBirth',this.bornDate +'/'+this.bornMonth+'/'+this.bornYear);
    this.fd.append('email', this.email);
    this.fd.append('residence', this.residence);
    // this.fd.append('price', this.price);
    this.fd.append('social_id', this.userName);
    this.fd.append('profile_url', this.profileWebsite1);
    this.fd.append('legal_name', this.legalName);
    this.fd.append('id_type', this.id_type);
    this.fd.append('idNumber', this.idNumber);
    this.fd.append('issuedBy', this.issuedBy);
    this.fd.append('Aliases1', this.Aliases1);
    this.fd.append('Aliases2', this.Aliases2);
    this.fd.append('Aliases3', this.Aliases3);
    this.fd.append('Aliases1', this.Aliases1);
    this.fd.append('profilePic',this.files);


    this.fd.append('street_number', this.street_number);
    this.fd.append('postal_code', this.postal_code);
    this.fd.append('street_address', this.street_address);
    this.fd.append('city', this.city);
    this.fd.append('county', this.county);
    this.fd.append('suburb', this.suburb);
    // this.fd.append('video_price',this.Videoprice);
    // this.fd.append('liveCamPrice',this.LiveCamprice);
    this.fd.append('expirydate',this.date1);

    //////////console.log.log.log(this.selectedFile)
   
   
    
    this.formsubmit(this.fd)
    //////////console.log.log.log(formValue);
  }

  formsubmit(formValue){
   var check = this.fullValidation();
   //alert(check);
   if(check == false){
     return false;
   }
    if(this.CheckBox2==false){
      alert("please accept our terms")
      return false;
    }
    //alert(this.iAgreeCheckBox); alert(this.CheckBox2)
    this.loading=true;
    this.userService.updateModelData(formValue).subscribe(
      response => {
        document.getElementById('documentTab3').click();
        this.scrollToTop();
        
        var d = document.getElementById("reviewactive");
        d.className += " active";
        var d = document.getElementById("contentactive");
        d.className += " active";
       ////////console.log.log.log(response);
       //this.isLoading=true;
       if(response['success']==1){
         ////////console.log.log.log(response);
         this.loading=false;
       //  alert(this.nameMain);
         this.data.changeMessage(this.nameMain);
         localStorage.setItem("name",this.nameMain);
         this.messageService.add({severity:'success', summary: 'success', detail:'We have received your profile. We will review and post you updates once your profile is approved.'});
         // this.isLoading=false;
       }
       else{
         ////////console.log.log.log(response);
         this.loading=false;
         this.messageService.add({severity:'error', summary: 'error', detail:'Something went Wrong Please Try After SomeTime'});
         //this.isLoading=false;
         //alert("No models Found");
       }
        
      },
      err => {
        //this.router.navigate(['/login']);
        this.messageService.add({severity:'error', summary: 'error', detail:'The Email Is already has been taken'});
        ////////console.log.log.log(err);
        this.loading=false;
      }
    );
    
  }
  customTrackBy(index: number, obj: any): any {
    return index;
  }


  scrollToTop() {
    (function smoothscroll() {
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
            window.requestAnimationFrame(smoothscroll);
            window.scrollTo(0, currentScroll - (currentScroll / 8));
            // element.classList.add("newClass");
          
        }
    })();
}
myFileClick(){
  //alert("myFileClick()");
  document.getElementById('myfile2').click();
  
}
readURL(event) 
{ 
  if (event.target.files && event.target.files[0]) 
    { const file = event.target.files[0]; 
      const reader = new FileReader(); 
      reader.onload = e => this.imageSrc3 = reader.result; 
      this.files = event.target.files[0];
      reader.readAsDataURL(file); 
      this.imagecCheck = 1;
      // if(this.files['name'] !='undefined' ){
      //   alert("sddddd")
      // }
      //console.log(this.files)
    } 
}
changeTab(){
 
  if(this.imagecCheck != 1){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Profile picture cannot be empty'});
    return false;
  }
  if(this.nameMain == "" || this.nameMain == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Name Field Cannot be Empty'});
    return false;
  }
  if(this.phone == "" || this.phone == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Phone Field Cannot be Empty'});
    return false;
  }
  if(this.bornYear == "" || this.bornYear == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Year SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornMonth == "" || this.bornMonth == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Month SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornDate == "" || this.bornDate == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Date SelectBox Cannot be Empty'});
    return false;
  }
  if(this.email == "" || this.email == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Field Cannot be Empty'});
    return false;
  }
  var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
  if(!pattern.test(this.email)){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Not Valid'});
    return false;
  }

  if(this.userName == "" || this.userName == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Add Your Social Account'});
    return false;
  }
  if(this.profileWebsite1 == "" || this.profileWebsite1 == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Profile URL Cannot be blank'});
    return false;
  }
  if(this.phone.length<7 || this.phone.length>13){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Phone number not valid'});
    return false;
  }
  if(this.street_number == "" || this.street_number == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Street Number Cannot be blank'});
    return false;
  }
  if(this.postal_code == "" || this.postal_code == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Postal Code Cannot be blank'});
    return false;
  }
  if(this.street_address == "" || this.street_address == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Street address Cannot be blank'});
    return false;
  }
  if(this.city == "" || this.city == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'City name Cannot be blank'});
    return false;
  }
  if(this.county == "" || this.county == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Country name Cannot be blank'});
    return false;
  }
  if(this.suburb == "" || this.suburb == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Suburb name Cannot be blank'});
    return false;
  }
  // if(this.profileWebsite2 == "" || this.profileWebsite2 == null){
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Profile Slug Cannot be blank'});
  //   return false;
  // }
  if(this.iAgreeCheckBox != true){
  //  alert(this.iAgreeCheckBox)
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Accept Our terms'});
    return false;
  }
  
  document.getElementById('documentTab').click();
  var d = document.getElementById("accountactive");
  d.className += " active";
  var d = document.getElementById("payoutactive");
  d.className += " active";
 
  //document.getElementById('accountactive').click();
  this.scrollToTop();
}

changeTab2(){
  this.payoutSubmit('next');
  
 
  //d.className += " active";
  // id="accountactive" 
  // id="payoutactive" 
  // id="documentactive"
  // id="reviewactive" 
}
changeTabPrevious(){
  document.getElementById('documentTab4').click();
  this.scrollToTop();
  document.getElementById("documentactive").classList.remove('active');
}
PreviousFromPayout(){
  this.payoutSubmit('prev');
 
}
PreviousFromReview(){
  document.getElementById('documentTab').click();
  this.scrollToTop();
  document.getElementById("contentactive").classList.remove('active');
}
changeTabToreview(){
      
      if(this.contentLength <3){
        this.messageService.add({severity:'error', summary: 'Error', detail:'Please Upload atleast of 3 content to proceed'});
        return false;
      }
      else{
        document.getElementById('documentTab2').click();
        this.scrollToTop();

        var d = document.getElementById("documentactive");
        d.className += " active";
      }
}
PreviousFromReview2(){
  document.getElementById('documentTab2').click();
  this.scrollToTop();
  document.getElementById("reviewactive").classList.remove('active');
}
OpenContentUpload(){
  document.getElementById('OpenContentUpload').click();
  this.showModal()
  //this.scrollToTop();
}


onFileSelected2(event) {
  var fileTypes = ["image/jpeg", "image/jpg", "image/png", "video/x-flv", "video/mp4", "video/mov", "video/avi", "video/mkv", "application/x-mpegURL", "video/MP2T", "video/3gpp", "video/quicktime", "video/x-msvideo", "video/x-ms-wmv"];
  const number = event.target.files.length;
  this.noOfUpload = event.target.files.length;
  for(var i=0;i<number;i++){
    this.selectedFile[i] = <File>event.target.files[i];

    var file = event.target.files[i];
    var fileReader = new FileReader();

    var mimeType = this.selectedFile[i].type;
    console.log(mimeType);
    var isSuccess = fileTypes.indexOf(mimeType) > -1;  
    if(!isSuccess){
      this.fileUploader.nativeElement.value = null;
      alert('Please selecte valid File');
      break;
    }
    
  }
}

onFileSelected(event) {
 // alert("rk");
  const number = event.target.files.length;
  this.selectedFile = <File>event.target.files[0]; 
  this.Incomplete =false;
  ////////console.log.log.log(this.selectedFile)
  this.fd.append('file[photo_id]', this.selectedFile, this.selectedFile.name);
  this.PhotoId="";
  this.PhotoIDCheck = 1 ;
  // const reader = new FileReader(); 
  // reader.onload = e => this.imageSrc = reader.result;
    const file = event.target.files[0]; 
    const reader = new FileReader(); 
    reader.onload = e => this.imageSrc = reader.result; 
    reader.readAsDataURL(file); 
      
}


onFileSelectedSelfie(event){
 // alert("ka");
  const number = event.target.files.length;
  this.selectedFileSelfie = <File>event.target.files[0]; 
  this.IncompleteSelfie =false;
  ////////console.log.log.log(this.selectedFileSelfie)
  this.fd.append('file[photo_Selfie]', this.selectedFileSelfie, this.selectedFileSelfie.name);
  this.PhotoSelfie="";
  this.SelfieCheck = 1
  
    const file = event.target.files[0]; 
    const reader = new FileReader(); 
    reader.onload = e => this.imageSrc2 = reader.result; 
    reader.readAsDataURL(file);
  
}

agreeCheckBox(){
  // alert("E")
  this.iAgreeCheckBox=true;
 // alert(this.iAgreeCheckBox)
}
checkBoxCheck(){
  this.CheckBox2=true;
}



fullValidation(){
  if(this.nameMain == "" || this.nameMain == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Name Field Cannot be Empty'});
    return false;
  }
  if(this.phone == "" || this.phone == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Phone Field Cannot be Empty'});
    return false;
  }
  if(this.bornYear == "" || this.bornYear == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Year SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornMonth == "" || this.bornMonth == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Month SelectBox Cannot be Empty'});
    return false;
  }
  if(this.bornDate == "" || this.bornDate == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Born Date SelectBox Cannot be Empty'});
    return false;
  }
  if(this.email == "" || this.email == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Field Cannot be Empty'});
    return false;
  }
  var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
  if(!pattern.test(this.email)){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Email Not Valid'});
    return false;
  }

  if(this.userName == "" || this.userName == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Add Your Social Account'});
    return false;
  }
  if(this.profileWebsite1 == "" || this.profileWebsite1 == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Profile Slug Cannot be blank'});
    return false;
  }
  // if(this.profileWebsite2 == "" || this.profileWebsite2 == null){
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Profile Slug Cannot be blank'});
  //   return false;
  // }
  // if(this.iAgreeCheckBox != true){
  //  // alert(this.iAgreeCheckBox)
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Please Accept Our terms'});
  //   return false;
  // }



  if(this.legalName == "" || this.legalName == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Please Fill Your Legal Name'});
    return false;
  }
  if(this.id_type == "" || this.id_type == null){
    //alert(this.iAgreeCheckBox)
    this.messageService.add({severity:'error', summary: 'Error', detail:'Id type Cannot Be Blank'});
    return false;
  }
  if(this.idNumber == "" || this.idNumber == null){
    this.messageService.add({severity:'error', summary: 'Error', detail:'Id Number Cannot be blank'});
    return false;
  }
  if(this.issuedBy == "" || this.issuedBy == null){
   // alert(this.iAgreeCheckBox)
    this.messageService.add({severity:'error', summary: 'Error', detail:'Issued By cannot Be Blank'});
    return false;
  }
  //this.PhotoIDCheck = 1 ;
  if(this.PhotoIDCheck != 1){
    // alert(this.iAgreeCheckBox)
     this.messageService.add({severity:'error', summary: 'Error', detail:'Photo Id cannot be empty'});
     return false;
   }
   if(this.SelfieCheck != 1){
    // alert(this.iAgreeCheckBox)
     this.messageService.add({severity:'error', summary: 'Error', detail:'Selfie cannot be empty'});
     return false;
   }
  ////////console.log.log.log(this.date1)
  if(this.date1 == "" || this.date1 == null){
    // alert(this.iAgreeCheckBox)
     this.messageService.add({severity:'error', summary: 'Error', detail:'Expiry date cannot be blank'});
     return false;
   }
  // if(this.Aliases1 == "" || this.Aliases1 == null){
  //   //alert(this.iAgreeCheckBox)
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Aliases cannot Be Blank'});
  //   return false;
  // }
  // if(this.Aliases2 == "" || this.Aliases2 == null){
  //  // alert(this.iAgreeCheckBox)
  //   this.messageService.add({severity:'error', summary: 'Error', detail:'Aliases cannot Be Blank'});
  //   return false;
  // }

  if(this.CheckBox2 != true){
    // alert(this.iAgreeCheckBox)
     this.messageService.add({severity:'error', summary: 'Error', detail:'Please Accept Our terms'});
     return false;
   }


}
onConfirm() {
  this.messageService.clear('c');
}

onReject() {
  this.messageService.clear('c');
}


showId(id){
  this.idPhoto =true;
  this.idSelfie =false;
  this.agreementModel ="type";
  //alert(id);
  if(id != ""){

    this.imageSrc = id;
  }
  document.getElementById('openModal').click();
}

showSelfie(selfie){
  this.idSelfie =true;
  this.idPhoto =false;
  this.agreementModel ="type";
  if(selfie != ""){
    this.imageSrc2 = selfie;
  } 
  document.getElementById('openModal').click();
}



agreement(type){
  //alert(type);
  this.agreementModel =type;
  this.idSelfie =false;
  this.idPhoto =false;
  document.getElementById('openModal').click();
  
}


hideErrors(){
  //alert("rk")
  this.passwordValidation3 = [];
  ////////console.log.log.log(this.passwordValidation3)
  this.newPassword2 = "";
  this.oldPassword2 ="";
  this.confirmCheck2 =""
  this.passwordValidation3['result'] = "";
  this.passwordValidation3['oldPassword'] = "Old password field cannot be blank wefrwefwafsdfws";
  this.passwordValidation3['newpassword'] = "";
  this.passwordValidation3['confirmPassword'] = "";
  document.getElementById('openPasswordrest').click();
}

onSubmitPasswordReset(){
  if(this.oldPassword2 =="undefined" || this.oldPassword2 ==undefined || this.oldPassword2 == ""){
    //////////console.log.log.log("here");
    this.passwordValidation3['oldpassword'] = "Old password field cannot be blank";
    return false;
  }
  else{
    this.passwordValidation3['oldpassword'] = "";
  }
  if(this.newPassword2 =="undefined" || this.newPassword2 ==undefined || this.newPassword2 == "") {
    //////////console.log.log.log("here");
    this.passwordValidation3['newpassword'] = "New password field cannot be blank";
    return false;
  }
  else{
    this.passwordValidation3['newpassword'] = "";
  }
  if(this.confirmCheck2 !="undefined" && this.confirmCheck2 != this.newPassword2) {
    //////////console.log.log.log("here");
    this.passwordValidation3['confirmPassword'] = "Confirm Pasword and New Password must be same";
    return false;
  }
  else{
    this.passwordValidation3['confirmPassword'] = "";
  }
  this.formDataReset.append('newPassword',this.newPassword2);
  this.formDataReset.append('oldPassword',this.oldPassword2);
  this.loading2=true;
  this.userService.ResetPassword(this.formDataReset).subscribe(
    response => {
      if(response['success']==1){
        this.passwordValidation3['result'] = "Password changed successfully";
        //$('#myModal').modal('hide')
        document.getElementById("model-close").click();
        this.loading2=false;
        this.newPassword2= "" ;
        this.oldPassword2= "";
        this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
        //this.passwordValidation['result']=""
      }
      if(response['success']==0){
        this.passwordValidation3['result'] = "Old Password does not matched";
      }
      if(response['success']==2){
        this.passwordValidation3['result'] = "Some error occured please try after sometime";
      }
      if(response['success']==3){
        this.passwordValidation3['result'] = "Some error occured please try after sometime";
      }
      
    },
    err => {
     
        this.passwordValidation3['result'] = "password must be of 6 chracter";
     
        //this.loading=false;
        //////////console.log.log.log(err);
        //alert("Something Went Wrong")
    }
  );
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      ////////console.log.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      //alert(this.activateDeactivate)
      if(this.activateDeactivate == "0"){
        //alert("y")
      }
      else if(this.activateDeactivate == "1"){
        //alert("y")
      }
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup2').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      ////////console.log.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup2').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup2').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}

checkpayout(){
  //alert("r")
  //getPayoutDetails(data){
    //
  this.userService.getPayoutDetails().subscribe(
      response => {
          ////////console.log.log.log(response);
          if(response['success'] == 0){
            // ////////console.log.log.log('')
              //document.getElementById('PayoutPopup').click();
              this.payOutValidation['NamePayout'] = '';
              this.payOutValidation['bankName'] = '';
              this.payOutValidation['accountType'] = '';
              this.payOutValidation['routingNo'] = '';
              this.payOutValidation['swiftCode'] = '';
              this.payOutValidation['IBAN'] = '';
          }
          else{
            this.selectedCountry = response['data']['location'];
            this.selectedCountry = this.country.find(country => country.name === this.selectedCountry);
            if(response['data']['location']== 'United States' ){
              this.routingNo = response['data']['routing_no']
              this.accountNumber = response['data']['account_no']
              this.accountType = response['data']['account_type']
            }
            else if(response['data']['location']!= 'United States' && response['data']['location'] != null && response['data']['location'] != 'Australia' && response['data']['location'] != 'New Zealand'){
              this.swiftCode = response['data']['swift_code']
              this.IBAN = response['data']['iban']
            }
            else if(response['data']['location']== 'Australia' && response['data']['location'] != null){
              this.bsb = response['data']['BSB']
              this.accountNumber = response['data']['account_no']
            }
            else{
              this.accountNumber = response['data']['account_no']
            }
            
            //this.selectedCountry = response['data']['location'];
          }
          //////////console.log.log.log("response['data']['location']",response['data']['location'])
      },
      err => {
       
       
          //this.loading=false;
          ////////console.log.log.log(err);
          //alert("Something Went Wrong")
      }
  );
  
  
}

openPayoutPopup(){
  document.getElementById('PayoutPopup').click();
}

payoutSubmit(v){

  if(this.selectedCountry['name'] == 'United States' && this.selectedCountry['name'] != null){

    if(this.routingNo == "" || this.routingNo == undefined){
      this.payOutValidation['routingNo'] = 'Routing No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['routingNo'] = '';
    }
    if(this.accountNumber == "" || this.accountNumber == undefined){
      this.payOutValidation['accountNumber'] = 'Account No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountNumber'] = '';
    }
    if(this.accountType == "" || this.accountType == undefined){
      this.payOutValidation['accountType'] = 'Account Type Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountType'] = '';
    }

   

        const data = {
          routingNo : this.routingNo,
          accountNumber: this.accountNumber,
          accountType: this.accountType,
          location: this.selectedCountry
        }
        ////////console.log.log.log(data)
      //alert("success")
      
      this.submitPayoutDetails(data,v)
  }
  else if(this.selectedCountry['name']!= 'United States' && this.selectedCountry['name'] != null && this.selectedCountry['name'] != 'Australia' && this.selectedCountry['name'] !='New Zealand'){
    //alert(this.selectedCountry)
    if(this.swiftCode == "" || this.swiftCode == undefined){
      this.payOutValidation['swiftCode'] = 'Swift Code Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['swiftCode'] = '';
    }
    if(this.IBAN == "" || this.IBAN == undefined){
      this.payOutValidation['IBAN'] = 'IBAN Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['IBAN'] = '';
    }
   
      const data = {
        swiftCode : this.swiftCode,
        IBAN: this.IBAN,
        location: this.selectedCountry
      }
      ////////console.log.log.log(data)
      this.submitPayoutDetails(data,v)
  }

  else if(this.selectedCountry['name'] == 'New Zealand'){
    //alert("here");
    if(this.accountNumber == "" || this.accountNumber == undefined){
      this.payOutValidation['accountNumber'] = 'Account No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountNumber'] = '';
    }
    const data = {
      accountNumber: this.accountNumber,
      location: this.selectedCountry
    }
    //console.log(data);
    this.submitPayoutDetails(data,v)
  }
  else{
    //alert("there");
    if(this.accountNumber == "" || this.accountNumber == undefined){
      this.payOutValidation['accountNumber'] = 'Account No Cannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['accountNumber'] = '';
    }
    if(this.bsb == "" || this.bsb == undefined){
      this.payOutValidation['bsb'] = 'BSB numberCannot Be Blank';
      return false;
    }
    else{
      this.payOutValidation['bsb'] = '';
    }

    const data = {
      accountNumber: this.accountNumber,
      bsb: this.bsb,
      location: this.selectedCountry
    }
    //console.log(data);
    this.submitPayoutDetails(data,v)
  }
 
}

  submitPayoutDetails(data,type){
    if(type == "prev"){
      this.loading4 =true;
     
    }
    else{
      this.loading3 =true;
     
    }
    ////////con
    this.userService.submitPayoutDetails(data).subscribe(
      response => {
        //this.loading3 =false;
        document.getElementById('model-closepayout').click();
          if(type == "prev"){
            this.loading4 =false;
            document.getElementById('documentTab0').click();
            this.scrollToTop();
            document.getElementById("payoutactive").classList.remove('active');
          }
          else{
            this.loading3 =false;
            document.getElementById('documentTab4').click();
            this.scrollToTop();
            var d = document.getElementById("contentactive");
            d.className += " active";
          }
          ////////console.log.log.log(response);
          this.loading4 =false;
          this.loading3 =false;
         
      },
      err => {
      
        this.loading4 =false;
        this.loading3 =false;
          //this.loading=false;
          ////////console.log.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

  ageCalculator(){
      var year = new Date().getFullYear();
      return year - this.bornYear
  }


  submitMediaUpload(time){
    this.editmode=false;
    this.loaded =[];
    var total = this.selectedFile.length;
    console.log(this.selectedFile);
    // return;
		for(var i=0;i<this.selectedFile.length;i++){
      // console.log("this.selectedFile[i]",this.selectedFile[i])
      this.totalSize = this.totalSize +this.selectedFile[i].size;
      this.loaded[this.selectedFile[i].name] = 0;
			this.uploadFiles(this.selectedFile[i],i,total);
    }
    console.log("this.totalSize",this.totalSize)
    this.timeSelected = time
    
	}
	

	uploadFiles(file,index,total) {
    this.hideprogress = true
    this.counter = 1;
    const contentType = file.type;
    this.loading= true;
		const bucket = new S3(
			{
				accessKeyId: 'AKIA4JXVAIZQ2G5B3T5Z',
				secretAccessKey: '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',
				region: 'ap-southeast-2'
			}
		);
		const params = {
			Bucket: 'yespipe',
			Key: 'video/' + file.name,
			Body: file,
			ACL: 'public-read',
			ContentType: contentType
		};
  
    var that = this;
    var i = index + 1;
    // var loadedTotal = 0;
		bucket.upload(params).on('httpUploadProgress', function (evt) {
      
      that.showMessage = true;
      var loadedTotal = 0;
      
      that.loaded[this.body.name] = evt.loaded;
      console.log('loaded Arr', that.loaded)
			for (var j in that.loaded) {
        console.log(j)
				loadedTotal += that.loaded[j];
      }
      console.log(loadedTotal)
      var progress = Math.round(loadedTotal / that.totalSize * 100);
      // var progress = (loadedTotal / that.totalSize) * 100;
      
      // console.log('progresss', progress,that.value)
      if(Number(progress) > Number(that.value)){
        that.value = parseInt(progress.toFixed(2));
      }
      if (loadedTotal === that.totalSize) {
        //successCallback(form, {files: resp.files, form: form.serialize()});
        that.value = 100;
      }
      if (loadedTotal >= that.totalSize) {
        //successCallback(form, {files: resp.files, form: form.serialize()});
        that.value = 100;
      }
      // if(progress > that.value){
      //   that.value = parseInt(progress.toFixed(2));
      // }
		}).send(function (err, data) {
			if (err) {
        // that.slimLoadingBarService.complete();
      }
        
     
      // var that = this;
      const fdata = {
        title:that.title,
        type:that.uploadAs,
        showTo:that.showTo,
      }
      var files=[];
      console.log(data)
      files.push(data.Location)
        
      fdata['file']=files;
      that.uploadCounter = that.uploadCounter + 1;
      fdata['uploader']="1"
      
      // console.log(fdata)
      if(that.timeSelected == 'time'){
        fdata['showOn']= new Date(that.formatDate(that.date7));
        // fd.append('showOn', new Date(this.formatDate(this.date7)).toUTCString());
      }
      else{
        fdata['showOn']= new Date(new Date(that.formatDate(new Date() )));
      }
      that.counter++;
      //that.value = 0
      console.log(fdata)
      that.userService.UploadContent(fdata).subscribe(
        response => {
        //  console.log(response);
         if(response['success']==1){
          this.loading= false;
              that.myModelContent = [];
              that.getModelMedia();
              
              
              
              
              console.log(total,that.uploadCounter);
              if(that.value  == 100 || total == that.uploadCounter){
                document.getElementById("model-closeContent").click();
                that.editmode=true;
                that.loading=false;
                that.value = 0;
                // that.uploadAs='1';
                that.selectedFile=[];
                that.showTo='2';
                that.noOfUpload='0';
                that.uploadFile=undefined;
                that.message="";
                that.date7 ="";
                that.type="image/png, image/jpeg";
                that.title='';
                that.hideprogress = false;
                that.selectedFile = [];
                that.loaded = [];
                that.totalSize=0;
                that.uploadCounter = 0;
              }
              // 
              
         }
         else{
          that.title='';
          that.uploadAs='1';
          that.selectedFile=[];
          that.showTo='2';
          that.noOfUpload='0';
          that.uploadFile=undefined;
          that.message="";
          that.date7 ="";
          that.type="image/png, image/jpeg";
          document.getElementById("model-closeContent").click();
          that.editmode=true;
          that.loading=false;
          alert("Something Went Wrong Please Contact Admin");
         }
          
        },
        err => {
          that.title='';
          that.uploadAs='1';
          that.selectedFile=[];
          that.showTo='2';
          that.noOfUpload='0';
          that.uploadFile=undefined;
          that.message="";
          that.date7 ="";
          that.type="image/png, image/jpeg";
          document.getElementById("model-closeContent").click();
          that.editmode=true;
          that.loading=false;
          //this.router.navigate(['/logout']);
          console.log(err);
        }
      );

		});
  }
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/')+' ' +
                [d.getHours(),
                d.getMinutes(),
                d.getSeconds()].join(':');
  }

  showModal(){
    this.title='';
    this.uploadAs='1';
    this.selectedFile=[];
    this.showTo='2';
    this.noOfUpload='0';
    this.uploadFile=undefined;
    this.message="";
    this.date7 ="";
    this.type="image/png, image/jpeg";
    this.myError['date']="";
    this.loading = false;
    this.value = 0;
    this.hideprogress = false;
    this.uploadCounter = 0
    // this.selectedFile = [];
    //$('#add-content').modal('show');
  }

}
