import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { UserService } from 'src/app/http.service';
import { IntlService } from '@progress/kendo-angular-intl';
import {MessageService} from 'primeng/api';
import { Router } from '@angular/router';
@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],

})
export class AvailabilityComponent implements OnInit {
 
  message: string = '';
  messages: string;
  date1:any;
  time:any;
  time2:any;
  date1Error:boolean = false;
  timeError:boolean  = false;
  time2Error:boolean  = false;
  dates:any=[];
  dateTime = new Date();
  public value: Date = new Date(2000, 2, 10, 10, 30, 0);
  calendar:any=[];
  create:boolean;
  loading:boolean = false;
  isloading:boolean = false;
  isLoading2 : boolean =true;
  id:Number;
  Month:any;
  Year:any;
  profileUrlData:any;
  passwordValidation: any =[];
  newPassword :string;
  oldPassword :string;
  confirmCheck:string;
  private formDataReset = new FormData();
  loading2:boolean;
  activateDeactivate:string;


  payOutValidation:any=[];
  NamePayout:string;
  routingNo:string;
  accountNumber:string;
  accountType:string;
  swiftCode:string;
  IBAN:string;
  selectedCountry:any=[];
  country: any=[];
  changeDate:any = [];
  currentEmail:string;
  deleteStopper:boolean=true;

  mid = localStorage.getItem('profileUrl');

  constructor(private router: Router,private data: DataService,private userService:UserService,private messageService: MessageService) { }

  ngOnInit() {
     this.dateTime.setDate(this.dateTime.getDate() );   
     this.data.currentMessage.subscribe(message => this.messages = message);
     this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
     var date = new Date();
     var date = new Date(), y = date.getFullYear(), m = date.getMonth();

     var firstDay = new Date(y, m, 1);
     var lastDay = new Date(y, m + 1, 1);
     const data ={
       firstDate:firstDay,
       lastDate:lastDay,
     }
    console.log(data);
     this.changeDate = data;
     //console.log(data)
     this.getData(data);
     this.profileUrlData = localStorage.getItem('profileUrl');
     if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
    ];  
     //this.dates=[new Date ('2019-08-30 18:30:00')]
  }

  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});

  }



	SaveData(){

		if(this.date1 == "" || this.date1 == undefined){
			this.date1Error = true;
			return false;
		}
		else{
			this.date1Error = false;
		}
		if(this.time == "" || this.time == undefined){
			this.timeError = true;
			return false;
		}
		else{
			this.timeError = false;
		}
		if(this.time2 == "" || this.time2 == undefined){
			this.time2Error = true;
			return false;
		}
		else{
			this.time2Error = false;
		}
// //console.log.log(this.date1);
// //console.log.log(this.time);return;
// return;
		var date = new Date(this.formatDate(this.date1)).toUTCString();
		////console.log.log(this.formatDate(this.date1)+' '+this.formatDateTime(this.time).split(' ')[1]);return false;
		var start_time = new Date(this.formatDate(this.date1)+' '+this.formatDateTime(this.time).split(' ')[1]).toUTCString()
		var end_time =   new Date(this.formatDate(this.date1)+' '+this.formatDateTime(this.time2).split(' ')[1]).toUTCString()
		
		const availability ={
			date:date,
			start_time:start_time,
			end_time:end_time,
		}
		 console.log(availability);
		// return;
		////console.log.log(availability);
		this.loading=true
		this.isloading = true
		this.userService.Setavailability(availability).subscribe(
			response => {
			if(response['success']==1){
				const data = {
					month:this.Month,
					year:this.Year,
        }
        
				this.getData(this.changeDate)
				document.getElementById('closeModal').click();
			}
			else{
				alert("Something Went Wrong Please Contact Admin");
			}
			
			},
			err => {
			this.isloading = true
			}
		);

	}


	convertDateUtc(date,type){
		var dates = new Date(date * 1000) ;
		if(type == "weekday"){
			var dates = new Date(date * 1000) ;
			var weekday=new Array(7);
			weekday[0]="Sun";
			weekday[1]="Mon";
			weekday[2]="Tue";
			weekday[3]="Wed";
			weekday[4]="Thu";
			weekday[5]="Fri";
			weekday[6]="Sat";
			return weekday[dates.getDay()];
		}
		else if(type == "day"){
			return dates.getDate()
		}
		else if(type == "start_time"){
			return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "end_time"){
			return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
	}

  getData(data){
    this.userService.getCalendarData(data).subscribe(
      response => {
       ////console.log.log(response);
       if(response['success']==1){
       // this.calendar =response['data'];
         //alert("done")
        ////console.log.log(response['data']);
         this.calendar = response['data']
         ////console.log.log(response['data']['date'])
        
         for(var i =0 ;i<response['data'].length;i++){
           this.dates.push(new Date (response['data'][i]['date']));
         }
         ////console.log.log(this.dates);
         this.isloading = true
       }
       else{
        this.isloading = true
        //alert("Something Went Wrong Please Contact Admin");
       }
       this.isLoading2 = false; 
      },
      err => {
        //this.router.navigate(['/logout']);
        alert("Something Went Wrong Please Contact Admin");
        this.isloading = true
        //console.log.log(err);
        this.isLoading2 = false;
      }
    );
  }


  getdate1(){
   // //console.log.log(this.date1);
    //alert(1);
  }

  formatDate(date) {
		////console.log.log(date)
		var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
	////console.log.log([month,day,year].join('-'))
    return [year,month,day].join('/');
}

  formatDateTime(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
	////console.log.log([month,day,year].join('-')+' ' +[d.getHours(),d.getMinutes(),d.getSeconds()].join(':'))
    return [year,month,day].join('/')+' ' +
                [d.getHours(),
                d.getMinutes(),
                d.getSeconds()].join(':');
  }

  deleteAvailability(id){
    if(!this.deleteStopper){
      return false;
    }
    this.deleteStopper = false;
    this.userService.deleteAvailability(id).subscribe(
      response => {
        this.deleteStopper = true;
       ////console.log.log(response);
       if(response['success']==1){
        this.messageService.add({severity:'success', summary: 'success', detail:'Availability deleted successfully'});
         this.dates =[];
         const data = {
           month:this.Month,
           year:this.Year,
         }
         this.getData(this.changeDate)
       }
       else{
        this.messageService.add({severity:'error', summary: 'error', detail:'Please try after sometime'});
       }

      },
      err => {
        //this.router.navigate(['/logout']);
        //console.log.log(err);
      }
    );
  }

  opneModal(create){
    this.create=true;
    this.date1="";
    this.time="";
    this.time2= "";
    this.timeError=false;
    this.date1Error=false;
    this.time2Error=false;
    document.getElementById('modalOpen').click();
    this.loading=false;
  }

  editAvailability(id,time1,time2,date){
    
    this.date1 = new Date(date * 1000);
    this.time = new Date(time1 * 1000) ;
    this.time2 = new Date(time2 * 1000);
    //console.log.log("time1",this.date1)
    //console.log.log("time2",this.time2)
    //console.log.log("date1", this.time)


    this.timeError=false;
    this.date1Error=false;
    this.time2Error=false;
    this.create=false;
    this.id = id;
    this.loading=false;
    document.getElementById('modalOpen').click();
  }
  
  UpdateData(){

    if(this.date1 == "" || this.date1 == undefined){
      this.date1Error = true;
      return false;
    }
    else{
      this.date1Error = false;
    }
    if(this.time == "" || this.time == undefined){
      this.timeError = true;
      return false;
    }
    else{
      this.timeError = false;
    }
    if(this.time2 == "" || this.time2 == undefined){
      this.time2Error = true;
      return false;
    }
    else{
      this.time2Error = false;
    }
    ////console.log.log(this.date1);

    var date = new Date(this.formatDate(this.date1)).toUTCString();
		////console.log.log(this.formatDate(this.date1)+' '+this.formatDateTime(this.time).split(' ')[1]);return false;
		var start_time = new Date(this.formatDate(this.date1)+' '+this.formatDateTime(this.time).split(' ')[1]).toUTCString()
		var end_time =   new Date(this.formatDate(this.date1)+' '+this.formatDateTime(this.time2).split(' ')[1]).toUTCString()


    const availability ={
      date:date,
      start_time:start_time,
      end_time:end_time,
      id:this.id
    }
   // //console.log.log(availability);return;
    this.loading=true
    this.userService.updateavailability(availability).subscribe(
      response => {
       //console.log.log(response);
       if(response['success']==1){
        this.loading=true
       // this.calendar =response['data'];
         //alert("done")
         this.dates=[];
         const data = {
          month:this.Month,
          year:this.Year,
        }
        this.getData(this.changeDate)
        document.getElementById('closeModal').click();
        //console.log.log(this.dates);

       }
       else{
        this.loading=false
        alert("Something Went Wrong Please Contact Admin");
       }
        
      },
      err => {
        //this.router.navigate(['/logout']);
        this.loading=false
        //console.log.log(err);
      }
    );
   
  }

  monthChange(month,year){
    var firstDay = new Date(year, month -1, 1);
    var lastDay = new Date(year, month,+1, 0);
   
    const data ={
      firstDate:firstDay,
      lastDate:lastDay,
    }
    // console.log(data)
    this.changeDate = data;
    this.getData(data);
  }

  hideErrors(){
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation['result'] = "Old Password does not matched";
          this.loading2=false;
        }
        if(response['success']==2){
          this.passwordValidation['result'] = "Some error occured please try after sometime";
          this.loading2=false;
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      //console.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      //console.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}



}
