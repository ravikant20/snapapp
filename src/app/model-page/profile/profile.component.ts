import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { DataService } from 'src/app/data.service';
import {MessageService} from 'primeng/api';
import { UserService } from 'src/app/http.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})
export class ProfileComponent implements OnInit {
  message: string;
  imageChange: string;
  currentEmail: string;
  mid = localStorage.getItem('profileUrl');

  isLoading:boolean=true;
  profileUrlData:any;
  passwordValidation: any =[];
  newPassword :string;
  oldPassword :string;
  confirmCheck:string;
  private formDataReset = new FormData();
  loading2:boolean;
  blockedUser:any=[];
  activateDeactivate:string;

  modelImage:string="";
  modelCover:string="";
  modelName:string="";
  userName:string="";
  bio:string="";
  website:string="";
  instagram:string="";
  twitter:string="";
  imageSrc: string | ArrayBuffer;
  imageSrc2:string | ArrayBuffer;
  files: any[];
  private formData = new FormData();
  loading:boolean=false;

  bioread:boolean=true;
  websiteread:boolean=true;
  instagramread:boolean=true;
  twitterread:boolean=true;

  constructor(private data: DataService,private messageService: MessageService,private userService:UserService, private router: Router,) { }

  ngOnInit() {
    this.getData();
    this.data.currentImage.subscribe(imageChange => this.imageChange = imageChange)
    this.data.currentMessage.subscribe(message => this.message = message)
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
  }

  copyText(val: string,val2: string){
    //this.copied = "copied"
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }

  edit(type){
    var t =type;
    //alert(type)
    if(type=="bioread"){
      this.bioread = false;
      //alert(this.bioread)
    }
    if(type=="websiteread"){
      this.websiteread = false;
    }
    if(type=="instagramread"){
      this.instagramread = false;
    }
    if(type=="twitterread"){
      this.twitterread = false;
    }

  }

  blurRemove(){
    //console.log(this.twitter)
    if(this.twitter.includes("@")){
      //console.log(this.twitter)
      this.twitter = this.twitter.replace("@", "");
    }
    if(this.instagram.includes("@")){
      this.instagram = this.instagram.replace("@", "");
    }
    
    this.bioread = true;
    this.websiteread = true;
    this.instagramread = true;
    this.twitterread = true;
  }


  hideErrors(){
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation['result'] = "Old Password does not matched";
          this.loading2=false;
        }
        if(response['success']==2){
          this.passwordValidation['result'] = "Some error occured please try after sometime";
          this.loading2=false;
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
          this.loading2=false;
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

  checkStatus(checked,userid){
    //alert(checked);
    ////console.log.log(this.blockedUser)
    const user_id = {
      user_id:userid,
      type:checked
    }
   //this.blockedUser = [];
    this.userService.blockUser(user_id).subscribe(
      response => {
       //console.log.log(response);
       if(response['success']==1){
         //alert("success");
         //this.blockedUser = [];
        //  this.modelDataFollowing = response['data'];
        //  ////console.log.log(response['data']);
        //  this.isLoading=false;
       }
       else{
        //  alert("No Subscriber Found");
        //  this.isLoading=false;
       }
        
      },
      err => {
        // this.router.navigate(['/login']);
        //console.log.log(err);
      }
    );
  }

onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      //console.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      //console.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

  delete(){
    this.activateDeactivate = '2';
  }

  getData(){
    this.userService.viewAccountPreview().subscribe(
       response => {
        this.isLoading = false;
         //if(response['data'].length >0){
          console.log(response['data']['profileImage'])
          if(response['data']['profileImage'] == undefined){
            this.modelImage = "assets/images/dummy-profile.png"
          }
          else{
            this.modelImage = response['data']['profileImage'];
            this.data.changeImage(response['data']['profileImage']);
          }
          if(response['data']['coverPic'] == undefined){
            this.modelCover = "assets/images/placeholder.png"
          }
          else{
            this.modelCover = response['data']['coverPic']
          }
          //this.modelCover = response['data']['coverPic'];
          this.modelName  = response['data']['name'];
          this.userName   = response['data']['profile_url'];
          this.bio = response['data']['bio']
          if(response['data']['bio'] == null){
            this.bio = ""
          }
          this.website = response['data']['website']
          if(response['data']['website'] == null){
            this.website = ""
          }
          this.instagram = response['data']['instagram']
          if(response['data']['instagram'] == null){
            this.instagram = ""
          }
          this.twitter = response['data']['twitter']
          if(response['data']['twitter'] == null){
            this.twitter = ""
          }
          //
          
         //}
        //  console.log(this.modelImage)
       },
       err => {
        this.isLoading = false;
        // this.router.navigate(['/logout']);
         //console.log.log(err);
       }
     );
  }

  changeProfilePic(){
    //alert("rk")
  }

  changeCoverPic(){
    document.getElementById('file2').click();
  }

  profilePIcChange(event){
    if (event.target.files && event.target.files[0]) 
    { const file = event.target.files[0]; 
      const reader = new FileReader(); 
      reader.onload = e => this.imageSrc = reader.result; 
      
      this.files = event.target.files;
      //console.log.log(event.target.files)
      this.formData.append("profilePic", event.target.files[0]);
      reader.readAsDataURL(file);
      // this.changePic();
      // this.type ='photo'
    } 
  }

  coverPIcChange2(event){
    //alert("sdf")
    if (event.target.files && event.target.files[0]) 
    { const file = event.target.files[0]; 
      const reader = new FileReader(); 
      reader.onload = e => this.imageSrc2 = reader.result; 
      this.files = event.target.files;
      //console.log.log(event.target.files)
      this.formData.append("cover", event.target.files[0]);
      reader.readAsDataURL(file);
      // this.changePic();
      // this.type ='photo'
    } 
  }

  saveProfileData(){
    if(this.twitter.includes("@")){
      this.twitter = this.twitter.replace("@", "");
    }
    if(this.instagram.includes("@")){
      this.instagram = this.instagram.replace("@", "");
    }
    if(this.twitter.includes(".com")){
      this.messageService.add({severity:'error', summary: 'error', detail:'Please enter the correct twitter username'});
      return false
    }
    if(this.instagram.includes(".com")){
      this.messageService.add({severity:'error', summary: 'error', detail:'Please enter the correct instagram username'});
      return false
    }
    this.loading = true;
    this.formData.append("bio",this.bio)
    this.formData.append("twitter",this.twitter)
    this.formData.append("instagram",this.instagram)
    this.formData.append("website",this.website)
    this.userService.saveProfileData(this.formData).subscribe(
      response => {
        this.loading = false;
        this.messageService.add({severity:'success', summary: 'success', detail:'Profile updated successfully'});
        this.getData();
      },
      err => {
        this.loading = false;
        this.messageService.add({severity:'error', summary: 'error', detail:'Image not valid'});
        
      }
    );
  }


}
