import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/http.service';
import { DataService } from "../../data.service";
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})
export class NotificationsComponent implements OnInit {
showData:any=[];
selectedValues: any = [];
messages:any;
profileUrlData:any

passwordValidation: any =[];
newPassword :string;
oldPassword :string;
confirmCheck:string;
private formDataReset = new FormData();
loading2:boolean;
activateDeactivate:string;
payOutValidation:any=[];
NamePayout:string;
routingNo:string;
accountNumber:string;
accountType:string;
swiftCode:string;
IBAN:string;
selectedCountry:any=[];
country: any=[];
isLoading:boolean = true;
currentEmail:string;

mid = localStorage.getItem('profileUrl');

  constructor(private userService:UserService,private router: Router,private data: DataService,private messageService: MessageService) { }

  ngOnInit() {
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
   }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.getyourNoti();
    this.data.currentMessage.subscribe(message => this.messages = message);
    this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
    this.profileUrlData = localStorage.getItem('profileUrl');
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
    ];   
  }

  copyText(val: string,val2: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val+''+val2
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
  }

  getyourNoti(){
    
    this.userService.getNotificationsModel().subscribe(
      response => {
         //console.log.log(response);
         this.showData = response['data'];
         this.isLoading = false;
      },
      err => {
         //console.log.log(err);
         this.isLoading = false;
      }
    );
 }
  deleteNotification(){
    this.userService.deleteNotificationsModel(this.selectedValues).subscribe(
      response => {
         //console.log.log(response);
         this.getyourNoti();
      },
      err => {
         //console.log.log(err);
      }
    );
  }




  hideErrors(){
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation['result'] = "Old Password does not matched";
          this.loading2=false;
        }
        if(response['success']==2){
          this.passwordValidation['result'] = "Some error occured please try after sometime";
          this.loading2=false;
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }

  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}


checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      //console.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      //console.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}


}
