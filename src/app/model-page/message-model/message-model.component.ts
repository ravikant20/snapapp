import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, ViewChildren, QueryList, ViewEncapsulation } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { UserService } from '../../http.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
import { DataService } from "../../data.service";
import {MessageService} from 'primeng/api';
interface Post {
  title: string;
  content: string;
}

@Component({
  selector: 'app-message-model',
  templateUrl: './message-model.component.html',
  styleUrls: ['./message-model.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})
export class MessageModelComponent implements OnInit {
  //@ViewChild('scrollMe',{static: false}) private myScrollContainer: ElementRef;
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm;  
  @ViewChildren("scrollMe") messageContainers: QueryList<ElementRef>;
  @ViewChild('window',{static: false}) window;
  //@ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  postsCol: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;
  tsCol: AngularFirestoreCollection<Post>;
  sts: Observable<Post[]>;

  title:string;
  content:string;
  getProfileData:any =[];
  isLoading:boolean =false;
  userID:string;
  modelId:string;
  modelImage:string;
  userImage:string;
  isModelChat:boolean = false;
  imageSrc: string | ArrayBuffer;
  files: any[];
  isUpload:boolean=false;
  type:string;
  messages:string;
  editmode:boolean=true;
  private formData = new FormData();
  chatCount:number;
  chatId:string;
  nameMain:string;
  imageMain:string;
  userNameToshow:string;
  userImageToshow:string;
  imageFile:string;


  typing:boolean =false;
   myVar;

  chatIdString:string;
  chatReference:any;
  messageReference:any;
  arrayModelId:any=[];
  ChatToShow:any=[];
  postRefrenceArray:any=[];
  getchatData=[];
  profileUrlData:any
 //window: any;
 loggedInuser:any = localStorage.getItem('id');

 timer1  = setTimeout(()=>{
  // this.stopTyping();
}, 5000); 
typingStatus:boolean =false;
currentActiveModel:any ='undefined';

passwordValidation: any =[];
newPassword :string;
oldPassword :string;
confirmCheck:string;
private formDataReset = new FormData();
loading2:boolean;

liveModel:any=[];
liveModelData:any=[];
activateDeactivate:string;


payOutValidation:any=[];
NamePayout:string;
routingNo:string;
accountNumber:string;
accountType:string;
swiftCode:string;
IBAN:string;
selectedCountry:any=[];
country: any=[];
currentEmail:string;

mid = localStorage.getItem('profileUrl');

 constructor(private afs: AngularFirestore,private userService:UserService,private router: Router,private data: DataService,private messageService: MessageService) {}

 ngOnInit() {
   this.nameMain = localStorage.getItem('name');
   if(localStorage.getItem('profileImage') != '0'){
    this.imageMain = localStorage.getItem('profileImage');
  }
  else{
    this.imageMain = 'assets/images/dummy-profile.png'
  }
   //this.imageMain = localStorage.getItem('profileImage');
   this.data.currentMessage.subscribe(message => this.messages = message);
   this.data.currentEmails.subscribe(changeEmail => this.currentEmail = changeEmail)
   this.modelId = localStorage.getItem('id');
   if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '1' ){
      this.router.navigate(['/your-feed']);
   }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
   // //console.log.log(this.userID);
    this.getallChat();
    this.profileUrlData = localStorage.getItem('profileUrl');
    this.country = [
      {name: 'Australia', code: 'aus'},
      {name: 'Great Britain', code: 'gb'},
      {name: 'Spain', code: 'sp'},
      {name: 'New Zealand', code: 'nz'},
      {name: 'United States', code: 'us'}
    ];  
 }

 copyText(val: string,val2: string){
  let selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '0';
  selBox.style.top = '0';
  selBox.style.opacity = '0';
  selBox.value = val+''+val2;
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  document.body.removeChild(selBox);
  this.messageService.add({severity:'success', summary: 'success', detail:'Profile Url Copied'});
}

 getLiveChatStatus(){
  const that = this;
  this.postsCol = this.afs.collection('posts', ref => ref.where('modelId', '==', Number(this.modelId)));
  //that.ChatToShow = []; 
  this.posts = this.postsCol.valueChanges();
  // //console.log.log(this.ChatToShow)
  this.posts.subscribe(res => {
  res.forEach(element1 => {
    var exists = false;
     
    for (var i=0; i<this.ChatToShow.length; i++) {
      var element2 = this.ChatToShow[i];
      if (element1['chatId'] == element2['chatId']) {
        this.ChatToShow[i]['lastMessage'] = element1['lastMessage'];
        this.ChatToShow[i]['time'] = element1['time'];
        this.ChatToShow[i]['unreadCount'] = element1['unreadCount'];

        exists = true;
        //alert(this.currentActiveModel)
        ////console.log.log("this.currentActiveModel",this.currentActiveModel)
        if(this.currentActiveModel !="undefined" || this.currentActiveModel !=undefined){
          ////console.log.log("type",element1);
          if(element1['userId'] == this.currentActiveModel){
            this.typingStatus = element1['typingStatus'][this.currentActiveModel];
          }
         
          // //console.log.log("this.typingStatus", this.typingStatus)
          // //console.log.log("this.typingcurrentActiveModelStatus",this.currentActiveModel)
        }
        break;
      }
    }
   
    ////console.log.log("this.ChatToShow ",this.ChatToShow)

    if (exists == false) {
      // Insert
      ////console.log.log("userIdGet",element1['modelId']);
      this.getModelData([element1['userId']]);	
      //con.log('Insert', element1['chatId']);
      this.ChatToShow.push(element1)
    }
   });

   this.ChatToShow.sort(function (a, b) {
    return b['time'].seconds - a['time'].seconds;
   });
  }) 
  return;
}

sendUnreadCount(modelId,UserID,docId){
  var pinDocRef = this.chatReference.ref;
  return firebase.firestore().runTransaction(function(transaction) {
    return transaction.get(pinDocRef).then(function(pinDoc) {
      if(!pinDoc.exists){
        ////console.log.log("Document does not exist!");
        return false;
      }
      ////console.log.log("pindoc",pinDoc)
      var unreadCount = pinDoc.data().unreadCount;
      for (var key in unreadCount) {
        if (key == localStorage.getItem('id')) {
          unreadCount[key] = 0;
        }
        else {
          unreadCount[key] = Number(pinDoc.data().unreadCount[key]) + 1;
        }
      }

      transaction.update(pinDocRef, { 'unreadCount': unreadCount});
    });
    }).then(function() {
      ////console.log.log("Transaction successfully committed!");
    }).catch(function(err) {
      ////console.log.log("Transaction failed: ", err);
    });
}

setUnreadCountZeroForReceiver(){
  var pinDocRef = this.chatReference.ref;
  return firebase.firestore().runTransaction(function(transaction) {
    return transaction.get(pinDocRef).then(function(pinDoc) {
      if(!pinDoc.exists){
        ////console.log.log("Document does not exist!");
        return false;
      }
      ////console.log.log("pindoc",pinDoc)
      var unreadCount = pinDoc.data().unreadCount;
      unreadCount[localStorage.getItem('id')] = 0;
      transaction.update(pinDocRef, { 'unreadCount': unreadCount});
    });
    }).then(function() {
      ////console.log.log("Transaction successfully committed!");
    }).catch(function(err) {
      ////console.log.log("Transaction failed: ", err);
    });
}

getallChat(){
  this.arrayModelId= [];
  var afs2 = this.afs;
  var that = this;
  this.afs.collection("posts", ref => ref.where('modelId', '==', Number(this.modelId)).orderBy('time','desc'))
  .get().toPromise().then(function(querySnapshot) {
    const sizeOfarray = querySnapshot.size;
    querySnapshot.forEach(function(doc) {
      that.ChatToShow.push(doc.data());
      that.arrayModelId.push(doc.data().userId);
      if(sizeOfarray == that.arrayModelId.length){
        that.getModelData(that.arrayModelId);
      }
    });
    ////console.log.log("that.arrayModelId",that.arrayModelId)
    that.arrayModelId.forEach(element => {
     // //console.log.log("element",element);
      var userId = element;
      var  modelId= localStorage.getItem('id');
      var chatId = userId + '_' + modelId;
      if (modelId < userId) {
        chatId = modelId + '_' + userId;
      }

      // //console.log.log("chatId", chatId);

      // //console.log.log("chatId", that.arrayModelId);

      
     // that.postRefrenceArray.push(listener2);
    });
  })

}

getallChatUser(chatId){
  var ref = this.afs.collection('posts').doc(chatId).collection("messsages");
  var listener2 = ref.valueChanges();
  listener2.subscribe(res => {
    ////console.log.log("newdfsdf",res);
    if (res.length > 0) {
      }
      ////console.log.log("res",res);

      res.sort(function (a, b) {
        return a['time'].seconds - b['time'].seconds;
      });
      this.getchatData = res;

    }

  
  )
}




 getModelData(data){
  this.userService.getModelForChat(data).subscribe(
    response => {
    //  //console.log.log(response);
    //  //console.log.log(this.ChatToShow)
      for(var i =0;i<response['data'].length;i++){
        for(var j =0;j<this.ChatToShow.length;j++){
          if(this.ChatToShow[j]['userId'] == response['data'][i]['id']){
            this.ChatToShow[j]['name'] =  response['data'][i]['name']
            this.ChatToShow[j]['profilePic'] =  response['data'][i]['profileImage']
            this.ChatToShow[j]['id'] =  response['data'][i]['id']
            this.ChatToShow[j]['modelId'] =  localStorage.getItem('id');
            break;
          }
        }
      }  
      this.ChatToShow.sort(function (a, b) {
        return b['time'].seconds - a['time'].seconds;
      });
      this.getLiveModel();
      ////console.log.log(this.ChatToShow)
       //console.log.log("sdfsd",this.ChatToShow);
      this.getLiveChatStatus();
      this.isLoading =false;
    },
    err => {
      this.isLoading =false;
      ////console.log.log(err);
    }
  );
}


addPost() {
  this.editmode=false;
  if(this.isUpload){
   this.type="photo";
   const api = {
     api_token:localStorage.getItem("loggedInUser")
    }
    this.userService.uploadChatPic(this.formData).subscribe(
      response => {
       ////console.log.log(response);
       //this.isLoading=true;
       if(response['success']==1){
         ////console.log.log(response);
          this.content =response['data'];
          this.getDataChat();
         // this.isLoading=false;
       }
       else{
         ////console.log.log(response);
         //this.isLoading=false;
         //alert("No models Found");
       }
        
      },
      err => {
        this.messageService.add({severity:'error', summary: 'error', detail:'Image not valid'});
        this.content="";
        this.editmode=true;
        //this.loading=false;
        ////console.log.log(err);
      }
    );
  }
  else{
    if(this.content == "" || this.content == null  || this.content == undefined ){
       this.editmode=true;  
       return false;   
    }
   this.type="text";
   this.getDataChat();
  }
}


getDataChat(){

	
  this.imageFile="";
  
  this.isUpload=false;


  var timestamp = firebase.firestore.Timestamp.now();

//var milliseconds = (new Date).getTime();

  this.messageReference.add({'message': this.content, 'sender': this.modelId,'userID': this.userID, 'modelId': Number(this.userID),'time':timestamp,'type':this.type,'read':false});
  var chatId = this.userID +'_'+ this.modelId;
  if(this.userID > this.modelId){
     var chatId = this.modelId +'_'+ this.userID;
}
// //console.log.log("chatIdString",this.chatIdString)
// //console.log.log("chatId",chatId)

  // this.afs.collection('posts').add({'chatId': chatId,'lastMessage':this.content, 'lastMessageTime':firebase.firestore.FieldValue.serverTimestamp(),'modelId':Number(this.modelId),'userId':this.userID});
if(this.type=="photo"){
		this.content = "@@@@@@@";
	}
  const that =this;
this.chatReference.update({'lastMessage':this.content,'time':timestamp}).then(function() {
  ////console.log.log('this.sendUnreadCount(Number(this.modelId),Number(this.userID),chatId)');
  that.sendUnreadCount(Number(that.modelId),Number(that.userID),chatId);
}).catch(function(error) {
  if (error.code == 'not-found') {
    // Set because update is not available
    var unreadCount = [];
    if(localStorage.getItem('id') == that.userID) {
      unreadCount[String(that.userID)] = 0;
      unreadCount[String(that.modelId)] = 1;
    }
    else{
      unreadCount[String(that.userID)] = 1;
      unreadCount[String(that.modelId)] = 0;
    }

    var typingStatus = [];
    typingStatus[String(that.userID)] = false;
    typingStatus[String(that.modelId)] = false;

    that.chatReference.set({'chatId': chatId,'lastMessage':that.content,'time':timestamp,'modelId':Number(that.userID),'userId':Number(that.modelId), 'unreadCount':unreadCount, 'typingStatus':typingStatus}).then(function(){
      that.sendUnreadCount(Number(that.modelId),Number(that.userID),chatId);
    });
  }
  else {
    // Unknown error
    
  }
});

//this.afs.collection('posts').doc(chatId).set({'chatId': chatId,'lastMessage':this.content,'time':timestamp,'modelId':Number(this.modelId),'userId':Number(this.userID)});






  ///to post chat count of unread message
  const afs3 =  this.afs;
  const USERID =this.userID
  const MODELID =this.modelId;


this.getchatData.push({'message': this.content,'sender': this.modelId,'userID': this.userID,'modelId': Number(this.modelId),'time':timestamp,'type':this.type,'read':true})
  
  const afs2 =  this.afs;



  
  if(this.type =="photo"){
    this.content="Image";
  }
  else{
    this.content=this.content;
  }
    this.content="";

  this.editmode=true;
}








 getChat(){
  
   const api = {
     api_token:localStorage.getItem("loggedInUser")
    }
    this.userService.getMsgChat(api).subscribe(
      response => {
       ////console.log.log(response);
       //this.isLoading=true;
       if(response['success']==1){
         ////console.log.log(response);
         this.getProfileData =response['data'];
         this.isLoading=false;
       }
       else{
         ////console.log.log(response);
         this.isLoading=false;
         //alert("No models Found");
       }
        
      },
      err => {
        this.router.navigate(['/login']);
        //console.log.log(err);
      }
    );

 }


 modelChat(user_id:string,model_id:string,user_image,USERNAME,device){


  if(device == 'phone'){
		document.getElementById('phoneViewList').style.display ='none';
		//document.getElementById('phoneViewData').style.display = 'block';
		
	}
  this.currentActiveModel = user_id;
  if (!(typeof this.chatIdString === 'undefined') && this.chatIdString.length > 0) {
		var countId = 'unreadCount.' + user_id;
    this.afs.collection('posts').doc(this.chatIdString).update({[countId]: 0});
    if(device == 'phone'){
			document.getElementById('phoneViewData').style.display = 'block';
		}
  }
  // this.modelNameToshow= modelNAME
  // this.modelImageToshow= model_image;
  this.getchatData = [];

    if(Number(user_id)>Number(model_id)){
      this.chatIdString = model_id+'_'+user_id;
    }
    else{
      this.chatIdString = user_id+'_'+model_id;
	}

  this.chatReference =  this.afs.collection('posts').doc(this.chatIdString);
  this.messageReference = this.chatReference.collection('messsages');

   this.userNameToshow  = USERNAME
   this.userImageToshow = user_image

   //alert("modelClicked");
   //this.isModelChat=true;
   // alert(model_id);
   // alert(user_id);
   this.userID = user_id;
   this.modelId = model_id;
   this.userImage = user_image;
   this.modelImage =  localStorage.getItem("profileImage");
  //  this.postsCol = this.afs.collection('posts', ref => ref.where('userID', '==', Number(this.userID)).where('modelId', '==', Number(this.modelId)).orderBy('time'));
  //  this.posts = this.postsCol.valueChanges();
   ////console.log.log(this.posts);
   this.afs.collection(`chatStatus`, ref => ref.where('userID', '==', user_id).where('modelId', '==', Number(this.modelId))).valueChanges().subscribe(res => {
    if (res.length > 0)
    {
      ////console.log.log(res)
      this.chatCount =1;
    }
    else
    {
      this.chatCount =0;
    }

    const thats  = this;  


    this.getallChatUser(this.chatIdString);




  const afs2 =  this.afs;
  this.afs.collection("chatStatus", ref => ref.where('userID', '==',user_id).where('modelId', '==', Number(this.modelId)))
  .get().toPromise().then(function(querySnapshot) {
  querySnapshot.forEach(function(doc) {
        ////console.log.log(doc.id);
        afs2.collection("chatStatus").doc(doc.id).set({'chatNotificationModel': true},{ merge: true }).then(function() {
          ////console.log.log("Document successfully written!");
        })
        .catch(function(error) {
          ////console.log.error("Error writing document: ", error);
        });
    });
  })  
});
   this.isModelChat=true;
   //this.scrollToBottom();
  
   this.setUnreadCountZeroForReceiver();
 }
 backMobile(){
  document.getElementById('phoneViewList').style.display ='block';
  document.getElementById('phoneViewData').style.display = 'none';
}
 readURL(event) 
 { 
   if (event.target.files && event.target.files[0]) 
     { const file = event.target.files[0]; 
       const reader = new FileReader(); 
       reader.onload = e => this.imageSrc = reader.result; 
       this.files = event.target.files;
       this.formData.append("file", event.target.files[0]);
       ////console.log.log(event.target.files)
       reader.readAsDataURL(file);
       this.isUpload=true; 
     } 
 }
 convertTime(time){

  var a = new Date(parseInt(time) * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes().toString();
  if(min.toString().length ==1){
    var min = '0'+min
  }
  var sec = a.getSeconds();
  var times = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min ;
  return times;
}
  valuechange(): void { 
    clearTimeout(this.timer1)
    if(this.typing == false) {
      this.typing = true;
      var typingStatus = 'typingStatus.' + this.loggedInuser;
      this.chatReference.update({[typingStatus]: true});
    }
    this.timer1  = setTimeout(()=>{
      this.stopTyping();
    }, 5000); 
  }

  stopTyping() {
    this.typing = false; 
    var typingStatus = 'typingStatus.' + this.loggedInuser;
    this.chatReference.update({[typingStatus]: false});
  }

  hideErrors(){
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }
  
  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')
          document.getElementById("model-close").click();
          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.messageService.add({severity:'success', summary: 'success', detail:'password changed successfully'});
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          this.passwordValidation['result'] = "Old Password does not matched";
          this.loading2=false;
        }
        if(response['success']==2){
          this.passwordValidation['result'] = "Some error occured please try after sometime";
          this.loading2=false;
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }



  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}
getLiveModel(){
  firebase.database().ref('/status/');
  const that = this;
  firebase.database().ref('/status/').on('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
    var childData = childSnapshot.val();
    if(childData['state'] == 'online' && childData['type'] == 'subscriber'){
      ////console.log.log("yes")
      that.liveModel.push(childData['id']);
    }
    else{
      ////console.log.log("no")
      var index = that.getUnique(that.liveModel).indexOf(childData['id']);
      if (index > -1) {
        that.getUnique(that.liveModel).splice(index, 1);
       }
    }
    ////console.log.log(that.getUnique(that.liveModel))
    });
    that.liveModelData = that.getUnique(that.liveModel);
    for(var i =0 ; i<that.ChatToShow.length ; i++){
    if(that.include(that.getUnique(that.liveModelData),that.ChatToShow[i]['userId'])){
      that.ChatToShow[i]['live'] = 'yes';
    }
    else{
      that.ChatToShow[i]['live'] = 'no';
    }
    }
  });
  
  }

  include(arr, obj) {
  for(var i=0; i<arr.length; i++) {
    if (arr[i] == obj) return true;
  }
}  

getUnique(array){
  var uniqueArray = [];
  for(var i=0; i < array.length; i++){
    if(uniqueArray.indexOf(array[i]) === -1) {
      uniqueArray.push(array[i]);
    }
  }
  return uniqueArray;
}

checkDeactivate(){
  this.userService.getModelDeactivateStatus().subscribe(
    response => {
      //console.log.log(response);
      this.activateDeactivate = response['data']['accountStatus'];
      document.getElementById('DeactivateDeleteButton').click();
    },
    err => {
  
    }
  );
}

setDeactivate(type){
  const types={
    type:type
  }
  if(type == 2){
    document.getElementById('closeDeactivatePopup').click();
    document.getElementById('model-close2').click();
   
    this.router.navigate(['/logout']);
  }
  this.userService.setDeactivate(types).subscribe(
    response => {
      //console.log.log(response);
      if(type == 1){
        this.activateDeactivate = type;
      }
      else{
        this.activateDeactivate = type;
      }
      if(type == 2){
        document.getElementById('closeDeactivatePopup').click();
        document.getElementById('model-close2').click();
       
        this.router.navigate(['/logout']);
      }
     
      document.getElementById('closeDeactivatePopup').click();
    },
    err => {
  
    }
  );
}

delete(){
  this.activateDeactivate = '2';
}





}




 



