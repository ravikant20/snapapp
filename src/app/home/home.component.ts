import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from '../http.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
// import 'bootstrap';
declare var $:any;
@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None,
  
})
export class HomeComponent implements OnInit {

	public params;
	myData: any[] = [];
	isLoading:boolean=true;
	countData:any = [];
	searchFilter:string;
  	search:any;
  	searchedOn:boolean;	
	featured:boolean=false;
	featuredName:string;
	ifLogin:boolean;
	addClass:string;
	homeTitle:string ="Featured";
	val: number;
	Arr = Array;
	opacity:any = 0;
	classACTIVE:any = "notactive";
	getNewData:any=[]
	//private id: String ;
	constructor(private userService:UserService, private router: Router, private route:ActivatedRoute) {
		if(localStorage.getItem('loggedInUser') != null){
			this.router.navigate(['/your-feed']);
		  }

		

	}

	ngOnInit() {
		this.getModels();
		this.getNewDta();
	// 	setInterval(()=>{    //<<<---    using ()=> syntax
	// 		this.changeSlider();
	// 		// console.log('a')
	//    }, 4000);
		// setInterval(()=>{    //<<<---    using ()=> syntax
		// 	this.changeSlider();
		// 	// console.log('a')
		// }, 15000);
	 }
	//  changeSlider(){
	// 	var elem = document.getElementById('changeSlider');
	// 	if(elem!=null){
	// 	  document.getElementById('changeSlider').click();
	// 	}
	// 	else{
	// 	  //clearTimeout(this.timer1)
	// 	}
	// 	}

	getNewDta(){
		this.userService.getNewDta().subscribe(
			response => {
				this.getNewData =response;
				//this.router.navigate(['/model-profile']);
			},
			err => {
				alert("something went wrong!");
				//console.log.log(err);
			}
		);
	}

	public getModels(){
		this.userService.getModels().subscribe(
			response => {
				console.log(response);
				this.myData = response['data'];
				this.countData = response['data'];
				this.isLoading=false;
				//this.router.navigate(['/model-profile']);
			},
			err => {
				alert("something went wrong!");
				//console.log.log(err);
			}
		);
	}
	getInfo(event){
		//console.log(event)
		var elem = document.getElementsByClassName('active12')
		if(elem.length>0){
		  if(event.target.id == "changeSlider"){
			this.opacity = 1;
		  }
		  else if(event.target.id == 'dropdownMenuButton' ){
			this.classACTIVE = "notactive"
			this.opacity =0;
		  }
		  else{
			this.classACTIVE = "notactive"
			this.opacity = 0;
		  }
		}
		
		if(event.target.id == 'dropdownMenuButton' ){
		  this.classACTIVE = "active12"
		  this.opacity =1;
		}
		// else{
		//   console.log("!@#42342222")
		//   this.opacity =0;
		// }
	  }
	  getInfo2(event){
		var elem = document.getElementsByClassName('active12')
		if(elem.length>0){
		 if(event.target.id == 'dropdownMenuButton' ){
			this.classACTIVE = "notactive"
			this.opacity =0;
		  }
		  else{
			this.classACTIVE = "notactive"
			this.opacity = 0;
		  }
		}
		
		if(event.target.id == 'dropdownMenuButton' ){
		  this.classACTIVE = "active12"
		  this.opacity =1;
		}
	  }
	
	convertDateUtc(date,type){
		var dates = new Date(date * 1000) ;
		if(type == "weekday"){
			var dates = new Date(date * 1000) ;
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
								"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
							];

			return dates.getDate() +' '+ monthNames[dates.getMonth()];
		}
		else if(type == "day"){
			return dates.getDate()
		}
		else if(type == "start_time"){
			return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "end_time"){
			return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
	}
	submitSearch(){
		//this.isLoading=true;
		this.searchedOn = false;
		this.homeTitle="";
		//alert(this.searchFilter);
		//this.router.navigate(['/models'], { queryParams: { order: this.searchFilter } });
	
		  this.search={
			order:this.searchFilter,
		  }
		
		// if(this.searchFilter == "order"){
		//   const search={
		//     topSelling:this.searchFilter,
		//   }
		// }
		this.userService.modelFilterHome(this.search).subscribe(
		  response => {
			//console.log.log(response);
			if(response['success']==1){
				this.myData = response['data'];
			  //console.log.log(response['data']);
			  this.isLoading=false;
			}
			else{
			  alert("No models Found");
			  this.isLoading=false;
			}
		   
			 
		   
		   
		  },
		  err => {
		   //console.log.log(err);
		  }
		)
	  }

	  filterModel(type){
   
		//this.searchFilter = type;
		///
		//this.isLoading=true;
		this.searchedOn = false;
		this.homeTitle="";
		//alert(this.searchFilter);
		//this.router.navigate(['/models'], { queryParams: { order: this.searchFilter } });
		if(type == "ASC"){
		  this.search={
			sort:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "DESC"){
		  this.search={
			sort:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "featured"){
		  this.search={
			featured:type,
		  }
		  this.featured=true;
		  this.featuredName='featured';
		}
		if(type == "trending"){
		  const search={
			trending:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "popular"){
		  const search={
			popular:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "nameSortASC"){
		  this.search={
			nameSort:'ASC',
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "nameSortDESC"){
		 // alert(type)
		  this.search={
			nameSort:'DESC',
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "mostUploads"){
		  this.search={
			mostUploads:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type == "topSelling"){
		  this.search={
			topSelling:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type== "recentlyActive"){
		  this.search={
			recentlyActive:type,
		  }
		  this.featured=false;
		  this.featuredName='';
		}
		if(type== "live"){
		  this.search={
			live:type,
		  }
		  this.featured=false;
		  this.featuredName='live';
		}
	
	
		this.userService.modelFilterHome(this.search).subscribe(
		  response => {
			console.log(response);
			if(response['success']==1){
			  this.myData = response['data'];
			  //console.log.log(response['data']);
			  this.isLoading=false;
			}
			else{
			  alert("No models Found");
			  this.isLoading=false;
			}
		   
			 
		   
		   
		  },
		  err => {
		   //console.log.log(err);
		  }
		)
	
	  }	  
}
