/* 
// Method not in use: payForVideo, buyVideo
*/

import { Component, OnInit, HostListener, Injectable, ViewEncapsulation } from '@angular/core';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
// import { Lightbox } from 'ngx-lightbox';
import {GalleriaModule} from 'primeng/galleria';
import {LightboxModule} from 'primeng/lightbox';
//const nisPackage = require('../../package.json');
@Component({
  selector: 'app-yourfeed',
  templateUrl: './yourfeed.component.html',
  styleUrls: ['./yourfeed.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class YourfeedComponent implements OnInit {
  //userService: any;
  FeedData: any =[];
  //router: any;
  isLoading:boolean; 
  page: number = 1;  
  gridView:boolean = true;
  viewType:string = 'grid';
  scrollView:boolean=false;
  viewContentShow:string = 'all';
  //ScrollView:boolean = false;
  contentType:number=1;
  PriceVideo:string;
  fvideoPrice :string;
  buyingasset:string;
  index:string;
  _albums:any[];
  hideShowMore:boolean = false
  _albumsImage:any[];
  imgsrc:any;
  index2:any;
  title:string;  
  
  constructor(private userService:UserService,private router: Router) { }


  
 
  ngOnInit() {

    this.getyourFeed(1,this.contentType);
    //this.isLoading=false;
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2'){
      this.router.navigate(['/model-content']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
  }


	getyourFeed(page,data){
		this.isLoading=true;
		const api = {
			api_token:localStorage.getItem("loggedInUser")
		}
		const dateNow = Date.now() / 1000;

		this.userService.getfeed(api,page,data,dateNow).subscribe(
			response => {
				/*
				//   Commented as not required
				this._albums = [];
				this._albumsImage = [];
				*/
				for (let i = 0; i < response['data'].length ; i++) {
					

					
					this.FeedData.push(response['data'][i]);
					if(response['data'][i]['next_page'] == null){
						this.hideShowMore = true;
					}
				}
				if(response['data'].length == 0){
					this.hideShowMore = true;
				}
				// //console.log.log(response)
				this.isLoading=false;
			},
			err => {
				this.isLoading=false;
			}
		);
	}



	loadMore(){
		this.page = this.page + 1;  
		this.getyourFeed(this.page,this.contentType);
	}
  
	viewChange(){
		if(this.viewType == 'grid'){
			this.gridView = true;
			this.scrollView =false;
		}
		else{
			this.gridView = false;
			this.scrollView =true;
		}
	}

	viewContent(){
		
		if(this.viewContentShow == 'images'){
			this.contentType=2;
			this.FeedData = [];
		}
		else if(this.viewContentShow == 'videos'){
			this.contentType=3;
			this.FeedData = [];
		}
		else{
			this.contentType=1;
			this.FeedData = [];
		}
		/*
		//   Commented as not required
		else if(this.viewContentShow == 'purchased_videos'){
			this.contentType=10;
			this.FeedData = [];
		}
		*/
		this.page = 1;
		this.getyourFeed(1,this.contentType);
	}


	payForVideo(asset_id,price,i){
		document.getElementById('showModel').click();
		this.PriceVideo = price;
		this.fvideoPrice = price;
		this.buyingasset= asset_id;
		this.index= i;
	}

	buyVideo(){
		this.userService.BuyVideo(this.buyingasset).subscribe(
			response => {
				this.FeedData[this.index]['paid'] = "paid";
				document.getElementById('closeModal').click()
				//  //console.log.log(response);
				//  this.FeedData =response['data'];
				//  this.isLoading=false;
			},
			err => {
				//  ////console.log.log(err);
				//  this.isLoading=false;
				// // this.router.navigate(['/login']);
				//  //console.log.log(err);
			}
		);
	}

	sharePOST(id){
		var baseURl=window.location.origin;
	}


	open(image,title,index): void {
		this.index2 = index
		this.title = title
		this.imgsrc = image;
		var modal = document.getElementById("myModalImage");
		modal.style.display = "block";
		var captionText = document.getElementById("caption");
		captionText.innerHTML = title;
	}
	closeModel(){
		var modal = document.getElementById("myModalImage");
		modal.style.display = "none";
	}

	changeSlideShow(type){
		var length = this.FeedData.length;
		if(type == 'right'){
			//alert(this.index2)
			if(length - 1   == this.index2){
				// this.index2 = 0
				this.loadMore();
			}
			else{
				this.index2  = parseInt(this.index2) + 1;
			}
			this.imgsrc =this.FeedData[this.index2]['model_image'];
			var captionText = document.getElementById("caption");
			captionText.innerHTML =this.FeedData[this.index2]['title'];
		}
		else{
			//alert(this.index2)
			if(this.index2 == 0){
				this.index2 = length - 1
			}
			else{
				this.index2  = parseInt(this.index2) - 1;
			}
			this.imgsrc =this.FeedData[this.index2]['model_image'];
			var captionText = document.getElementById("caption");
			captionText.innerHTML =this.FeedData[this.index2]['title'];
		}
	}




}
