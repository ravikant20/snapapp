import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
userLogged:boolean=false;
role:string
  constructor( private router: Router,) { }

  ngOnInit() {
    if(localStorage.getItem('loggedInUser')!=null){
      this.userLogged = true
      this.role = localStorage.getItem('role')
    }
  }

  changeStatus(){
    this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
    //this.router.navigate(["model-profile",{uid: idUser } ])); 
    this.router.navigate(['/signup'], { queryParams: {join: 'model' } }));
  }

}
