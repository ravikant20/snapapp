import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from 'src/app/http.model';
import { UserService } from 'src/app/http.service';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService]
})
export class ForgotComponent implements OnInit {
  public params;
  myPasswordResponse:any =[];
  Password:string ="";
  ConfirmPassword:string="";
  loading=false;
  constructor(private userService:UserService, private router: Router,private messageService: MessageService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => this.params = params);
    if(this.params.id == undefined ){
      this.router.navigate(['/login']);
    }
  }
  resetPassword(){
    //console.log(this.Password )
    if(this.Password == ""){
      this.myPasswordResponse['response']="this field cannot be blank";
      return false;
    }
    else{
      this.myPasswordResponse['response']="";
    }
    if(this.Password != this.ConfirmPassword){
      this.myPasswordResponse['response']="Password not matched";
      return false;
    }
    else{
      this.myPasswordResponse['response']="";
    }
    const uid={
      uid:this.params.id,
      newpassword:this.Password 
    }
    this.loading =true;
    this.userService.forgotPassword(uid).subscribe(
      response => {
        //console.log(response)
        if(response['success'] == 1){
          this.loading =false;
          this.messageService.add({severity:'success', summary: 'Success ', detail:'Password Reset successfully'});
          this.router.navigate(['/login']);
        }
        // else if(response['success'] == 0){
        //   this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        // }
        // else if(response['success'] == 2){
        //   this.messageService.add({severity:'error', summary: 'error ', detail:'Something went wrong please contact admin'});
        // }
      },
      err => {
   
      }
  );

  }


onConfirm() {
    this.messageService.clear('c');
  }

onReject() {
    this.messageService.clear('c');
  }
}
