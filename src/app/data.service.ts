import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {


  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  private imageSource = new BehaviorSubject('default image');
  currentImage = this.imageSource.asObservable();  

  private email = new BehaviorSubject(window.location.origin+"/");
  currentEmails = this.email.asObservable();  

  private joinAsa = new BehaviorSubject('1');
  currentjoinAsa = this.joinAsa.asObservable();  

  constructor() { 
    //#/model-profile?uid=193
    //console.log();
  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  changeImage(imageChange: string) {
    this.imageSource.next(imageChange)
  }
  changeEmail(email: string) {
    this.email.next(email)
  }
  changeStatus(email: string) {
    this.joinAsa.next(email)
  }

}