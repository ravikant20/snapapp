import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response, RequestMethod } from '@angular/http';
import { Headers } from '@angular/http';
import { User } from './http.model';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // getModelsProfile() {
  //   throw new Error("Method not implemented.");
  // }
  
  rootUrl: string;
  isLoading: boolean;
  
  constructor(private http : HttpClient) { }

  loginUser(_loginUser: { email: any; password: any; }){
    
    //this.rootUrl ='http://localhost:8888/laravel/snapapi/public/api'
    this.rootUrl = environment.baseUrl
    const data = {
      email: _loginUser.email,
      password: _loginUser.password,
    
  };

  //console.log.log(data);
    return this.http.post(this.rootUrl+'/login',data);
    
    
  }

  RegisterUser(RegisterData: { email: String; password: String; name: String; role:String;}) {
    this.rootUrl = environment.baseUrl
    const data = {
      email: RegisterData.email,
      password: RegisterData.password,
      name: RegisterData.name,
      role: RegisterData.role,
    
  };

  //console.log.log(data);
    return this.http.post(this.rootUrl+'/signup',data);
  }
  

public createAuthorizationHeader(headers: Headers) {
    
    headers.append('Content-Type', 'Application/json');
    const auth_token = localStorage.getItem('loggedInUser');
    if (auth_token) {
     headers.append('Authorization', 'Bearer ' + auth_token);
        
    }
}

  getProfile(id){
   this.rootUrl = environment.baseUrl;
   const dat = {
      uid : id.id,
      callback : id.callback,
   };
   ////console.log.log(dat);
   const api_token = localStorage.getItem('loggedInUser')
   let headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };

    //  // this.createAuthorizationHeader(Headers); 
      //return this.http.post(this.rootUrl+'/details',null, options );
      return this.http.post(this.rootUrl+'/details',dat,options);
  }

  getModels(){
    this.rootUrl = environment.baseUrl;

    return this.http.post(this.rootUrl+'/models',null);
  }

  getUserData(api){
    ////console.log.log(api);
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+api.api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/Userdata',null, options );
  }

  UploadContent(data){
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('loggedInUser') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/uploadModelContent',data, options );
  }


  
  UpdateUser(UpdateData){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/Userupdate',UpdateData, options );
  }

  getAllModels(api,listmodel){
      this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+api.api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/'+listmodel,api, options );
  }

  FollowUnfollow(followData){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+followData.api_token });
    let options = { headers: headers };
    const data = {
      model_id: followData.model_id,
      status: followData.id,
    };
    return this.http.post(this.rootUrl+'/followUnfollow',data, options );
  }

  getfeed(api,page,data,dateNow){
    const dat = {
      page :page,
      content:data,
      nowDate:dateNow,
    };
      this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+api.api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/yourfeed',dat, options );
  }

  BuyVideo(data){
      const dat = {  
        data:data
      };
      const api = localStorage.getItem('loggedInUser')
      this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+api});
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/BuyVideo',dat, options );
  }

  getAllFollowingModels(api){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+api.api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/followingModel',null, options );
  }

  ResetPassword(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/resetPassword',data, options );
  }


  ModelPrice(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/modelPrice',data, options );
  }



  Payment(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/FollowModelPayment',data, options );
  }

  getMsgChat(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/msgStatus',null, options );
  }
  SaveLastmsgUser(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/saveLastmsgUser',data, options );
  }
  SaveLastmsgModel(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/saveLastmsgModel',data, options );
  }

  modelFilter(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/filterModel',data ,options);
  }
  modelFilterFollowing(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/modelFilterFollowing',data, options );
  }
  uploadChatPic(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/uploadPic',data, options );
  }


  getAccoutDetail(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getAccoutDetail',null, options );
  }

  updateModelData(form){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updateAccountDetail',form, options );
  }

  getModelData(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelContent',null, options);
  }

  ChangeImage(form){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/changeModelPic',form, options );
  }

  ChangePostStatus(ids){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/ChangePostStatus',ids, options );
  }

  DeletePost(ids){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/DeletePost',ids, options );
  }

  Setavailability(availability){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/saveAvailability',availability, options );
  }
  
  getCalendarData(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getCalendarData',data, options );
  }

  getAllFollowingUsers(api){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelFollowers',null, options );
  }

  deleteAvailability(id){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      const ids = {
        id:id
      }
      return this.http.post(this.rootUrl+'/deleteAvailability',ids, options);
  }

  updateavailability(availability){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updateAvailability',availability, options );
  }

  modelFilterHome(data){
    return this.http.post(this.rootUrl+'/filterModelHome',data);
  }

  getModelComment(id){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getModelComment',id, options);
  }

  getUserComment(id){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getUserComment',id, options);
  }

  saveUserComment(data){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/saveUserComment',data, options);
  }

  getComment(share_id){
    const share ={
      post_id : share_id
    }
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getComment',share, options);
  }

  postComment(comment,share_id){
    const share ={
      post_id : share_id,
      comment : comment,
    }
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/postComment',share, options);
  }

  // LiveBroadCasting(data){
    
    
  //   return this.http.post('https://www.enacteservices.net/snapapp/livebroadcasting/',data);
  // }

  // ModelName(MID){
  //   const modelId ={
  //     model_id:MID,
  //   }
  //   const api_token = localStorage.getItem('loggedInUser');
  //   this.rootUrl = environment.baseUrl;
  //   let headers = new HttpHeaders({
  //   "Accept": "multipart/form-data" ,
  //   'Authorization': 'Bearer '+api_token });
  //   let options = { headers: headers };
  //   return this.http.post(this.rootUrl+'/ModelName',modelId, options);
  // }
  getNotifications(page){
    const data = {
      page:page,
    }
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getNotifications',data, options);
  }


  getFollowstatus(getFollowstatus){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getFollowstatus',getFollowstatus, options);
  }

  getVideostatus(livestatus){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getVideostatus',livestatus, options);
  }


  GetModelAvailability(data){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getModelAvailability',data, options);
  }
  
  AppointmentNotification(){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/AppointmentNotification',null, options);
  }

  ChangeNotificationStatus(data){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/ChangeNotificationStatus',data, options);
  }

  getMessageRechargeStatus(data){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getMessageRechargeStatus',data, options);
  }

  getCalendarDataModel(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getCalendarDataModel',data, options );
  }

  Billingstatus(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/Billingstatus',null, options );
  }

  bookingstatus(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/bookingstatus',null, options );
  }
  
  getModelForChat(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelForChat',data, options );
  }

  verifyEmail(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/verifyEmail',data, options );
  }

  getModelStats(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelStats',null, options );
  }

  getModelStatsFilter(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelStatsFilter',data, options );
  }

  deleteNotifications(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/deleteNotifications',data, options );
  }

  modelFilterWithoutLogin(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/modelFilterWithoutLogin',data);
  }

  getNotificationsModel(){
    const api_token = localStorage.getItem('loggedInUser');
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+api_token });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getNotificationsModel',null, options);
  }

  deleteNotificationsModel(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/deleteNotificationsModel',data, options );
  }


  blockUser(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/blockUser',data, options );
  }

  modelFilterLive(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/modelFilterLive',data, options );
  }

  getModelDeactivateStatus(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelDeactivateStatus',null, options );
  }


  setDeactivate(types){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/setDeactivate',types, options );
  }

  submitPayoutDetails(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/submitPayoutDetails',data, options );
  }
  getPayoutDetails(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getPayoutDetails',null, options );
  }

  LiveChatLogs(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/LiveChatLogs',data, options );
  }

  ResetPasswordLink(email){
    email={
      email:email
    }
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/ResetPasswordLink',email );
  }

  forgotPassword(data){
 
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/forgotPassword',data );
  }

  PaymentStatus(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/CheckCustomerDetail',data,options );
  }

  CreatePayment(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/CreatePayment',data,options );
  }

  GetMsgCount(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/GetMsgCount',data,options );
  }

  updateMsgCount(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updateMsgCount',data,options );
  }

  DeleteComment(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/DeleteComment',data,options );
  }

  getAccoutPrice(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getModelPrices',null,options );
  }
  updateModelPrices(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updateModelPrices',data,options );
  }

  resendLink(data){
    const email = {
      email:data
    }
    return this.http.post(this.rootUrl+'/resendLink',email);

  }
  deleteUserAccount(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
    return this.http.post(this.rootUrl+'/deleteAccount',null,options);
  }

  viewAccountPreview(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
    return this.http.post(this.rootUrl+'/viewAccountPreview',null,options);
  }

  saveProfileData(data){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
    return this.http.post(this.rootUrl+'/saveProfileData',data,options);
  }

  getNewDta(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data" });
      let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getDataHome',null,options);
  }

  checkStatusAccount(){
    const api_token = localStorage.getItem('loggedInUser')
    this.rootUrl = environment.baseUrl;
      let headers = new HttpHeaders({
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer '+api_token });
      let options = { headers: headers };
    return this.http.post(this.rootUrl+'/checkStatusAccount',null,options);
  }
  

  
}
