import { Component, OnInit, ViewEncapsulation,ViewChildren,QueryList,ElementRef } from '@angular/core';
import { UserService } from '../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class ShareComponent implements OnInit {

  @ViewChildren("scrollMe") messageContainers: QueryList<ElementRef>;

  public params;
  showCcomment:any=[];
  modelData:any;
  NameImage:string;
  CommentData:string;
  PriceVideo:string;
  fvideoPrice :string;
  buyingasset:string;
  index:string;
  loading:boolean=true;
  URL:string;
  editmode:boolean=true;
  SameModel:string
  role:any;
  userId:any;
  commentUseId:any
  constructor(private userService:UserService,private router: Router, private route:ActivatedRoute,private messageService: MessageService) {
    
   }

  ngOnInit() {
   if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
    this.route.queryParams.subscribe((params: Params) => this.params = params);
    this.getDataComment();
    this.role = localStorage.getItem('role')
    if(localStorage.getItem('profileImage') != '0'){
			this.NameImage = localStorage.getItem('profileImage');
		}
		else{
			this.NameImage = 'assets/images/dummy-profile.png'
    }
    this.userId = localStorage.getItem('id');
  }


  getDataComment(){
    //this.loading=true;
    this.userService.getComment(this.params.shareId).subscribe(
      response => {
         console.log(localStorage.getItem('role'));
         this.showCcomment = response['data']['comment_data'];
         this.modelData = response['data'];
         this.commentUseId = response['data']['user_id']
        /// alert(this.modelData['paid'])
        // this.NameImage = localStorage.getItem('profileImage');
         if(response['data']['followOrNot'] !=1 && (localStorage.getItem('role')=='1' || localStorage.getItem('role')==null) && response['data']['model_id'] != localStorage.getItem('id')){
          this.router.navigate(['/model-profile'], { queryParams: {uid: response['data']['id'] } });
         }
         if(response['data']['model_id'] == localStorage.getItem('id')){
          this.SameModel = 'yes';
          }
          else{
            this.SameModel = 'no';
          }
         this.loading=false;
        // 
      },
      err => {
         ////console.log.log(err);
         //this.isLoading=false;
        // this.router.navigate(['/login']);
         //console.log.log(err);
      }
    );
  }

  postComment(){
    if(this.CommentData==""){
      return false;
    }
    this.editmode=false;
    this.userService.postComment(this.CommentData,this.params.shareId).subscribe(
      response => {
        this.getDataComment();
        this.CommentData="";
        this.messageService.add({severity:'success', summary: 'Success', detail:'Comment Posted'});
        this.editmode=true;
        // 
      },
      err => {
         ////console.log.log(err);
         //this.isLoading=false;
        // this.router.navigate(['/login']);
         //console.log.log(err);
      }
    );
  }


  payForVideo(price){
    
    document.getElementById('showModel').click();
    this.PriceVideo = price;
    // alert("sfad");
  }



  deleteComment(id){
    const data = {
      comment_id:id
    }
    this.userService.DeleteComment(data).subscribe(
      response => {
        this.messageService.add({severity:'success', summary: 'Success', detail:'Comment Deleted Successfully'});

        this.getDataComment();
        //document.getElementById('closeModal').click()
      },
      err => {
        console.log(err)
      }
    );
  }

  SelectCopy(){
    var copyText = window.location.origin;
    //copyText.select();
  }

  onConfirm() {
    this.messageService.clear('c');
}

  onReject() {
      this.messageService.clear('c');
  }

}
