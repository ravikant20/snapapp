import { Component, OnInit, ViewChild, ElementRef, NgZone  } from '@angular/core';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
//import { StripeScriptTag } from "stripe-angular";
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { Http} from '@angular/http';
import * as $AB from 'jquery';
import * as bootstrap from 'bootstrap';
@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.css']
})

export class ExploreComponent implements OnInit {
  //@ViewChild('formDirective' {static: false}) private formDirective: NgForm;
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  @ViewChild('editForm', {static: false})
  private publishableKey:string = "pk_test_XsCV2lAW6EG6jRrulmXgma2M00cMQ8KnqI"
  modelData: any =[];
  editmode =true;
  modelPrice:any =[];
  modelID:any =[];
  private formData = new FormData();
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  display='none';
  nameMain:string;
  elementsOptions: ElementsOptions = {
    locale: 'es'
  };
  showModal=true;
  paymentConfirmation:any =[];
  loading: boolean;
  form: FormGroup;
  editForm: NgForm;
  isLoading:boolean=true;
  constructor(private userService:UserService,private router: Router,private stripeService: StripeService,private http : Http,
    private fb: FormBuilder,private _zone: NgZone,) { 
    // this.StripeScriptTag.setPublishableKey( this.publishableKey )
  }

  ngOnInit() {
    this.getAllModels();
 
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
      this.router.navigate(['/model-account']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
  }



  getAllModels(){
    
    const api = {
      api_token:localStorage.getItem("loggedInUser")
     }
     this.userService.getAllModels(api,'listModel').subscribe(
       response => {
        console.log(response);
        if(response['success']==1){
          this.modelData = response['data'];
          console.log(response['data'])
          this.isLoading=false;
        }
        else{
          alert("No models Found");
          this.isLoading=false;
        }
         
       },
       err => {
         this.router.navigate(['/login']);
         console.log(err);
       }
     );
  }

  FollowModel(id,model_id,i){
    this.editmode = false;
    console.log(id);
    console.log(model_id);
    const followData = {
      id: id,
      model_id: model_id,
      api_token:localStorage.getItem("loggedInUser")
    }
    this.userService.FollowUnfollow(followData).subscribe(
      response => {
       //console.log(response);
       if(response['success']==1){
        if(id==1){
          var mod =0;
        }
        else if(0==id){
          var mod =1;
        }
        //console.log(mod)
        this.modelData[i]['following_status'] =mod;
        this.editmode = true;
        // console.log('following_status', this.modelData.following_status);
        //this.getAllModels();
       } 
      },
      err => {
        //this.router.navigate(['/login']);
        console.log(err);
        alert("something went wrong")
      }
    );
  }

  getModelPrice(id,routeId){

   // console.log(id);
    const followData = {
      model_id: id,
    }
    this.userService.ModelPrice(followData).subscribe(
      response => {
        console.log(response);
        this.modelPrice['price']=response['data']['price'];
        this.modelID['id']=id;
        this.modelPrice['routeId']=routeId;
        this.display='block';
        jQuery.noConflict();
        //this.editForm.resetForm();
        this.mytemplateForm.reset();
        document.getElementById('paymentPopup').click();
        
        //this.display='none';
        //$("#payment").modal('show');
      },
      err => {
       
      }
    )
  }




  getToken(price,routeId,modelId) {
    this.loading=true;
    (<any>window).Stripe.card.createToken({
      number: this.cardNumber,
      exp_month: this.expiryMonth,
      exp_year: this.expiryYear,
      cvc: this.cvc,
    }, (status: number, response: any) => {
      console.log(response);
      this._zone.run(() => {
        if (status === 200) {
          const paymentToken = {
            stripeToken: response.id,
            model_id: modelId,
          }
          this.userService.Payment(paymentToken).subscribe(
            response => {
              //console.log(response);
              if(response['success'] == '1'){
                alert("payment successfull");
                this.FollowModel(0,modelId,routeId);
                jQuery.noConflict();
               // $(".modal").modal('hide');
               document.getElementById('closeModel').click();
                this.loading=false;
              }
              else{
                this.loading=false;
                this.message='payment failed please try again'
                alert("payment failed please try after sometime");
              }
             
            },
            err => {
              alert("payment failed please try after sometime");
            }
          )
         
        } else {
          alert(response['error']['code']);
          this.loading=false;
          this.message =response['error']['code']
        }
      });
    }
    );
  }
  
  checkDigit() {
    // alert("rk")
     var allowedChars = "0123456789";
     var entryVal = (<HTMLInputElement>document.getElementById('txt_cardNumber')).value;
     //alert(entryVal.length);
     var flag;
     if(entryVal.length == 4 || entryVal.length == 9|| entryVal.length == 14){
       this.cardNumber = this.cardNumber+" ";
     }
     // for(var i=0; i<entryVal.length; i++){       
     //     flag = false;
 
     //     for(var j=0; j<allowedChars.length; j++){
     //         if(entryVal.charAt(i) == allowedChars.charAt(j)) {
     //             flag = true; 
     //         }
     //     }
 
     //     if(flag == false) { 
     //         entryVal = entryVal.replace(entryVal.charAt(i),""); i--; 
     //     }
     // }
 
     return true;
 }

 




}
