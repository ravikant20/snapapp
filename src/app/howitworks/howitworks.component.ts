import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-howitworks',
  templateUrl: './howitworks.component.html',
  styleUrls: ['./howitworks.component.css']
})
export class HowitworksComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  changeStatus(){
    this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
    //this.router.navigate(["model-profile",{uid: idUser } ])); 
    this.router.navigate(['/signup'], { queryParams: {join: 'model' } }));
  }
}
