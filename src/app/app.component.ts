import { Component } from '@angular/core';
import { PresenceService } from './presence.service';
import { Router, ActivatedRoute,Params } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public presence: PresenceService) {

    //alert("R")
    setInterval(()=>{    //<<<---    using ()=> syntax
      this.changeSlider();
    // console.log('a')
    }, 5000);
    
  }
  changeSlider(){
    var elem = document.getElementById('changeSlider');
    if(elem!=null){
    document.getElementById('changeSlider').click();
    }
    else{
    //clearTimeout(this.timer1)
    }
  }
  title = 'snap';
  onActivate(event) {
    let scrollToTop = window.setInterval(() => {
        let pos = window.pageYOffset;
        if (pos > 0) {
            window.scrollTo(0, pos - 280); // how far to scroll on each step
        } else {
            window.clearInterval(scrollToTop);
        }
    }, 16);
}
}
