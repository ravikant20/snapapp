import { Component, OnInit, ComponentFactoryResolver, ɵConsole, ViewChild, ViewEncapsulation } from '@angular/core';
import { UserService } from '../http.service';
import { Router } from '@angular/router';
import {  HttpClient } from '@angular/common/http';
import { DataService } from "../data.service";
import { $ } from 'protractor';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
import {MessageService} from 'primeng/api';
import {CheckboxModule} from 'primeng/checkbox';

interface Post {
  title: string;
  content: string;
}
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  providers: [MessageService],
  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    encapsulation: ViewEncapsulation.None,
})
export class SettingsComponent implements OnInit {
  @ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
  postsCol: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;
  getProfileData:any =[];
  files: any[];
  imageSrc: string | ArrayBuffer;
  result:String;
  public email: string ;
  public name: string;
  public bio: string;
  myValidation: any =[];
  loading: boolean;
  loading2:boolean;
  private formData = new FormData();
  private formDataReset = new FormData();
  public newPassword: string ;
  public oldPassword: string;
  passwordValidation: any =[];
  fileData: File = null;
  message:string;
  isLoading:boolean=true;
  confirmCheck:string;
  selectedNotifications: any = [1];
  //loading:boolean;
  constructor(private userService:UserService,private router: Router,private http: HttpClient,private data: DataService,private afs: AngularFirestore,private messageService: MessageService) { }

  ngOnInit() {
   // alert("sdf")
    this.getModels();
    ///this.data.currentMessage.subscribe(message => this.message = message)
    if(localStorage.getItem('loggedInUser') != null  && localStorage.getItem('role') == '2' && localStorage.getItem('status') == '0'){
      this.router.navigate(['/model-account']);
    }
    else if(localStorage.getItem('role') == '2' && localStorage.getItem('status') == '1'){
      this.router.navigate(['/model-account']);
      //alert("status confirmed")
    }
    else if(localStorage.getItem('loggedInUser') == null ){
      this.router.navigate(['/login']);
    }
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }
  // code to view image before uploading
  readURL(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        console.log(this.formData)
        reader.readAsDataURL(file); 
      } 
  }
  public getModels(){
    const api = {
     api_token:localStorage.getItem("loggedInUser")
    }
    this.userService.getUserData(api).subscribe(
      response => {
        if(response['success']==1){
           this.getProfileData['name']=response['data'][0]['user_name'];
           this.getProfileData['user_email']=response['data'][0]['user_email'];
           this.selectedNotifications = response['data'][0]['notification_for'];
           //console.log.log(response['data'][0]['user_image'])
           if(response['data'][0]['user_image']!=""){
             ////console.log.log("here");
            this.getProfileData['user_image']=response['data'][0]['user_image'];
           }
           else{
            ////console.log.log("there");
            this.getProfileData['user_image']="assets/images/dummy-profile.png";
           }
           
           this.getProfileData['user_id']=response['data'][0]['user_id'];
           this.getProfileData['user_bio']=response['data'][0]['user_bio'];
           //console.log.log(this.getProfileData);
           this.isLoading=false;
           
        }
        else{
          this.router.navigate(['/login']);
          alert("something went wrong");
        }
        
      },
      err => {
        this.router.navigate(['/login']);
        //console.log.log(err);
       
      }
		);
  }



  onSubmit() {
    if(this.email =="undefined" || this.email ==undefined){
      ////console.log.log("here");
      this.email = this.getProfileData['user_email'];
    }
    else{
      this.email = this.email;
    }
    if(this.name =="undefined" || this.name ==undefined){
      this.name = this.getProfileData['name'];
    }
    else{
      this.name = this.name;
    }
    if(this.bio =="undefined" || this.bio ==undefined){
      this.bio = this.getProfileData['user_bio'];
    }
    else{
      this.bio = this.bio;
    }
    if(this.name==""){
      this.myValidation['name']="Name Cannot Be empty"
      return false;
    }
    else{
      this.myValidation['name']=""
    }
    if(this.email==""){
      this.myValidation['email']="Email Cannot Be empty"
      return false;
    }
    else{
      this.myValidation['email']=""
    }
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

    if (!pattern.test(this.email) ) {
      //alert("ds")
      //console.log.log(this.email);
      this.myValidation["email"] = "Email Not Valid!";
      return false;
    }
    else{
      this.myValidation["email"] = "";
    }
    //this.isLoading=true;
    this.formData.append('email', this.email);
    this.formData.append('name', this.name);
    this.formData.append('bio', this.bio);
    // //console.log.log(this.selectedNotifications.toString())
    this.formData.append('notificationFor', this.selectedNotifications.toString());
    this.loading=true;
    this.userService.UpdateUser(this.formData).subscribe(
      response => {
        //console.log.log(response);
        this.messageService.add({severity:'success', summary: 'Success', detail:'Profile Updated'});
        if(response['success']==0){
          this.data.changeMessage(this.name);
          //console.log.log(response['data']['mainImage'])
          if(0 != response['data']['mainImage'] ){
            this.data.changeImage(response['data']['mainImage']);
            localStorage.setItem("profileImage", response['data']['mainImage']);
          }
          else{
            this.data.changeImage("assets/images/dummy-profile.png");
            localStorage.setItem("profileImage", "assets/images/dummy-profile.png");
            //console.log.log("There");
          }
          
          localStorage.setItem("name", this.name);
          
          this.loading=false;
        }
        this.loading=false;
        ////console.log.log(response);
        //this.isLoading=false;
      },
      err => {
        this.loading=false;
        // console.log(err['error']);
        this.messageService.add({severity:'error', summary: 'Please Retry', detail:err['error']['error']});
        //this.myValidation['err'] = ''
      }
    );
  }

  onSubmitPasswordReset(){
    if(this.oldPassword =="undefined" || this.oldPassword ==undefined || this.oldPassword == ""){
      ////console.log.log("here");
      this.passwordValidation['oldpassword'] = "Old password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['oldpassword'] = "";
    }
    if(this.newPassword =="undefined" || this.newPassword ==undefined || this.newPassword == "") {
      ////console.log.log("here");
      this.passwordValidation['newpassword'] = "New password field cannot be blank";
      return false;
    }
    else{
      this.passwordValidation['newpassword'] = "";
    }
    if(this.confirmCheck !="undefined" && this.confirmCheck != this.newPassword) {
      ////console.log.log("here");
      this.passwordValidation['confirmPassword'] = "Confirm Pasword and New Password must be same";
      return false;
    }
    else{
      this.passwordValidation['confirmPassword'] = "";
    }
    this.formDataReset.append('newPassword',this.newPassword);
    this.formDataReset.append('oldPassword',this.oldPassword);
    this.loading2=true;
    this.userService.ResetPassword(this.formDataReset).subscribe(
      response => {
        if(response['success']==1){
          document.getElementById('dismissmodal').click();
          this.messageService.add({severity:'success', summary: 'Success', detail:'Password changed successfully'});
          
          //this.passwordValidation['result'] = "Password changed successfully";
          //$('#myModal').modal('hide')

          this.loading2=false;
          this.newPassword= "" ;
          this.oldPassword= "";
          this.confirmCheck="";
          //this.passwordValidation['result']=""
        }
        if(response['success']==0){
          document.getElementById('dismissmodal').click();
          this.messageService.add({severity:'error', summary: 'error', detail:'Old Password does not matched'});
          //this.passwordValidation['result'] = "Old Password does not matched";
        }
        if(response['success']==2){
          document.getElementById('dismissmodal').click();
          this.messageService.add({severity:'error', summary: 'error', detail:'Some error occured please try after sometime'});
          //this.passwordValidation['result'] = "Some error occured please try after sometime";
        }
        
      },
      err => {
       
          this.passwordValidation['result'] = "password must be of 6 chracter";
       
          //this.loading=false;
          //console.log.log(err);
          //alert("Something Went Wrong")
      }
    );
  }



  hideErrors(){
    this.passwordValidation['result'] = "";
    this.passwordValidation['oldpassword'] = "";
    this.passwordValidation['newpassword'] = "";
    this.passwordValidation['confirmPassword'] = "";
    this.newPassword = "";
    this.oldPassword ="";
    this.confirmCheck =""
  }


  onConfirm() {
    this.messageService.clear('c');
}

onReject() {
    this.messageService.clear('c');
}

  setDeactivate(){
    this.userService.deleteUserAccount().subscribe(
      response => {
          document.getElementById('closeDeactivatePopup2').click();
          this.router.navigate(['/logout']);
      },
      err => {

      }
    );
  }
}


