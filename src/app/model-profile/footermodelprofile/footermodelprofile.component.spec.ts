import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FootermodelprofileComponent } from './footermodelprofile.component';

describe('FootermodelprofileComponent', () => {
  let component: FootermodelprofileComponent;
  let fixture: ComponentFixture<FootermodelprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FootermodelprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FootermodelprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
