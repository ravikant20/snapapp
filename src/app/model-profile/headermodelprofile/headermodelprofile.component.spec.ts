import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadermodelprofileComponent } from './headermodelprofile.component';

describe('HeadermodelprofileComponent', () => {
  let component: HeadermodelprofileComponent;
  let fixture: ComponentFixture<HeadermodelprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadermodelprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadermodelprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
