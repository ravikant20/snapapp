import { Component, OnInit,ViewChild, ElementRef, NgZone, ViewChildren, QueryList,ViewEncapsulation } from '@angular/core';
import { UserService } from '../http.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
//import { StripeScriptTag } from "stripe-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { Http} from '@angular/http';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
import {RatingModule} from 'primeng/rating';
import {MessageService} from 'primeng/api';
import {DomSanitizer} from "@angular/platform-browser";
import { environment } from '../../environments/environment';
interface Post {
	title: string;
	content: string;
  }
@Component({
	selector: 'app-root',
	templateUrl: './model-profile.component.html',
	styleUrls: ['./model-profile.component.css'],
	encapsulation: ViewEncapsulation.None,
	providers: [MessageService],
	styles: [`
			:host ::ng-deep button {
				margin-right: .25em;
			}

			:host ::ng-deep .custom-toast .ui-toast-message {
				color: #ffffff;
				background: #FC466B;
				background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
				background: linear-gradient(to right, #3F5EFB, #FC466B);
			}

			:host ::ng-deep .custom-toast .ui-toast-close-icon {
				color: #ffffff;
			}
		`],
})
export class ModelProfileComponent implements OnInit {
	@ViewChild('myForm',{static: false}) mytemplateForm : NgForm; 
	@ViewChildren("scrollMe") messageContainers: QueryList<ElementRef>;
	@ViewChild('window',{static: false}) window;
	
	@ViewChild('editForm', {static: false})
	postsCol: AngularFirestoreCollection<Post>;
	posts: Observable<Post[]>;
	tsCol: AngularFirestoreCollection<Post>;
	sts: Observable<Post[]>;
	getChatount: AngularFirestoreCollection<Post>;
	countChatUser: Observable<Post[]>;
	
	private publishableKey:string = "pk_test_XsCV2lAW6EG6jRrulmXgma2M00cMQ8KnqI"
	private api_tokens:any;
	myData: any[] = [];
	myDataProfile: any = [];
	public params;
	isLoading:boolean =true;
	checkFollowing:string;
	modelData: any =[];
	editmode =true;
	modelPrice:any =[];
	modelID:any =[];
	private formData = new FormData();
	cardNumber: string;
	expiryMonth: string;
	expiryYear: string;
	cvc: string;
	message: string;
	nameMain:string;
	display='none';
	elementsOptions: ElementsOptions = {
	  locale: 'es'
	};
	showModal=true;
	paymentConfirmation:any =[];
	loading: boolean;
	form: FormGroup;
	editForm: NgForm;
	searchFilter:string;
	//public param;
	search:any;
	searchedOn:boolean;
	featured:boolean=false;
	featuredName:string;
	public email: String ;
	public password: String;
	myEmailResponse: any = [];
	myPasswordResponse: any = [];
	myIncorrectPassword: any =[];
	model_id:string;
	user_id:string;
	loggedIn:boolean =false;
	userId:number;
	userName:string;
	userProfile:string;
	title:string;
	content:string;

	getProfileData:any =[];
	
	dates:any=[];

	userID:any;
	modelId:string;
	modelImage:string;
	userImage:string;
	isModelChat:boolean = false;
	imageSrc: string | ArrayBuffer;
	files: any[];
	isUpload:boolean=false;
	type:string;
	messages:string;
	
	chatCount:number;
	chatId:string;

	maxChat:number;
	totalChat:number;
	msgOver:boolean=true;
	val: number;
	getData :any =[];
	emptyGetdata:boolean;
	showAverage:string;
	totalComment:string;
	alreadyCommented:boolean;
	SaveduserName:string = localStorage.getItem('name');
	SaveduserImage:string = localStorage.getItem('profileImage');
	CommentTextArea:string;
	showAverage2:string;
	method:string;
	imageFile:string;
	loader123:boolean=false;
	responseAvailability:any=[];
	livevideoCamPrice:any;
	formUrl:any =  this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton.php')
	//this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl('https://enacteservices.net/snapapp/savedata/formButton.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+ this.modelPrice['price']);

	epochTime:any;
	epochDate:any;
	showContent:any;
	followingStatus:any;

	uniqueId:any;
	messagePrice:any
	Month:any;
	Year:any;
	calendar:any=[];

	appointmentId :any;
	appointmentTime :any;
	appointmentDate :any;

	appointmentweekday :any;
	appointmentstart_time:any;
	appointmentyear :any;
	appointmentmonth:any;
	priceCalc:number = 1;
	chatIdString:string;
	chatReference:any;
	messageReference:any;
	getchatData=[];
	getLogStatus:boolean =false;;
	loggedInuser:any = localStorage.getItem('id');
	typing:boolean = false;
	timer1  = setTimeout(()=>{
		// this.stopTyping();
	  }, 5000); 
	currentActiveModel:any ='undefined';
	typingStatus:boolean =false;
	imgsrc:any;  
	blockedStatus:any;  
	AddActiveClass:string = 'finpay';
	expYear:string
	expMonth:string
	nameCard:string
	minChat:Number
	loadingAvailability:boolean =false
	index2:any
	ImgArray:any = [];
	time:any;
	appointmentend_time:string;
	dateTime = new Date();
	dateTimeMax:any;
	ShowStaticTip:boolean = false;
	staticTip:string;
	AddActiveClass2:string;
	modelMessageAccount:string;
	part:String;
	constructor(private userService:UserService,private router: Router, private route:ActivatedRoute,private _zone: NgZone,private afs: AngularFirestore,private messageService: MessageService,private domSanitizer : DomSanitizer) { 
		this.isLoading=true;
	}
	
	ngOnInit() {
		this.isLoading=true;
		//alert("init")
		this.route.queryParams.subscribe((params: Params) => this.params = params);
		////console.log.log(this.params);
		this.currentActiveModel = this.params.uid
		//console.log("this.currentActiveModel",this.currentActiveModel)
		if(this.currentActiveModel == undefined){
			var currentURL = document.createElement('a');  
			currentURL.href = (document.URL); // returns http://myplace.com/abcd
			var c_url =  currentURL.pathname
			console.log("c_url",c_url)
			this.part = c_url.split("/")[c_url.split("/").length - 1];
			console.log("part",this.part);
			console.log("this.params.uid",this.params.uid);
			this.getModelProfile(this.part,"username");
		}
		else{
			this.currentActiveModel= this.params.uid;
			console.log("this.params.uid",this.params.uid);
			this.getModelProfile(this.params.uid,"id");
		}
		
		// this.ModelProfile()
		
		if(localStorage.getItem('loggedInUser') != null ){
			this.getLogStatus = true;
			this.userId = Number(localStorage.getItem('id'));
			this.userName = localStorage.getItem('name');
			this.userProfile = localStorage.getItem('profileImage');
		  }
		  else{
			this.getLogStatus = false;
		  }

		  if(localStorage.getItem('role') == '2' ){
			this.router.navigate(['/model-content']);
		  }
		  this.getLiveChatStatus();
		  this.livechatStatus();
		//   this.postsCol = this.afs.collection('posts', ref => ref.where('userID', '==', Number(this.userId)).where('modelId', '==', Number(this.params.uid)).orderBy('time'));
		//   this.posts = this.postsCol.valueChanges();  
		// this.getChatount= this.afs.collection('chatCounter', ref => ref.where('userID', '==', Number(this.userID)).where('modelId', '==',  Number(this.params.uid)));
		// this.countChatUser= this.tsCol.valueChanges();
		
		// alert(part); // alerts abcd
	}

	livechatStatus(){
		var arrayModelId = [this.params.uid];
		//console.log.log("arrayModelId",arrayModelId)
		arrayModelId.forEach(element => {
			var modelId = element;
			var userId = localStorage.getItem('id');
			var chatId = userId + '_' + this.currentActiveModel;
			if (this.currentActiveModel < userId) {
				chatId = this.currentActiveModel + '_' + userId;
			}

			//console.log.log("chatId", chatId);

			var ref = this.afs.collection('posts').doc(chatId).collection("messsages");
			var listener = ref.valueChanges();
			listener.subscribe(res => {
				////console.log.log("newdfsdf",res);
				if (res.length > 0) {
				var aMsg = res[0];
				var mId = aMsg['modelId'];
				var uId = aMsg['userID'];
				var cId = uId + '_' + mId;
				if (modelId < uId) {
					cId = mId + '_' + uId;
				}
				////console.log.log('chatId', cId);

				res.sort(function (a, b) {
					return a['time'].seconds - b['time'].seconds;
				});
				this.getchatData = res;

				}

			})
			//this.postRefrenceArray.push(listener);
		});
	}

	public getModelProfile(id: String,callback){
		//console.log.log(id);
		const Data = {
			id: id,
			callback:callback
		}
		//this.isLoading=true;
		this.userService.getProfile(Data).subscribe(
			response => {
				this.params ={'uid':response['data']['model_id']};
				this.currentActiveModel = this.params.uid;
				this.part = response['data']['profile_url']
				this.loader123=true;
				this.isLoading=false;
				//console.log.log(response);
				if(response['data'].length == 0){
					document.getElementById('alertBox').click();
					this.modelMessageAccount = "Model you are trying to reach is currently unavailable!"
					return;
				}
				if(response['success'] == 1){
					if(response['data']['status'] == "0"){
						document.getElementById('alertBox').click();
						this.modelMessageAccount = "Model you are trying to reach is in pending state please try again later"
						return;
					}
					if(response['data']['status'] == "2"){
						document.getElementById('alertBox').click();
						this.modelMessageAccount = "Model account has been deactivated please contact admin, or try again later"
						return;
					}
					if(response['data']['status'] == "3"){
						document.getElementById('alertBox').click();
						this.modelMessageAccount = "Model account has been deleted please contact admin"
						return;
					}
					if(response['data']['status'] == "4"){
						document.getElementById('alertBox').click();
						this.modelMessageAccount = "Admin has disabled this account please try after sometime"
						return;
					}
					if(response['data']['accountStatus'] == "1"){
						document.getElementById('alertBox').click();
						this.modelMessageAccount = "Model has deactivated her account Please try again later"
						return;
					}
					if(response['data']['accountStatus'] == "2"){
						document.getElementById('alertBox').click();
						this.modelMessageAccount = "Model has deleted her account"
						return;
					}
					this.isLoading=false;
					//console.log.log(response);
					this.blockedStatus = response['data']['blocked'];
					this.myDataProfile["media"] = response['data']['media'];
					//this.myDataProfile["comment"] = response['data']['comment'];
					// console.log(this.myDataProfile["comment"])
					for(var i=0;i<response['data']['media'].length;i++){
						if(response['data']['media'][i]['type'] == 1)
						{
							this.ImgArray.push({"media":response['data']['media'][i]['mainMedia'],"id":i,"title":response['data']['media'][i]['title']}) ;
						}
					}
					if(response['data']['cover_Image'] ==undefined || response['data']['cover_Image'] == null || response['data']['cover_Image'] == ""){
						this.myDataProfile["cover"] = "assets/images/placeholder.png";
					}
					else{
						this.myDataProfile["cover"] = response['data']['cover_Image'];
					}
					if(response['data']['profile_image']==undefined || response['data']['profile_image'] == null || response['data']['profile_image'] == ""){
						this.myDataProfile["profile"] = "assets/images/dummy-profile.png";
					}
					else{
						this.myDataProfile["profile"] = response['data']['profile_image'];
					}
					
					this.myDataProfile["name"] = response['data']['name'];
					this.myDataProfile["website"] = response['data']['website'];
					this.myDataProfile["bio"] = response['data']['bio'];
					this.myDataProfile["twitter"] = response['data']['twitter'];
					this.myDataProfile["instagram"] = response['data']['instagram'];
					this.myDataProfile["countVideo"] = response['data']['countVideo'];
					this.myDataProfile["countImage"] = response['data']['countImage'];
					this.myDataProfile["price"] = response['data']['price'];
					this.myDataProfile["model_id"] = response['data']['model_id'];
					this.myDataProfile["user_id"] = response['data']['user_id'];
					this.myDataProfile["model_comment"] = response['data']['model_comment'];
					this.checkFollowing = response['data']['following'];
					this.followingStatus = response['data']['followingStatus'];
					this.livevideoCamPrice = response['data']['livevideoCamPrice'];
					if(response['data']['following'] == 1){
						this.loggedIn=true;
					}
					else{
						this.loggedIn=false;
					}
					//console.log.log(this.myDataProfile);
					this.isLoading=false;
				}
				else{
					alert("something went wrong!");
				}
					
			},
			err => {
				this.loader123=true;
				alert("something went wrong!");
				//console.log.log(err);
			}
		);
	}

	continueClick(){
		document.getElementById('model-closeEmailReset22').click();
		this.router.navigate(['/login']);
	}

	readURL(event) 
	{ 
	  if (event.target.files && event.target.files[0]) 
		{ const file = event.target.files[0]; 
		  const reader = new FileReader(); 
		  reader.onload = e => this.imageSrc = reader.result; 
		  this.files = event.target.files;
		  this.formData.append("file", event.target.files[0]);
		  //console.log.log(event.target.files)
		  reader.readAsDataURL(file);
		  this.isUpload=true; 
		} 
	}




	 changeStatus(model_id,user_id,status){
		 if(status==0){
			this.getModelPrice(model_id)
			document.getElementById('paymentPopup').click();

		 }
		 else if(status==1){

		 }
		 else if(status==2){
			document.getElementById('loginPopupMain').click();
		}
		//
	 }



	 public loginUser(idUser){
		////console.log.log(this.email);
			
		const loginData = {
		  email: this.email,
		  password: this.password
		}
	  ////console.log.log(this.email)
	  if(undefined == this.email || ''==this.email ){
		this.myEmailResponse["response"] = "Email Cannot Be Empty!";
		return false;
	  }
	   else{
		this.myEmailResponse["response"] = "";
		//   //return false;
	   }
	  if(this.password == undefined || ''==this.password){
		this.myPasswordResponse["response"] = "Password Cannot Be Empty!";
		return false;
	  }
	   else {
		this.myPasswordResponse["response"] = "";
	  //   //return false;
	  }
	  this.loading=true;
	  this.userService.loginUser(loginData).subscribe(
		response => {
		  this.loading=false;
		  if(response['success']['token']!="" && response['success']['role'] == 1){
			  //alert(this.part)
			localStorage.setItem("loggedInUser", response['success']['token']);
			localStorage.setItem("role", '1');
			localStorage.setItem("status", response['success']['status']);
			localStorage.setItem("id", response['success']['id']);
			////console.log.log(response['success']['profileImage']);
			//console.log.log(response['success']['token']);
			if(response['success']['profileImage']){
			  localStorage.setItem("profileImage", response['success']['profileImage']);
			}
			else{
			  localStorage.setItem("profileImage", '0');
			}
		   
			localStorage.setItem("name", response['success']['name']);
			//this.router.navigate(['/model-profile?uid='+this.model_id]);
			//window.location.reload();
			document.getElementById('closeLoginPopup').click();
			this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
			//this.router.navigate(["model-profile",{uid: idUser } ])); 
			this.router.navigate([this.part]));
		  }
		  else if(response['success']['token']!="" && response['success']['role'] == 2){
			document.getElementById('closeLoginPopup').click();
			this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
			//this.router.navigate(["model-profile",{uid: idUser } ])); 
			this.router.navigate([this.part]));
			localStorage.setItem("role", '2');
			localStorage.setItem("status", response['success']['status']);
			localStorage.setItem("loggedInUser", response['success']['token']);
			localStorage.setItem("id", response['success']['id']);
			//console.log.log(response['success']['token']);
			if(response['success']['profileImage']){
			  localStorage.setItem("profileImage", response['success']['profileImage']);
			}
			else{
			  localStorage.setItem("profileImage", '0');
			}
		   
			localStorage.setItem("name", response['success']['name']);
			//alert("model")
			//this.router.navigate(['/model-account']);
			
		  }
	
		  else{
			this.router.navigate(['/login']);
			if(response['success']['profileImage']){
			  localStorage.setItem("profileImage", response['success']['profileImage']);
			}
			else{
			  localStorage.setItem("profileImage", '0');
			}
		   
			localStorage.setItem("name", response['success']['name']);
		  }
		},
		err => {
		  this.loading=false;
		  if(err.error.error == "Unauthorised"){
			this.myIncorrectPassword["response"] = "Incorrect Email Or Password";
			
		  }
		  else{
			this.myIncorrectPassword["response"] = "Something went wrong please try after sometime";
		  }
		}
	);
	  }

	  getModelPrice(id){
		//alert(id)
		// //console.log.log(id);
		 const followData = {
		   model_id: this.params.uid,
		 }
		 this.userService.ModelPrice(followData).subscribe(
		   response => {
			 //console.log.log(response);
			 this.modelPrice['price']=response['data']['price'];
			 //this.modelID['id']=id;
			// this.modelPrice['routeId']=routeId;
			 this.display='block';
			 document.getElementById('openpaymentSelector').click()
			// jQuery.noConflict();
			 //this.editForm.resetForm();
			 //this.mytemplateForm.reset();
			// this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl('https://enacteservices.net/snapapp/savedata/formButton.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+response['data']['price']);
			 //document.getElementById('paymentPopup').click();
			// document.getElementById('paymentPopup').click();
			 //$("#payment").modal('show');
			 
			 //this.display='none';
			 //$("#payment").modal('show');
		   },
		   err => {
			
		   }
		 )
	   }

	   getToken(modelId) {
		   //alert(modelId)
		   modelId = this.params.uid;
		this.loading=true;
		(<any>window).Stripe.card.createToken({
		  number: this.cardNumber,
		  exp_month: this.expiryMonth,
		  exp_year: this.expiryYear,
		  cvc: this.cvc,
		}, (status: number, response: any) => {
		  //console.log.log(response);
		  this._zone.run(() => {
			if (status === 200) {
			  const paymentToken = {
				stripeToken: response.id,
				model_id: modelId,
			  }
			  this.userService.Payment(paymentToken).subscribe(
				response => {
				  ////console.log.log(response);
				  if(response['success'] == '1'){
					alert("payment successfull");
					this.FollowModel(0,modelId);
					var userId = localStorage.getItem('id');
					this.afs.collection('chatCounter').add({'MaxChat': 10,'userId': Number(userId), 'modelId': Number(this.params.uid),'time':firebase.firestore.FieldValue.serverTimestamp(),'totalChat':0,'sender':'user'});
					jQuery.noConflict();
					document.getElementById('closePaymentPopup').click();
					this.loading=false;
				  }
				  else{
					this.loading=false;
					this.message='payment failed please try again'
					alert("payment failed please try after sometime");
				  }
				 
				},
				err => {
				  alert("payment failed please try after sometime");
				}
			  )
			 
			} else {
			  alert(response['error']['code']);
			  this.loading=false;
			  this.message =response['error']['code']
			}
		  });
		}
		);
	  }

	  FollowModel(id,model_id){
		  //alert(model_id);
		model_id=this.params.uid
		this.editmode = false;
		//console.log.log(id);
		//console.log.log(model_id);
		const followData = {
		  id: id,
		  model_id: model_id,
		  api_token:localStorage.getItem("loggedInUser")
		}
		var userId=localStorage.getItem('id'); 
		this.userService.FollowUnfollow(followData).subscribe(
		  response => {
		   ////console.log.log(response);
		   if(response['success']==1){
			this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
			//this.router.navigate(["model-profile",{uid: idUser } ])); 
			this.router.navigate(['/model-profile'], { queryParams: {uid: this.params.uid } }));
			if(id==1){
				var mod ='0';
				this.loggedIn=false;
				const afs2 =  this.afs;
				this.afs.collection("chatCounter", ref => ref.where('userId', '==', Number(userId)).where('modelId', '==', Number(this.params.uid)))
				.get().toPromise().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					////console.log.log(doc.id, " => ", doc.data());
						//this.chatId = doc.id;
						//console.log.log(doc.id);
						doc.ref.delete();
						afs2.collection("chatStatus").doc(doc.id).delete().then(function() {
						//console.log.log("Document successfully deleted!");
						}).catch(function(error) {
							//console.log.error("Error removing document: ", error);
						});
						//alert("!2")
						// Build doc ref from doc.id
						// afs2.collection("chatStatus").doc(doc.id).update({'content': content,'time':firebase.firestore.FieldValue.serverTimestamp(),'type':type,'userProfile':userProfile,'modelProfile': modelProfile,'chatNotificationUser':true,'chatNotificationModel':false}).then(function() {
						//   //console.log.log("Document successfully written!");
						// })
						// .catch(function(error) {
						//   //console.log.error("Error writing document: ", error);
						// });
						
					});
				})
				this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
				//this.router.navigate(["model-profile",{uid: idUser } ])); 
				this.router.navigate(['/model-profile'], { queryParams: {uid: this.params.uid } }));
			}
			else if(0==id){
			  var mod ='1';
			  this.loggedIn=true;
			}
			////console.log.log(mod)
			this.checkFollowing = mod;
			this.maxChat=10;
			this.totalChat= 0;
			//this.modelData[i]['following_status'] =mod;
			this.editmode = true;
			// //console.log.log('following_status', this.modelData.following_status);
			//this.getAllModels();
		   } 
		  },
		  err => {
			//this.router.navigate(['/login']);
			//console.log.log(err);
			alert("something went wrong")
		  }
		);
	  }




	getLiveChatStatus(){
		const that = this;
		this.postsCol = this.afs.collection('posts', ref => ref.where('userId', '==', Number(localStorage.getItem('id'))).where('modelId', '==', Number(this.params.uid)));
		//that.ChatToShow = []; 
		this.posts = this.postsCol.valueChanges();
		this.posts.subscribe(res => {
			res.forEach(element1 => {
				////console.log.log("wawaw",element1['typingStatus'][this.params.uid]);
				this.typingStatus = element1['typingStatus'][this.params.uid]
				
			});
			
		})
	}
	  addPost() {
		this.editmode=false;  
		if(this.isUpload){
		  this.type="photo";
		  const api = {
			api_token:localStorage.getItem("loggedInUser")
		   }
		   this.userService.uploadChatPic(this.formData).subscribe(
			 response => {
			  //console.log.log(response);
			  //this.isLoading=true;
			  if(response['success']==1){
				//console.log.log(response);
				 this.content =response['data'];
				// this.isLoading=false;
				this.getDataChat();
			  }
			  else{
				//console.log.log(response);
				//this.isLoading=false;
				//alert("No models Found");
			  }
			   
			 },
			 err => {
			   this.router.navigate(['/login']);
			   //console.log.log(err);
			 }
		   );
		 }
		 else{
		  
		  if(this.content.trim() == "" || this.content == null  || this.content == undefined || this.content == " " ){
			this.editmode=true;  
			return false;   
		 }
		 this.type="text";
		  this.getDataChat();
		 }
	   
	  }
	 
	  getDataChat(){
		this.imageFile="";
		this.isUpload=false;
		//  ÷(this.maxChat);
		// alert(this.totalChat);
		
		if(Number(this.maxChat) <= Number(this.minChat)){
			alert("sorry this message can't be sent as your transaction is over recharge now to get or post message");
			this.msgOver = false;
			this.editmode=true;
			return false;
		   
		}
		else{
			this.msgOver = true;
			this.editmode=false;
		}
		this.minChat =  Number(this.minChat) + 1
		this.afs.collection('message_counter').doc(this.chatIdString).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":this.maxChat,"minChat":Number(this.minChat)}).then(function(){
			console.log("Added")
		});
		var milliseconds = (new Date).getTime();
		//alert(milliseconds);

		
		  
		var useID = localStorage.getItem('id');
		this.chatIdString = useID +'_'+ this.params.uid;
		if(Number(useID) > Number(this.params.uid)){
			this.chatIdString =  this.params.uid+'_'+ useID;
		}
		this.chatReference =  this.afs.collection('posts').doc(this.chatIdString);
		this.messageReference = this.chatReference.collection('messsages');
		var timestamp = firebase.firestore.Timestamp.now();
		//console.log.log("chatIdString",this.chatIdString)
		this.messageReference.add({'message': this.content, 'sender': useID,'userID':Number(useID), 'modelId': Number(this.params.uid),'time':timestamp,'type':this.type,'read':false});


		//this.afs.collection('posts').add({'message': this.content, 'sender': 'user','userID': Number(useID), 'modelId': Number(this.params.uid),'time':firebase.firestore.FieldValue.serverTimestamp(),'type':this.type,'read':false});
		const afs2 =  this.afs;
		this.totalChat++;
		const toChat = this.totalChat;


		const that =this;
		if(this.type=="photo"){
			this.content = "@@@@@@@";
		}
		const cont = this.content
		this.chatReference.update({'lastMessage':this.content,'time':timestamp}).then(function() {
			//console.log.log('this.sendUnreadCount(Number(this.modelId),Number(this.userID),chatId)');
			that.sendUnreadCount(Number(that.modelId),Number(that.userID),that.chatIdString);
		}).catch(function(error) {
			if (error.code == 'not-found') {
				// Set because update is not available
				var key = "unreadCount";
				var person = {[key]:"John"};
				//console.log.log(person); // should print  Object { name="John"}
				
				var idUser = localStorage.getItem('id');
				var typingStatus = {[idUser]:false,[that.params.uid]:false,};
				var unreadCount = {[idUser]:0,[that.params.uid]:1,};
				// var typingStatus2 = 'typingStatus.' + that.params.uid;
				// var unreadCount1 = 'unreadCount.' + localStorage.getItem('id');
				// var unreadCount2 = 'unreadCount.' + that.params.uid;
			
				//console.log.log("striong",{'chatId': that.chatIdString,'lastMessage':cont,'time':timestamp,'modelId':Number(that.params.uid),'userId':Number(localStorage.getItem('id')), typingStatus,unreadCount})
				that.chatReference.set({'chatId': that.chatIdString,'lastMessage':cont,'time':timestamp,'modelId':Number(that.params.uid),'userId':Number(localStorage.getItem('id')),typingStatus,unreadCount}).then(function(){
					that.sendUnreadCount(Number(that.params.uid),Number(localStorage.getItem('id')),that.chatIdString);
				});
				// that.afs.collection('messageCounter').doc(that.chatIdString).set({'maxChat':0,'minChat':0}).then(function(){
				// 	that.minChat = 0;
				// 	that.maxChat = 0
				// });
			}
			else {
		// Unknown error
		
			}
		});

		this.getchatData.push({'message': this.content,'sender': localStorage.getItem('id'),'userID': Number(localStorage.getItem('id')),'modelId': Number(this.modelId),'time':timestamp,'type':this.type,'read':true})



		
		this.content="";
		this.editmode=true;
		this.loading=false;
		this.livechatStatus();
	  }
	  
	  
	  sendUnreadCount(modelId,UserID,docId){
		////console.log.log("Tmmitted!12323");
		var pinDocRef =this.afs.collection('posts').doc(docId).ref;
		//this.chatReference =  this.afs.collection('posts').doc(this.chatIdString);
		this.messageReference = this.chatReference.collection('messsages');
		return firebase.firestore().runTransaction(function(transaction) {
		  return transaction.get(pinDocRef).then(function(pinDoc) {
			if(!pinDoc.exists){
			  //console.log.log("Document does not exist!");
			  return false;
			}
			//console.log.log("pindoc",pinDoc)
			var unreadCount = pinDoc.data().unreadCount;
			for (var key in unreadCount) {
			  if (key == localStorage.getItem('id')) {
				unreadCount[key] = 0;
			  }
			  else {
				unreadCount[key] = Number(pinDoc.data().unreadCount[key]) + 1;
			  }
			}
	  
			transaction.update(pinDocRef, { 'unreadCount': unreadCount});
		  });
		  }).then(function() {
			//console.log.log("Transaction successfully committed!12323");
		  }).catch(function(err) {
			//console.log.log("Transaction failed: ", err);
		  });
	  }



	  modelChat(model_id,model_name,modelProfile){
		
		this.getchatData=[];
		this.userId = Number(localStorage.getItem('id'));
		var userId= localStorage.getItem("id");
		const afs2 =  this.afs;
		 var maxCha;
		//  totalCha;
		///alert(this.userId);alert(this.model_id);
	
		this.userName = localStorage.getItem('name');
		this.userProfile = localStorage.getItem('profileImage');
		this.userImage =  localStorage.getItem("profileImage");

		var thats = this;
		this.chatIdString = localStorage.getItem('id') +'_'+ this.params.uid;
		if(Number(localStorage.getItem('id')) > Number(this.params.uid)){
			this.chatIdString =  this.params.uid+'_'+ localStorage.getItem('id');
		}
		//console.log.log("this.chatIdString",this.chatIdString)
		this.chatReference =  this.afs.collection('posts').doc(this.chatIdString);
		this.messageReference = this.chatReference.collection('messsages');
		var timestamp = firebase.firestore.Timestamp.now();
		this.messageReference.get().toPromise().then(function(querySnapshot) {
			querySnapshot.forEach(function(doc) {
			  thats.getchatData.push(doc.data());
			});
			thats.getchatData.sort(function (a, b) {
				return a['time'].seconds - b['time'].seconds;
			});
		})

		const model={
			model_id :this.params.uid, 
		}

		this.messageCounter(this.chatIdString);

		
		// this.userService.GetMsgCount(model).subscribe(
		// 	response => {
		// 		console.log(response)
		// 		if(response['success'] == 0){
		// 			this.maxChat = 0;
		// 			this.minChat = 0
		// 		}
		// 		else{
		// 			this.maxChat = 0;
		// 			this.minChat = 0
		// 		}
		// 	},
		// 	err => {

		// 	}
		// );



		this.isModelChat=true;

	  }
	  
	  getMessagePrice(){
		//document.getElementById('showModel').click();
//		this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl('https://enacteservices.net/snapapp/savedata/formButton4.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+price+'.00&uniqueId='+this.uniqueId);

	  }
	
	  rechargeMessage(price){
		this.messagePrice =price;
		this.uniqueId = (new Date()).getTime();
		this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton4.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+price+'.00&uniqueId='+this.uniqueId);
		// document.getElementById('modalClose').click();
		document.getElementById('msgPopUPClose').click();
		document.getElementById('paymentPopup').click();
	 }

	   checkForMessageRecharge(){
		   const data={
			   uniqueId:this.uniqueId
		   }
		this.userService.getMessageRechargeStatus(data).subscribe(
			response => {
				if(response['success'] == 1){
					const userId=localStorage.getItem("id");
					const afs2 =  this.afs;
					
					const fprice = Number(this.messagePrice) + Number(this.maxChat);
					this.maxChat = Number(fprice);
					this.msgOver = true;
					this.afs.collection("chatCounter", ref => ref.where('userId', '==', Number(userId)).where('modelId', '==', Number(this.params.uid)))
					.get().toPromise().then(function(querySnapshot) {
					querySnapshot.forEach(function(doc) {
						  ////console.log.log(doc.id);
						  afs2.collection("chatCounter").doc(doc.id).update({'MaxChat': Number(fprice)}).then(function() {
							//console.log.log("Document successfully written!123");
							document.getElementById('modalClose').click();
						  })
						  .catch(function(error) {
							//console.log.error("Error writing document: ", error);
						  });
					  });
					})
				}
			},
			err => {
			  //console.log.log(err);
			  
			}
		  );
	   }
	   
	   
	   getModelComment(close){
		   const id ={
			   id:this.params
		   }
		   this.userService.getModelComment(id).subscribe(
			response => {
				if(response['success'] == 1){
					//.log("here")
					this.emptyGetdata = false;
					this.showAverage = response['data'][0]['totalaverage']
					this.totalComment = response['data'][0]['totalCommentCount']
					if(response['data'][0]['alreadyCommented'] > 0){
						this.alreadyCommented = true;
					}
					else{
						this.alreadyCommented = false;
					}
				}
				else{
					this.emptyGetdata = true;
					this.alreadyCommented = false;
				}
				////console.log.log(response['data'][0]['posted_on'])
				
				this.getData=response['data'];
				// //console.log.log(this.getData);
				
				//this.emptyGetdata = false;
				if(close =='close'){
					document.getElementById('commentPopup').click();
				}
				
			},
			err => {
			  //console.log.log(err);
			  
			}
		  );
		   
	   }
	   
	   writeReview(method){
		   //alert(method)
		if(method == 'edit'){
			this.method = method;
			const id= {
				model_id:this.params
			}
			this.userService.getUserComment(id).subscribe(
				response => {
					if(response['success'] == 1){
						document.getElementById('reviewPopup').click();	
						document.getElementById('modelCommentHide').click();
						this.CommentTextArea = response['data'][0]['comment'];
						this.showAverage2 = response['data'][0]['rating'];
					}
					
				},
				err => {
				  //console.log.log(err);
				  
				}
			  );
			
		}
		else{
			this.CommentTextArea = "";
			this.showAverage2 = "";
			this.method = method;
			document.getElementById('reviewPopup').click();
			document.getElementById('modelCommentHide').click();
		}
	   }   


	   submitReview(){
		  // alert(this.CommentTextArea);  alert(this.showAverage2)
		   if(this.showAverage2 == ""){
			   return false;
		   }
		   this.loading = true;
		const data= {
			model_id:this.params.uid,
			comment:this.CommentTextArea,
			review:this.showAverage2,
			method:this.method
		}
		if(this.method ==  'edit'){
			this.userService.saveUserComment(data).subscribe(
				response => {
					if(response['success'] == 1){
						this.loading = false;
						this.getModelComment('notClose');
						//document.getElementById('review').click();
						 document.getElementById('closeReviewModal').click();
						 document.getElementById('commentPopup').click();	
						// this.CommentTextArea = response['data'][0]['comment'];
						// this.showAverage2 = response['data'][0]['rating'];
					}
					
				},
				err => {
				  //console.log.log(err);
				  this.loading = false;
				  alert("something went wrong please try after some time")
				  document.getElementById('closeReviewModal').click();
				}
			  );
		}
		else{
		//	alert(this.method)
			this.userService.saveUserComment(data).subscribe(
				response => {
					if(response['success'] == 1){
						this.getModelComment('notClose');
						this.loading = false;
						//document.getElementById('review').click();
						 document.getElementById('closeReviewModal').click();	
						 document.getElementById('commentPopup').click();	
						// this.CommentTextArea = response['data'][0]['comment'];
						// this.showAverage2 = response['data'][0]['rating'];;
					}
					
				},
				err => {
					this.loading = false;
					//console.log.log(err);
					alert("something went wrong please try after some time")
					document.getElementById('closeReviewModal').click();
				  
				}
			  );
		}
	   }
	   
	   openSignUp(){
		   //alert("R")
		document.getElementById('closeLoginPopup').click();
		this.router.navigate(['/signup']);
	   }



	   convertDate(date){
		// var dat = new Date(date).toString();
		// date  = new Date(dat);
		var a = new Date(date);
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var dates = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		var times = dates + ' ' + month + ' ' + year + ' ' + hour + ':' + min ;
		return times;
	   }

	modelAvailability(){
		this.loadingAvailability =true;
		var date = new Date(), y = date.getFullYear(), m = date.getMonth();


		
		var firstDay = new Date(y, m, 1);
		var lastDay = new Date(y, m + 1, 1);
		const data ={
		  firstDate:firstDay,
		  lastDate:lastDay,
		  model_id:this.params.uid
		}

		//console.log(data)
		this.getCalendarDataModel(data,1);	
				// this.CommentTextArea = response['data'][0]['comment'];
				// this.showAverage2 = response['data'][0]['rating'];;
	}

	monthChange(month,year){

		
		var firstDay = new Date(year, month -1, 1);
		var lastDay = new Date(year, month, +1,0);
		const data ={
			firstDate:firstDay,
			lastDate:lastDay,
			model_id:this.params.uid
		}
		this.getCalendarDataModel(data,0);
	}
	   


	   getCalendarDataModel(data,iu){
		this.userService.getCalendarDataModel(data).subscribe(
			response => {
				this.loadingAvailability = false;
			 ////console.log.log(response);
			 if(response['success']==1){
				 if(iu ==1){
					document.getElementById('openModelAvailability').click();
				 }
				
			 // this.calendar =response['data'];
			   //alert("done")
			  //console.log.log(response['data']);
			   this.calendar = response['data']
			   ////console.log.log(response['data']['date'])
			  
			   for(var i =0 ;i<response['data'].length;i++){
				 this.dates.push(new Date (response['data'][i]['date']));
			   }
			   //console.log.log(this.dates);
			   //this.isloading = true
			 }
			 else{
			  //this.isloading = true
			  //alert("Something Went Wrong Please Contact Admin");
			 }
			  
			},
			err => {
			  //this.router.navigate(['/logout']);
			  alert("Something Went Wrong Please Contact Admin");
			  //this.isloading = true
			  //console.log.log(err);
			}
		  );
	   }
		formatDate(date) {
			////console.log.log(date)
			var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
			////console.log.log([month,day,year].join('-'))
			return [year,month,day].join('/');
		}

		formatDateTime(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
				var hour = d.getHours();
				var minute = d.getMinutes();
				var second = d.getSeconds();
			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
			////console.log.log([month,day,year].join('-')+' ' +[d.getHours(),d.getMinutes(),d.getSeconds()].join(':'))
			return [year,month,day].join('/')+' ' +
						[d.getHours(),
						d.getMinutes(),
						d.getSeconds()].join(':');
		}


	   scheduleAppointment(id,time,date,date2,day,month,start_time,year,end_time){
			 this.appointmentId =id;
			 this.appointmentTime =time;
			 this.appointmentDate =date;
			 this.appointmentweekday = day;
			 this.appointmentmonth = month;
			 this.appointmentstart_time = start_time;
			 this.appointmentend_time = end_time;
			 this.appointmentyear = year;
			 this.appointmentDate = new Date(this.appointmentDate * 1000);
			 this.dateTime = this.appointmentDate
			 this.dateTimeMax = new Date(date2  * 1000);
			 this.time = this.appointmentDate
			 this.appointmentTime = new Date(this.formatDateTime(this.time)).toUTCString()
			//  console.log(this.appointmentTime)
			//  console.log(this.appointmentDate)
			 document.getElementById('dismissModel').click();
			 //document.getElementById('paymentPopup').click();
			 document.getElementById('showminutePopup').click();
		}

		finalSchedule(){
				
			if(localStorage.getItem('loggedInUser') == null  && localStorage.getItem('role') != '1'){
				this.router.navigate(['/login']);  
			}
				
				// //console.log.log(id);
			const followData = {
				model_id: this.params.uid,
			}
			this.appointmentTime = new Date(this.formatDateTime(this.time)).toUTCString();
			this.appointmentDate = new Date(this.formatDateTime(this.appointmentDate)).toUTCString();
			console.log(this.appointmentTime)
			console.log(this.appointmentDate)
			var fprice = Number(this.priceCalc) * this.livevideoCamPrice;
			if(Number(this.priceCalc) < 3  || fprice < 5){
				alert("Please make sure minutes should be at least 3 minutes and total spend should be at least $5")
				return false
			}
			if(fprice > 180){
				alert("Please reduce the minutes because maximum paying price is $200")
				return false
			}
			this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton3.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+fprice+'.00&time='+this.appointmentTime+'&date='+ this.appointmentDate+'&availabilityId='+this.appointmentId+'&minutes='+this.priceCalc);
			document.getElementById('closeminutepopup').click();
			document.getElementById('paymentPopup').click() ;
		}


		modalClose(){
			// alert(this.modelMainId)    
			// alert(this.modePosition)
			
			const getFollowstatus ={
			  user_id:localStorage.getItem('id'),
			  model_id:this.params.uid
			}
			this.userService.getFollowstatus(getFollowstatus).subscribe(
			  response => {
				//console.log.log(response);
				if(response['data'] == 1){
				// this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
				// 			//this.router.navigate(["model-profile",{uid: idUser } ])); 
				// this.router.navigate(['/model-profile'], { queryParams: {uid: this.params.uid } }));
				document.getElementById('modalCloseClick').click();
				  
				  var mod ='1';
				  this.loggedIn=true;
				  ////console.log.log(mod)
				  this.checkFollowing = mod;
				  this.maxChat=10;
				  this.totalChat= 0;
				  //this.modelData[i]['following_status'] =mod;
				  this.editmode = true;
				  // //console.log.log('following_status', this.modelData.following_status);
				  //this.getAllModels();
				}
				else{
				  document.getElementById('modalCloseClick').click();
				}
			  },
			  err => {
			   //console.log.log(err);
			  }
			)
			document.getElementById('modalCloseClick').click();
			this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
			this.router.navigate(['/model-profile'], { queryParams: {uid: this.params.uid } }));
			//this.router.navigate(['/models']));  
		  }


	valuechange(): void { 
		clearTimeout(this.timer1)
		if(this.typing == false) {
			this.typing = true ;
			var typingStatus = 'typingStatus.' + this.loggedInuser;
			this.chatReference.update({[typingStatus]: true});
		}
		this.timer1  = setTimeout(()=>{
			this.stopTyping();
		}, 5000); 
	}

	stopTyping() {
		this.typing = false; 
		var typingStatus = 'typingStatus.' + this.loggedInuser;
		this.chatReference.update({[typingStatus]: false});
	}
	
	open(index: number,image,title): void {
		//alert("2")
		this.index2 = index
		this.imgsrc = image;
		//console.log.log(this.imgsrc)
		var modal = document.getElementById("myModalImage");
		modal.style.display = "block";
		var captionText = document.getElementById("caption");
		captionText.innerHTML =title;
	  }
	  closeModel(){
		var modal = document.getElementById("myModalImage");
		modal.style.display = "none";
	  }

	  convertDateUtc(date,type){
		var dates = new Date(date * 1000) ;
		if(type == "weekday"){
			var dates = new Date(date * 1000) ;
			var weekday=new Array(7);
			weekday[0]="Sun";
			weekday[1]="Mon";
			weekday[2]="Tue";
			weekday[3]="Wed";
			weekday[4]="Thu";
			weekday[5]="Fri";
			weekday[6]="Sat";
		  //console.log.log(date)
		 return weekday[dates.getDay()];
		  
		}
		else if(type == "day"){
		  return dates.getDate()
		}
		else if(type == "start_time"){
		  return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "end_time"){
		  return dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		}
		else if(type == "year"){
			return dates.getFullYear()
		}
		else if(type == "month"){
			return dates.getMonth()+1
		}
	  }

	  changePaymentMethod(type){
		  //alert(type)
		if(type == "ccbill"){
		  this.AddActiveClass = "ccbill"
		}
		else{
		  this.AddActiveClass = "finpay"
		}
	  }
	  
	  continuePayment(type){
		//this.modalClose()
		document.getElementById('closeCCfinpay').click();
		if(type == 'ccbill'){
		  this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+ this.modelPrice['price']);
		  document.getElementById('paymentPopup').click();
	  
		}
		else{
		  //document.getElementById('finPayPopup').click();
		  const data={
			model_id :this.params.uid,
	  
		  }
		  this.userService.PaymentStatus(data).subscribe(
			response => {
			  //console.log(response);
			  if(response['success']== 0){
				document.getElementById('finPayPopup').click();
			  }
			  else{
				console.log(response)
			  }
			 
			},
			err => {
			 //console.log.log(err);
			}
		  )
		}
	  }
	  
	  
	  finpayPayment(){
		const data={
		  model_id :this.params.uid,
		  firstName:this.nameCard,
		  lastname:"123",
		  exp_year:this.expYear,
		  exp_month:this.expMonth,
		  number:this.cardNumber,
		  cvc:this.cvc,
		}
		this.userService.CreatePayment(data).subscribe(
	  
		  response => {
			console.log(response);
			if(response['success']== 0){
			  //document.getElementById('finPayPopup').click();
			}
			else{
			  console.log(response)
			}
		   
		  },
		  err => {
		   //console.log.log(err);
		  }
		)
	  }

	  
	  messageCounter(chat_id){
		var docRef = this.afs.collection("message_counter").doc(chat_id);
		const  that =this;
		docRef.get().toPromise().then(function(doc) {
			if (doc.exists) {
				//alert('213')
				//console.log("Document data:", doc.data());
				that.userService.GetMsgCount({'model_id':that.params.uid}).subscribe(
					response => {
						// console.log(response)
						if(response['success'] == 1){
							if(Number(response['data'][0]['max_chat']) >  Number(doc.data().maxChat)){
								that.afs.collection("message_counter").doc(chat_id).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":response['data'][0]['max_chat'],"minChat":doc.data().minChat});
								that.maxChat = Number(response['data'][0]['max_chat']) 
								that.minChat = doc.data().minChat
								that.editmode=true;
								that.msgOver = true;
								return false;
							}
							
							else{
								const data ={
									model_id:that.params.uid,
									max_chat:doc.data().maxChat
								}
								console.log(data)
								that.userService.updateMsgCount(data).subscribe(
									response => {
										console.log(response)
									},
									err => {
				
									}
								);
								that.maxChat = doc.data().maxChat;
								that.minChat = doc.data().minChat;
							}

							if(Number(doc.data().minChat) ==  Number(doc.data().maxChat)){
								that.msgOver = false;
								//that.editmode=true;
								return false;
							}
							else{
								that.msgOver = true;
								//that.editmode=false;
							}
						}
						else{
							const data ={
								model_id:that.params.uid,
								max_chat:doc.data().maxChat
							}
							console.log(data)
							that.userService.updateMsgCount(data).subscribe(
								response => {
									console.log(response)
								},
								err => {
			
								}
							);
						}
						
					},
					err => {

					}
				);
			} else {
				//alert("123")
				that.editmode=true;
				// doc.data() will be undefined in this case
				const model_id = {
					model_id:that.params.uid
				}			
				that.userService.GetMsgCount(model_id).subscribe(
					response => {
						console.log(response)
						
						if(response['success'] == 0){
							that.maxChat =5;
							that.minChat = 0;
							that.afs.collection("message_counter").doc(chat_id).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":5,"minChat":0});
						}
						else{
							that.afs.collection("message_counter").doc(chat_id).set({"timestamp":firebase.firestore.Timestamp.now(),"maxChat":0,"minChat":0});
						}
					},
					err => {

					}
				);
			}
		}).catch(function(error) {
			console.log("Error getting document:", error);
		});
	  }


	  changeSlideShow(type){
		// var length = this.ImgArray;
		//console.log(this.ImgArray)
		if(type == 'right'){
			//alert(this.index2)
			
			//alert(this.index2)
			// this.index2 ;
			for(var i=0;i< this.ImgArray.length; i++){
				if(this.index2  == this.ImgArray.length -1){
					this.index2 = 0;
					break;
				}
				if(i== this.index2){
					this.index2 = i + 1;
					break;
				}
				
				
			}

			//alert(this.index2)
			this.imgsrc = this.ImgArray[this.index2]['media'];

			var captionText = document.getElementById("caption");
			captionText.innerHTML =this.ImgArray[this.index2]['title'];
		}
		else{
			//alert(this.index2)
			// this.index2 ;
			for(var i=0;i< this.ImgArray.length; i++){
				if(this.index2   == 0){
					this.index2 = this.ImgArray.length -1;
					break;
				}
				if(i == this.index2){
					this.index2 = i - 1;
					console.log(this.index2)
					break;
				}
				
				
			}
			//alert(this.index2)
				this.imgsrc = this.ImgArray[this.index2]['media'];
			//}
			var captionText = document.getElementById("caption");
			captionText.innerHTML =this.ImgArray[this.index2]['title'];
		}
	}

	giveTip(){
		this.ShowStaticTip = false;
		this.staticTip = "";
		document.getElementById('tippopupopen').click();
		// this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton5.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+ this.modelPrice['price']);
		// document.getElementById('paymentPopup').click();
	}

	setTipPrice(){
		//alert(Number(this.staticTip))
		if(Number(this.staticTip) < 5){
			
			this.staticTip = '5';
		}
	}

	givetipFinal(value){
		this.AddActiveClass2 = 'active'+value
		if(value=="static"){
			this.AddActiveClass2 = 'activestatic'
			this.ShowStaticTip = true;
		}
		else{
			this.staticTip = value;
		}
	}
	leaveTip(){
		document.getElementById('closeTipPOPUP').click()
		this.formUrl= this.domSanitizer.bypassSecurityTrustResourceUrl(environment.baseUrl_main+'/savedata/formButton5.php?model_id='+this.params.uid+'&user_id='+localStorage.getItem('id')+'&price='+ this.staticTip);
		document.getElementById('paymentPopup').click();
	}

	blurNumber(){
		if(this.priceCalc < 1 ){
			this.priceCalc = 1
		}
		// if(this.priceCalc > 1 ){
		// 	this.priceCalc = 1
		// }
	}

	playvideo(type,id){
		if(type == 'play'){
			document.getElementById('videoPlay'+id).style.display = 'none'
			document.getElementById('videoPause'+id).style.display = 'block'
			//document.getElementById('comment'+id).style.display = 'none'
			let audiolayer: HTMLMediaElement = <HTMLVideoElement>document.getElementById('videoId'+id)
			//document.getElementById('videoId'+id).setAttribute("controls", 'false')
			audiolayer.play()

		}
		if(type == 'pause'){
			document.getElementById('videoPlay'+id).style.display = 'block'
			document.getElementById('videoPause'+id).style.display = 'none'
			//document.getElementById('comment'+id).style.display = 'block'
			let audiolayer: HTMLMediaElement = <HTMLVideoElement>document.getElementById('videoId'+id)
			//document.getElementById('videoId'+id).setAttribute("controls", 'false')
			audiolayer.pause()

		}
	}



}
