<?php

namespace App\Http\Controllers;
use App\ApiModel;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\AssestModel;
use App\Url;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
//use VideoThumbnail;
use Thumbnail;
use FFMpeg;
use App\GetAvailability;
use App\ScheduleVideoCall;
use App\AppointmentNotification;
use Mail;
// header('Access-Control-Allow-Origin: *');  
// header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
//use Pawlox\VideoThumbnail\Models\VideoThumbnail;
class ApiController extends Controller
{
    //
     /**
     * Create user
     *
     * 
@param  [string] name
     * 
@param  [string] email
     * 
@param  [string] password
     * 
@param  [string] password_confirmation
     * 
@return [string] message
     */
  public $successStatus = 200;

    
    public function signup(Request $request)
    {
      //print_r($request->input());die;
      $requests = $request->input();
      $validator = Validator::make($request->all(), [
              'name' => ['required', 'string', 'max:255'],
              'email' => ['required', 'string', 'email', 'max:255','unique:users'],
              'password' => ['required', 'string', 'min:6'],
              'role' => ['required'],
           ]); 
      if ($validator->fails()) {
        $valid = $validator->errors();
        $data=[];
        if(isset($valid->messages()['password'])){
          $data['message'] = $valid->messages()['password'][0];
        }
        if(isset($valid->messages()['email'])){
          $data['message'] = $valid->messages()['email'][0];
        }
        if(isset($valid->messages()['name'])){
          $data['message'] = $valid->messages()['name'][0];
        }

       

        // echo "Basic Email Sent. Check your inbox.";
           return response()->json(['error'=>'0','data'=>$data], 401);            
       }
       else{
          $input = $request->all();
         
          $str = str_shuffle("123456789012aqszxwertgbvf31");
          $input['password'] = bcrypt($input['password']);
          $input['role'] = $input['role'];

          $input['verification_id'] = $str;
           //print_r($input);die;
          $user = User::create($input);
          if($user->role==2 && $user->id!=""){
            $dat  = DB::table('users')
                  ->select('name')   
                  ->where('name','=',$user->name)->get();
                 // print_r(count($dat));die;
            $count  = count($dat);
            // for()
            $save = DB::table('model_data')->insert(
              ['model_id' => $user->id,'profile_url'=>str_replace(' ', '', $user->name)."".$count]);

          }
          
          $success['token'] =  $user->createToken('MyApp')->accessToken;
          $success['name'] =  $user->name;
          $success['role'] =  $user->role;
          $success['status'] =  $user->status;



          Mail::send('email',['email'=>$user['email'],'name'=>$user['name'],'verification_id'=>$user['verification_id']],function ($message) use($user) {
            $message->from('noreply@yespiper.com','YesPiper');
            $message->to($user['email'],$user['name']);
            $message->subject('Confirm your Yes Piper email address!');
         });
          return response()->json(['success'=>$success], $this->successStatus);
       }
      
    }
  
    /**
     * Login user and create token
     *
     * 
@param  [string] email
     * 
@param  [string] password
     * 
@param  [boolean] remember_me
     * 
@return [string] access_token
     * 
@return [string] token_type
     * 
@return [string] expires_at
     */
    public function login()
    {
      
       if(Auth::attempt(['email' => request('email'), 'password' => request('password')]) ){
           $user = Auth::user();
           $success['token'] =  $user->createToken('MyApp')->accessToken;
           if(Auth::user()->profileImage !=""){
              $success['profileImage'] =  url('/')."/".Auth::user()->profileImage;
           }
           else{
              $success['profileImage'] ="";
           }
           $success['status'] =  Auth::user()->status;
           $success['role'] =  Auth::user()->role;
           $success['name'] = Auth::user()->name;
           $success['id'] = Auth::user()->id;
           $success['emailVerify'] = Auth::user()->emailVerify;
           $success['accountStatus'] =  $user->accountStatus;
           $success['status'] =  $user->status;
           //echo $userId;die;
           if(Auth::user()->role == 2){
            $suc = DB::table('model_data')->select('profile_url')->where('model_data.model_id',Auth::user()->id)->get();
            //print_r($suc);die;
            $success['profileUrl'] = $suc[0]->profile_url;
           }
           
           //print_r($success);die;
           return response()->json(['success' => $success], $this->successStatus);
       }
       else{
           return response()->json(['error'=>'Unauthorised'], 401);
       }
    }
    
    public function resendLink(Request $request){
       $str = str_shuffle("123456789012aqszxwertgbvf31");
       $input = $request->all();
       $save = DB::table('users')->where('email', $input['email'])->update(
              ['verification_id' => $str]
            );
       if($save){
          $checkForUser = DB::table('users')->where('email',$input['email'])->get();
          if($checkForUser){
              Mail::send('email',['email'=> $checkForUser[0]->email,'name'=>$checkForUser[0]->name,'verification_id'=>$checkForUser[0]->verification_id],function ($message) use($checkForUser) {
                $message->from('noreply@yespiper.com','YesPiper');
                $message->to($checkForUser[0]->email,$checkForUser[0]->name);
                $message->subject('Confirm your Yes Piper email address!');
              });
               return response()->json([ 'success'=>1], 200);
          }
           
       }
       else{
        return response()->json([ 'success'=>0, 'data'=>[]], 200);
       }

      



    }


    public function verifyEmail(Request $request){
       $id = $request->get('uid');
       $CheckId = DB::table('users')->where('verification_id',$id)->where('emailVerify','0')->count();
       //print_r($CheckId);
       if($CheckId){
           $save = DB::table('users')->where('verification_id', $id)->update(
              ['verification_id' => '0000000', 'emailVerify' => '1']
            );
           if($save){
            return response()->json([ 'success'=>1, 'data'=>[]], 200);
           }
           else{
            return response()->json([ 'success'=>2, 'data'=>[]], 200);
           }
       }
       else{
        return response()->json([ 'success'=>0, 'data'=>[]], 200);
       }
    }
    
  
    /**
     * Logout user (Revoke the token)
     *
     * 
@return [string] messagegetCa
     */
  public function details(Request $request) 
    { 
          // $user_id = Auth::user()->id;
      //print_r($request->input());die;
       $id = $request->get('uid');
       $user_id = auth('api')->user();
        
       if($request->get('callback') == "id"){
        $data = "users.id = ".$id;
       } 
       else{
        $data = "model_data.profile_url = '".$id."'";   /////profile url is taken because it was only unique column in table
        $id = DB::table('model_data')->where('profile_url',$id)->get();
        $id = $id[0]->model_id;
       }


      if(($user_id!="" || $user_id!=null) && isset($user_id->id)){
        $checkForModel = DB::table('users')->where('id',$user_id->id)->get();
        if($checkForModel[0]->role == 2){
           $checkFollow = 3;
        }
        else{
          $checkFollow = DB::table('user_follow')->where('user_id',$user_id->id)->where('model_id',$id)->count();
          $getFollow = DB::table('user_follow')->where('user_id',$user_id->id)->where('model_id',$id)->get()->toArray();
        }
      }
       else{
        $checkFollow = 2;
       }
      
      
      //print_r($checkForModel);die;
     // echo $checkFollow;die;
      if($checkFollow == 1 && $checkFollow!=0){
          $dataIds =[1,2];
          //$users = DB::table('users')->leftJoin('model_assets', 'model_assets.model_id', '=', 'users.id')->join('model_data','model_data.model_id', '=', 'users.id')->where('users.id',$id)->where('model_assets.showOn', '<=', time())->whereIn('model_assets.showto',$dataIds)->tosql();
          // $users =  DB::select("select * from `users` LEFT join `model_assets` on (`users`.`id` = `model_assets`.`model_id` and `model_assets`.`showOn` <= ".time()." and `model_assets`.`showto` in (1,2)) LEFT join `model_data` on `users`.`id` = `model_data`.`model_id` LEFT join (SELECT count(*) AS countComment,AVG(rating) AS averageRating,model_id FROM `model_comment` GROUP BY model_id) c on users.id = c.model_id LEFT join (SELECT post_id, count(*) cnt FROM model_post_comment ) x ON `model_assets`.`id` = `x`.`post_id` where `users`.`id` =  ".$id." AND  users.accountStatus = '0'  ");
        //print_r($users);die;


        $users =  DB::select("select users.*,
                  model_assets.*, model_assets.id AS post_id,model_data.model_id AS model_ids,model_data.profile_url AS profile_url,
                  model_data.*,
                  (SELECT count(*) FROM `model_comment` WHERE users.id = model_comment.model_id ) as countComment,
                  (SELECT AVG(rating) FROM `model_comment` WHERE users.id = model_comment.model_id ) as averageRating,
                  (SELECT count(*) FROM model_post_comment WHERE `model_assets`.`id` = `model_post_comment`.`post_id`) as cnt
                  from `users` 
                  LEFT join `model_assets` on `users`.`id` = `model_assets`.`model_id` and `model_assets`.`showOn` <= ".time()." and `model_assets`.`showto` in (1,2) 
                  LEFT join `model_data` on `users`.`id` = `model_data`.`model_id` 
                  LEFT join `model_comment` on users.id = model_comment.model_id 
                  LEFT join `model_post_comment` ON `model_assets`.`id` = `model_post_comment`.`post_id`
                  
                  WHERE ".$data." AND  users.accountStatus = '0' GROUP BY model_assets.id");




          // LEFT join (SELECT count(*) AS countPostComment FROM `model_post_comment` GROUP BY post_id ) AS post_comment') d on model_assets.id = model_post_comment.post_id
       }
       else if($checkFollow == 2){
        $dataIds =[2];
        $users =  DB::select("select users.*,model_data.model_id AS model_ids,model_data.profile_url AS profile_url,
                  model_assets.*, model_assets.id AS post_id,
                  model_data.*,
                  (SELECT count(*) FROM `model_comment` WHERE users.id = model_comment.model_id ) as countComment,
                  (SELECT AVG(rating) FROM `model_comment` WHERE users.id = model_comment.model_id ) as averageRating,
                  (SELECT count(*) FROM model_post_comment WHERE `model_assets`.`id` = `model_post_comment`.`post_id`) as cnt
                  from `users` 
                  LEFT join `model_assets` on `users`.`id` = `model_assets`.`model_id` and `model_assets`.`showOn` <= ".time()." and `model_assets`.`showto` in (2) 
                  LEFT join `model_data` on `users`.`id` = `model_data`.`model_id` 
                  LEFT join `model_comment` on users.id = model_comment.model_id 
                  LEFT join `model_post_comment` ON `model_assets`.`id` = `model_post_comment`.`post_id`
                  
                  WHERE ".$data." AND  users.accountStatus = '0' GROUP BY model_assets.id");
       }
      else if($checkFollow == 0){
        $dataIds =[2];
        $users =  DB::select("select users.*,model_data.model_id AS model_ids,model_data.profile_url AS profile_url,
                  model_assets.*, model_assets.id AS post_id,
                  model_data.*,
                  (SELECT count(*) FROM `model_comment` WHERE users.id = model_comment.model_id ) as countComment,
                  (SELECT AVG(rating) FROM `model_comment` WHERE users.id = model_comment.model_id ) as averageRating,
                  (SELECT count(*) FROM model_post_comment WHERE `model_assets`.`id` = `model_post_comment`.`post_id`) as cnt
                  from `users` 
                  LEFT join `model_assets` on `users`.`id` = `model_assets`.`model_id` and `model_assets`.`showOn` <= ".time()." and `model_assets`.`showto` in (2) 
                  LEFT join `model_data` on `users`.`id` = `model_data`.`model_id` 
                  LEFT join `model_comment` on users.id = model_comment.model_id 
                  LEFT join `model_post_comment` ON `model_assets`.`id` = `model_post_comment`.`post_id`
                  
                  WHERE ".$data." AND  users.accountStatus = '0' GROUP BY model_assets.id");
       }
       else if($checkFollow == 3){
          $users = DB::select("select users.*,model_data.model_id AS model_ids,model_data.profile_url AS profile_url,
                  model_assets.*, model_assets.id AS post_id,
                  model_data.*,
                  (SELECT count(*) FROM `model_comment` WHERE users.id = model_comment.model_id ) as countComment,
                  (SELECT AVG(rating) FROM `model_comment` WHERE users.id = model_comment.model_id ) as averageRating,
                  (SELECT count(*) FROM model_post_comment WHERE `model_assets`.`id` = `model_post_comment`.`post_id`) as cnt
                  from `users` 
                  LEFT join `model_assets` on `users`.`id` = `model_assets`.`model_id`   
                  LEFT join `model_data` on `users`.`id` = `model_data`.`model_id` 
                  LEFT join `model_comment` on users.id = model_comment.model_id 
                  LEFT join `model_post_comment` ON `model_assets`.`id` = `model_post_comment`.`post_id`
                  
                  WHERE ".$data." GROUP BY model_assets.id");
       }
       
      //  print_r($users);die;
      // die;
         //echo Auth::user()->id;die;
      // if(isset(Auth::user())){

      // }
      
      
     
      $data=[];
      if($users){
        foreach ($users as $key => $value) {
          if($checkFollow == 1 && $getFollow[0]->blocked == 1){
              $data['user_id']=$value->id;
              $data['model_id'] = $value->model_ids;
              $data['name'] = $value->name;
              $data['website'] = $value->website;
              $data['profile_url'] = $value->profile_url;
              $data['profile_image'] = url('/')."/".$value->profileImage;
              $data['model_comment'] = $value->countComment;
              $data['averageRating'] = $value->averageRating;
              $data['blocked'] = "y";
              $data['status'] = $value->status;
              if($value->type == "3"){
                $data['cover_Image'] = url('/')."/".$value->media;
                //$data["media"][$key]['type']=$value->type;
              }
              // else{
              $data["media"][$key]['media'] = "";
              $data["media"][$key]['type']="";
              // }
              $data['countImage']=$this->mediaCount($value->model_ids,1); 
              $data['countVideo']=$this->mediaCount($value->model_ids,2);
              $data['price'] = "";
              $data['livevideoCamPrice'] = ""; 
              break;
          }
          $data['user_id']=$value->id;
          $data['model_id'] = $value->model_ids;
          $data['name'] = $value->name;
          $data['website'] = $value->website;
          $data['profile_url'] = $value->profile_url;
          $data['profile_image'] = url('/')."/".$value->profileImage;
          $data['model_comment'] = $value->countComment;
          $data['averageRating'] = $value->averageRating;
          $data['blocked'] = "n";
          $data['status'] = $value->status;
          if($value->type == "3"){
            $data['cover_Image'] = url('/')."/".$value->media;
            //$data["media"][$key]['type']=$value->type;
          }
          // else{
            $data["media"][$key]['media'] = url('/')."/".$value->media;
            $data["media"][$key]['type']=$value->type;
            $data["media"][$key]['title']=$value->title;
            $data["media"][$key]['comment']=$value->cnt;
            $data["media"][$key]['post_id']=$value->post_id;
          // }
          $data['countImage']=$this->mediaCount($value->model_ids,1); 
          $data['countVideo']=$this->mediaCount($value->model_ids,2);
          $data['price'] = $value->mainPrice;
          $data['livevideoCamPrice'] = $value->video_price; 

          
        }
        // echo "<pre>";
        // $this->mediaCount($id,1);die;
       // echo $checkFollow;die;
         if($checkFollow == 1){
            $data['following']= 1;
          }
          else if($checkFollow == 2){
            $data['following']= 2;
          }
          else{
            $data['following']=0;
          }
          if(isset($getFollow[0]->status)){
            $data['followingStatus'] =$getFollow[0]->status;
          } 
          else{
            $data['followingStatus'] = 3;
          }
        return response()->json([ 'success'=>1, 'data'=>$data], 200);
      }
      else{
        return response()->json([ 'success'=>1, 'data'=>[]], 200);
      }
      
      //if(!empty($data))
 
    } 

     public function models()
    {
      //['users.id AS user_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS user_profileImage']
      $users = DB::table('featured_model')->select(['users.id AS user_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS user_profileImage','model_data.profile_url AS profile_url',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating',])->join('users','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->groupBy('featured_model.model_id')->where('users.role','2')->where('users.status','1')->where('users.accountStatus','0')->limit(8)->get();


    
      $dataIds = [1, 2];
      $modelData = DB::table('model_assets')->whereIn('model_assets.type',$dataIds)->get([DB::raw('COUNT( DISTINCT model_id )AS model_count'),DB::raw('COUNT(media) as media_count'),( DB::raw('(select count(*) FROM model_assets where type = 2)  AS `video_count` ')   ),( DB::raw('(select count(*) FROM model_assets where type = 1)  AS `images_count` ')   ) ] ) ;

      // $latestUsers = DB::table('model_assets')->select(['users.id AS user_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS user_profileImage'])->distinct('user_id')->join('users','users.id', '=', 'model_assets.model_id')->where('users.role','2')->where('users.status','1')->whereI('model_assets.type',[1,2])->orderBy('users.id','DESC')->limit(4)->get();
      // // print_r($users);
        
      // print_r($latestUsers);die;
      
      $dat=[];
      //$data=[];
      foreach ($users as $key => $value) {
  //      echo $key;
        $data['user_id']=$value->user_id;
        $data['model_name']=$value->model_name;
        $data['model_email'] = $value->model_email;
        $data['model_website'] = $value->model_website;
        $data['profile_url'] = $value->profile_url;
        $data['model_profile'] = url('/')."/".$value->user_profileImage;
        $data['image_count'] = $this->mediaCount($value->user_id,1);
        $data['video_count'] = $this->mediaCount($value->user_id,2); //10 Sep @ 11:30 AM
        
        if($value->start_time == null){
          $data['start_time'] = "";
        }
        else{
          $data['start_time'] = $value->start_time;
        }
        if($value->countComment == null){
          $data['countComment'] = 0;
        }
        else{
          $data['countComment'] = $value->countComment;
        }
        
        if(round($value->averageRating) == null){
          $data['averageRating'] = "";
        }
        else{
          $data['averageRating'] = round($value->averageRating);
        }
        


        $dat['model_data'][$key] = $data;

      }
//die;
      $dat['model_count'] = $modelData[0]->model_count;
      $dat['media_count'] = $modelData[0]->media_count;
      $dat['total_video'] = $modelData[0]->video_count;
      $dat['total_images'] = $modelData[0]->images_count;

      if(!empty($data)){
        return response()->json(['success'=>1,'data'=>$dat], 200);
      }
      else{
        return response()->json(['success'=>'0', 'data'=>[]], 200);
      }
    }

    // public function getAllModelsData(){

    // } 

    public function Userdata(){
        $user_id = Auth::user()->id;
        $users = DB::table('users')->select(['users.id AS user_id','users.name AS user_name','users.email AS user_email','users.bio AS user_bio','users.profileImage AS user_profileImage','users.notification_for AS notification_for'])->where('users.id',$user_id)->get();
        //print_r($users);
        $data=[];
        foreach ($users as $key => $value) {
          $data[$key]['user_id']=$value->user_id;
          $data[$key]['user_name']=$value->user_name;
          $data[$key]['user_email'] = $value->user_email;
          $data[$key]['notification_for'] = explode(",",$value->notification_for);
          if($value->user_bio == null){
           $data[$key]['user_bio'] = "";  
          }
          else{
            $data[$key]['user_bio'] = $value->user_bio;  
          }
          if($value->user_profileImage == null){
           $data[$key]['user_image'] = "";  
          }
          else{
            $data[$key]['user_image'] =  url('/')."/".$value->user_profileImage;  
          }
          
        }
        if(!empty($data)){
          return response()->json(['success'=>1,'data'=>$data], 200);
        }
        else{
          return response()->json(['success'=>'0','data'=>[]], 200);
        }
    }

    public function Userupdate(Request $request){
      // print_r($_POST);
      // print_r($_FILES); 
      // die;
      $user_id = Auth::user()->id;
      //echo $user_id;
      // print_r($request->input());
      // print_r($request->file());die;
      $users = DB::table('users')->where('id', $user_id)->first();
      //print_r($users);
      $requests = $request->input();
      $validator = Validator::make($request->all(), [
              //'name' => ['required', 'string', 'max:255'],
             'email' => 'required|unique:users,email,' . $user_id
      ]);
      if ($validator->fails()) {
           return response()->json(['error'=>$validator->errors()], 401);            
       }
       else{
            $input = $request->all();
            //print_r($input);die;
            //return response()->json(['success'=>$success], $this->successStatus);
            if ($request->hasFile('file')) {
              $imageName = time().'.'.request()->file->getClientOriginalExtension();
                if( request()->file->move(public_path('images'), $imageName)){
                  $imagepath =  public_path('images')."/".$imageName;
                }
                $input['image']="images/".$imageName;
               //url('/')."/".$value->user_profileImage;  
                $input['mainImage'] =  url('/')."/".$input['image'];  
            }
            else{
                $input['image']=$users->profileImage;
               if($input['image'] !=""){
                    $input['mainImage'] =  url('/')."/".$users->profileImage;
                }
                else{
                    $input['mainImage'] ="0";
                }
                //$input['mainImage'] =  url('/')."/".Auth::user()->$users->profileImage;
            }
            //print_r($input);die;
           $save = DB::table('users')->where('id', $user_id)->update(
              ['email' => $input['email'], 'bio' => $input['bio'],'name' =>$input['name'],'profileImage'=>$input['image'],'notification_for'=>$input['notificationFor']]
            );
          // print_r($save);
            if($save){
              return response()->json(['success'=>'0','data'=>$input], 200);
            }
            else{
              return response()->json(['success'=>'1','data'=>[]], 200);
            }
       }

    }

    public function ListModel(){
      $user_id = Auth::user()->id;
      //echo $user_id;
      // $users = DB::table('user_follow')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Image'])->join('users', 'users.id', '=', 'user_follow.model_id')->where('user_follow.user_id', $user_id)->where('users.role','2')->get();
      $users = DB::table('user_follow')->select(['user_follow.model_id AS model_id','user_follow.status'])->leftJoin('users','users.id','=','user_follow.model_id')->where('user_follow.user_id', $user_id)->where('users.accountStatus', '0')->get();


      $allModels =  DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url AS profile_url','users.live AS live_model','count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->groupBy('users.id')->where('users.role','2')->where('users.accountStatus','0')->where('users.status','1')->paginate(12)->toArray();

      //print_r($allModels);die;
      $mod=[];
      foreach ($users as $key => $value) {
        //print_r($value);
        $mod[$users[$key]->model_id]=$value->status;
      }
     // print_r($mod[6]);die;
      $data=[];
      foreach ($users as $key => $value) {
        //print_r($value);
        $data[]=$value->model_id;
      }
     // print_r($data);die;
      $followed = [];
      foreach ($allModels['data'] as $key => $value) {
        //print_r($value);
        if (in_array($value->model_id,$data)){
           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           if($value->model_Image =="" || $value->model_Image == null){
              $followed[$key]['model_Image']=url('/')."/images/modal.png";
            }
           else{
              $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
              //$data[$key]['model_Image'] = url('/')."/".$value->profileImage;
            }
           $followed[$key]['profile_url']=$value->profile_url;
          // $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['next_page'] = $allModels['next_page_url'];
           if($mod[$value->model_id] ==3){
            $followed[$key]['following_status']='0';
           }
           else{
            $followed[$key]['following_status']='1';
           }
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           if($value->start_time == null){
              $followed[$key]['start_time'] = "";
           }
           else{
              $followed[$key]['start_time'] =$value->start_time;
           }
           if($value->countComment == null){
              $followed[$key]['countComment'] = 0;
           }
           else{
             $followed[$key]['countComment'] = $value->countComment;
           }

           if(round($value->averageRating) == null){
              $followed[$key]['averageRating'] = "";
           }
           else{
              $followed[$key]['averageRating'] = round($value->averageRating);
           }
           if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
            if($mod[$value->model_id] == 3){
              $followed[$key]['following_status']='0';
            }
           }
           
           //print_r($mod)
           

        }
        else{
           $followed[$key]['profile_url']=$value->profile_url;
           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           if($value->model_Image =="" || $value->model_Image == null){
              $followed[$key]['model_Image']=url('/')."/images/modal.png";
            }
           else{
              $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
              //$data[$key]['model_Image'] = url('/')."/".$value->profileImage;
            }
           //$followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['following_status']='0';
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           $followed[$key]['next_page'] = $allModels['next_page_url'];
           if($value->start_time == null){
              $followed[$key]['start_time'] = "";
           }
           else{
              $followed[$key]['start_time'] = $value->start_time;
           }
           if($value->countComment == null){
              $followed[$key]['countComment'] = 0;
           }
           else{
              $followed[$key]['countComment'] = $value->countComment;
           }

           if(round($value->averageRating) == null){
              $followed[$key]['averageRating'] = "";
           }
           else{
              $followed[$key]['averageRating'] = round($value->averageRating);
           }
          if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
           }
           else{
             $followed[$key]['status']='0';
           }         
        }
      }
     // print_r($followed);die;
      if(!empty($followed)){
              return response()->json(['success'=>'1','data'=>$followed], 200);
      }
      else{
              return response()->json(['success'=>'0','data'=>[]], 200);
      } 
    }



    public function followUnfollow(Request $request){
         $curl = curl_init();
         $user_id = Auth::user()->id;
         //echo $user_id;
         $requests = $request->input();
         if($requests['status']==0){
            $save = DB::table('user_follow')->insert(
              ['user_id' => $user_id, 'model_id' => $requests['model_id']]);
            if($save){
               return response()->json(['success'=>'1','data'=>'1'], 200);
            }
            else{
              return response()->json(['success'=>'0'], 200);
            }
         }
         else{

          $getSid =  DB::table('user_follow')->where('user_id', '=',$user_id)->where('model_id','=',$requests['model_id'])->get();
        //echo $getSid[0]->subscription_id;die;
          if(isset($getSid[0]->subscription_id)){


                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://datalink.ccbill.com/utils/subscriptionManagement.cgi?password=yes11piPEr@@&action=cancelSubscription&clientSubacc=0000&subscriptionId=".$getSid[0]->subscription_id."&username=y3spiper&clientAccnum=952038",
 
                ));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                $response = curl_exec($curl);
               
                //$response  = curl_setopt($response,CURLOPT_RETURNTRANSFER,TRUE);
                curl_close($curl);
                //echo $response;die;
                //die;
                // Send the request & save response to $resp
                //$resp = curl_exec($curl);
                //print_r($curl);die;
                 $csv = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $response));
                //print_r($csv);die;
                if($csv[1][0] == 1){
                    $unfollow = DB::table('user_follow')->where('user_id', '=',$user_id)->where('model_id','=',$requests['model_id'])->update(
                      ['status'=>2]
                    );
                    if($unfollow){
                      $unfollow = DB::table('model_subscription')->where('user_id', '=',$user_id)->where('model_id','=',$requests['model_id'])->where('subscription_id','=',$getSid[0]->subscription_id)->update(['status'=>'0']);
                      return response()->json(['success'=>'1','data'=>'2'], 200);
                    }
                    else{
                      return response()->json(['success'=>'0'], 200);
                    }
                }
                else{
                  return response()->json(['success'=>'0'], 200);
                }
               
                //$xml = new \SimpleXMLElement($this->create_gk_xml($resp), 0, true);
                //$xml = new SimpleXMLElement($resp);
                
          }
            
         }

    }
    public function yourfeed(Request $request){
      // print_r($request->input());
      // die;
      $user_id = Auth::user()->id;
      //echo $user_id;
      $page =$request->input('page');
      $perPage = 9;
      $offset = ($page * $perPage) - $perPage;

      if($request->input('content') == '1' || $request->input('content') == 1){
         $dataIds = [1,2,5];
          $users = DB::table('model_assets as ms')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','ms.media AS model_image','ms.uploaded_at AS uploaded_at','ms.type AS media_type','ms.id AS modelAsset_id','model_data.video_price AS price','count_comment_post.totalComment AS Comment','ms.title AS title','model_data.profile_url AS profile_url'])->join('users', 'users.id', '=', 'ms.model_id')->join('user_follow', 'user_follow.model_id', '=', 'ms.model_id')->leftJoin('model_data', 'model_data.model_id', '=', 'ms.model_id')->leftJoin('count_comment_post', 'ms.id', '=', 'count_comment_post.post_id')->where('user_follow.user_id', $user_id)->where('users.accountStatus','=', '0')->where('users.role','2')->whereIn('ms.type',$dataIds)->where('users.status','1')->where('ms.showOn', '<=', time())->where('ms.showOn', '<=', $request->input('nowDate'))->whereIn('ms.showto',['1','2'])->where('user_follow.blocked','=','0')->orderBy('ms.id', 'DESC')->paginate(9)->toArray();
          //echo "1";die;
      }
      else if($request->input('content') == '2' || $request->input('content') == 2){
         $dataIds = [1];
        
            $users = DB::table('model_assets as ms')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','ms.media AS model_image','ms.uploaded_at AS uploaded_at','ms.type AS media_type','ms.id AS modelAsset_id','model_data.video_price AS price','count_comment_post.totalComment AS Comment','ms.title AS title','model_data.profile_url AS profile_url'])->join('users', 'users.id', '=', 'ms.model_id')->join('user_follow', 'user_follow.model_id', '=', 'ms.model_id')->leftJoin('model_data', 'model_data.model_id', '=', 'ms.model_id')->leftJoin('count_comment_post', 'ms.id', '=', 'count_comment_post.post_id')->where('user_follow.user_id', $user_id)->where('users.accountStatus','=', '0')->where('users.role','2')->whereIn('ms.type',$dataIds)->where('users.status','1')->where('ms.showOn', '<=', time())->where('ms.showOn', '<=', $request->input('nowDate'))->whereIn('ms.showto',['1','2'])->where('user_follow.blocked','=','0')->orderBy('ms.id', 'DESC')->paginate(9)->toArray();
          //print_r($users);die;
         //echo "2";die;
      }
      else if($request->input('content') == '3' || $request->input('content') == 3){
        $dataIds = [2,5];
         $users = DB::table('model_assets as ms')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','ms.media AS model_image','ms.uploaded_at AS uploaded_at','ms.type AS media_type','ms.id AS modelAsset_id','model_data.video_price AS price','count_comment_post.totalComment AS Comment','ms.title AS title','model_data.profile_url AS profile_url'])->join('users', 'users.id', '=', 'ms.model_id')->join('user_follow', 'user_follow.model_id', '=', 'ms.model_id')->leftJoin('model_data', 'model_data.model_id', '=', 'ms.model_id')->leftJoin('count_comment_post', 'ms.id', '=', 'count_comment_post.post_id')->where('user_follow.user_id', $user_id)->where('users.accountStatus','=', '0')->where('users.role','2')->whereIn('ms.type',$dataIds)->where('users.status','1')->where('ms.showOn', '<=', time())->where('ms.showOn', '<=', $request->input('nowDate'))->whereIn('ms.showto',['1','2'])->where('user_follow.blocked','=','0')->orderBy('ms.id', 'DESC')->paginate(9)->toArray();
        // echo "3";die;
      }
      else  if($request->input('content') == '10' || $request->input('content') == 10){
        $dataIds = [2];
         $users = DB::table('model_assets as ms')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','ms.media AS model_image','ms.uploaded_at AS uploaded_at','ms.type AS media_type','ms.id AS modelAsset_id','model_data.video_price AS price','count_comment_post.totalComment AS Comment','ms.title AS title','model_data.profile_url AS profile_url'])->join('users', 'users.id', '=', 'ms.model_id')->join('user_follow', 'user_follow.model_id', '=', 'ms.model_id')->leftJoin('model_data', 'model_data.model_id', '=', 'ms.model_id')->leftJoin('count_comment_post', 'ms.id', '=', 'count_comment_post.post_id')->where('user_follow.user_id', $user_id)->where('users.accountStatus','=', '0')->where('users.role','2')->whereIn('ms.type',$dataIds)->where('users.status','1')->where('ms.showOn', '<=', time())->where('ms.showOn', '<=', $request->input('nowDate'))->whereIn('ms.showto',['1','2'])->where('user_follow.blocked','=','0')->orderBy('ms.id', 'DESC')->paginate(9)->toArray();
         //echo "4";die;
      }
      // echo "<pre>";
      // print_r($users);die;
    
      $paidUnpaid = DB::table('paid-status')->where('user_id',$user_id)->get();
      $dat=[];
      foreach ($paidUnpaid as $key => $value) {
        $dat[]=$value->asset_id;
      }

      $data=[];
      foreach ($users['data'] as $key => $value) {

        $data[$key]['paid'] = "paid";
        //}
        $data[$key]['model_name'] = $value->model_name;
        $data[$key]['asset_id'] = $value->modelAsset_id;
        $data[$key]['model_id'] = $value->model_id;
        $data[$key]['profile_url'] = $value->profile_url;
        $data[$key]['profile_image'] = url('/')."/".$value->model_Profile;
        $data[$key]['model_image'] = url('/')."/".$value->model_image;
        $data[$key]['title'] = $value->title;
        $data[$key]['media_type'] = $value->media_type;
        $data[$key]['price'] = $value->price;
        $data[$key]['next_page'] = $users['next_page_url'];
        if($value->Comment == null){
           $data[$key]['comment'] = "0";
        }
        else{
           $data[$key]['comment'] = $value->Comment;
        }
        //$data[$key]['comment'] = $value->Comment;
        $data[$key]['uploaded_at'] = $this->uploadedat($value->uploaded_at);

      }
      //$data['next_page'] = $users['next_page_url'];
      if(!empty($data)){
        return response()->json(['success'=>'1','data'=>$data], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
      //print_r($data);

    }

    public function followingModel(){
      $user_id = Auth::user()->id;

     // echo $user_id;die;
      $users = DB::table('user_follow')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','users.live AS live',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating','user_follow.status AS status'  ])->join('users', 'users.id', '=', 'user_follow.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('user_follow.user_id', $user_id)->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();


      $data =[];
      foreach ($users as $key => $value) {
        $data[$key]['model_name']=$value->model_name;
        $data[$key]['model_id']=$value->model_id;
        $data[$key]['model_Profile']=url('/')."/".$value->model_Profile;
        $data[$key]['following_status']='1';
        $data[$key]['live']=$value->live;
        $data[$key]['status']=$value->status;
        $data[$key]['image_count'] = $this->mediaCount($value->model_id,1);
        $data[$key]['video_count'] = $this->mediaCount($value->model_id,2);
        if($value->start_time == null){
           $data[$key]['start_time'] = "";
         }
         else{
            $data[$key]['start_time'] = $value->start_time;
         }
         if($value->countComment == null){
            $data[$key]['countComment'] = 0;
         }
         else{
           $data[$key]['countComment'] = $value->countComment;
         }

         if(round($value->averageRating) == null){
            $data[$key]['averageRating'] = "";
         }
         else{
            $data[$key]['averageRating'] = round($value->averageRating);
         }
      }
      if(!empty($data)){
        return response()->json(['success'=>'1','data'=>$data], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
      //print_r($data);
      //print_r(array_unique($users));
    }


    public function BuyVideo(Request $request){
        $data = $request->all();
        $user_id = Auth::user()->id;
        $requests = $request->input();
        //print_r($requests);
        $save = DB::table('paid-status')->insert(
              ['user_id' => $user_id, 'asset_id' => $requests['data']]
            );
       if($save){
        return response()->json(['success'=>'1',], 200);
        }
        else{
          return response()->json(['success'=>'0'], 200);
        }
    }

    private function uploadedat($time){
      $nowtime =strtotime(gmdate('y-m-d H:i:s'));
      $uploadedTime = $time;
      $timeDifference = $nowtime - $time;
      $time = ceil($timeDifference/60);
      if($time<60){
        return $time." Minutes ago";
      }
      else if($time>60 && $time<1440){
        return ceil($time/60) ." Hour ago"; 
      }
      else if($time>1440){
        return round($time/(60*24)) ." Day Ago";
      }
    }



    public function resetPassword(Request $request) {
       $data = $request->all();
       $user = Auth::user()->email;
       $user_id = Auth::user()->id;
      
       if( isset($data['oldPassword']) && !empty($data['oldPassword']) && $data['oldPassword'] != "" && $data['oldPassword'] !='undefined') {
              $this->validate($request, [
                'oldPassword'     => 'required',
                'newPassword'     => 'required|min:6',
                //'confirm_password' => 'required',
              ]);

              if(!Hash::check($data['oldPassword'], Auth::user()->password)){
                //echo "a";
                return response()->json(['success'=>'0'], 200);die;
              }else{
                 $password = bcrypt($data['newPassword']);
                 //echo "b";
              // write code to update password
                 $save = DB::table('users')->where('id', $user_id)->update(
                    ['password'=>$password]
                 );
                 if($save){
                    return response()->json(['success'=>'1'], 200);die;
                 }
                 else{
                     return response()->json(['success'=>'2'], 200);die;
                 }
              }
          }
          else{
            return response()->json(['success'=>'3'], 200);die;
          }
    }

    public function modelPrice(Request $request){
        $data = $request->all();
        $user_id = Auth::user()->id;
        $requests = $request->input();
        $users = DB::table('model_data')->select(['price'])->where('model_id', $request['model_id'])->get();
        //print_r($users[0]->price);die;
        if($users[0]->price ==""){
          $price = "0";
        }
        else{
          $price = $users[0]->price;
        }
        return response()->json(['success'=>'1','data'=>['price'=>$price.'.00']], 200);die;////get through query
        //print_r($data);
    }

    public function FollowModelPayment(Request $request){
        $data = $request->all();
        $user_id = Auth::user()->id;
        $requests = $request->input();
        $priceModel =DB::table('model_data')->select(['price'])->where('model_id', $request['model_id'])->get();
        if($priceModel == ""){
          $priceModel =0;
        }
        else{
          $priceModel = $priceModel;
        }
        $request->email = Auth::user()->email;
       // print_r( $request->email);die;
          try {
         Stripe::setApiKey(env('STRIPE_SECRET'));
        
          
          $customer = Customer::create(array(
          'email' => $request->email,
          'source' => $request->stripeToken
          ));
          $stripeCustomerId= $customer->id;
          $card = Customer::createSource($stripeCustomerId, ['source'=>'tok_visa']);
          //$user->newSubscription('main',$request->subscription)->create($request->token);
          $charge = Charge::create(array(
          'customer' => $customer->id,
          'amount' => $priceModel*100,
          'currency' => 'usd'
          ));
         // print_r($charge);die;
           return response()->json(['success'=>'1'], 200);die;
      } catch (\Exception $ex) {
            return response()->json(['success'=>'0'], 200);die;
      }
          //return 'Charge successful, you get the course!';
         
    }

    public function msgStatus(Request $request){
      $user_id = Auth::user()->id;
     // echo $user_id;die;
      $user_role = Auth::user()->role;
      if($user_role == 1 || $user_role == 0){
          $users = DB::table('user_chat')->select(['users.name AS model_name','users.profileImage AS model_image','users.id AS model_id','users.live AS model_live','user_chat.last_message AS message','user_chat.sent_at AS sent'])->join('users', 'users.id', '=', 'user_chat.model_id')->where('user_chat.user_id', $user_id)->get();
          //print_r($users);
          $data =[];
          foreach ($users as $key => $value) {
            $data[$key]['model_name'] = $value->model_name;
            $data[$key]['model_image'] = url('/')."/".$value->model_image;
            $data[$key]['id'] = $value->model_id;
            $data[$key]['model_live'] = $value->model_live;
            $data[$key]['message'] = $value->message;
            $data[$key]['sent'] =  date(" H:i A", ($value->sent)/1000);
            $data[$key]['user_id'] = $user_id;
          }
          if(!empty($users)){
            return response()->json(['success'=>'1','data'=>$data], 200);die;
          }
          else{
             return response()->json(['success'=>'0','data'=>[]], 200);die;
          }
      }
      else{
         $users = DB::table('user_chat')->select(['users.name AS model_name','users.profileImage AS model_image','users.id AS model_id','users.live AS model_live','user_chat.last_message AS message','user_chat.sent_at AS sent'])->join('users', 'users.id', '=', 'user_chat.user_id')->where('user_chat.model_id', $user_id)->get();
          $data =[];
          foreach ($users as $key => $value) {
            $data[$key]['user_name'] = $value->model_name;
            $data[$key]['user_image'] = url('/')."/".$value->model_image;
            $data[$key]['id'] = $value->model_id;
            //$data[$key]['model_live'] = $value->model_live;
            $data[$key]['message'] = $value->message;
            $data[$key]['sent'] =  date(" H:i A", ($value->sent)/1000);
            $data[$key]['model_id'] = $user_id;
          }
          if(!empty($users)){
            return response()->json(['success'=>'1','data'=>$data], 200);die;
          }
          else{
             return response()->json(['success'=>'0','data'=>[]], 200);die;
          }
      }
      // else{

      // }
    }


    public function saveLastmsgUser(Request $request){
        $user_id = Auth::user()->id;
        $requests = $request->input();
        $count = DB::table('user_chat')->where('user_id',$user_id)->where('model_id',$requests['model_id'])->count();
         if($count == 0){
            $save = DB::table('user_chat')->insert(
              ['user_id' => $user_id, 'model_id' => $requests['model_id'], 'last_message' => $requests['last_message'],'sent_at' => (strtotime(gmdate('y-m-d H:i:s')) * 1000)]);
            if($save){
               return response()->json(['success'=>'1'], 200);
            }
            else{
              return response()->json(['success'=>'0'], 200);
            }
         }
         else{
           $save = DB::table('user_chat')->where('user_id', $user_id)->where('model_id', $requests['model_id'])->update(
              ['last_message' => $requests['last_message'],'sent_at' => (strtotime(gmdate('y-m-d H:i:s')) * 1000)]);

          $users = DB::table('user_chat')->select(['users.name AS model_name','users.profileImage AS model_image','users.id AS model_id','users.live AS model_live','user_chat.last_message AS message','user_chat.sent_at AS sent'])->join('users', 'users.id', '=', 'user_chat.model_id')->where('user_chat.user_id', $user_id)->get();
          $data =[];
          foreach ($users as $key => $value) {
            $data[$key]['model_name'] = $value->model_name;
            $data[$key]['model_image'] = url('/')."/".$value->model_image;
            $data[$key]['id'] = $value->model_id;
            //$data[$key]['model_live'] = $value->model_live;
            $data[$key]['message'] = $value->message;
            $data[$key]['sent'] =  date(" H:i A", ($value->sent)/1000);
            $data[$key]['user_id'] = $user_id;
          }
            if($save){
               return response()->json(['success'=>'1','data'=>$data], 200);
            }
            else{
              return response()->json(['success'=>'0'], 200);
            }
         }
    }
    public function saveLastmsgModel(Request $request){
        $user_id = Auth::user()->id;
        $requests = $request->input();
        $count = DB::table('user_chat')->where('model_id',$user_id)->where('user_id',$requests['user_id'])->count();
         if($count == 0){
            $save = DB::table('user_chat')->insert(
              ['model_id' => $user_id, 'user_id' => $requests['user_id'], 'last_message' => $requests['last_message'],'sent_at' => (strtotime(gmdate('y-m-d H:i:s')) * 1000)]);
            if($save){
               return response()->json(['success'=>'1'], 200);
            }
            else{
              return response()->json(['success'=>'0'], 200);
            }
         }
         else{
           $save = DB::table('user_chat')->where('model_id', $user_id)->where('user_id', $requests['user_id'])->update(
              ['last_message' => $requests['last_message'],'sent_at' => (strtotime(gmdate('y-m-d H:i:s')) * 1000)]);

          $users = DB::table('user_chat')->select(['users.name AS model_name','users.profileImage AS model_image','users.id AS model_id','users.live AS model_live','user_chat.last_message AS message','user_chat.sent_at AS sent'])->join('users', 'users.id', '=', 'user_chat.user_id')->where('user_chat.model_id', $user_id)->get();
          $data =[];
          foreach ($users as $key => $value) {
            $data[$key]['user_name'] = $value->model_name;
            $data[$key]['user_image'] = url('/')."/".$value->model_image;
            $data[$key]['id'] = $value->model_id;
            //$data[$key]['model_live'] = $value->model_live;
            $data[$key]['message'] = $value->message;
            $data[$key]['sent'] =  date(" H:i A", ($value->sent)/1000);
            $data[$key]['model_id'] = $user_id;
          }
            if($save){
               return response()->json(['success'=>'1','data'=>$data], 200);
            }
            else{
              return response()->json(['success'=>'0'], 200);
            }
         }
    }


    public function filterModel(Request $request){
       $user_id = Auth::user()->id;
       //print_r($user_id);die;
       $users = DB::table('user_follow')->select(['user_follow.model_id AS model_id','user_follow.status AS status'])->where('user_follow.user_id', $user_id)->where('users.status','1')->join('users','users.id', '=', 'user_follow.model_id')->where('users.accountStatus','=', '0')->get();    

       $requests = $request->input();
       if(isset($requests['sort']) ){
         $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','model_data.profile_url as profile_url','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->orderBy('users.id', $requests['sort'])->groupBy('model_id','start_time')->get();

       }
      
       else if(isset($requests['featured'])) {
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url as profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();

          

       }
       else if(isset($requests['trending'])){
          //$queryParam = 'DESC';
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url as profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id','=','featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->groupBy('model_id','start_time')->get();

       }
       else if(isset($requests['popular'])){
          //$queryParam = 'ASC';
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url as profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating' ])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();
       }
       else if(isset($requests['nameSort'])){
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','model_data.profile_url as profile_url','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->orderBy('name', $requests['nameSort'])->groupBy('model_id','start_time')->get();
       }
      
       else if(isset($requests['mostUploads'])){
        //SELECT model_id, COUNT(*) FROM model_assets GROUP BY model_id ORDER BY COUNT(*) DESC
          $model_data = DB::table('users')->select(['model_assets.model_id',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','model_data.profile_url as profile_url','view_rating.avg_rating AS averageRating','users.name AS model_name' , 'users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model'])->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('model_assets','users.id', '=', 'model_assets.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->groupBy('model_assets.model_id')->orderBy('total', 'DESC')->get();
          
       }
       else if(isset($requests['topSelling'])){
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url as profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();
       }
       else if(isset($requests['recentlyActive'])){
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url as profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();    

       }
       else if(isset($requests['order']) ){
        // echo "1";
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','model_data.profile_url as profile_url','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.status','1')->where('users.role','2')->where('name', 'like', '%'.$requests['order'].'%')->groupBy('model_id','start_time')->get();

          
          //print_r($model_data);die;
       }
       else if(isset($requests['live'])) {
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'model_data.profile_url as profile_url','count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.status','1')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->where('users.live','1')->groupBy('model_id','start_time')->get();

          
       }
       else{
          $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url as profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.status','1')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->limit(8)->groupBy('model_id','start_time')->get();

          
       }
      $mod=[];
      foreach ($users as $key => $value) {
        //print_r($value);
        $mod[$users[$key]->model_id]=$value->status;
      }
      $data=[];
      foreach ($users as $key => $value) {
        //print_r($value);
        $data[]=$value->model_id;
      }
         
      $followed = [];
      foreach ($model_data as $key => $value) {
        //print_r($value);
        if (in_array($value->model_id,$data)){
           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['following_status']='1';
           $followed[$key]['profile_url']=$value->profile_url;
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
            if($mod[$value->model_id] ==3){
              $followed[$key]['following_status']='0';
            }
           }
           
           //if(isset())
        }
        else{
           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['following_status']='0';
           $followed[$key]['profile_url']=$value->profile_url;
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           // if(isset($mod[$value->model_id])){
           //  $followed[$key]['status']='3';
           // }
           if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
           }
           else{
             $followed[$key]['status']='0';
           }
           
        }
        if($value->start_time == null){
         $followed[$key]['start_time'] = "";
       }
       else{
          $followed[$key]['start_time'] = $value->start_time;
       }
       if($value->countComment == null){
          $followed[$key]['countComment'] = 0;
       }
       else{
         $followed[$key]['countComment'] = $value->countComment;
       }

       if(round($value->averageRating) == null){
          $followed[$key]['averageRating'] = "";
       }
       else{
          $followed[$key]['averageRating'] = round($value->averageRating);
       }
      }
      if(!empty($followed)){
              return response()->json(['success'=>'1','data'=>$followed], 200);
      }
      else{
              return response()->json(['success'=>'0','data'=>[]], 200);
      } 
       
    }

    public function modelFilterWithoutLogin(Request $request){
       $requests = $request->input();
       if(isset($requests['sort']) ){
         $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model','model_data.profile_url AS profile_url',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->orderBy('users.id', $requests['sort'])->groupBy('model_id','start_time')->get();

       }
      
       else if(isset($requests['featured'])) {
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url AS profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->groupBy('model_id','start_time')->get();

          

       }
       else if(isset($requests['trending'])){
          //$queryParam = 'DESC';
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url AS profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.status','1')->where('users.accountStatus','=', '0')->groupBy('model_id','start_time')->get();

       }
       else if(isset($requests['popular'])){
          //$queryParam = 'ASC';
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url AS profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating' ])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();
       }
       else if(isset($requests['nameSort'])){
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','model_data.profile_url AS profile_url','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->orderBy('name', $requests['nameSort'])->groupBy('model_id','start_time')->get();
       }
      
       else if(isset($requests['mostUploads'])){
        //SELECT model_id, COUNT(*) FROM model_assets GROUP BY model_id ORDER BY COUNT(*) DESC
          $model_data = DB::table('users')->select(['model_assets.model_id',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','model_data.profile_url AS profile_url','view_rating.avg_rating AS averageRating','users.name AS model_name' , 'users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model'])->leftJoin('model_assets','users.id', '=', 'model_assets.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->groupBy('model_assets.model_id')->orderBy('total', 'DESC')->get();
          
       }
       else if(isset($requests['topSelling'])){
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url AS profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();
       }
       else if(isset($requests['recentlyActive'])){
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','model_data.profile_url AS profile_url','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->groupBy('model_id','start_time')->get();    

       }
       else if(isset($requests['order']) ){
        // echo "1";
          $model_data = DB::table('users')->select(['users.name AS model_name','model_data.profile_url AS profile_url','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.status','1')->where('users.role','2')->where('name', 'like', '%'.$requests['order'].'%')->groupBy('model_id','start_time')->get();

          
          //print_r($model_data);die;
       }
       else if(isset($requests['live'])) {
          $model_data = DB::table('users')->select(['users.name AS model_name','model_data.profile_url AS profile_url','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.status','1')->where('users.role','2')->where('users.status','1')->where('users.live','1')->groupBy('model_id','start_time')->get();

          
       }
       else{
          $model_data = DB::table('users')->select(['users.id AS model_id','model_data.profile_url AS profile_url','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.status','1')->where('users.role','2')->where('users.status','1')->limit(8)->groupBy('model_id','start_time')->get();

          
       }
         
      $followed = [];
      foreach ($model_data as $key => $value) {

           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['profile_url']=$value->profile_url;
           $followed[$key]['following_status']='1';
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
            if($mod[$value->model_id] ==3){
              $followed[$key]['following_status']='0';
            }
           }
        if($value->start_time == null){
         $followed[$key]['start_time'] = "";
        }
         else{
            $followed[$key]['start_time'] = $value->start_time;
         }
         if($value->countComment == null){
            $followed[$key]['countComment'] = 0;
         }
         else{
           $followed[$key]['countComment'] = $value->countComment;
         }

         if(round($value->averageRating) == null){
            $followed[$key]['averageRating'] = "";
         }
         else{
            $followed[$key]['averageRating'] = round($value->averageRating);
         }
      }
      if(!empty($followed)){
              return response()->json(['success'=>'1','data'=>$followed], 200);
      }
      else{
              return response()->json(['success'=>'0','data'=>[]], 200);
      } 

    }

    public function modelFilterFollowing(Request $request){
      $user_id = Auth::user()->id;
       $requests = $request->input();

      $users = DB::table('user_follow')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','users.live AS live',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users', 'users.id', '=', 'user_follow.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('user_follow.user_id', $user_id)->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->where('name', 'like', '%'.$requests['order'].'%')->groupBy('model_id','start_time')->get();

       $data =[];
      foreach ($users as $key => $value) {
        $data[$key]['model_name']=$value->model_name;
        $data[$key]['model_id']=$value->model_id;
        $data[$key]['model_Profile']=url('/')."/".$value->model_Profile;
        $data[$key]['following_status']='1';
        $data[$key]['live']=$value->live;
        $data[$key]['image_count'] = $this->mediaCount($value->model_id,1);
        $data[$key]['video_count'] = $this->mediaCount($value->model_id,2);
       if($value->start_time == null){
         $data[$key]['start_time'] = "";
       }
       else{
          $data[$key]['start_time'] = $value->start_time;
       }
       if($value->countComment == null){
          $data[$key]['countComment'] = 0;
       }
       else{
         $data[$key]['countComment'] = $value->countComment;
       }

       if(round($value->averageRating) == null){
          $data[$key]['averageRating'] = "";
       }
       else{
          $data[$key]['averageRating'] = round($value->averageRating);
       }
      }
      if(!empty($data)){
        return response()->json(['success'=>'1','data'=>$data], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }

    }

    public function uploadPic(Request $request){
       $user_id = Auth::user()->id;
       //print_r($request->file());die;
       if ($request->hasFile('file')) {
            $imageName = time().'.'.request()->file->getClientOriginalExtension();
            if( request()->file->move(public_path('images'), $imageName)){
                  $imagepath =  public_path('images')."/".$imageName;
                }
                $input['image']="images/".$imageName;
               //url('/')."/".$value->user_profileImage;  
                $input['mainImage'] =  url('/')."/".$input['image'];  
            }
            else{
                $input['image']=$users->profileImage;
               if($input['image'] !=""){
                    $input['mainImage'] =  url('/')."/".$users->profileImage;
                }
                else{
                    $input['mainImage'] ="0";
                }
                //$input['mainImage'] =  url('/')."/".Auth::user()->$users->profileImage;
            }
            return response()->json(['success'=>'1','data'=>$input['mainImage']], 200);
    }


    public function getAccoutDetail(){
      $user_id = Auth::user()->id;
      $users = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.bio AS model_bio','users.profileImage AS model_profileImage','model_data.phone as modelPhone','model_data.dateOfBirth as Dob','model_data.residence as residence','model_data.mainPrice as price','model_data.social_id as social_id','model_data.profile_url as profile_url','model_data.legal_name as legal_name','model_data.id_type as id_type','model_data.id_number as id_number','model_data.id_expiry as id_expiry','model_data.issuedBy as issuedBy','model_data.aliases as aliases','model_data.photo_id as photo_id','model_data.terms as terms','model_data.video_price as video_price','model_data.liveCamPrice as liveCamPrice','model_data.photo_selfie as photo_selfie','model_data.profile_url as profile_url','model_data.id_expiry as id_expiry'])->leftJoin('model_data', 'model_data.model_id', '=', 'users.id')->where('users.id',$user_id)->get();
      if(!empty($users)){
        return response()->json(['success'=>'1','data'=>$users], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }



    public function getModelPrices(){
      $user_id = Auth::user()->id;
      $users = DB::table('users')->select(['users.id AS model_id','model_data.mainPrice as price','model_data.video_price as video_price','model_data.liveCamPrice as liveCamPrice'])->leftJoin('model_data', 'model_data.model_id', '=', 'users.id')->where('users.id',$user_id)->get();
      if(!empty($users)){
        return response()->json(['success'=>'1','data'=>$users], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }

    public function updateModelPrices(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $data['price']=$input['price'] + ($input['price']/10);
      $data['mainPrice']=$input['price'] ;
      $data['video_price']=$input['video_price'];
      $data['liveCamPrice']=$input['liveCamPrice'];
      $save = DB::table('model_data')->where('model_id', $user_id)->update($data);

      if($save){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);
      }
    }

    public function updateAccountDetail(Request $request){
       $user_id = Auth::user()->id;
       $input = $request->all();

     //print_r($request->allFiles());die;
       $data=[];
      
        $data['phone']=$input['phone'];
        $data['dateOfBirth']=$input['dateOfBirth'];
        $data['residence']=$input['residence'];
        // $data['price']=$input['price'] + ($input['price']/10);
        // $data['mainPrice']=$input['price'] ;
        $data['social_id']=$input['social_id'];
        $data['profile_url']=$input['profile_url'];
        $data['legal_name']=$input['legal_name'];
        $data['id_type']=$input['id_type'];
        $data['id_number']=$input['idNumber'];

        // $data['video_price']=$input['video_price'];
        // $data['liveCamPrice']=$input['liveCamPrice'];

        $data['terms']=1;
        $data['id_expiry']=$input['expirydate'];
       // $data['id_expiry']=$input['id_expiry'];
        $data['issuedBy']=$input['issuedBy'];
        if($input['name']!=""  && $input['email']!=""){
           $validator = Validator::make($request->all(), [
                          //'name' => ['required', 'string', 'max:255'],
                          'email' => 'required|unique:users,email,' . $user_id
                        ]);
            if ($validator->fails()) {
                 $suser =0;
                 return response()->json(['error'=>$validator->errors()], 200);die;            
             }
             else{
               $saveUser = DB::table('users')->where('id', $user_id)->update([ 'email'=>$input['email'],'name'=>$input['name'] ]);
               if($saveUser){
                $suser =1;
               }
               else{
                $suser =0;
               }
             }
        }
        if($request->hasFile('file'))
        {
          $files = $request->file('file');
          foreach ($files as $file) {
            $filename = $file->getClientOriginalName();
            //request()->file->move(public_path('images'), $imageName)
            $file->move(public_path('images').'/uploads', $filename);
          }
          if(isset($files['photo_id'])){
            $data['photo_id']=url('/')."/images/uploads/".$files['photo_id']->getClientOriginalName();
          }
          if(isset($files['photo_Selfie'])){
            $data['photo_selfie']=url('/')."/images/uploads/".$files['photo_Selfie']->getClientOriginalName();
          }
          
        }
       
       //print_r($data);
      // print_r(['email' => "sadf", 'bio' => "sadf",'name' =>"sdf",'profileImage'=>"safd"]);
       // $save = DB::table('users')->where('id', $user_id)->update(
       //        ['email' => $input['email'], 'bio' => $input['bio'],'name' =>$input['name'],'profileImage'=>$input['image']]
       //      );
        $save = DB::table('model_data')->where('model_id', $user_id)->update($data);
        if($save || $suser){
          return response()->json(['success'=>'1','data'=>$data], 200);
        }
        else{
          return response()->json(['success'=>'1','data'=>$data], 200);
        }
    }

    public function getModelContent(){
       $user_id = Auth::user()->id;
        $dataIds = [2,1,5];
        $users = DB::table('model_assets')->select(['model_assets.type AS mediaType','model_assets.media AS media','model_assets.model_id AS model_id','model_assets.id AS id','model_assets.showto AS showTo','model_assets.showOn AS showOn','model_assets.title AS title',DB::raw("(SELECT count(*) FROM model_post_comment where post_id =  model_assets.id ) AS comment")])->where('model_assets.model_id',$user_id)->whereIn('model_assets.type',$dataIds)->orderBy('id', 'DESC')->get();
        //print_r($users);
        $data=[];
        foreach ($users as $key => $value) {
          //if($value->mediaType!=3 ){
             $data[$key]['id']=$value->id;
             $data[$key]['model_id']=$value->model_id;
             $data[$key]['mediaType']=$value->mediaType;
             $data[$key]['media']=url('/')."/".$value->media;
             $data[$key]['showTo']=$value->showTo;
             $data[$key]['showOn']=$value->showOn;
             $data[$key]['title']=$value->title;
             $data[$key]['comment']=$value->comment;
          //}
             
        }
      //  print_r($data);die;
      if(!empty($users)){
        return response()->json(['success'=>'1','data'=>$data], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }

   }


    private function mediaCount($id,$type)
    {

      $image_count = DB::table('model_assets')->where('model_id',$id)->where('type',$type)->count();
      return $image_count;
    }


    public function uploadModelContent(Request $request){
      //print_r(strtotime($request->input('showOn')));die;
      // print_r($request->input());die;
      // echo strtotime($request->input('showOn'));

      // die;
       $user_id = Auth::user()->id;
       $model_Name = Auth::user()->name;
       $input = $request->all();
       $requests = $request->input();
       // if($files=$request->file('images')){
       //  foreach($files as $file){
       //      $name=$file->getClientOriginalName();
       //      $file->move('image',$name);
       //      $images[]=$name;
       //  }
       // }
       $usersData = DB::table('user_follow')->select()->where('model_id',$user_id)->get();
       //print_r($users);die;


       $s =0; 
        if ($files = $request->file('file')) {
          foreach($files as $file){
            $imageName = str_shuffle("abcdefghijklmnopqrstuvwxyz123456789").''.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images'), $imageName);
            $imagepath =  public_path('images')."/".$imageName;
            $input['image']="images/".$imageName;
            //$timeVariable = substr($input['showOn'], 0, strpos($input['showOn'], " ("));
            //echo $variable;
            $save = DB::table('model_assets')->insert(
              ['model_id' => $user_id, 'type' => $requests['type'], 'media' => $input['image'], 'title' => $input['title'], 'showto' => $input['showTo'], 'uploaded_at' => strtotime(gmdate('y-m-d H:i:s')),'showOn' => strtotime($input['showOn'])]   );
            if($save){
              
              $s=1;
            }
            else{
              $s=0;
            }

          }

        $dataIds = [2,1,5];
        $users = DB::table('model_assets')->select()->where('model_assets.model_id',$user_id)->whereIn('model_assets.type',$dataIds)->orderBy("id","DESC")->get();
        //print_r($users);
        $data=[];
        foreach ($users as $key => $value) {
             $data[$key]['id']=$value->id;
             $data[$key]['model_id']=$value->model_id;
             $data[$key]['mediaType']=$value->type;
             $data[$key]['media']=url('/')."/".$value->media;
             $data[$key]['showTo']=$value->showto;
             $data[$key]['showOn']=$value->showOn;
          //}
             
        }
          if($s){
            if($input['showTo'] == 1 || $input['showTo'] == 2){
                $dat =[];
                $id = DB::table('model_assets')->select('id')->orderBy('id', 'desc')->first();
                //print_r($id);die;
                foreach ($usersData as $key => $value) {  
                  $dat[] = 
                      array('user_id'=>$value->user_id, 'model_name'=> $model_Name,'type' => $requests['type'],'noti_read' => '0','showOn' => strtotime($input['showOn']), 'content_id' => $id->id ) ;
                }
                DB::table('notification')->insert($dat);
               
              }
            $this->sendEmailNotification($user_id,$model_Name,'content');
            return response()->json(['success'=>'1','data'=>[$data]], 200);
           
          }
          else{
            return response()->json(['success'=>'0','data'=>[]], 200);
          }


        }

       // $save = DB::table('users')->where('id', $user_id)->update(
       //        ['email' => $input['email'], 'bio' => $input['bio'],'name' =>$input['name'],'profileImage'=>$input['image']]
       //      );
       
    }

    public function changeModelPic(Request $request){
       $user_id = Auth::user()->id;
       $input = $request->all();
       $requests = $request->input();
       $input = $request->all();
       if ($request->hasFile('file')) {
              $imageName = time().'.'.request()->file->getClientOriginalExtension();
                if( request()->file->move(public_path('images'), $imageName)){
                  $imagepath =  public_path('images')."/".$imageName;
                }
                $input['image']="images/".$imageName;
               //url('/')."/".$value->user_profileImage;  
                $input['mainImage'] =  url('/')."/".$input['image'];  
        }
        if($input['MediaType'] == '1'){
          $save = DB::table('users')->where('id', $user_id)->update(
              ['profileImage' => $input['image']]
            );
        }
        else if($input['MediaType'] == '3'){
           $check = DB::table('model_assets')->where('model_id', $user_id)->where('type', 3)->count();
           if($check >0){
            $save = DB::table('model_assets')->where('model_id', $user_id)->where('type', 3)->update(
              ['media' => $input['image']]
            );
           }
           else{
            $save = DB::table('model_assets')->insert(
              ['model_id' => $user_id,'type'=>3,'showto'=>2,'media'=>$input['image'],'uploaded_at'=>strtotime(gmdate('y-m-d H:i:s')),'model_assets.showOn'=> time() ] );
           }  
           
        }
        // $save = DB::table('users')->where('id', $user_id)->update(
        //       ['image' => $input['image'], 'cover' => $input['bio'],'name' =>$input['name'],'profileImage'=>$input['image']]
        //     );
        //   // print_r($save);
        if($save){
          return response()->json(['success'=>'0','data'=>$input], 200);
        }
        else{
          return response()->json(['success'=>'1','data'=>[]], 200);
        }
    }

    public function saveAvailability(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $requests = $request->input();
      $input = $request->all();
      $uniqueId= time().''.mt_rand();
      $save = DB::table('availability')->insert(
              ['model_id' => $user_id,'date'=>gmdate(strtotime($input['date'])),'start_time'=>gmdate(strtotime($input['start_time'])),'end_time'=>gmdate(strtotime($input['end_time'])),'created_at'=>gmdate(strtotime('y-m-d H:i:s')),'updated_at'=>gmdate(strtotime('y-m-d H:i:s')),'liveUrlId'=> $uniqueId ] );
           
      if($save){
        $this->sendEmailNotification($user_id,Auth::user()->name,'availability');
        $fsave =    DB::table('availability')->select()->where('availability.model_id',$user_id)->get();
        if($fsave){

            return response()->json(['success'=>'1'], 200);
        }
        else{
          return response()->json(['success'=>'0'], 200);
        }
      }
      else{
           return response()->json(['success'=>'0'], 200);
      }    

    }

    public function getCalendarData(Request $request){

      $user_id = Auth::user()->id;

      $requests = $request->input();
      $input = $request->all();
      $fsave = DB::table('availability')->select()->where('availability.model_id',$user_id)->whereBetween('start_time', array(strtotime($input['firstDate']),strtotime($input['lastDate'])))->orderBy('availability.start_time','ASC')->get();
        // print_r($fsave);die;
       if(!empty($fsave) && isset($fsave[0])){
          $date=[];
          foreach ($fsave as $key => $value) {
            $date[$key]['day'] = $value->date;
            $date[$key]['id'] = $value->id;
            $date[$key]['date'] = date("m/d/Y h:i:s A T", $value->start_time);
            $date[$key]['rawtime1'] = $value->start_time;
            $date[$key]['rawtime2'] = $value->end_time;
          }
          if($date){
            return response()->json(['success'=>'1','data'=>$date], 200);
          }
        }
        else{
          return response()->json(['success'=>'1','data'=>[]], 200);
        }
    }

    public function updateAvailability(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $requests = $request->input();
      $input = $request->all();


      $save = DB::table('availability')->where('availability.id',$input['id'])->update(
              ['model_id' => $user_id,'date'=>strtotime($input['date']),'start_time'=>strtotime($input['start_time']),'end_time'=>strtotime($input['end_time']),'updated_at'=>strtotime(gmdate('y-m-d H:i:s')) ] );
           
      if($save){
        $fsave =    DB::table('availability')->select()->where('availability.model_id',$user_id)->get();
        if($fsave){
          //return response()->json(['success'=>'1','data'=>$fsave], 200);
          //print_r($fsave);
          $date=[];
          foreach ($fsave as $key => $value) {
            //print_r($value);
            $date[$key]['day'] = date("d",$value->date);
            $date[$key]['weekday'] = date("D",$value->date);
            $date[$key]['start_time'] = date("h:i A",$value->start_time);
            $date[$key]['end_time'] = date("h:i A",$value->end_time);
            $date[$key]['id'] = $value->id;
            $date[$key]['date'] = date("m/d/Y h:i:s A T",$value->date);
            $date[$key]['rawtime1'] = date("m/d/Y h:i:s A",$value->start_time);
            $date[$key]['rawtime2'] = date("m/d/Y h:i:s A",$value->end_time);
          }
          if($date){
            return response()->json(['success'=>'1','data'=>$date], 200);
          }
        }
        else{
          return response()->json(['success'=>'0','data'=>[]], 200);
        }
      }
      else{
           return response()->json(['success'=>'0','data'=>[]], 200);
      } 
    } 


   public function getModelFollowers(){
       $serverUrl = $_SERVER['SERVER_NAME'];
       $user_id = Auth::user()->id;
      $users =  DB::select("select `users`.`name` as `model_name`, `user_follow`.`model_id` as `model_id`, `user_follow`.`blocked` as `blocked`, `users`.`profileImage` as `model_Profile`, `users`.`live` as `live`, `users`.`id` as `idUsers`, `users`.`email` as `email`, (select MS.price from model_subscription as MS WHERE MS.user_id = user_follow.user_id ORDER BY id DESC limit 1) AS price from `user_follow` inner join `users` on `users`.`id` = `user_follow`.`user_id` where `user_follow`.`model_id` = ".$user_id);
      //print_r($users);die;
      
      // print_r($users);die;
       $data =[];
       foreach ($users as $key => $value) {
        $data[$key]['user_name']=$value->model_name;
        $data[$key]['user_id']=$value->idUsers;
        $data[$key]['email']=$value->email;
        $data[$key]['price']=$value->price;
        $data[$key]['blocked']=$value->blocked;
        if($value->model_Profile == "" || $value->model_Profile == null){
          $data[$key]['user_Profile']=url('/')."/images/dummy-profile.png";
        }
        else{
          $data[$key]['user_Profile']=url('/')."/".$value->model_Profile;  
        }
         
       }
       if($data){
          return response()->json(['success'=>'1','data'=>$data], 200);
        }
        else{
          return response()->json(['success'=>'0','data'=>[]], 200);
        }
   }


    public function ChangePostStatus(Request $request){
       $input = $request->all();
       $user_id = Auth::user()->id;
       // print_r($input);
       // echo $user_id;die;
       $save = DB::table('model_assets')->where('model_id', $user_id)->where('id', $input['id'])->update(
              ['showto' => $input['showTo']]);
       if($save){
          return response()->json(['success'=>'1','data'=>[]], 200);
        }
        else{
          return response()->json(['success'=>'0','data'=>[]], 200);
        }
    }

    public function DeletePost(Request $request){
       $input = $request->all();
       $user_id = Auth::user()->id;
       // print_r($input);
       // echo $user_id;die;
       $save = DB::table('model_assets')->where('model_id', '=',$user_id)->where('id','=',$input['id'])->delete();
       if($save){
          return response()->json(['success'=>'1','data'=>[]], 200);
        }
        else{
          return response()->json(['success'=>'0','data'=>[]], 200);
        }
    }

    public function deleteAvailability(Request $request){
       $input = $request->all();
       $user_id = Auth::user()->id;
       $save = DB::table('availability')->where('id','=',$input['id'])->delete();
       $fsave =    DB::table('availability')->select()->where('availability.model_id',$user_id)->get();
        if($fsave &&$save){
          //return response()->json(['success'=>'1','data'=>$fsave], 200);
          //print_r($fsave);
          $date=[];
          foreach ($fsave as $key => $value) {
            //print_r($value);
            $date[$key]['day'] = date("d",$value->date);
            $date[$key]['weekday'] = date("D",$value->date);
            $date[$key]['start_time'] = date("h:i A",$value->start_time);
            $date[$key]['end_time'] = date("h:i A",$value->end_time);
            $date[$key]['id'] = $value->id;
            $date[$key]['date'] = date("m/d/Y h:i:s A T", $value->date);
          }
          if($date){
            return response()->json(['success'=>'1','data'=>$date], 200);
          }
          else{
             return response()->json(['success'=>'1','data'=>[]], 200);
          }
        }
        else{
          return response()->json(['success'=>'0','data'=>[]], 200);
        }

    }

    public function listModelHome(){

      $allModels = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model'])->where('users.role','2')->where('users.status','1')->get();
      
      $allModels =  DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model','model_data.profile_url AS profile_url',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->leftJoin('model_data','model_data.model_id', '=', 'users.id')->where('users.role','2')->where('users.status','1')->where('users.accountStatus','=', '0')->groupBy('model_id','start_time')->get();

       // $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','next_available.date AS start_date','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.status','1')->orderBy('users.id', $requests['sort'])->groupBy('model_id','start_time')->get();

       //print_r($allModels);die;
      $followed = [];

      foreach ($allModels as $key => $value) {

           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           if($value->model_Image =="" || $value->model_Image == null){
              $followed[$key]['model_Image']=url('/')."/images/modal.png";
            }
           else{
              $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
              //$data[$key]['model_Image'] = url('/')."/".$value->profileImage;
            }
           //$followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['profile_url']=$value->profile_url;
           $followed[$key]['following_status']='1';
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           if($value->start_time == null){
             $followed[$key]['start_time'] = "";
           }
           else{
              $followed[$key]['start_time'] = $value->start_time;
           }
           if($value->countComment == null){
              $followed[$key]['countComment'] = 0;
           }
           else{
             $followed[$key]['countComment'] = $value->countComment;
           }

           if(round($value->averageRating) == null){
              $followed[$key]['averageRating'] = "";
           }
           else{
              $followed[$key]['averageRating'] = round($value->averageRating);
           }
        
      }
      if(!empty($followed)){
              return response()->json(['success'=>'1','data'=>$followed], 200);
      }
      else{
              return response()->json(['success'=>'0','data'=>[]], 200);
      } 
    }
        
   public function filterModelHome(Request $request){
       
      

       $requests = $request->input();
       // print_r($requests);die;
      $requests = $request->input();
       // print_r($requests);die;
       if(isset($requests['sort']) ){
         $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.accountStatus','=', '0')->where('users.status','1')->orderBy('users.id', $requests['sort'])->groupBy('model_id','start_time')->get();

         // $users = DB::table('user_follow')->select(['users.name AS model_name','user_follow.model_id AS model_id','users.profileImage AS model_Profile','users.live AS live',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','next_available.date AS start_date','view_rating.avg_rating AS averageRating'  ])->join('users', 'users.id', '=', 'user_follow.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('user_follow.user_id', $user_id)->where('users.role','2')->where('users.status','1')->get();

       }
      
       else if(isset($requests['featured'])) {
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();

          

       }
       else if(isset($requests['trending'])){
          //$queryParam = 'DESC';
        $model_data = DB::table('featured_model')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->join('users','users.id','=','featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.status','1')->where('users.accountStatus','=', '0')->groupBy('model_id','start_time')->get();

       }
       else if(isset($requests['popular'])){
          //$queryParam = 'ASC';
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating' ])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.status','1')->where('users.accountStatus','=', '0')->groupBy('model_id','start_time')->get();
       }
       else if(isset($requests['nameSort'])){
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->orderBy('name', $requests['nameSort'])->groupBy('model_id','start_time')->get();
       }
      
       else if(isset($requests['mostUploads'])){
        //SELECT model_id, COUNT(*) FROM model_assets GROUP BY model_id ORDER BY COUNT(*) DESC
          $model_data = DB::table('users')->select(['model_assets.model_id',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating','users.name AS model_name' , 'users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model'])->leftJoin('model_assets','users.id', '=', 'model_assets.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->groupBy('model_assets.model_id')->orderBy('total', 'DESC')->get();
     

          
       }
       else if(isset($requests['topSelling'])){
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();
       }
       else if(isset($requests['recentlyActive'])){
        $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->groupBy('model_id','start_time')->get();



          

       }
       else if(isset($requests['order']) ){
        // echo "1";
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.status','1')->where('users.accountStatus','=', '0')->where('users.role','2')->where('name', 'like', '%'.$requests['order'].'%')->groupBy('model_id','start_time')->get();

          
          //print_r($model_data);die;
       }
       else if(isset($requests['live'])) {
          $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.status','1')->where('users.role','2')->where('users.status','1')->where('users.accountStatus','=', '0')->where('users.live','1')->groupBy('model_id','start_time')->get();

          
       }
       else{
          $model_data = DB::table('users')->select(['users.id AS model_id','users.name AS model_name','users.email AS model_email','users.website AS model_website','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('featured_model','users.id', '=', 'featured_model.model_id')->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.status','1')->where('users.accountStatus','=', '0')->where('users.role','2')->where('users.status','1')->limit(8)->groupBy('model_id','start_time')->get();

          
       }
     //print_r($model_data);die;
      $followed = [];
      foreach ($model_data as $key => $value) {
        //print_r($value);
           $followed[$key]['user_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           $followed[$key]['model_profile']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['following_status']='0';
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           if($value->start_time == null){
             $followed[$key]['start_time'] = "";
           }
           else{
              $followed[$key]['start_time'] = $value->start_time;
           }
           if($value->countComment == null){
              $followed[$key]['countComment'] = 0;
           }
           else{
             $followed[$key]['countComment'] = $value->countComment;
           }

           if(round($value->averageRating) == null){
              $followed[$key]['averageRating'] = "";
           }
           else{
              $followed[$key]['averageRating'] = round($value->averageRating);
           }
       
      }
      if(!empty($followed)){
              return response()->json(['success'=>'1','data'=>$followed], 200);
      }
      else{
              return response()->json(['success'=>'0','data'=>[]], 200);
      } 
       
    }

    public function getModelComment(Request $request) {
       $user_id = auth('api')->user();
       $input = $request->all();
       if($user_id!="" || $user_id != null ){
        $checkComment = DB::table('model_comment')->select()->where('model_comment.user_id',$user_id->id)->where('model_comment.model_id',$input['id'])->count();
        //echo $checkComment;die;
       }
       else{
        $checkComment =0;
       }

       $allComment =  DB::table('model_comment')->select('model_comment.comment','model_comment.rating','model_comment.posted_on','users.name','users.profileImage')->join('users', 'users.id', '=', 'model_comment.user_id')->where('users.accountStatus','=', '0')->where('model_comment.model_id',$input['id'])->groupBy('model_comment.id')->orderBy('model_comment.id','DESC')->get();
       $totalCommentCount = DB::table('model_comment')->where('model_comment.model_id',$input['id'])->count();
       $totalAverage = round(DB::table('model_comment')->where('model_comment.model_id',$input['id'])->avg('rating'));
       //print_r($allComment);die;
      $data=[];
       foreach ($allComment as $key => $value) {
         $data[$key]['rating'] = $value->rating;
         $data[$key]['comment'] = $value->comment;
         $data[$key]['posted_on'] =date("m/d/Y h:i:s A T",$value->posted_on);
         $data[$key]['name'] = $value->name;
         if($value->profileImage =="" || $value->profileImage == null){
           $data[$key]['profileImage'] = "";
         }
         else{
          $data[$key]['profileImage'] = url('/')."/".$value->profileImage;
         }
          $data[$key]['totalCommentCount'] = $totalCommentCount;
          $data[$key]['totalaverage'] = $totalAverage;
          $data[$key]['alreadyCommented'] = $checkComment;
         
       }
      
       if(!empty($data)){
          return response()->json(['success'=>'1','data'=>$data], 200);
      }
      else{
          return response()->json(['success'=>'0','data'=>[]], 200);
      } 

      

    }

    public function getUserComment(Request $request){
       $user_id = auth('api')->user();
       $input = $request->all();
       $checkComment = DB::table('model_comment')->select()->where('model_comment.user_id',$user_id->id)->where('model_comment.model_id',$input['model_id'])->get();
       //print_r($checkComment);
        $data=[];
       foreach ($checkComment as $key => $value) {
         $data[$key]['rating'] = $value->rating;
         $data[$key]['comment'] = $value->comment;
         $data[$key]['posted_on'] =date("d M h:i A T",  $value->posted_on);
         
       }
        if(!empty($checkComment)){
          return response()->json(['success'=>'1','data'=>$data], 200);
        }
        else{
            return response()->json(['success'=>'0','data'=>[]], 200);
        } 
    }

    public function saveUserComment(Request $request){
       $user_id = auth('api')->user();
       $id = $user_id->id;
      // print_r($user_id->id);die;
       $input = $request->all();
       if($input['method'] == 'edit'){
        $saveComment = DB::table('model_comment')->where('model_comment.model_id',$input['model_id'])->where('model_comment.user_id',$id)->update(
              ['comment' =>$input['comment'],'rating'=>$input['review'],'posted_on'=>strtotime(gmdate('y-m-d H:i:s') ) ] );
       }
       else{
        $saveComment = DB::table('model_comment')->insert(
              ['user_id' => $user_id->id, 'model_id' => $input['model_id'],'comment' =>$input['comment'],'rating'=>$input['review'],'posted_on'=>strtotime(gmdate('y-m-d H:i:s'))]
            );
       }
       ///$checkComment = DB::table('model_comment')->select()->where('model_comment.user_id',$user_id->id)->where('model_comment.model_id',$input['model_id'])->get();
       //print_r($checkComment)
        if($saveComment){
          return response()->json(['success'=>'1'], 200);
        }
        else{
            return response()->json(['success'=>'0'], 200);
        } 
    }    


    public function getComment(Request $request){
       $input = $request->all();
       $user_id = Auth::user()->id;
       //echo $user_id;die;
     //  print_r($input);die;
        $paidUnpaid = DB::table('paid-status')->where('user_id',$user_id)->get();
        $dat=[];
        foreach ($paidUnpaid as $key => $value) {
          //print_r($value);
          $dat[]=$value->asset_id;
        }
       $getComment = DB::table('users')->select("model_post_comment.post_id AS post_id","model_post_comment.comment as comment","model_post_comment.commeted_on as comment_on","model_post_comment.commeted_on as comment_on","users.name as user_name","users.profileImage as user_image","model_post_comment.id as comment_id","users.id as user_id","model_assets.type as type","model_assets.media  as mediaType","model_assets.model_id  as model_id","model_assets.uploaded_at  as uploaded_at","model_assets.id  as modelAsset_id")->leftJoin('model_post_comment','model_post_comment.user_id','=','users.id')->leftJoin('model_assets','model_assets.id','=','model_post_comment.post_id')->where('model_assets.id',$input['post_id'])->get();
       $data =[];
       if(!empty($getComment) && isset($getComment[0]->model_id)){
          // print_r($getComment);die;
            $model_id = $getComment[0]->model_id;
            $followOrNot = DB::table('user_follow')->where('user_id',$user_id)->where('model_id',$getComment[0]->model_id)->count();
            $getModelData = DB::table('users')->select()->leftJoin('model_data','model_data.model_id','=','users.id')->where('users.id',$model_id)->get();

           $data =[];
           foreach ($getComment as $key => $value) {
            $data['paid'] = "paid";
            if(in_array($value->user_id,[$user_id])){
               $data['comment_data'][$key]['commented'] = 1;
            }
            else{
                $data['comment_data'][$key]['commented'] = 0;
            }
              $data['comment_data'][$key]['post_id'] = $value->post_id; 
              $data['comment_data'][$key]['comment'] = $value->comment; 
              $data['comment_data'][$key]['comment_on'] = $value->comment_on; 
              $data['comment_data'][$key]['user_name'] = $value->user_name;
              $data['comment_data'][$key]['type'] = $value->type;
              $data['comment_data'][$key]['uploaded_at'] =  $this->uploadedat($value->uploaded_at);
              $data['comment_data'][$key]['mediaType'] = url('/')."/".$value->mediaType;
              $data['comment_data'][$key]['user_id'] = $value->user_id;
              $data['comment_data'][$key]['model_id'] = $getComment[0]->model_id;
              if($value->user_image == ""){
                $data['comment_data'][$key]['user_image'] = url('/')."/images/dummy-profile.png"; 
              }
              else{
                $data['comment_data'][$key]['user_image'] = url('/')."/".$value->user_image; 
              }
              
              $data['comment_data'][$key]['comment_id'] = $value->comment_id; 
           }
            $data['model_name'] = $getModelData[0]->name;
            $data['price'] = $getModelData[0]->video_price;
           $model_image =  $getModelData[0]->profileImage;
           if($getModelData[0]->profileImage =""){
           // echo "asd";
            $data['model_image'] = url('/')."/images/dummy-profile.png"; 
           }
           else{
            //echo "asdsdf";
            $data['model_image'] = url('/')."/".$model_image;
           }
           $data['id'] = $getModelData[0]->model_id;
           $data['followOrNot'] = $followOrNot;
           $data['user_id'] = $user_id;
         //  die;
           //print_r($checkComment)
            if($data){
              return response()->json(['success'=>'1','data'=>$data], 200);
            }
            else{
                return response()->json(['success'=>'1','data'=>[]], 200);
            }
          }
          else{
             $getComment = DB::table('model_assets')->select("model_assets.type as type","model_assets.model_id as model_id","model_assets.media  as mediaType","model_assets.model_id  as model_id","model_assets.uploaded_at  as uploaded_at","model_assets.id  as modelAsset_id")->where('model_assets.id',$input['post_id'])->get();//print_r($getComment);die;
            $getModelData = DB::table('users')->select()->leftJoin('model_assets','model_assets.model_id','=','users.id')->leftJoin('model_data','model_data.model_id','=','users.id')->where('model_assets.id',$input['post_id'])->groupBy('model_assets.model_id')->get();
            //$getModelData = DB::table('users')->select()->leftJoin('model_data','model_data.model_id','=','users.id')->where('users.id',$model_id)->get();
            $followOrNot = DB::table('user_follow')->where('user_id',$user_id)->where('model_id',$getComment[0]->model_id)->count();
            $data =[];
           foreach ($getComment as $key => $value) {
            //print_r($value);
            //if (in_array($value->modelAsset_id,$dat)){
              $data['paid'] = "paid";
 
              $data['comment_data'][$key]['post_id'] = ""; 
              $data['comment_data'][$key]['comment'] = ""; 
              $data['comment_data'][$key]['comment_on'] = ""; 
              $data['comment_data'][$key]['user_name'] = "";
              $data['comment_data'][$key]['type'] =$value->type; 
              $data['comment_data'][$key]['mediaType'] = url('/')."/".$value->mediaType; 
              $data['comment_data'][$key]['user_image'] = ""; 
              $data['comment_data'][$key]['comment_id'] = "";
              $data['comment_data'][$key]['uploaded_at'] =  $this->uploadedat($value->uploaded_at); 
           }
           $data['model_name'] = $getModelData[0]->name;
           $data['price'] = $getModelData[0]->video_price;
           $data['model_id'] = $getModelData[0]->model_id;
           $model_image =  $getModelData[0]->profileImage;
           if($getModelData[0]->profileImage =""){
           // echo "asd";
            $data['model_image'] = url('/')."/images/dummy-profile.png"; 
           }
           else{
            //echo "asdsdf";
            $data['model_image'] = url('/')."/".$model_image;
           }
           $data['id'] =$getModelData[0]->id;
           $data['followOrNot'] = $followOrNot;
         //  die;
           //print_r($checkComment)
            if($data){
              return response()->json(['success'=>'1','data'=>$data], 200);
            }
            else{
                return response()->json(['success'=>'1','data'=>[]], 200);
            }
          }

    }

    public function postComment(Request $request){
       $input = $request->all();
       $user_id = Auth::user()->id;
       $save = DB::table('model_post_comment')->insert(
              ['user_id' => $user_id,'post_id'=>$input['post_id'],'comment'=>$input['comment']]);
          
       if($save){
        return response()->json(['success'=>'1'], 200);
       }
       else{
        return response()->json(['success'=>'1'], 200);
       }    
    }


    public function getNotifications(Request $request){
       $user_id = Auth::user()->id;
       $getNoti = DB::table('notification')->select()->where('user_id',$user_id)->orderBy('id','desc')->paginate(6)->toArray();
       //print_r($getNoti);die;
       $data =[];
       foreach ($getNoti['data'] as $key => $value) {
          //print_r($value);
        $data[$key]['id'] = $value->id;
        $data[$key]['model_name'] = $value->model_name;
        $data[$key]['noti_read'] = $value->noti_read;
        $data[$key]['time'] = $value->time;
        $data[$key]['post_id'] = $value->content_id;
        $data[$key]['nextPage'] =$getNoti['next_page_url'];
        if($value->type==1){
          $data[$key]['type'] = "image";
        }
        else if($value->type==2){
          $data[$key]['type'] = "video";
        }
        else{
          $data[$key]['type'] = "teaser";
        }
       }
       //die;
       // print_r($getNoti);die;
       if($data){
        return response()->json(['success'=>'1','data'=>$data], 200);
       }
       else{
        return response()->json(['success'=>'0','data'=>[] ], 200);
       }

    }

    public function getFollowstatus(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $Check = DB::table('user_follow')->where('user_id',$input['user_id'])->where('model_id',$input['model_id'])->where('status',1)->count();
     // print_r($Check);
      if($Check){
        return response()->json(['success'=>'1','data'=>$Check], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }

    }

    public function getVideostatus(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $Check = DB::table('video_status')->where('model_id',$input['model_id'])->where('token_id',$input['token'])->count();
     // print_r($Check);
      if($Check){
        return response()->json(['success'=>'1','data'=>$Check], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }



    public function getModelAvailability(Request $request){
      
      $user_id = Auth::user()->id;
      $input = $request->all();
      $data = GetAvailability::where('model_id', '=', $input['model_id'])
                                ->with(array('modeldata'=>function($query){
                                    $query->select('model_id','video_price');
                                  }))
                                ->get()->toArray();//
      
                       
      $dat=[];
      foreach ($data as $key => $value) {
          if($value['next_available'] == null){
              $dat['start_time'] = "";
              $dat['start_date'] = "";
           }
           else{
              $dat['start_time'] = date("h:i A ", $value['next_available']);
              $dat['start_month_full'] = date("F", $value['date']);
              $dat['start_month_short'] = date("M", $value['date']);
              $dat['start_week_short'] = date("D", $value['date']);
              $dat['start_week_full'] = date("l", $value['date']);
              $dat['start_full_year'] = date("Y", $value['date']);
              $dat['start_full_year'] = date("Y", $value['date']);
              $dat['start_numberic_day'] = date("d", $value['date']);
              $dat['epoch_time'] = $value['next_available'];
              $dat['epoch_date'] = $value['date'];
              if($value['modeldata']['video_price'] == ''   || $value['modeldata']['video_price'] == null){
                $dat['price'] ="0";
              }
              else{
                $dat['price'] =$value['modeldata']['video_price'];
              }
              
              
           }
      }
      if($data){
        $da = ScheduleVideoCall::where([ ['scheduledOn', '=', $data[0]['date']],['scheduledTime','=',$data[0]['next_available']],['model_id','=',$input['model_id']],['user_id','=',$user_id] ])->get()->toArray(); //print_r($da);die;
        if(count($da)>0){
          if($da[0]['confirmation'] == 0){
            $dat['status'] = "pending";
            return response()->json(['success'=>'2','data'=>[$dat]], 200);die;
          }
          if($da[0]['confirmation'] == 1){
            $dat['status'] = "Confirmed";
            return response()->json(['success'=>'2','data'=>[$dat]], 200);die;
          }
          if($da[0]['confirmation'] == 2){
            $dat['status'] = "rejected";
            return response()->json(['success'=>'2','data'=>[$dat]], 200);die;
          }
        }
          
        //if()
      }
       
      if($dat){
        return response()->json(['success'=>'1','data'=>[$dat]], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
      //print_r($data);die;

    }


    public function AppointmentNotification(){
      $user_id = Auth::user()->id;
      $data = AppointmentNotification::where('model_id', '=', $user_id)
                                ->with(array('modeldata'=>function($query){
                                    $query->select('id','name','profileImage');
                                  }))->with(array('availibilityData'=>function($query){
                                    $query->select('id','liveUrlId');
                                  }))->orderBy('id', 'DESC')
                                ->get()->toArray();//
                                //print_r($data);die;
      $dat=[];
      foreach ($data as $key => $value) {
        $dat[$key]['scheduledOn'] = $value['scheduledOn'];
        // $dat[$key]['start_month_short'] = date("M", $value['scheduledOn']);
        // $dat[$key]['start_full_year'] = date("Y", $value['scheduledOn']);
        // $dat[$key]['start_time'] = date("h:i A ", $value['scheduledTime']);
        $dat[$key]['UserName'] = $value['modeldata']['name'];
        if($value['modeldata']['profileImage'] == null){
           $dat[$key]['profile'] = url('/')."/images/dummy-profile.png"; 
            //$dat[$key]['profile'] = url('/')."/".$value['modeldata']['profileImage']; 
        }
        else{
          $dat[$key]['profile'] =  url('/')."/".$value['modeldata']['profileImage'];  
        }
        //$dat[$key]['profile'] = url('/')."/".$value['modeldata']['profileImage'];
        $dat[$key]['user_id'] = $value['modeldata']['id'];
        $dat[$key]['id'] = $value['id'];
        $dat[$key]['minute'] = $value['minute'];
        $dat[$key]['confirmation'] = $value['confirmation'];
        $dat[$key]['status'] = $value['status'];
        $dat[$key]['day'] = $value['scheduledOn'];
        $dat[$key]['rawtime1'] = $value['scheduledTime'];
        if(isset($value['availibility_data']) && isset($value['availibility_data']['liveUrlId']) ){
          $dat[$key]['liveUrlId'] = $value['availibility_data']['liveUrlId'];
        }                      
        if(($value['scheduledOn'] + 300)>strtotime(gmdate('y-m-d H:i:s'))){
          $dat[$key]['passed'] = "no";
        }
        else{
          $dat[$key]['passed'] = "yes";
        }
      }
      if($dat){
        return response()->json(['success'=>'1','data'=>[$dat]], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }

    public function ChangeNotificationStatus(Request $request){
       $curl = curl_init();
       $user_id = Auth::user()->id;
       $input = $request->all();
      // $data = AppointmentNotification::where('id', '=', 1)->update(['confirmation' => 2]); //
       $data = AppointmentNotification::find($input['not_id']);
       $data->confirmation = $input['status'];



     //print_r($data->subscription_id);die;
        if($data->save()){
            if(isset($data->subscription_id)){
                if($input['not_id'] == "2"){
                 curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://datalink.ccbill.com/utils/subscriptionManagement.cgi?password=yes11piPEr@@&action=refundTransaction&clientSubacc=0000&subscriptionId=".$data->subscription_id."&username=y3spiper&clientAccnum=952038&amount=".$data->price,
 
                ));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                $response = curl_exec($curl);
                curl_close($curl);
                $csv = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $response));
                //print_r($data->subscription_id);die;
                if($csv[1][0] == 1){
                    $refund = DB::table('modelIncome')->where('user_id', '=',$data->user_id)->where('model_id','=',$data->model_id)->where('subscription_id','=',$data->subscription_id)->update(
                      ['refund'=>1]
                    );
                    if($refund){
                      return response()->json(['success'=>'1'], 200);
                    }
                    else{
                      return response()->json(['success'=>'0'], 200);
                    }
                }
                    else{
                      return response()->json(['success'=>'0'], 200);
                    }
                }
                else{
                      return response()->json(['success'=>'1'], 200);
                }


                
            }
          //return response()->json(['success'=>'1'], 200);
        }
       else{
          return response()->json(['success'=>'0'], 200);
       }
    }

    public function getMessageRechargeStatus(Request $request){
       $user_id = Auth::user()->id;
       $input = $request->all();
       $Check = DB::table('message_recharge')->where('uniqueId ',$input['uniqueId'])->count();
       if($Check){
        return response()->json(['success'=>'1','data'=>$Check], 200);
       }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }


    public function getCalendarDataModel(Request $request){

      $user_id = Auth::user()->id;

      $requests = $request->input();
      $input = $request->all();

// ->select(['availability.id AS availabilityId','availability.start_time AS start_time','availability.end_time AS end_time','availability.date AS date','schedule_video_call.confirmation AS confirmation'])
      // $fsave = DB::table('availability')->select()->where('availability.model_id',$user_id)->whereBetween('start_time', array(strtotime($input['firstDate']),strtotime($input['lastDate'])))->orderBy('availability.start_time','ASC')->get();


      $fsave = DB::table('availability')->select(['availability.id AS availabilityId','availability.start_time AS start_time','availability.end_time AS end_time','availability.date AS date','schedule_video_call.confirmation AS confirmation','schedule_video_call.user_id AS user_id'])->leftJoin('schedule_video_call', 'schedule_video_call.availabilityId', '=', 'availability.id')->where('availability.model_id',$input['model_id'])->whereBetween('start_time', array(strtotime($input['firstDate']),strtotime($input['lastDate'])))->where('start_time','>',strtotime(gmdate('y-m-d H:i:s')))->orderBy('availability.start_time','ASC')->get();

      $fPrice = DB::table('model_data')->select()->where('model_data.model_id',$input['model_id'])->get()->toArray();

     // print_r($fsave);die;

 
       if(!empty($fsave) && isset($fsave[0])){
          //return response()->json(['success'=>'1','data'=>$fsave], 200);
          //print_r($fsave);
          $date=[];
          foreach ($fsave as $key => $value) {
            //print_r($value);
            // $date[$key]['day'] = $value->date;
            // $date[$key]['id'] = $value->id;
            // $date[$key]['date'] = date("m/d/Y h:i:s A T", $value->start_time);
            // $date[$key]['rawtime1'] = $value->start_time;
            // $date[$key]['rawtime2'] = $value->end_time;
            


            $date[$key]['id'] = $value->availabilityId;
            $date[$key]['day'] = $value->date;
            $date[$key]['month'] = date("m", $value->date);
            $date[$key]['date'] = date("m/d/Y h:i:s A T", $value->start_time);
            $date[$key]['rawtime1'] = $value->start_time;
            $date[$key]['rawtime2'] = $value->end_time;
            $date[$key]['epoch_time'] = $value->start_time;
            $date[$key]['epoch_date'] = $value->date;

            if($value->user_id == $user_id  ){
              $date[$key]['bookedDate'] ="1";
            }
            else if($value->confirmation !=2){
              $date[$key]['bookedDate'] ="0";
            }
            $data[$key]['price'] = $fPrice[0]->price;
          }
          //$data[0]['price'] = $fPrice[0]->price;
          if($date){
            return response()->json(['success'=>'1','data'=>$date], 200);
          }
            
        }
        else{
          return response()->json(['success'=>'1','data'=>[]], 200);
        }
    }


    public function Billingstatus(){
       $user_id = Auth::user()->id;
        $fget = DB::table('model_subscription')->select(['model_subscription.subscription_id AS subscription_id','model_subscription.next_check AS next_check','model_subscription.status AS status','model_subscription.created_at AS followedDate','users.name AS model_name','users.profileImage AS profileImage','users.id AS model_id','model_subscription.price AS price'])->leftJoin('users', 'model_subscription.model_id', '=', 'users.id')->where('model_subscription.user_id',$user_id)->orderBy('model_subscription.id', 'DESC')->get();
        $data=[];
          foreach ($fget as $key => $value) {
            //print_r($value);
            $data[$key]['subscription_id'] = $value->subscription_id;
            $data[$key]['next_check'] = date("d-m-Y", $value->next_check);
            $data[$key]['status'] = $value->status;
            $data[$key]['followedDate'] = date("d-m-Y", $value->followedDate);
            $data[$key]['model_name'] = $value->model_name;
            $data[$key]['profileImage'] = url('/')."/".$value->profileImage;
            $data[$key]['price'] = $value->price;
            $data[$key]['model_id'] = $value->model_id;
            $data[$key]['user_id'] = $user_id;
            
          }
          if($data){
            return response()->json(['success'=>'1','data'=>$data], 200);
          }
          else{
            return response()->json(['success'=>'1','data'=>[]], 200);
          }
        //print_r($data);die;
    }

    public function bookingstatus(){
      $user_id = Auth::user()->id;
      $data = AppointmentNotification::where('user_id', '=', $user_id)
                                ->with(array('userdata'=>function($query){
                                    $query->select('id','name','profileImage');
                                  }))->with(array('availibilityData'=>function($query){
                                    $query->select('id','liveUrlId');
                                  }))->orderBy('id', 'DESC')
                                ->get()->toArray();//
                                //print_r($data);die;

      $dat=[];
      foreach ($data as $key => $value) {
        $dat[$key]['start_numberic_day'] = date("d", $value['scheduledOn']);
        $dat[$key]['start_month_short'] = date("M", $value['scheduledOn']);
        $dat[$key]['start_full_year'] = date("Y", $value['scheduledOn']);
        $dat[$key]['start_time'] = date("h:i A ", $value['scheduledTime']);
        $dat[$key]['start_full_year'] = date("Y", $value['scheduledOn']);
        $dat[$key]['start_time'] = date("h:i A ", $value['scheduledTime']);
        $dat[$key]['ModelName'] = $value['userdata']['name'];
        $dat[$key]['profile'] = url('/')."/".$value['userdata']['profileImage'];
        $dat[$key]['user_id'] = $value['userdata']['id'];
        $dat[$key]['id'] = $value['id'];
        $dat[$key]['minute'] = $value['minute'];
        $dat[$key]['confirmation'] = $value['confirmation'];
        $dat[$key]['status'] = $value['status'];
        $dat[$key]['availabilityId'] = $value['availabilityId'];
        $dat[$key]['day'] = $value['scheduledOn'];
        $dat[$key]['rawtime1'] = $value['scheduledTime'];

        if(isset($value['availibility_data']) && isset($value['availibility_data']['liveUrlId']) ){
          $dat[$key]['liveUrlId'] = $value['availibility_data']['liveUrlId'];
        }
        if($value['scheduledOn'] + 300 > strtotime(gmdate('y-m-d H:i:s')) )
        {
          $dat[$key]['completed']="no";
        }
        else{
          $dat[$key]['completed']="yes";
        }
      }
      if($dat){
        return response()->json(['success'=>'1','data'=>$dat], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }


    public function getModelForChat(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      //print_r($input);die;
      $fget = DB::table('users')->select('id','name','profileImage')->whereIn('id', $input)->get()->toArray();
      //print_r($fget);die;
      $dat=[];
      foreach ($fget as $key => $value) {
        $dat[$key]['id'] = $value->id;
        $dat[$key]['name'] = $value->name;
        $dat[$key]['profileImage'] = url('/')."/".$value->profileImage;
      }
      if($dat){
        return response()->json(['success'=>'1','data'=>$dat], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }

    }

    public function getModelStats(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $previousmonth =  strtotime("-1 month");
      $fget = DB::table('modelIncome')->select(DB::raw("Round(SUM(price)) as price"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id." AND status!=0  ) AS activeSubscription"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id." AND status=1  ) AS cancelledSubscriptions"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=0  ) AS videoChatSale"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=1  ) AS messageSale"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=2  ) AS liveStreamingSale"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id."   ) AS newSubscription"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=3  ) AS subscriptionSales"))->where('model_id','=',$user_id)->get()->toArray();
      //print_r($fget);die;
      if($fget){
        return response()->json(['success'=>'1','data'=>$fget], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }

    }

    public function getModelStatsFilter(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();

      if(isset($input['all'])){
         $fget = DB::table('model_subscription')->select(DB::raw("Round(SUM(price)) from modelIncome where model_id=".$user_id.""),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id." AND status!=0  ) AS activeSubscription"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id." AND status=1  ) AS cancelledSubscriptions"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=0  ) AS videoChatSale"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=1  ) AS messageSale"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=2  ) AS liveStreamingSale"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id."   ) AS newSubscription"),DB::raw("(SELECT count(*) FROM modelIncome where model_id = ".$user_id." AND type=3  ) AS subscriptionSales"))->where('model_id','=',$user_id)->get()->toArray();

      }
      else{
         $fget = DB::table('model_subscription')->select(DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id." AND status!=0  AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))." ) AS activeSubscription"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id." AND status=1 AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  ) AS cancelledSubscriptions"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=0 AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  ) AS videoChatSale"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=1 AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate'])).") AS messageSale"),DB::raw("(SELECT Round(sum(price)) FROM modelIncome where model_id = ".$user_id." AND type=2 AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  ) AS liveStreamingSale"),DB::raw("(SELECT count(*) FROM model_subscription where model_id = ".$user_id."   AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  ) AS newSubscription"),DB::raw("(SELECT count(*) FROM modelIncome where model_id = ".$user_id." AND type=3  AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  ) AS subscriptionSales"),DB::raw("(SELECT Round(SUM(price)) FROM modelIncome where model_id = ".$user_id." AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  AND created_at BETWEEN ".gmdate(strtotime($input['fromDate']))." AND ".gmdate(strtotime($input['toDate']))."  ) AS price"))->get()->toArray();
      }

      //print_r($fget);
      
      if($fget){
        return response()->json(['success'=>'1','data'=>$fget], 200);
      }
      else{
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
    }

    public function deleteNotifications(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $noti = DB::table('notification')->whereIn('id', $input)->delete();
      if($noti){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }
      //print_r($input);die;
      //for
    }

    private function sendEmailNotification($model_id,$model_name,$type){
       //$user_id = Auth::user()->id;
       //$input = $request->all();
       //print_r($type);
       $getFollower = DB::table('user_follow')->select('user_follow.user_id AS user_id')->where('model_id',$model_id)->get()->toArray();
       //print_r($getFollower);die;
       foreach ($getFollower as $key => $value) {
        $getFollowerData = DB::table('users')->select('users.id AS user_id','users.email AS email','users.name AS user_name','users.notification_for AS notification_for')->where('id',$value->user_id)->get()->toArray();
        //$count = count($getFollowerData);
        foreach($getFollowerData as $val){
            if (strpos($val->notification_for, '2') !== false &&     $type == 'content' ) {
                //print_r($getFollowerData);
                    Mail::send('content',['email'=>$val->email,'name'=>$val->user_name,'model_name'=>$model_name],function ($message) use($val) {
                        $message->from('noreply@yespiper.com','YesPiper');
                        $message->to($val->email,$val->user_name);
                        $message->subject('Model Has uploaded Content');
                      });
            }
                if(strpos($val->notification_for, '3') !== false && $type == 'availability'){
                //echo $getFollowerData[0]->email;
                    Mail::send('availability',['email'=>$val->email,'name'=>$val->user_name,'model_name'=>$model_name],function ($message) use($val) {
                    $message->from('noreply@yespiper.com','YesPiper');
                    $message->to($val->email,$val->user_name);
                    $message->subject('Model Has Set An Availability');
                });
            }
        }
        //   if (strpos($getFollowerData[0]->notification_for, '2') !== false && $type == 'content' ) {
        //       print_r($getFollowerData);
        //     // Mail::send('content',['email'=>$getFollowerData[0]->email,'name'=>$getFollowerData[0]->user_name,'model_name'=>$model_name],function ($message) use($getFollowerData) {
        //     //     $message->from('forsmtp@enacteservices.net','YesPiper');
        //     //     $message->to($getFollowerData[0]->email,$getFollowerData[0]->user_name);
        //     //     $message->subject('Model Has uploaded Content');
        //     //   });
        //   }
        //   if(strpos($getFollowerData[0]->notification_for, '3') !== false && $type == 'availability'){
        //     //echo $getFollowerData[0]->email;
        //       Mail::send('availability',['email'=>$getFollowerData[0]->email,'name'=>$getFollowerData[0]->user_name,'model_name'=>$model_name],function ($message) use($getFollowerData) {
        //         $message->from('forsmtp@enacteservices.net','YesPiper');
        //         $message->to($getFollowerData[0]->email,$getFollowerData[0]->user_name);
        //         $message->subject('Model Has Set An Availability');
        //       });
        //   }
        }
    }


    public function getNotificationsModel(Request $request){
       $user_id = Auth::user()->id;
       $getNoti = DB::table('notification_model')->select("users.name AS user_name","notification_model.id AS id","notification_model.time AS time","notification_model.type AS type","notification_model.price AS price")->leftJoin('users', 'notification_model.user_id', '=', 'users.id')->where('model_id',$user_id)->orderBy('id','desc')->get();
       $data =[];
       foreach ($getNoti as $key => $value) {
        $data[$key]['id'] = $value->id;
        $data[$key]['user_name'] = $value->user_name;
        $data[$key]['time'] = $value->time;
        $data[$key]['type'] = $value->type;
        $data[$key]['price'] = $value->price;
        
       }
       if($data){
        return response()->json(['success'=>'1','data'=>$data], 200);
       }
       else{
        return response()->json(['success'=>'0','data'=>[] ], 200);
       }

    }

    public function deleteNotificationsModel(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $noti = DB::table('notification_model')->whereIn('id', $input)->delete();
      if($noti){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }
    }

    public function blockUser(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      //print_r($input);die;
      if($input['type'] ){
        $noti = DB::table('user_follow')->where('user_id', $input['user_id'])->where('model_id', $user_id)->update(
              ['blocked' => '1']
            );
      }
      else{
         $noti = DB::table('user_follow')->where('user_id', $input['user_id'])->where('model_id', $user_id)->update(
              ['blocked' => '0']
            );
      }
      
      if($noti){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }
    }

    public function modelFilterLive(Request $request){
       $user_id = Auth::user()->id;
       $users = DB::table('user_follow')->select(['user_follow.model_id AS model_id','user_follow.status AS status'])->where('user_follow.user_id', $user_id)->where('users.status','1')->join('users','users.id', '=', 'user_follow.model_id')->get();    
       $input = $request->all();
       //$getName = DB::table('users')->select("name")->where('users.id',$input['model_ids'])->get();print_r($getName);die;
       $model_data = DB::table('users')->select(['users.name AS model_name','users.id AS model_id','users.profileImage AS model_Image','users.live AS live_model',DB::raw('count(*) AS total'),'count_comment.totalComment as countComment','next_available.next_available AS start_time','view_rating.avg_rating AS averageRating'])->leftJoin('count_comment','users.id', '=', 'count_comment.model_id')->leftJoin('next_available','users.id', '=', 'next_available.model_id')->leftJoin('view_rating','users.id', '=', 'view_rating.id')->where('users.role','2')->where('users.status','1')->whereIn('users.id', $input['model_ids'])->groupBy('model_id','start_time')->get();
       $mod=[];
      foreach ($users as $key => $value) {
        //print_r($value);
        $mod[$users[$key]->model_id]=$value->status;
      }
      $data=[];
      foreach ($users as $key => $value) {
        //print_r($value);
        $data[]=$value->model_id;
      }
         
      $followed = [];
      foreach ($model_data as $key => $value) {
        //print_r($value);
        if (in_array($value->model_id,$data)){
           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['following_status']='1';
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
            if($mod[$value->model_id] ==3){
              $followed[$key]['following_status']='0';
            }
           }
           
           //if(isset())
        }
        else{
           $followed[$key]['model_id']=$value->model_id;
           $followed[$key]['model_name']=$value->model_name;
           $followed[$key]['model_Image']=url('/')."/".$value->model_Image;
           $followed[$key]['live_model']=$value->live_model;
           $followed[$key]['following_status']='0';
           $followed[$key]['image_count'] = $this->mediaCount($value->model_id,1);
           $followed[$key]['video_count'] = $this->mediaCount($value->model_id,2);
           // if(isset($mod[$value->model_id])){
           //  $followed[$key]['status']='3';
           // }
           if(isset($mod[$value->model_id])){
            $followed[$key]['status']=$mod[$value->model_id];
           }
           else{
             $followed[$key]['status']='0';
           }
           
        }
        if($value->start_time == null){
         $followed[$key]['start_time'] = "";
       }
       else{
          $followed[$key]['start_time'] = $value->start_time;
       }
       if($value->countComment == null){
          $followed[$key]['countComment'] = 0;
       }
       else{
         $followed[$key]['countComment'] = $value->countComment;
       }

       if(round($value->averageRating) == null){
          $followed[$key]['averageRating'] = "";
       }
       else{
          $followed[$key]['averageRating'] = round($value->averageRating);
       }
      }
      if(!empty($followed)){
              return response()->json(['success'=>'1','data'=>$followed], 200);
      }
      else{
              return response()->json(['success'=>'0','data'=>[]], 200);
      } 

    }


    public function getModelDeactivateStatus(){
      $user_id = Auth::user()->id;
      $noti = DB::table('users')->select('accountStatus')->where('id', $user_id)->get()->toArray();;

      //print_r($noti);die;

      
      if($noti){
        return response()->json(['success'=>'1','data'=>$noti[0]], 200);
      }
      else{
        return response()->json(['success'=>'0','data'=>[]], 200);  
      }
    }


    public function setDeactivate(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      if($input['type'] == "1"){
      $noti = DB::table('users')->where('id',$user_id)->update(
              ['accountStatus' => number_format($input['type'])]
            );
      }
      else if($input['type'] == "0"){
        $noti = DB::table('users')->where('id',$user_id)->update(
              ['accountStatus' => number_format($input['type'])]
            );
      }
      else if($input['type'] == "2"){
        $noti = DB::table('users')->where('id',$user_id)->update(
              ['accountStatus' => number_format($input['type'])]
            );
      }
      
      if($noti){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }
    }


    public function getPayoutDetails(){
      $user_id = Auth::user()->id;
      $noti = DB::table('payout_details')->where('model_id', $user_id)->get()->toArray();
      if($noti){
        return response()->json(['success'=>'1','data'=>$noti[0]], 200);
      }
      else{
        return response()->json(['success'=>'0','data'=>[]], 200);  
      }
    }

    public function submitPayoutDetails(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $count = DB::table('payout_details')->where('model_id',$user_id)->count();
      //print_r($input);die;
      if($count > 0){
       if($input['location']['name'] == 'United States'){

         $save = DB::table('payout_details')->where("model_id",$user_id)->update(
              ['routing_no'=>$input['routingNo'],'account_no'=>$input['accountNumber'],'account_type'=>$input['accountType'],'location'=>$input['location']['name'] ] );
        }
        else if($input['location']['name'] == 'Australia'){
           $save = DB::table('payout_details')->where("model_id",$user_id)->update(
              ['BSB'=>$input['bsb'],'account_no'=>$input['accountNumber'],'location'=>$input['location']['name'] ] );
        }
        else if($input['location']['name'] == 'New Zealand'){
           $save = DB::table('payout_details')->where("model_id",$user_id)->update(
              ['account_no'=>$input['accountNumber'],'location'=>$input['location']['name'] ] );
        }
        else{
            $save = DB::table('payout_details')->where("model_id",$user_id)->update(
              ['iban'=>$input['IBAN'],'swift_code'=>$input['swiftCode'],'location'=>$input['location']['name'] ] );
        }
      }
      else{
        if($input['location']['name'] == 'United States'){

         $save = DB::table('payout_details')->insert(
              ['model_id' => $user_id,'routing_no'=>$input['routingNo'],'account_no'=>$input['accountNumber'],'account_type'=>$input['accountType'],'location'=>$input['location']['name'] ] );
        }
        else if($input['location']['name'] == 'Australia'){
           $save = DB::table('payout_details')->insert(
              ['model_id' => $user_id,'BSB'=>$input['bsb'],'account_no'=>$input['accountNumber'],'location'=>$input['location']['name'] ] );
        }
        else if($input['location']['name'] == 'New Zealand'){
           $save = DB::table('payout_details')->insert(
              ['model_id' => $user_id,'account_no'=>$input['accountNumber'],'location'=>$input['location']['name'] ] );
        }
        else {
           $save = DB::table('payout_details')->insert(
              ['model_id' => $user_id,'iban'=>$input['IBAN'],'swift_code'=>$input['swiftCode'],'location'=>$input['location']['name'] ] );
        }
      }
      //$noti = DB::table('payout_details')->where('model_id', $user_id)->get()->toArray();;
      if($save){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }
    
    }


    public function LiveChatLogs(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $save = DB::table('liveChat_logs')->insert(
              ['model_id' => $input['model_id'],'user_id'=>$input['user_id'],'start_time'=>$input['start_time'],'end_time'=>$input['end_time'],'minute'=>$input['minute'] ] );

      if($save){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }
    }


    public function ResetPasswordLink(Request $request){
      //print_r($requests);die;
     $input = $request->all();
     $dat  = DB::table('users')->where('email','=',$input['email'])->count();
     if($dat>0){
        $save = DB::table('users')->where('email', $input['email'])->update(
              ['resetPassword' =>  time().''.str_shuffle("123456789012")]
            );
        if($save){
          $dat  = DB::table('users')->where('email','=',$input['email'])->get();
          //print_r($dat);die;
           Mail::send('resetPassword',['email'=>$dat[0]->email,'name'=>$dat[0]->name,'link'=>$dat[0]->resetPassword],function ($message) use($dat) {
            $message->from('noreply@yespiper.com','YesPiper');
            $message->to($dat[0]->email,$dat[0]->name);
            $message->subject('Reset password link');
         });
           return response()->json(['success'=>'1'], 200);  
        }
     }
     else{
      return response()->json(['success'=>'0'], 200);  
     }
     //print_r($dat);die;
    }


    public function forgotPassword(Request $request){
        $input = $request->all();
        $password = bcrypt($input['newpassword']);
        $save = DB::table('users')->where('resetPassword', $input['uid'])->update(
              ['password' =>  $password]
            );
         if($save){
          return response()->json(['success'=>'1'], 200);  
         }
         else{
          return response()->json(['success'=>'0'], 200);  
         }
    }


    public function GetMsgCount(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $getMsgCount = DB::table('message_counter')->where('user_id', $user_id)->where('model_id', $input['model_id'])->get()->toArray();
      if($getMsgCount){
        return response()->json(['success'=>'1','data'=>$getMsgCount], 200);  
      }
      else{
        return response()->json(['success'=>'0','data'=>$getMsgCount], 200);  
      }
    }


    public function updateMsgCount(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $count = DB::table('message_counter')->where('user_id', $user_id)->where('model_id', $input['model_id'])->count();
      if($count>0){
        $save = DB::table('message_counter')->where('user_id', $user_id)->where('model_id', $input['model_id'])->update(
          ['max_chat' =>  $input['max_chat']]
        );
      }
      else{
        $save = DB::table('message_counter')->where('user_id', $user_id)->where('model_id', $input['model_id'])->insert(
           ['max_chat' =>  $input['max_chat'],'user_id' =>  $user_id,'model_id' => $input['model_id']]
        );
      }
      if($save){
        return response()->json(['success'=>'1'], 200);  
      }
      else{
        return response()->json(['success'=>'0'], 200);  
      }

    }

    public function DeleteComment(Request $request){
      $user_id = Auth::user()->id;
      $input = $request->all();
      $save = DB::table('model_post_comment')->where('id','=',$input['comment_id'])->delete();
      if($save){
        return response()->json(['success'=>'1','data'=>[]], 200);
      }
      else{
        return response()->json(['success'=>'0','data'=>[]], 200);
      }
    }

    public function deleteAccount(){
      $user_id = Auth::user()->id;
      $users = DB::table('users')->delete($user_id);
      if($users){
        return response()->json(['success'=>'1'], 200);
      }
      else{
        return response()->json(['success'=>'0'], 200);
      }
    }

    public function viewAccountPreview(){
      $user_id =Auth::user()->id;

      $users = DB::table('users')->select(['model_assets.media AS coverPic','users.profileImage AS profileImage','model_data.profile_url AS profile_url','users.name AS name','model_data.bio AS bio','model_data.website AS website','model_data.instagram AS instagram','model_data.twitter AS twitter','users.name AS name'])->leftJoin('model_data','users.id', '=', 'model_data.model_id')->leftJoin('model_assets','users.id', '=', 'model_assets.model_id')->where('users.id',$user_id)->where('model_assets.type',"3")->get();
      $data = [];
      foreach ($users as $key => $value) {
        $data['coverPic']=url('/')."/".$value->coverPic;
        $data['profileImage']=url('/')."/".$value->profileImage;
        $data['profile_url']=$value->profile_url;
        $data['name']=$value->name;
        $data['bio']=$value->bio;
        $data['instagram']=$value->instagram;
        $data['twitter']=$value->twitter;
        $data['website']=$value->website;
      }
      // print_r($data);
      // die;
      //print_r($users);die;
      if($data){
        return response()->json(['success'=>'1','data'=>$data], 200);
      }
      else{
        return response()->json(['success'=>'0','data'=>[]], 200);
      }
    }

    public function saveProfileData(Request $request){
        $input = $request->all();
        $user_id = Auth::user()->id;
        $input = $request->all();
        $requests = $request->input();
        $input = $request->all();
        //print_r($input);
         if ($request->hasFile('profilePic')){
            $imageName = time().'.'.request()->profilePic->getClientOriginalExtension();
            if( request()->profilePic->move(public_path('images'), $imageName)){
             $imagepath =  public_path('images')."/".$imageName;
            }
            $input['profilePic']="images/".$imageName;
            $save = DB::table('users')->where('id', $user_id)->update(
              ['profileImage' => $input['profilePic']]
            );
         }
         if ($request->hasFile('cover')){
           $imageName = time().'.'.request()->cover->getClientOriginalExtension();
            if( request()->cover->move(public_path('images'), $imageName)){
             $imagepath =  public_path('images')."/".$imageName;
            }
            $input['cover']="images/".$imageName;
           $check = DB::table('model_assets')->where('model_id', $user_id)->where('type', 3)->count();
           if($check >0){
            $save = DB::table('model_assets')->where('model_id', $user_id)->where('type', 3)->update(
              ['media' => $input['cover']]
            );
           }
           else{
            $save = DB::table('model_assets')->insert(
              ['model_id' => $user_id,'type'=>3,'showto'=>2,'media'=>$input['cover'],'uploaded_at'=>strtotime(gmdate('y-m-d H:i:s')),'model_assets.showOn'=> time() ] );
           }  
         }
          $save = DB::table('model_data')->where('model_id', $user_id)->update( ['bio' => $input['bio'],'instagram'=>$input['instagram'],'twitter'=>$input['twitter'],'website'=>$input['website'] ]);
          if($save){
            return response()->json(['success'=>'1'], 200);
          }
          else{
            return response()->json(['success'=>'0'], 200);
          }
        //die;

    }



    public function logout(Request $request)
    {
       /* $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);*/
    }
  
    /**
     * Get the authenticated User
     *
     * 
@return [json] user object
     */
    public function user(Request $request)
    {
       // return response()->json($request->user());
    }
}
