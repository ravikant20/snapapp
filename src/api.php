<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Auth::routes(['verify' => true]);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::get('/', 'ApiController@login')->name('api');
Route::post('/signup', 'ApiController@signup')->name('api');
//Route::post('/login', 'ApiController@login')->middleware('cors');
Route::post('/login', ['middleware' => 'cors','uses' => 'ApiController@login']);
/*Route::get('/url','controller@function') -> middleware('cors');*/
Route::group(['prefix' => 'v1/api','middleware'=> ['cors']], function(){});
//Route::middleware('auth:api')->group(function () {
//Route::post('/details', ['middleware' => 'auth:api','uses' => 'ApiController@details']);
Route::post('/details', ['uses' => 'ApiController@details']);
Route::post('/models', ['uses' => 'ApiController@models']);
Route::get('/models', ['uses' => 'ApiController@models']);
Route::post('/Userdata', ['middleware' => 'auth:api','uses' => 'ApiController@Userdata']);
Route::post('/Userupdate', ['middleware' => 'auth:api','uses' => 'ApiController@Userupdate']);
Route::post('/ListModel', ['middleware' => 'auth:api','uses' => 'ApiController@ListModel']);
Route::post('/followUnfollow', ['middleware' => 'auth:api','uses' => 'ApiController@followUnfollow']);
Route::post('/yourfeed', ['middleware' => 'auth:api','uses' => 'ApiController@yourfeed']);
Route::post('/followingModel', ['middleware' => 'auth:api','uses' => 'ApiController@followingModel']);
Route::post('/resetPassword', ['middleware' => 'auth:api','uses' => 'ApiController@resetPassword']);
Route::post('/modelPrice', ['middleware' => 'auth:api','uses' => 'ApiController@modelPrice']);
Route::post('/FollowModelPayment', ['middleware' => 'auth:api','uses' => 'ApiController@FollowModelPayment']);
Route::post('/msgStatus', ['middleware' => 'auth:api','uses' => 'ApiController@msgStatus']);
Route::post('/saveLastmsgUser', ['middleware' => 'auth:api','uses' => 'ApiController@saveLastmsgUser']);
Route::post('/saveLastmsgModel', ['middleware' => 'auth:api','uses' => 'ApiController@saveLastmsgModel']);
Route::post('/filterModel', ['middleware' => 'auth:api','uses' => 'ApiController@filterModel']);
Route::post('/modelFilterFollowing', ['middleware' => 'auth:api','uses' => 'ApiController@modelFilterFollowing']);
Route::post('/uploadPic', ['middleware' => 'auth:api','uses' => 'ApiController@uploadPic']);
Route::post('/getAccoutDetail', ['middleware' => 'auth:api','uses' => 'ApiController@getAccoutDetail']);
Route::post('/updateAccountDetail', ['middleware' => 'auth:api','uses' => 'ApiController@updateAccountDetail']);
Route::post('/getModelContent', ['middleware' => 'auth:api','uses' => 'ApiController@getModelContent']);
Route::post('/uploadModelContent', ['middleware' => 'auth:api','uses' => 'ApiController@uploadModelContent']);
Route::post('/changeModelPic', ['middleware' => 'auth:api','uses' => 'ApiController@changeModelPic']);
Route::post('/checkForVideo', ['uses' => 'ApiController@checkForVideo']);
Route::post('/ChangePostStatus', ['middleware' => 'auth:api','uses' => 'ApiController@ChangePostStatus']);
Route::post('/DeletePost', ['middleware' => 'auth:api','uses' => 'ApiController@DeletePost']);
Route::post('/saveAvailability', ['middleware' => 'auth:api','uses' => 'ApiController@saveAvailability']);
Route::post('/getCalendarData', ['middleware' => 'auth:api','uses' => 'ApiController@getCalendarData']);
Route::post('/getModelFollowers', ['middleware' => 'auth:api','uses' => 'ApiController@getModelFollowers']);
Route::post('/deleteAvailability', ['middleware' => 'auth:api','uses' => 'ApiController@deleteAvailability']);
Route::post('/updateAvailability', ['middleware' => 'auth:api','uses' => 'ApiController@updateAvailability']);
Route::post('/listModelHome', ['uses' => 'ApiController@listModelHome']);
Route::post('/filterModelHome', ['uses' => 'ApiController@filterModelHome']);
Route::post('/getModelComment', ['uses' => 'ApiController@getModelComment']);
Route::post('/getUserComment', ['uses' => 'ApiController@getUserComment']);
Route::post('/saveUserComment', ['uses' => 'ApiController@saveUserComment']);
Route::post('/BuyVideo', ['middleware' => 'auth:api','uses' => 'ApiController@BuyVideo']);
Route::post('/getComment', ['middleware' => 'auth:api','uses' => 'ApiController@getComment']);
Route::post('/postComment', ['middleware' => 'auth:api','uses' => 'ApiController@postComment']);
Route::post('/getNotifications', ['middleware' => 'auth:api','uses' => 'ApiController@getNotifications']);
Route::post('/getFollowstatus', ['middleware' => 'auth:api','uses' => 'ApiController@getFollowstatus']);
Route::post('/getVideostatus', ['middleware' => 'auth:api','uses' => 'ApiController@getVideostatus']);
Route::post('/getModelAvailability', ['middleware' => 'auth:api','uses' => 'ApiController@getModelAvailability']);
Route::post('/AppointmentNotification', ['middleware' => 'auth:api','uses' => 'ApiController@AppointmentNotification']);
Route::post('/ChangeNotificationStatus', ['middleware' => 'auth:api','uses' => 'ApiController@ChangeNotificationStatus']);
Route::post('/getMessageRechargeStatus', ['middleware' => 'auth:api','uses' => 'ApiController@getMessageRechargeStatus']);
Route::post('/getCalendarDataModel', ['middleware' => 'auth:api','uses' => 'ApiController@getCalendarDataModel']);
Route::post('/Billingstatus', ['middleware' => 'auth:api','uses' => 'ApiController@Billingstatus']);
Route::post('/bookingstatus', ['middleware' => 'auth:api','uses' => 'ApiController@bookingstatus']);
Route::post('/getModelForChat', ['middleware' => 'auth:api','uses' => 'ApiController@getModelForChat']);
Route::post('/verifyEmail', ['uses' => 'ApiController@verifyEmail']);
Route::post('/getModelStats', ['middleware' => 'auth:api','uses' => 'ApiController@getModelStats']);
Route::post('/getModelStatsFilter', ['middleware' => 'auth:api','uses' => 'ApiController@getModelStatsFilter']);
Route::post('/deleteNotifications', ['middleware' => 'auth:api','uses' => 'ApiController@deleteNotifications']);
Route::post('/modelFilterWithoutLogin', ['uses' => 'ApiController@modelFilterWithoutLogin']);
Route::post('/getNotificationsModel', ['middleware' => 'auth:api','uses' => 'ApiController@getNotificationsModel']);
Route::post('/deleteNotificationsModel', ['middleware' => 'auth:api','uses' => 'ApiController@deleteNotificationsModel']);
Route::post('/blockUser', ['middleware' => 'auth:api','uses' => 'ApiController@blockUser']);
Route::post('/modelFilterLive', ['middleware' => 'auth:api','uses' => 'ApiController@modelFilterLive']);
Route::post('/getModelDeactivateStatus', ['middleware' => 'auth:api','uses' => 'ApiController@getModelDeactivateStatus']);
Route::post('/setDeactivate', ['middleware' => 'auth:api','uses' => 'ApiController@setDeactivate']);
Route::post('/getPayoutDetails', ['middleware' => 'auth:api','uses' => 'ApiController@getPayoutDetails']);
Route::post('/submitPayoutDetails', ['middleware' => 'auth:api','uses' => 'ApiController@submitPayoutDetails']);
Route::post('/LiveChatLogs', ['middleware' => 'auth:api','uses' => 'ApiController@LiveChatLogs']);
Route::post('/ResetPasswordLink', ['uses' => 'ApiController@ResetPasswordLink']);
Route::post('/forgotPassword', ['uses' => 'ApiController@forgotPassword']);
Route::post('/CheckCustomerDetail', ['middleware' => 'auth:api','uses' => 'PaymentController@CheckCustomerDetail']);
Route::post('/CreatePayment', ['middleware' => 'auth:api','uses' => 'PaymentController@CreatePayment']);
Route::post('/GetMsgCount', ['middleware' => 'auth:api','uses' => 'ApiController@GetMsgCount']);
Route::post('/updateMsgCount', ['middleware' => 'auth:api','uses' => 'ApiController@updateMsgCount']);
Route::post('/DeleteComment', ['middleware' => 'auth:api','uses' => 'ApiController@DeleteComment']);
Route::post('/getModelPrices', ['middleware' => 'auth:api','uses' => 'ApiController@getModelPrices']);
Route::post('/updateModelPrices', ['middleware' => 'auth:api','uses' => 'ApiController@updateModelPrices']);
Route::post('/test', ['uses' => 'ApiController@test']);

Route::post('/resendLink', ['uses' => 'ApiController@resendLink']);
Route::post('/deleteAccount', ['middleware' => 'auth:api','uses' => 'ApiController@deleteAccount']);
Route::post('/viewAccountPreview', ['middleware' => 'auth:api','uses' => 'ApiController@viewAccountPreview']);
Route::post('/saveProfileData', ['middleware' => 'auth:api','uses' => 'ApiController@saveProfileData']);



//ModelName
//Route::post('/changePassword', ['middleware' => 'cors','uses' => 'ApiController@changePassword']);
/*});*/



