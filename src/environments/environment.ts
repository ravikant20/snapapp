// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
//baseUrl:'http://enacteservices.com/snapapp/snapapi/public/api',
  baseUrl:'http://localhost:8888/laravel/snapapi/public/api',
  //baseUrl:'https://yespiper.com/snapapi/public/api',
  //baseUrl:'http://enacteservices.net/snapapp/snapapi/public/api',
  //baseUrl_main:'https://ene5g.hosts.cx/snapapp'
  baseUrl_main:'https://yespiper.com'
  //baseUrl:'https://enacteservices.net/snapapp/snapapi/public/api'
  //stripeKey:"pk_test_XsCV2lAW6EG6jRrulmXgma2M00cMQ8KnqI"
}; 

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
 